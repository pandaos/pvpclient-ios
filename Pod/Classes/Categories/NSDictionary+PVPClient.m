//
//  NSDictionary+PVPClient.m
//  PVPClient
//
//  Created by Oren Kosto,  on 3/1/16.
//
//

#import "NSDictionary+PVPClient.h"

@implementation NSDictionary (PVPClient)

+(NSDictionary *)dictionaryFromJSONString:(NSString *)string {
    NSDictionary *jsonDict = nil;
    if (string) {
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    }
    return jsonDict;
}

-(id)safeValueForKeyPath:(NSString *)keyPath {
    return [self safeValueForKeyPath:keyPath withDefaultValue:nil];
}

-(id)safeValueForKeyPath:(NSString *)keyPath withDefaultValue:(id)defaultValue {
    NSArray<NSString *> *pathComponents = [keyPath componentsSeparatedByString:@"."];
    NSString *component = pathComponents[0];
    
    if (pathComponents.count == 1) {
        return self[component]; //we have reached the last component
    } else {
        if (self[component] && [self[component] isKindOfClass:NSDictionary.class]) {
            NSString *trimmedKeyPath = [keyPath substringFromIndex:[component length] + 1];
            return [self[component] safeValueForKeyPath:trimmedKeyPath withDefaultValue:defaultValue]; //advance one more step
        }
    }
    return defaultValue; //return default value upon failure to retrieve the value
}

-(NSDictionary *)dictionaryByMergingWithDictionary:(NSDictionary *)dictionary {
    NSMutableDictionary *result = [self mutableCopy];
    if (dictionary) {
        for (NSString *key in dictionary) {
            result[key] = dictionary[key];
        }
    }
    
    return result;
}

@end
