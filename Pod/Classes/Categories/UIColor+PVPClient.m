//
//  UIColor+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//
//

#import "UIColor+PVPClient.h"
#import <objc/runtime.h>

@implementation UIColor (PVPClient)

@dynamic gradientImage;

+(void)setGradientImage:(UIImage *)gradientImage {
    
    objc_setAssociatedObject(self, @selector(gradientImage), gradientImage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+(UIImage *)gradientImage {
    
    return objc_getAssociatedObject(self, @selector(gradientImage));
}

+(UIColor *)colorWithGradientStyle:(UIGradientColorStyle)gradientStyle withFrame:(CGRect)frame withColors:(NSArray<UIColor *> *)colors
{
    return [self colorWithGradientStyle:gradientStyle withFrame:frame withColors:colors withLocations:nil];
}

+(UIColor *)colorWithGradientStyle:(UIGradientColorStyle)gradientStyle withFrame:(CGRect)frame withColors:(NSArray<UIColor *> *)colors withLocations:(NSArray<NSNumber *> *)locations
{
    //Create our background gradient layer
    CAGradientLayer *backgroundGradientLayer = [CAGradientLayer layer];
    UIImage *backgroundColorImage;
    
    //Set the frame to our object's bounds
    backgroundGradientLayer.frame = frame;
    
    //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
    NSMutableArray *cgColors = [[NSMutableArray alloc] init];
    for (UIColor *color in colors) {
        [cgColors addObject:(id)[color CGColor]];
    }
    
    switch (gradientStyle) {
        case UIGradientColorStyleLeftToRight: {
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors;
            if (locations != nil && locations.count == colors.count) {
                backgroundGradientLayer.locations = locations;
            }
            
            //Specify the direction our gradient will take
            [backgroundGradientLayer setStartPoint:CGPointMake(0.0, 0.5)];
            [backgroundGradientLayer setEndPoint:CGPointMake(1.0, 0.5)];
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size,NO, [UIScreen mainScreen].scale);
            [backgroundGradientLayer renderInContext:UIGraphicsGetCurrentContext()];
            break;
        }
            
        case UIGradientColorStyleRadial: {
            UIGraphicsBeginImageContextWithOptions(frame.size,NO, [UIScreen mainScreen].scale);
            
            //Specific the spread of the gradient (For now this gradient only takes 2 locations)
            CGFloat defaultLocations[2] = {0.0, 1.0};
            
            //Default to the RGB Colorspace
            CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
            CFArrayRef arrayRef = (__bridge CFArrayRef)cgColors;
            
            //Create our Fradient
            CGGradientRef myGradient = CGGradientCreateWithColors(myColorspace, arrayRef, defaultLocations);
            
            
            // Normalise the 0-1 ranged inputs to the width of the image
            CGPoint myCentrePoint = CGPointMake(0.5 * frame.size.width, 0.5 * frame.size.height);
            float myRadius = MIN(frame.size.width, frame.size.height) * 1.0;
            
            // Draw our Gradient
            CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), myGradient, myCentrePoint,
                                         0, myCentrePoint, myRadius,
                                         kCGGradientDrawsAfterEndLocation);
            
            // Clean up
            CGColorSpaceRelease(myColorspace); // Necessary?
            CGGradientRelease(myGradient); // Necessary?
            break;
        }
            
        case UIGradientColorStyleTopToBottom:
        default: {
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors;
            if (locations != nil && locations.count == colors.count) {
                backgroundGradientLayer.locations = locations;
            }
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size,NO, [UIScreen mainScreen].scale);
            [backgroundGradientLayer renderInContext:UIGraphicsGetCurrentContext()];
            break;
        }
            
    }
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setGradientImage:backgroundColorImage];
    return [UIColor colorWithPatternImage:backgroundColorImage];
}

+(UIColor *)colorWithGradientAngle:(CGFloat)angle withFrame:(CGRect)frame withColors:(NSArray *)colors
{
    //Create our background gradient layer
    CAGradientLayer *backgroundGradientLayer = [CAGradientLayer layer];
    UIImage *backgroundColorImage;
    CGPoint start = CGPointZero, end = CGPointZero;
    CGFloat normalizedAngle;
    
    //Set the frame to our object's bounds
    backgroundGradientLayer.frame = frame;
    
    //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
    NSMutableArray *cgColors = [[NSMutableArray alloc] init];
    for (UIColor *color in colors) {
        [cgColors addObject:(id)[color CGColor]];
    }
    //Set out gradient's colors
    backgroundGradientLayer.colors = cgColors;
    
    //Calculate start and end points
    normalizedAngle = fmod(angle, 360);
    CGFloat radAngle = normalizedAngle / 180 * M_PI; //Angle in radians
    if (normalizedAngle >= 0 && normalizedAngle <= 45) {
        CGFloat a = 0.5 * tan(radAngle);
        start.x = 0;
        start.y = 0.5 - a;
        end.x = 1;
        end.y = 0.5 + a;
    } else if (normalizedAngle <= 90) {
        CGFloat a = 0.5 * tan(M_PI_2 - radAngle);
        start.x = 0.5 - a;
        start.y = 0;
        end.x = 0.5 + a;
        end.y = 1;
    } else if (normalizedAngle <= 135) {
        CGFloat a = 0.5 * tan(radAngle - M_PI_2);
        start.x = 0.5 + a;
        start.y = 0;
        end.x = 0.5 - a;
        end.y = 1;
    } else if (normalizedAngle <= 180) {
        CGFloat a = 0.5 * tan(M_PI - radAngle);
        start.x = 1;
        start.y = 0.5 - a;
        end.x = 0;
        end.y = 0.5 + a;
    } else if (normalizedAngle <= 225) {
        CGFloat a = 0.5 * tan(radAngle - M_PI);
        start.x = 1;
        start.y = 0.5 + a;
        end.x = 0;
        end.y = 0.5 - a;
    } else if (normalizedAngle <= 270) {
        CGFloat a = 0.5 * tan(M_PI + M_PI_2 - radAngle);
        start.x = 0.5 + a;
        start.y = 1;
        end.x = 0.5 - a;
        end.y = 0;
    } else if (normalizedAngle <= 315) {
        CGFloat a = 0.5 * tan(radAngle - M_PI - M_PI_2);
        start.x = 0.5 - a;
        start.y = 1;
        end.x = 0.5 + a;
        end.y = 0;
    } else if (normalizedAngle <= 360) {
        CGFloat a = 0.5 * tan(M_PI + M_PI - radAngle);
        start.x = 0;
        start.y = 0.5 + a;
        end.x = 1;
        end.y = 0.5 - a;
    }
    
    [backgroundGradientLayer setStartPoint:start];
    [backgroundGradientLayer setEndPoint:end];
    
    //Convert our CALayer to a UIImage object
    UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size,NO, [UIScreen mainScreen].scale);
    [backgroundGradientLayer renderInContext:UIGraphicsGetCurrentContext()];
    backgroundColorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setGradientImage:backgroundColorImage];
    return [UIColor colorWithPatternImage:backgroundColorImage];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            NSLog(@"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString);
            return [UIColor whiteColor];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (UIColor *)lighterColor
{
    CGFloat h, s, b, a;
    if ([self getHue:&h saturation:&s brightness:&b alpha:&a])
        return [UIColor colorWithHue:h
                          saturation:s
                          brightness:MIN(b * 1.5, 1.0)
                               alpha:a];
    return self;
}

@end
