//
//  NSArray+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 2/14/17.
//
//

#import <Foundation/Foundation.h>

/**
 * The NSArray category offers different convenience functions for handling and manipulating NSArrays.
 */
@interface NSArray (PVPClient)

/**
 *  @name Public Methods
 */

/**
 * Map the array using a code block.
 *
 * @param block The block to apply for the mapping.
 *
 * @return The resulting mapped array.
 */
- (NSArray * _Nonnull)mapObjectsUsingBlock:(id _Nonnull (^_Nonnull)(id _Nonnull obj, NSUInteger idx))block;

/**
 *  Constructs a NSArray from a JSON-formatted NSString.
 *
 *  @param string A JSON-formatted NSString.
 *
 *  @return A NSArray constructed from the given string, or nil if the method fails to parse the string.
 */
+(NSArray * _Nullable)arrayFromJSONString:(NSString * _Nonnull)string;

@end
