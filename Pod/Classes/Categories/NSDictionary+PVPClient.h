//
//  NSDictionary+PVPClient.h
//  PVPClient
//
//  Created by Oren Kosto,  on 3/1/16.
//
//

#import <Foundation/Foundation.h>

/**
 *  The PVPClient category for the NSDictionary class provides useful functionality related to handling and manipulating NSDictionaries, beyond what the standard Foundation framework offers.
 */
@interface NSDictionary (PVPClient)

/**
 *  @name Public Methods
 */

/**
 *  Constructs a NSDictionary from a JSON-formatted NSString.
 *
 *  @param string A JSON-formatted NSString.
 *
 *  @return A NSDictionary constructed from the given string, or nil if the method fails to parse the string.
 */
+(NSDictionary * _Nullable)dictionaryFromJSONString:(NSString * _Nonnull)string;

/**
 *  A recursive method that safely returns the value in the given key path.
 *
 *  @param keyPath The key path of the value we wish to return.
 *
 *  @return The value of the given key path, or nil as the default value.
 */
-(id _Nullable)safeValueForKeyPath:(NSString * _Nonnull)keyPath;

/**
 *  A recursive method that safely returns the value in the given key path.
 *
 *  @param keyPath The key path of the value we wish to return.
 *  @param defaultValue The default value to return in case the method fails.
 *
 *  @return The value of the given key path, or the default value.
 */
-(id _Nullable)safeValueForKeyPath:(NSString * _Nonnull)keyPath withDefaultValue:(id _Nullable)defaultValue;


/**
 Creates a new NSDictionary by merging the current instance with a given dictionary.

 @param dictionary The dictionary to merge with the current instance. In the case of matching keys, the values from this dictionary will override the ones in the current instance.
 
 @return The resulting dictionary from the merge.
 */
-(NSDictionary * _Nonnull)dictionaryByMergingWithDictionary:(NSDictionary * _Nonnull)dictionary;

@end
