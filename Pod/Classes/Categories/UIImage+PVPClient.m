//
//  UIImage+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 9/22/16.
//
//

#import "UIImage+PVPClient.h"

@implementation UIImage (PVPClient)

+ (UIImage *)imageWithColor:(UIColor *)color withFrame:(CGRect)frame {
    UIGraphicsBeginImageContext(frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, frame);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
