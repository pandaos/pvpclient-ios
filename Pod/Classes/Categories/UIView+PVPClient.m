//
//  UIView+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 3/15/16.
//
//

#import "UIView+PVPClient.h"

@implementation UIView (PVPClient)

- (void)fadeIn
{
    [self fadeInWithDuration:FADE_DURATION];
}

- (void)fadeOut
{
    [self fadeOutWithDuration:FADE_DURATION];
}

- (void)fadeInWithDuration:(CGFloat)duration
{
    [self fadeInWithDuration:duration withTargetAplha:1.0];
}

- (void)fadeInWithTargetAplha:(CGFloat)targetAlpha
{
    [self fadeInWithDuration:FADE_DURATION withTargetAplha:targetAlpha];
}

- (void)fadeInWithDuration:(CGFloat)duration withTargetAplha:(CGFloat)targetAlpha
{
    if ( !self.hidden && ( self.alpha == 1 || self.alpha == targetAlpha ) ) {
        return;
    }
    
    if ( self.hidden && self.alpha != 0 ) {
        self.alpha = 0;
    }

    self.hidden = NO;
    [ UIView animateWithDuration: duration
                      animations: ^{
                          self.alpha = targetAlpha;
                      }];
}

- (void)fadeOutWithDuration:(CGFloat)duration
{
    if (self.hidden) {
        return;
    }
    
    [UIView animateWithDuration:duration
                     animations:^{
                         self.alpha = 0;
                     } completion:^(BOOL finished) {
                         self.hidden = YES;
                     }];
}

- (UIViewController *)viewController {
    UIResponder *responder = self;
    while (![responder isKindOfClass:[UIViewController class]]) {
        responder = [responder nextResponder];
        if (nil == responder) {
            break;
        }
    }
    return (UIViewController *)responder;
}

@end
