//
//  UIView+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 3/15/16.
//
//

#import <UIKit/UIKit.h>

#define FADE_DURATION 0.5

/**
 *  The PVPClient category for the UIView class provides useful functionality related to handling and manipulating UIView objects, beyond what the standard UIKit framework offers.
 */
@interface UIView (PVPClient)

/**
 *  @name Public Methods
 */

/**
 *  A convenience function that makes the view fade in.
 */
- (void)fadeIn;

/**
 *  A convenience function that makes the view fade out.
 */
- (void)fadeOut;

/**
 *  A convenience function that makes the view fade in, with a custom fade duration.
 *
 *  @param duration The duration of the fade animation.
 */
- (void)fadeInWithDuration:(CGFloat)duration;

/**
 *  A convenience function that makes the view fade out, with a custom fade duration.
 *
 *  @param duration The duration of the fade animation.
 */
- (void)fadeOutWithDuration:(CGFloat)duration;

/**
 *  A convenience function that makes the view fade out, with a custom target alpha.
 *
 *  @param targetAlpha The alpha to set to the view in the fade animation.
 */
- (void)fadeInWithTargetAplha:(CGFloat)targetAlpha;

/**
 *  A convenience function that makes the view fade out, with a custom fade duration and target alpha.
 *
 *  @param duration    duration The duration of the fade animation.
 *  @param targetAlpha The alpha to set to the view in the fade animation.
 */
- (void)fadeInWithDuration:(CGFloat)duration withTargetAplha:(CGFloat)targetAlpha;

/**
 * Gets this view's containing view controller.
 *
 * @return This view's containing view controller.
 */
- (UIViewController *)viewController;

@end
