//
//  NSDate+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 7/30/17.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (PVPClient)

- (NSTimeInterval)timeIntervalSince1970WithServerOffset;
+ (NSDate *)dateWithTimeIntervalSince1970WithServerOffset:(NSTimeInterval)secs;
+ (NSDate *)currentDateInServerTimezone;
- (NSDate *)dateInServerTimezone;
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;

@end
