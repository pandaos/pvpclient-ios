//
//  NSDate+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 7/30/17.
//
//

#import "NSDate+PVPClient.h"
#import "PVPConfigHelper.h"

@implementation NSDate (PVPClient)

-(NSTimeInterval)timeIntervalSince1970WithServerOffset {
    NSString *offsetStr = [PVPConfigHelper getLocalConfiguration:@"serverTimeOffset"];
    return [self timeIntervalSince1970] + [offsetStr floatValue];
}

+(NSDate *)dateWithTimeIntervalSince1970WithServerOffset:(NSTimeInterval)secs {
    NSString *offsetStr = [PVPConfigHelper getLocalConfiguration:@"serverTimeOffset"];
    float offset = [offsetStr floatValue];
    CGFloat adjustedSecs = secs + offset;
    return [NSDate dateWithTimeIntervalSince1970:adjustedSecs];
}

+ (NSDate *) currentDateInServerTimezone {
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSTimeZone* destinationTimeZone = [NSTimeZone timeZoneWithName:[[PVPConfigHelper sharedInstance] timezoneString]];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    return destinationDate;
}

- (NSDate *) dateInServerTimezone {
    NSDate* sourceDate = self;
    
    NSTimeZone* sourceTimeZone = [NSTimeZone systemTimeZone];
    NSTimeZone* destinationTimeZone = [NSTimeZone timeZoneWithName:[[PVPConfigHelper sharedInstance] timezoneString]];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    return destinationDate;
}

+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: inputDate];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    
    NSDate *sourceDate = [gregorian dateFromComponents: components];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithName:[[PVPConfigHelper sharedInstance] timezoneString]];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:-interval sinceDate:sourceDate];
    
    return destinationDate;
}

@end
