//
//  NSString+PVPClient.h
//  PVPClient
//
//  Created by Oren Kosto,  on 3/1/16.
//
//

#import <Foundation/Foundation.h>

/**
 *  The PVPClient category for the NSString class provides useful functionality related to handling and manipulating NSStrings, beyond what the standard Foundation framework offers.
 */
@interface NSString (PVPClient)

/**
 *  @name Public Methods
 */

/**
 *  Constructs a Hex string from given raw data.
 *
 *  @param data The data we would like to turn into a string.
 *
 *  @return The Hex string constructed from the given raw data.
 */
+(NSString *)hexadecimalStringFromData:(NSData *)data;

/**
 *  Normalizes (shortens) a number and returns it as a string. i.e.:
 *  1700 turns into 1.7K.
 *  2847630 turns into 2.8M.
 *
 *  @param number The number we woud like to normalize.
 *
 *  @return A string representation of the normalized number.
 */
+(NSString *)stringWithNormalizedNumber:(int)number;

/**
 *  Constructs a JSON-formatted string from a given NSDictionary.
 *
 *  @param dictionary The dictionary we would like to receive in string format.
 *
 *  @return A JSON-formatted NSString representation of the given dictionary.
 *
 *  @see JSONStringFromMutableDictionary:
 */
+(NSString *)JSONStringFromDictionary:(NSDictionary *)dictionary;

/**
 *  Constructs a JSON-formatted string from a given NSMutableDictionary.
 *
 *  @param dictionary The mutable dictionary we would like to receive in string format.
 *
 *  @return A JSON-formatted NSString representation of the given dictionary.
 *
 *  @see JSONStringFromDictionary:
 */
+(NSString *)JSONStringFromMutableDictionary:(NSMutableDictionary *)dictionary;

/**
 * Checks if the string represents a valid email address (i.e. foo@bar.com)
 *
 * @return YES or NO, indicating whether the string is a valid email address.
 */
- (BOOL)isValidEmailAddress;

- (NSString *)detectLanguage;

- (BOOL)isRTL;

- (NSString *)localizedString;

@end
