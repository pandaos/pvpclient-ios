//
//  UIColor+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

/**
 *  @name Macros
 */

/**
 *  Convenience macro that generates a UIColor for given RGBA signature.
 *
 *  @param r Red value, between 0-255
 *  @param g Green value, between 0-255
 *  @param b Blue value, between 0-255
 *  @param a Alpha value, between 0-1
 *
 *  @return A UIColor object, with the color represented by the given RGBA signature.
 */
#define rgba(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

/**
 *  Convenience macro that generates a UIColor for a given RGB signature.
 *
 *  @param r Red value, between 0-255
 *  @param g Green value, between 0-255
 *  @param b Blue value, between 0-255
 *
 *  @return A UIColor object, with the color represented by the given RGBA signature.
 */
#define rgb(r,g,b) rgba(r,g,b,1.0)

/**
 *  Defines the gradient style and direction of the gradient color.
 */
typedef NS_ENUM (NSUInteger, UIGradientColorStyle) {
    /**
     *  Gradual blend between colors originating at the leftmost point of an object's frame, and ending at the rightmost point of the object's frame.
     */
    UIGradientColorStyleLeftToRight,
    /**
     *  Gradual blend between colors originating at the center of an object's frame, and ending at all edges of the object's frame. NOTE: Supports a Maximum of 2 Colors.
     */
    UIGradientColorStyleRadial,
    /**
     *  Gradual blend between colors originating at the topmost point of an object's frame, and ending at the bottommost point of the object's frame.
     */
    UIGradientColorStyleTopToBottom
};

/**
 *  The PVPClient category for the UIColor class provides useful functionality related to handling and manipulating UIColor, beyond what the standard UIKit framework offers.
 */
@interface UIColor (PVPClient)

/**
 *  @name Public Properties
 */

/**
 *  Stores the UIColor image if the UIColor was created using colorWithGradient.
 */
@property (nonatomic, strong) UIImage  * _Nullable gradientImage;

/**
 *  @name Public Methods
 */

/**
 *  Generates a gradient-style color, for a given frame and set of colors.
 *
 *  @param gradientStyle The style of the gradient, represented by a UIGradientColorStyle enum value.
 *  @param frame         The target frame for the color.
 *  @param colors        The set of colors to assemble the gradient from.
 *
 *  @return A gradient UIColor, generated from the given parameters.
 */
+(nonnull UIColor *)colorWithGradientStyle:(UIGradientColorStyle)gradientStyle withFrame:(CGRect)frame withColors:(nonnull NSArray<UIColor *> *)colors;

/**
 *  Generates a gradient-style color, for a given frame and set of colors, and custom locations for each color.
 *
 *  @param gradientStyle The style of the gradient, represented by a UIGradientColorStyle enum value.
 *  @param frame         The target frame for the color.
 *  @param colors        The set of colors to assemble the gradient from.
 *  @param locations     The set of locations for each color.
 *
 *  @warning The locations array must be with the same size as the color array, and contain
 *
 *  @return A gradient UIColor, generated from the given parameters.
 */
+(nonnull UIColor *)colorWithGradientStyle:(UIGradientColorStyle)gradientStyle withFrame:(CGRect)frame withColors:(nonnull NSArray<UIColor *> *)colors withLocations:(nullable NSArray<NSNumber *> *)locations;

/**
 *  Generates a gradient-style color at a given angle of choice, for a given frame and set of colors.
 *
 *  @param angle  The angle of the direction in which we want the gradient to go, in degrees. 0 represents Left-To-Right, and the angle direction is clockwise.
 *  @param frame  The target frame for the color.
 *  @param colors The set of colors to assemble the gradient from.
 *
 *  @return A gradient UIColor, generated from the given parameters.
 */
+(nonnull UIColor *)colorWithGradientAngle:(CGFloat)angle withFrame:(CGRect)frame withColors:(nonnull NSArray<UIColor *> *)colors;

/**
 * Generates a UIColor from a hex string.
 *
 * Supported formats:
 * #RGB, #ARGB, #RRGGBB, #AARRGGBB
 *
 * For unsupported formats, this method will return the color white.
 *
 * @param hexString The hex string representing the color.
 *
 * @return A UIColor object, genenerated from the hex string.
 */
+ (nonnull UIColor *)colorWithHexString:(nonnull NSString *)hexString;

- (UIColor *)lighterColor;

@end
