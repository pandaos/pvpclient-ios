//
//  NSString+PVPClient.m
//  PVPClient
//
//  Created by Oren Kosto,  on 3/1/16.
//
//

#import "NSString+PVPClient.h"
#import "PVPLocalizationHelper.h"

@implementation NSString (PVPClient)

+(NSString *)hexadecimalStringFromData:(NSData *)data
{
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

+(NSString *)stringWithNormalizedNumber:(int)number
{
    if (number >= 1000000) {
        float numberFloat = number / 1000000;
        return [NSString stringWithFormat:@"%.01fM", numberFloat];
    } else if (number >= 1000) {
        float numberFloat = number / 1000;
        return [NSString stringWithFormat:@"%.01fK", numberFloat];
    }
    return [NSString stringWithFormat:@"%d", number];
}

+(NSString *)JSONStringFromDictionary:(NSDictionary *)dictionary
{
    return [self JSONStringFromMutableDictionary:[NSMutableDictionary dictionaryWithDictionary:dictionary]];
}

+(NSString *)JSONStringFromMutableDictionary:(NSMutableDictionary *)dictionary
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
}

- (BOOL)isValidEmailAddress {
    
    //Create a regex string
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" ;
    
    //Create predicate with format matching your regex string
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    
    //return true if email address is valid
    return [emailTest evaluateWithObject:self];
}

- (NSString *)detectLanguage {

    NSString *language = nil;

    @try {

        if (self.length < 10) {
            NSArray *tagschemes = [NSArray arrayWithObjects:NSLinguisticTagSchemeLanguage, nil];
            NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:tagschemes options:0];
            [tagger setString:self];
            language = [tagger tagAtIndex:0 scheme:NSLinguisticTagSchemeLanguage tokenRange:NULL sentenceRange:NULL];
        }
        else {
            language = (NSString *)CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)self, CFRangeMake(0, 10)));
        }

    } @catch (NSException *exception) {}

    return language ? language : [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
}

- (BOOL)isRTL {
    NSString *language = [self detectLanguage];
    return [language rangeOfString:@"he"].location != NSNotFound || [language rangeOfString:@"ar"].location != NSNotFound;
}

- (NSString *)localizedString {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    
    if (!bundle) {
        bundle = [NSBundle mainBundle];
    }
    return bundle ? NSLocalizedStringWithDefaultValue(self, nil, bundle, self, @"") : self;
}

@end
