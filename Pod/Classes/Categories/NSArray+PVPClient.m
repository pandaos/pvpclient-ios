//
//  NSArray+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 2/14/17.
//
//

#import "NSArray+PVPClient.h"

@implementation NSArray (PVPClient)

- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [result addObject:block(obj, idx)];
    }];
    return result;
}

+(NSArray *)arrayFromJSONString:(NSString *)string {
    NSArray *jsonArray = nil;
    if (string) {
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    }
    return jsonArray;
}

@end
