//
//  UILabel+PVPClient.m
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//
//

#import "UILabel+PVPClient.h"

@implementation UILabel (PVPClient)

-(CGRect)frameforText
{
    CGRect rect = CGRectZero;
    NSDictionary *attrDict = @{NSFontAttributeName : self.font};
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    CGSize labelSize = [self.text sizeWithAttributes:attrDict];
//    labelSize.width *= mainScreen.scale;
    labelSize.height *= mainScreen.scale;
    
    if (labelSize.height < self.frame.size.height * mainScreen.scale) {
        labelSize.height = self.frame.size.height * mainScreen.scale;
    }
    if (labelSize.width > self.frame.size.width * mainScreen.scale) {
        labelSize.width = self.frame.size.width / mainScreen.scale;
    }
    
    rect.size = labelSize;
    
    return rect;
}

@end
