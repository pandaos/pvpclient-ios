//
//  UIImage+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 9/22/16.
//
//

#import <UIKit/UIKit.h>

/**
 * The UIImage category offers different convenience functions for handling and manipulating UIImages.
 */
@interface UIImage (PVPClient)

/**
 *  @name Public Methods
 */

/**
 * Generates a UIImage with a solid color.
 *
 * @param color The color to use for the image.
 * @param frame The required image frame (size, position).
 *
 * @return A generated UIImage with the given solid color and frame.
 */
+ (UIImage *)imageWithColor:(UIColor *)color withFrame:(CGRect)frame;

@end
