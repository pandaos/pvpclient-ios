//
//  NSString+MD5.h
//  PVPClient
//
//  Created by Kovtun Vladimir on 20.05.2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (MD5)

- (NSString*)MD5;
- (NSData*)MD5CharData;

@end

NS_ASSUME_NONNULL_END
