//
//  UILabel+PVPClient.h
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//
//

#import <UIKit/UIKit.h>

/**
 *  The PVPClient category for the UILabel class provides useful functionality related to handling and manipulating UILabels, beyond what the standard UIKit framework offers.
 */
@interface UILabel (PVPClient)

/**
 *  @name Public Methods
 */

/**
 *  Calculates the frame for the text inside the label.
 *
 *  @return A CGRect object, with the dimensions of the text inside the label, regardless of the label's frame.
 */
-(CGRect)frameforText;

@end
