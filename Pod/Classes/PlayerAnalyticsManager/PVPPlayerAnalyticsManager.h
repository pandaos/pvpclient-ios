//
//  PVPPlayerAnalyticsManager.h
//  PVPClient
//
//  Created by Kovtun Vladimir on 22.03.2021.
//

#import <Foundation/Foundation.h>
#import "PVPClient.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, PVPPlayerAnalyticsEvent) {
    PVPPlayerLoadEvent = 0,
    PVPPlayerStartEvent,
    PVPPlayerPlaysEvent,
    PVPPlayerTimePlayedEvent,
    PVPPlayerFirstQuartileEvent,
    PVPPlayerMidPointEvent,
    PVPPlayerThirdQuartileEvent,
    PVPPlayerCompleteEvent,
    PVPPlayerPauseEvent,
    PVPPlayerResumeEvent,
    PVPPlayerMuteEvent,
    PVPPlayerUnmuteEvent,
    PVPPlayerNextEvent,
    PVPPlayerPrevEvent,
    PVPPlayerErrorEvent,
    PVPPlayerClickEvent
};

@interface PVPPlayerAnalyticsManager : NSObject

+(nonnull instancetype)sharedInstance;

- (void)setItemID:(NSString *)itemID;
- (void)setChannelID:(NSString *)channelID;

- (void)logEvent:(PVPPlayerAnalyticsEvent)event;
- (void)logPlayDurationEvent;
- (void)logLastPartDurationEvent;

- (void)logChannelSwitchEvent:(PVPChannel *)channel scheduleItem:(PVPChannelScheduleItem *)item;

@end

NS_ASSUME_NONNULL_END
