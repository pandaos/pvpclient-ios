//
//  PVPPlayerAnalyticsManager.m
//  PVPClient
//
//  Created by Kovtun Vladimir on 22.03.2021.
//

#import "PVPPlayerAnalyticsManager.h"
#import "NSDate+PVPClient.h"

NSString *const kAniviewAccountID = @"5ae5d3e5073ef4387f40bc94";
NSString *const aniviewBaseURL = @"https://track1.avplayer.com/ctrack";
NSString *const bPID = @"55546cc1abf6484c12362d3e";

@interface PVPPlayerAnalyticsManager()

@property(nonatomic, copy) NSString *currentItemID;
@property(nonatomic, copy) NSString *currentChannelID;

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSMutableDictionary *defaultParams;

@property (nonatomic) NSDate *lastDurationEventDate;

@property (nonatomic) BOOL didMakeStart;
@property (nonatomic) BOOL didMakePlay;
@property (nonatomic) PVPPlayerAnalyticsEvent lastEvent;

@end

@implementation PVPPlayerAnalyticsManager

static PVPPlayerAnalyticsManager *sharedInstance = nil;
static dispatch_once_t onceToken;


+ (instancetype)sharedInstance
{
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });

    return sharedInstance;
}

+ (NSDictionary *)eventTypeNames
{
    return @{@(PVPPlayerLoadEvent) : @"cpll",
             @(PVPPlayerStartEvent) : @"cpst",
             @(PVPPlayerPlaysEvent) : @"cply",
             @(PVPPlayerTimePlayedEvent) : @"ctpl",
             @(PVPPlayerFirstQuartileEvent) : @"cfqt",
             @(PVPPlayerMidPointEvent) : @"cmdp",
             @(PVPPlayerThirdQuartileEvent) : @"ctqt",
             @(PVPPlayerCompleteEvent) : @"cmpt",
             @(PVPPlayerPauseEvent) : @"cpau",
             @(PVPPlayerResumeEvent) : @"cres",
             @(PVPPlayerMuteEvent) : @"cmut",
             @(PVPPlayerUnmuteEvent) : @"cunm",
             @(PVPPlayerNextEvent) : @"cnxt",
             @(PVPPlayerPrevEvent) : @"cprv",
             @(PVPPlayerErrorEvent) : @"cerr",
             @(PVPPlayerClickEvent) : @"cclk"
    };
}

- (NSString *)stringFromType:(PVPPlayerAnalyticsEvent)event
{
    return [[self class] eventTypeNames][@(event)];
}

- (void)setup {
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    self.baseUrl = aniviewBaseURL;
    
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString *instanceId = [PVPConfigHelper sharedInstance].currentInstanceId;
    NSString *userId = [[ApiManager sharedInstance] loggedInUser].mongoId;
    
    self.defaultParams = [NSMutableDictionary dictionaryWithDictionary:
                          @{
                              @"pid" : kAniviewAccountID, // publisherID
                              @"cos" : ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomTV) ? @"tvOS" : @"iOS", // current OS
                              //@"osv" : [[NSProcessInfo processInfo] operatingSystemVersionString], //iOS Version
                              @"bv" : appVersion, // build version
                              @"app" : appName ?: @"", // application name
                              @"cd2" : instanceId, // iid
                              @"cd3" : bPID, // Bamboo Parent ID
                          }];
    
    if (userId != nil && userId.length > 0) {
        self.defaultParams[@"cd1"] = userId; // user id
    }
}

- (void)setItemID:(NSString *)itemID {
    self.didMakePlay = NO;
    self.didMakeStart = NO;
    self.currentItemID = itemID ?: nil;
    self.currentChannelID = nil;
    self.lastDurationEventDate = nil;
}

- (void)setChannelID:(NSString *)channelID {
    self.didMakePlay = NO;
    self.didMakeStart = NO;
    self.currentChannelID = channelID ?: nil;
    self.currentItemID = nil;
    self.lastDurationEventDate = nil;
}

- (NSURL *) eventUrlWithParams:(NSDictionary *)eventParams {
    NSURLComponents *components = [NSURLComponents componentsWithString:self.baseUrl];
    NSArray *queryItems = @[];
    
    NSDictionary *finalParams = [self.defaultParams dictionaryByMergingWithDictionary:eventParams];
    for (NSString *key in finalParams) {
        BOOL isString = [finalParams[key] isKindOfClass:[NSString class]];
        if ( isString ) {
            queryItems = [queryItems arrayByAddingObject: [NSURLQueryItem queryItemWithName:key value:finalParams[key]] ];
        }
    }
    
    components.queryItems = queryItems;
    
    return components.URL;
}

- (void)logEvent:(PVPPlayerAnalyticsEvent)event {
    
    if (event == PVPPlayerStartEvent) {
        if (self.didMakeStart) {
            return;
        }
        self.didMakeStart = YES;
    }
    
    if (event == PVPPlayerPlaysEvent || (event == PVPPlayerResumeEvent && !self.didMakePlay)) {
        if (self.didMakePlay) {
            return;
        }
        self.didMakePlay = YES;
        event = PVPPlayerPlaysEvent;
    }
    
    if (event == PVPPlayerPauseEvent && (!self.didMakePlay || self.lastEvent == PVPPlayerPauseEvent)) {
        return;
    }
    
    self.lastEvent = event;
    
    [self logEvent:event withParams:nil separateChannelID:NO];
}

- (void)logEvent:(PVPPlayerAnalyticsEvent)event withParams:(NSDictionary *)customParams separateChannelID:(BOOL)separateChannel {
    
    NSLog(@"aniview anlytics event: %li", event);
    
    if (self.currentItemID == nil && self.currentChannelID == nil) {
        return;
    }
    
    NSString *eventString = [self stringFromType:event];
    if ([eventString length] > 0) {
        NSInteger now = @([[NSDate date] timeIntervalSince1970WithServerOffset] * 1000).integerValue;
        
        NSString *videoID = self.currentChannelID ?: self.currentItemID ?: @"";
        NSString *channelID = @"";
        
        if (self.currentChannelID != nil && self.currentItemID != nil && separateChannel) {
            videoID = self.currentItemID;
            channelID = self.currentChannelID;
        }
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
            @"cb" : @(now).stringValue, // timestamp
            @"e" : eventString, // event name
            @"cvid" : videoID, // video id
            @"cd4" :channelID, // channel id
            @"cd5" :videoID,// video id
        }];
        
        if ([[customParams allKeys] count] > 0) {
            [params addEntriesFromDictionary:customParams];
        }
        
        NSURL *url = [self eventUrlWithParams:params];
        
        NSLog(@"aniview: %@", url.absoluteString);
        NSURLSessionDataTask *task = [self.session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSLog(@"finished");
        }];

        [task resume];
    }
}

- (void)logLastPartDurationEvent {
    if (self.lastDurationEventDate != nil) {
        NSInteger interval = (NSInteger)[[NSDate date] timeIntervalSinceDate:self.lastDurationEventDate];
        
        [self logEvent:PVPPlayerTimePlayedEvent withParams:@{@"cppt" : @(interval).stringValue} separateChannelID:YES];
    }
}

- (void)logPlayDurationEvent {
    self.lastDurationEventDate = [NSDate date];
    [self logEvent:PVPPlayerTimePlayedEvent withParams:@{@"cppt" : @"10"} separateChannelID:YES];
}

- (void)logChannelSwitchEvent:(PVPChannel *)channel scheduleItem:(PVPChannelScheduleItem *)item {
    
    self.didMakePlay = YES;

    if (self.currentChannelID != channel.mongoId) {
        if (self.currentChannelID != nil) {
            [self logLastPartDurationEvent];
        }
        
        self.lastDurationEventDate = nil;
    }
    
    self.currentItemID = item.entry.id;
    self.currentChannelID = channel.mongoId;
    
    [self logEvent:PVPPlayerPlaysEvent withParams:@{} separateChannelID:YES];
}

@end
