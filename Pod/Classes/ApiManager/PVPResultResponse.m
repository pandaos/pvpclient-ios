//
//  PVPResultResponse.m
//  Pods
//
//  Created by Oren Kosto,  on 6/20/16.
//
//

#import "PVPResultResponse.h"

@implementation PVPResultResponse

+(NSString *)resultKeyPathForJSONDictionary:(NSDictionary *)JSONDictionary
{
    return @"result";
}

@end
