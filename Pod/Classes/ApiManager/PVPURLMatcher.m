//
//  PVPURLMatcher.m
//  PVPClient
//
//  Created by Kovtun Vladimir on 15.06.2021.
//

#import "PVPURLMatcher.h"

@interface PVPURLMatcher (Extension)
- (OVCURLMatcherNode *)matcherNodeForPath:(NSString *)path;
@end

@implementation PVPURLMatcher

- (Class)modelClassForURLRequest:(NSURLRequest *)request andURLResponse:(NSHTTPURLResponse *)response {
    NSString *urlString = (response.URL ?: request.URL).relativeString;
    urlString = [[urlString stringByReplacingOccurrencesOfString:[response.URL query] withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"?"]];
    
    if ([urlString containsString:[response.URL host]]) {
        NSRange range = [urlString rangeOfString:[response.URL host]];
        urlString = [urlString substringFromIndex:(range.location + range.length)];
    }
    return [[self matcherNodeForPath:urlString]
            modelClassForURLRequest:request andURLResponse:response];
}

@end
