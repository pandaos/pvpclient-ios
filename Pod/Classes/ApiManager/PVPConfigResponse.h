//
//  PVPDataResponse.h
//  TVOS Test
//
//  Created by Oren Kosto,  on 6/14/16.
//  Copyright © 2016 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Overcoat/Overcoat.h>
#import "PVPConfig.h"

@interface PVPConfigResponse : OVCResponse

@end
