//
//  PVPDataResponse.m
//  TVOS Test
//
//  Created by Oren Kosto,  on 6/14/16.
//  Copyright © 2016 Panda-OS. All rights reserved.
//

#import "PVPDataResponse.h"

@implementation PVPDataResponse

+(NSString *)resultKeyPathForJSONDictionary:(NSDictionary *)JSONDictionary
{
    return @"data";
}

@end
