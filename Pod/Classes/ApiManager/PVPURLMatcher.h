//
//  PVPURLMatcher.h
//  PVPClient
//
//  Created by Kovtun Vladimir on 15.06.2021.
//

#import <Overcoat/Overcoat.h>

NS_ASSUME_NONNULL_BEGIN

@interface PVPURLMatcher : OVCURLMatcher

@end

NS_ASSUME_NONNULL_END
