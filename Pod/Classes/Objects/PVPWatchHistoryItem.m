//
//  PVPWatchHistoryItem.m
//  Pods
//
//  Created by Oren Kosto,  on 2/9/17.
//
//

#import "PVPWatchHistoryItem.h"

@implementation PVPWatchHistoryItem

-(id)init
{
    self = [super init];
    if (self) {
        _entry = nil;
        _time = 0;
    }
    return self;
}

-(void)setEntry:(PVPEntry *)entry {
    _entry = entry;
    [self refreshState];
}

-(void)setTime:(int)time {
    _time = time;
    [self refreshState];
}

-(void)refreshState {
    if (_time >= _entry.duration * 0.95) {
        _state = PVPWatchHistoryItemStateWatched;
    } else {
        _state = PVPWatchHistoryItemStateWatching;
    }
}

+ (NSValueTransformer *)entryJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPEntry.class];
}

+ (NSValueTransformer *)stateJSONTransformer {
    return [self JSONTransformerForInt];
}

@end
