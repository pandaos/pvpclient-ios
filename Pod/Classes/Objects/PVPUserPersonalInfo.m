//
//  PVPUserPersonalInfo.m
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import "PVPUserPersonalInfo.h"

@implementation PVPUserPersonalInfo

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"relationshipStatus": @"relationshipStatus",
             @"height": @"height",
             @"weight": @"weight",
             @"isSmoker": @"isSmoker",
             @"politicalView": @"politicalView",
             @"religionView": @"religionView",
             @"education": @"education",
             @"job": @"job",
             @"interests": @"interests",
             @"bio": @"bio",
             @"aboutMe": @"aboutMe",
             @"partnerDescription": @"partnerDescription"
             };
}

+ (NSValueTransformer *)relationshipStatusJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)heightJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)weightJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)isSmokerJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)educationJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)jobJSONTransformer {
    return [self JSONTransformerForInt];
}

@end
