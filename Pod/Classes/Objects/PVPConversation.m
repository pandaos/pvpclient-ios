//
//  PVPConversation.m
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPConversation.h"

@implementation PVPConversation

- (id)init {
    self = [super init];
    if (self) {
        self.conversation = [[PVPMessage alloc] init];
        self.userDetails = [[PVPUser alloc] init];
    }
    return self;
}

+ (NSDictionary *) JSONKeyPathsByPropertyKey {
    return @{@"conversation": @"conversation",
             @"userDetails": @"userDetails"};
}

+ (NSValueTransformer *)conversationJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPMessage.class];
}

+ (NSValueTransformer *)userDetailsJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUser.class];
}

@end
