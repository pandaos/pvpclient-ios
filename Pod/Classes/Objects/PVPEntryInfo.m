//
//  PVPEntryInfo.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/31/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPEntryInfo.h"

@implementation PVPEntryInfo

-(id) init
{
    self = [super init];
    self.price = @"";
    self.appleEventProduct = @"";
    self.isPrivate = 0;
    self.watchTime = 0;
    self.watchState = 0; // 0 not watched, PVPWatchHistoryItemStateWatching = 1, PVPWatchHistoryItemStateWatched = 2
    self.package = @"";
    self.packageEntryId = @"";
    return self;
}

+ (NSValueTransformer *)priceJSONTransformer {
    return [self JSONTransformerForString];
}

-(void) setEntryProduct:(SKProduct *)product
{
    self.appleEventProduct = product.productIdentifier;
}

-(SKProduct *)entryProduct
{
    NSString *productId = self.appleEventProduct;
    if (productId && productId.length > 0) {
        for (SKProduct *product in [[IAPHelper sharedInstance] eventProducts]) {
            if ([product.productIdentifier isEqualToString:productId]) {
                return product;
            }
        }
    }
    return nil;
}

-(NSString *)localizedPriceString
{
    NSString *productId = self.appleEventProduct;
    if (productId != nil) {
        return [[IAPHelper sharedInstance] localizedPriceStringForEventProductWithIdentifier:productId];
    }
    return @"";
}

@end
