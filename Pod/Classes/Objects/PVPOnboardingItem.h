//
//  PVPOnboardingItem.h
//  PVPClient
//
//  Created by Kovtun Vladimir on 26.11.2020.
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface PVPOnboardingItem : NSObject

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *imageUrl;
@property(nonatomic, copy) NSString *itemDescription;

@property(nonatomic) NSInteger order;

@end

NS_ASSUME_NONNULL_END
