//
//  PVPContact.h
//  Pods
//
//  Created by Oren Kosto,  on 5/16/16.
//
//

#import "PVPBaseObject.h"

/**
 *  The PVPContact object represents a user's contact list entry on the Bamboo platform. This object inherits most properties from the PVPBaseObject object.
 */
@interface PVPContact : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The contact's full name.
 */
@property (nonatomic, strong) NSString *fullName;

/**
 *  The contact's email address.
 */
@property (nonatomic, strong) NSString *email;

/**
 *  The contact's phone number.
 */
@property (nonatomic, strong) NSString *phone;

/**
 *  The contact's profile picture URL.
 */
@property (nonatomic, strong) NSString *profilePictureUrl;

@end
