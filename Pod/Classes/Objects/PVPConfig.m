//
//  PVPConfig.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/21/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPConfig.h"

@implementation PVPConfig

+ (NSValueTransformer *)homeJSONTransformer {
    return [PVPJSONAdapter transformerForModelPropertiesOfClass:NSDictionary.class];
}

+ (NSValueTransformer *)socialJSONTransformer {
    return [PVPBaseObject JSONTransformerForDictionary];
}

@end
