//
//  PVPPalConfig.h
//  PVPClient
//
//  Created by Prajeet Shrestha on 30/11/2022.
//

#import <Foundation/Foundation.h>


@interface PVPPalConfig : NSObject

@property (nonatomic, strong) NSString *omidPartnerName;
@property (nonatomic, strong) NSString *omidPartnerVersion;
@property (nonatomic, strong) NSString *omidVersion;

@end

