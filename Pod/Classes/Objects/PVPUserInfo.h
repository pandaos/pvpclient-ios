//
//  PVPSendUserInfo.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/23/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"
#import "PVPUserPersonalInfo.h"
#import "PVPUserSearchInfo.h"
#import "PVPConfigHelper.h"
#import "PVPContact.h"
#import "PVPInstance.h"

/**
 *  The PVPUserInfo object represents a PVPUser additional info. every PVP user has this object under the "info" field.
 *
 *  @see PVPUser
 */
@interface PVPUserInfo : PVPBaseObject

/**
 * @name Public Properties
 */

/**
 *  The user's personal info.
 *
 *  @see PVPUserPersonalInfo
 */
//@property (nonatomic, strong) PVPUserPersonalInfo *personalInfo;

/**
 *  The user's search info.
 *
 *  @see PVPUserSearchInfo
 */
//@property (nonatomic, strong) PVPUserSearchInfo *searchInfo;

/**
 *  @name Public Properties
 */

/**
 *  The user's phone number.
 */
@property (nonatomic, copy) NSString *phone;

/**
 *  The users promocode.
 */
@property (nonatomic, copy) NSString *promocode;

/**
 *  The URL of the user's profile picture.
 */
@property (nonatomic, copy) NSString *profilePictureUrl;

/**
 *  The URL of the user's cover photo.
 */
@property (nonatomic, copy) NSString *coverPhotoUrl;

/**
 * Number of profile views for this user.
 */
@property (nonatomic, assign, readonly) int profileViews;

//@property (nonatomic, assign, readonly) BOOL bypassCountryRestriction;

/**
 *  An array of push notification tokens for all of the user's devices.
 *  The objects in the array are in the format: 
 *
 *  ````
 *  @{@"udid": <device-udid>,
 *  @"platform": <platform>,
 *  @"token": <device-token>}
 *  ````
 *
 */
@property (nonatomic, copy) NSArray *deviceTokens;

/**
 *  A boolean indicating if the user has agreed to the terms of service.
 */
@property (nonatomic, copy) NSString *hasAcceptedTerms;

/**
 *  An array of the objects that this user is subscribed to.
 *  The objects in the array are in the format:
 *
 *  ````
 *  @{@"id": <object-id>,
 *  @"type": <object-type>}
 *  ````
 *
 */
@property (nonatomic, copy) NSArray *subscription;

/**
 *  An array of the objects that this user has liked.
 *  The objects in the array are in the format:
 *
 *  ````
 *  @{@"id": <object-id>,
 *  @"type": <object-type>}
 *  ````
 *
 */
@property (nonatomic, copy) NSArray *like;

/**
 *  A dictionary containing all of the IDs of the products linked to this user.
 *  The fields will be in the format:
 *
 *  ````
 *  @{<service-name>: <product-id>}
 *  ````
 *
 *  The dictionary may also contain the price of the subscription to the user in USD (i.e. $3.99), in the format:
 *
 *  ````
 *  @{@"price": <price>}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *products;
//@property (nonatomic, copy) NSString *price;

/**
 *  An array of the user's in-app contacts.
 */
@property (nonatomic, copy) NSArray<PVPContact *> *contacts;

/**
 *  An array of URLs of the user's photo gallery photos.
 */
@property (nonatomic, copy) NSArray *photoAlbum;


/**
 *  @name Public Methods
 */

/**
 *  Update the current device's push notifications token in the tokens array.
 */
-(void) updateCurrentDeviceToken;

/**
 Generates and returns the user profile picture URL with a given size.

 @param size The desired size of the profile picture URL.

 @return The profile picture URL, updated with the given size.
 */
- (NSString *)profilePictureUrlWithSize:(CGSize)size;

/**
 *  Parses the user's photo album into an array of dictionaries.
 *
 *  @return The user's photo album in array format.
 */
-(NSArray *) photoAlbumAsArray;

/**
 *  Add a new photo dictionary object to photoAlbum.
 *
 *  @warning The URL of the photo is a CDN url - which means it isn't 100% reliable!
 *
 *  @param photoObject PhotoObject is a NSDictionary object with 2 fields:
 *
 *  ````
 *  @{@"id": <photo-id>,
 *  @"url": <photo-url>}
 *  ````
 *
 *  This object represents a photo in the user's photo gallery.
 *
 */
-(void) addPhotoToAlbum:(NSDictionary *)photoObject;

/**
 *  Remove a photo object from photoAlbum.
 *
 *  @param photoId the photo's unique identifier.
 */
-(void) removePhotoFromAlbumById:(NSString *)photoId;

/**
 *  Remove a photo object from photoAlbum.
 *
 *  @param photoUrl the photo's URL.
 */
-(void) removePhotoFromAlbumByUrl:(NSString *)photoUrl;

@end
