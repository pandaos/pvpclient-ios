//
//  PVPChannelSchedule.h
//  Pods
//
//  Created by Oren Kosto,  on 1/16/17.
//
//

#import "PVPBaseObject.h"
#import "PVPChannelScheduleItem.h"

@interface PVPChannelSchedule : PVPBaseObject <PVPBaseObjectProtocol>

@property (nonatomic, assign) int type;
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int date;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *creator;
@property (nonatomic, copy) NSString *owner;
@property (nonatomic, copy) NSString *instanceId;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, copy) NSArray <PVPChannelScheduleItem *>*scheduleItems;

@end
