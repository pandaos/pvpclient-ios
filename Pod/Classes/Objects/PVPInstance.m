//
//  PVPInstance.h
//  Pods
//
//  Created by  on 17/2/19.
//
//

#import "PVPInstance.h"

@interface PVPInstance ()

/**
 *  @name Private Properties
 */

/**
 *  The MongoId object of the user, in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *mongoIdObj;

@end

@implementation PVPInstance

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    return dict;
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [self JSONTransformerForInt];
}

-(NSString *)mongoId
{
    return [PVPMongoHelper mongoIdStringFromObject:self.mongoIdObj];
}

@end
