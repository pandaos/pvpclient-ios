//
//  PVPEntry.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPEntry.h"
#import "PVPLocalizationHelper.h"

@interface PVPEntry()

//@property (readwrite, copy) NSString *id;

@end

@implementation PVPEntry

-(id)init
{
    self = [super init];
    [self setMediaType:[KalturaMediaType IMAGE]];
    [self setSearchProviderType:0];
    [self setMediaDateFromString:@"0"];
    [self setPlaysFromString:@"0"];
    [self setViewsFromString:@"0"];
    [self setWidthFromString:@"0"];
    [self setHeightFromString:@"0"];
    [self setDurationFromString:@"0"];
    [self setPartnerIdFromString:@"111"];
    [self setModerationStatusFromString:@"0"];
    [self setModerationCountFromString:@"0"];
    [self setCreatedAtFromString:@"0"];
    [self setUpdatedAtFromString:@"0"];
    [self setTotalRankFromString:@"0"];
    [self setVotesFromString:@"0"];
    [self setGroupId:0];
    [self setLicenseType:0];
    [self setVersionFromString:@"0"];
    [self setAccessControlId:0];
    [self setStartDate:0];
    [self setEndDate:0];
    [self setPartnerSortValue:0];
    [self setConversionProfileId:0];
    [self setMsDuration:0];
    [self setRankFromString:@"0"];
    self.info = [[PVPEntryInfo alloc] init];
    return self;
}

- (id)initWithId:(NSString *)entryId {
    self = [super init];
    if (self) {
        self.id = entryId;
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    //KalturaBaseEntry -> KalturaplayableEntry -> KalturaMediaEntry

    return @{@"id":@"id",
             @"name":@"name",
             @"description":@"description",
             @"partnerId":@"partnerId",
             @"userId":@"userId",
             @"creatorId":@"creatorId",
             @"tags":@"tags",
             @"adminTags":@"adminTags",
             @"categories":@"categories",
             @"categoriesIds":@"categoriesIds",
             @"status":@"status",
             @"moderationStatus":@"moderationStatus",
             @"moderationCount":@"moderationCount",
             @"type":@"type",
             @"createdAt":@"createdAt",
             @"updatedAt":@"updatedAt",
             @"rank":@"rank",
             @"totalRank":@"totalRank",
             @"votes":@"votes",
             @"groupId":@"groupId",
             @"partnerData":@"partnerData",
             @"downloadUrl":@"downloadUrl",
             @"searchText":@"searchText",
             @"licenseType":@"licenseType",
             @"version":@"version",
             @"thumbnailUrl":@"thumbnailUrl",
             @"accessControlId":@"accessControlId",
             @"startDate":@"startDate",
             @"endDate":@"endDate",
             @"referenceId":@"referenceId",
             @"replacingEntryId":@"replacingEntryId",
             @"replacedEntryId":@"replacedEntryId",
             @"replacementStatus":@"replacementStatus",
             @"partnerSortValue":@"partnerSortValue",
             @"conversionProfileId":@"conversionProfileId",
             @"redirectEntryId":@"redirectEntryId",
             @"rootEntryId":@"rootEntryId",
//             @"operationAttributes":@"operationAttributes",
             @"entitledUsersEdit":@"entitledUsersEdit",
             @"entitledUsersPublish":@"entitledUsersPublish",
             
             @"plays":@"plays",
             @"views":@"views",
             @"lastPlayedAt":@"lastPlayedAt",
             @"width":@"width",
             @"height":@"height",
             @"duration":@"duration",
             @"msDuration":@"msDuration",
             @"durationType":@"durationType",
             
             @"mediaType":@"mediaType",
             @"conversionQuality":@"conversionQuality",
             @"sourceType":@"sourceType",
             @"searchProviderType":@"searchProviderType",
             @"searchProviderId":@"searchProviderId",
             @"creditUserName":@"creditUserName",
             @"creditUrl":@"creditUrl",
             @"mediaDate":@"mediaDate",
             @"dataUrl":@"dataUrl",
             @"flavorParamsIds":@"flavorParamsIds",
             
             @"objectType":@"objectType",
             @"info":@"info"};
}

+ (NSValueTransformer *)infoJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPEntryInfo.class];
}

+ (Class)classForParsingJSONDictionary:(NSDictionary *)JSONDictionary {
    if (JSONDictionary[@"mediaType"]) { //dynamic mapping according to the entry media type
        int mediaType = [JSONDictionary[@"mediaType"] intValue];
        if (mediaType == [KalturaMediaType LIVE_STREAM_FLASH]) {
            return NSClassFromString(@"PVPLiveEntry");
        }
        else {
            return self;
        }
    }
    return self;
}

-(NSString *)name {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *key = [NSString stringWithFormat:@"%@_name", language];
    
    return self.info.allProperties[key] ? self.info.allProperties[key] : super.name;
}

-(NSString *)description {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *key = [NSString stringWithFormat:@"%@_description", language];
    
    return self.info.allProperties[key] ? self.info.allProperties[key] : super.description;
}

-(NSString *)tags {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *key = [NSString stringWithFormat:@"%@_tags", language];
    
    return self.info.allProperties[key] ? self.info.allProperties[key] : super.tags;
}

-(BOOL)isAd {
    BOOL result = NO;
    NSString *regex = @">Ad[s]?(>|$)";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    NSArray<NSString *> *splitCategories = [self.categories componentsSeparatedByString:@","];
    for (NSString *category in splitCategories) {
        if ([category rangeOfString:regex options:NSRegularExpressionSearch].location != NSNotFound) {
            result = YES;
            break;
        }
    }
    return result;
}

- (NSString *)pvpDownloadUrl {
    return [self pvpDownloadUrlWithFlavorParamId:0];
}

- (NSString *)pvpDownloadUrlWithFlavorParamId:(int)flavorParamId {
    return [NSString stringWithFormat:@"%@/p/%@/sp/0/playManifest/entryId/%@/format/url/flavorParamId/%d/ks/%@/video.mp4",
                                      [PVPConfigHelper sharedInstance].kalturaServiceUrl,
                                      [PVPConfigHelper sharedInstance].partnerId,
                                      self.id,
                                      flavorParamId,
                                      [PVPConfigHelper sharedInstance].currentConfig.userKs];
}

- (NSString *)pvpAdaptiveHLSPlaybackUrl {
    return [NSString stringWithFormat:@"%@/api/entry/%@/flavors/playlist.m3u8",
                                      [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"],
                                      self.id];
}

- ( NSString * ) pvpThumbnailUrl {
    return [self pvpThumbnailUrlWithTime:self.duration / 2];
}

- ( NSString * ) pvpThumbnailUrlWithTime:( int )time {
    return [self pvpThumbnailUrlWithTime:time withSize:CGSizeZero];
}

- (NSString *)pvpThumbnailUrlWithTime:(int)time withSize:(CGSize)size {
    return [self pvpThumbnailUrlWithTime:time withSize:size withType:nil];
}

- (NSString *)pvpThumbnailUrlWithTime:(int)time withSize:(CGSize)size withType:(NSString *)type {
    if (self.mediaType == [KalturaMediaType LIVE_STREAM_FLASH]) {
        return [PVPEntry kalturaThumbnailUrlForEntryId:self.id withSize:size withType:type];
    }
    
    NSString *widthStr = @"";
    NSString *heightStr = @"";
    NSString *typeStr = @"";
    
    if (size.height > 0) {
        heightStr = [NSString stringWithFormat:@"height=%d", (int) roundf(size.height)];
    }
    if (size.width > 0) {
        heightStr = [NSString stringWithFormat:@"width=%d", (int) roundf(size.width)];
    }
    if (type) {
        typeStr = [NSString stringWithFormat:@"type=%@", type];
    }
    
    return [NSString stringWithFormat:@"%@%@/entry/%@/thumbnail/%d?%@&%@&%@",
            [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"], [PVPConfigHelper pvpServerPrefix],
            self.id,
            time,
            widthStr,
            heightStr,
            typeStr];
}

- (BOOL)isYouTubeEntry {
    return (self.info.allProperties[@"youtube"] != nil && [self youTubeID] != nil && self.status.intValue == 7);
}

- (BOOL)isExternalEntry {
    return (self.info.allProperties[@"hlsUrl"] != nil && [self.info.allProperties[@"hlsUrl"] length] > 0 && self.status.intValue == 7);
}

- (NSString *)externalHLSUrl {
    if (self.info.allProperties[@"hlsUrl"] != nil && [self.info.allProperties[@"hlsUrl"] length] > 0) {
        return self.info.allProperties[@"hlsUrl"];
    }
    
    return nil;
}

- (NSString *)youTubeID {
    if (self.info.allProperties[@"youtube"] != nil) {
        NSDictionary *dict = [NSDictionary dictionaryFromJSONString:self.info.allProperties[@"youtube"]];
        return dict[@"id"];
    }
    
    return nil;
}

- (NSString *)articleLink {
    return [PVPLocalizationHelper canUseString:self.info.allProperties[@"articleLink"]] ? self.info.allProperties[@"articleLink"] : nil;
}

- (NSString *)trailerID {
    return [PVPLocalizationHelper canUseString:self.info.allProperties[@"trailerId"]] ? self.info.allProperties[@"trailerId"] : nil;
}

- (NSString *)posterUrlString {
    return [PVPLocalizationHelper canUseString:self.info.allProperties[@"poster"]] ? self.info.allProperties[@"poster"] : nil;
}

- (NSString *)eventIDString {
    return [NSString stringWithFormat:@"%@|%@", self.id, self.name ?: @""];
}

+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId {
    return [self kalturaThumbnailUrlForEntryId:entryId withSize:CGSizeZero];
}

+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId withSize:(CGSize)size {
    return [self kalturaThumbnailUrlForEntryId:entryId withSize:size withType:NO];
}

+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId withSize:(CGSize)size withType:(NSString *)type {
    NSString *widthStr = @"";
    NSString *heightStr = @"";
    NSString *typeStr = @"";
    
    if (size.height > 0) {
        heightStr = [NSString stringWithFormat:@"height/%d/", (int) roundf(size.height)];
    }
    if (size.width > 0) {
        widthStr = [NSString stringWithFormat:@"width/%d/", (int) roundf(size.width)];
    }
    if (type) {
        typeStr = [NSString stringWithFormat:@"type/%@", type];
    }
    return [NSString stringWithFormat:@"%@/p/%@/thumbnail/entry_id/%@/%@%@%@",
            [[PVPConfigHelper sharedInstance] kalturaCDNUrl],
            [[PVPConfigHelper sharedInstance] partnerId],
            entryId,
            widthStr,
            heightStr,
            typeStr];
}


+ ( NSString * ) hlsManifestUrlWithId: ( NSString * ) entryId {
    NSString *bambooCdnUrl = [PVPConfigHelper sharedInstance].currentConfig.application[@"bambooCdnUrl"];
    if (bambooCdnUrl && [bambooCdnUrl hasPrefix:@"//"]) {
        bambooCdnUrl = [NSString stringWithFormat:@"https:%@", bambooCdnUrl];
    }
    NSString *playUrl = [NSString stringWithFormat:@"%@/api/entry/%@/flavors/playlist.m3u8?iid=%@",
                         [bambooCdnUrl isKindOfClass:[NSString class]] ? bambooCdnUrl : [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"],
                         entryId,
                         [PVPConfigHelper sharedInstance].currentInstanceId];
    
    return playUrl;
}

- (NSArray<PVPPackage *> *)entryPackages {
    if ([[self.info package] length] > 0) {
        NSMutableArray *packs = [NSMutableArray new];
        
        NSArray *entryPacks = [self.info.package componentsSeparatedByString:@","];
        
        for (NSString *packID in entryPacks) {
            for (PVPPackage *pack in [[IAPHelper sharedInstance] appPackages]) {
                if ([[pack mongoId] isEqualToString:packID]) {
                    [packs addObject:pack];
                    break;
                }
            }
        }
        
        return packs;
    }
    
    return nil;
}

- (BOOL)hasPurchases {
    return ((self.info.package && self.info.package.length > 0) || ([self.info.price length] > 0));
}

- (BOOL)isPurchased {
    if (self.info.package && self.info.package.length > 0) {
        NSArray *purchases = [[IAPHelper sharedInstance].purchasedPackages copy];
        NSArray *packageIDS = [self.info.package componentsSeparatedByString:@","];
        for (NSString *packID in packageIDS) {
            if ([purchases containsObject:packID]) {
                return YES;
            }
        }
    }
    
    if([self.info.price length] > 0) {
        NSArray *purchases = [[IAPHelper sharedInstance].purchasedEntries copy];
        if ([purchases containsObject:self.id]) {
            return YES;
        }
    }
    
    return NO;
}

@end
