//
//  XYZUser.m
//  rest-clieny
//
//  Created by Oren Kosto,  on 6/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPUser.h"

@interface PVPUser ()

/**
 *  @name Private Properties
 */

/**
 *  The MongoId object of the user, in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *mongoIdObj;

@end

@implementation PVPUser

-(id)init
{
    self = [super init];
    if (self) {
        self.info = [[PVPUserInfo alloc] init];
        self.fullName = @"";
        self.firstName = @"";
        self.lastName = @"";
        self.area = @"";
        self.city = @"";
        self.phone = @"";
        self.info.profilePictureUrl = @"";
        self.info.hasAcceptedTerms = @"NO";
    }
    return self;
}

-(NSString *)mongoId
{
    return [PVPMongoHelper mongoIdStringFromObject:self.mongoIdObj];
}

-(NSString *)userFullName {
    if (self.fullName && self.fullName.length > 0) {
        return self.fullName;
    } else {
        return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    }
}

-(BOOL)bypassSchedulingRestriction {
    if (self.info && self.info.allProperties && self.info.allProperties[@"bypassSchedulingRestriction"]) {
        return [self.info.allProperties[@"bypassSchedulingRestriction"] boolValue];
    } else {
        return NO;
    }
}

-(BOOL)bypassCountryRestriction {
    if (self.info && self.info.allProperties && self.info.allProperties[@"bypassCountryRestriction"]) {
        return [self.info.allProperties[@"bypassCountryRestriction"] boolValue];
    } else {
        return NO;
    }
}

-(BOOL)bypassPurchaseRestriction {
    if (self.info && self.info.allProperties && self.info.allProperties[@"bypassPurchaseRestriction"]) {
        return [self.info.allProperties[@"bypassPurchaseRestriction"] boolValue];
    } else {
        return NO;
    }
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    dict[@"pvpRole"] = @"role";
    return dict;
//    return @{
//            @"mongoIdObj":@"_id",
//            @"password":@"password",
//            @"pvpRole":@"role",
//            @"id":@"id",
//            @"partnerId":@"partnerId",
//            @"screenName":@"screenName",
//            @"fullName": @"fullName",
//            @"email":@"email",
//            @"dateOfBirth":@"dateOfBirth",
//            @"country":@"country",
//            @"state":@"state",
//            @"city":@"city",
//            @"zip":@"zip",
//            @"thumbnailUrl":@"thumbnailUrl",
//            @"description":@"description",
//            @"tags":@"tags",
//            @"adminTags":@"adminTags",
//            @"gender":@"gender",
//            @"status":@"status",
//            @"createdAt":@"createdAt",
//            @"updatedAt":@"updatedAt",
//            @"partnerData":@"partnerData",
//            @"indexedPartnerDataInt":@"indexedPartnerDataInt",
//            @"indexedPartnerDataString":@"indexedPartnerDataString",
//            @"storageSize":@"storageSize",
//            @"password":@"password",
//            @"firstName":@"firstName",
//            @"lastName":@"lastName",
//            @"isAdmin":@"isAdmin",
//            @"lastLoginTime":@"lastLoginTime",
//            @"statusUpdatedAt":@"statusUpdatedAt",
//            @"deletedAt":@"deletedAt",
//            @"loginEnabled":@"loginEnabled",
//            @"roleIds":@"roleIds",
//            @"roleNames":@"roleNames",
//            @"isAccountOwner":@"isAccountOwner",
//            @"allowedPartnerIds":@"allowedPartnerIds",
//            @"allowedPartnerPackages":@"allowedPartnerPackages",
//            @"area" : @"area",
//            @"info": @"info",
//            @"isOnline":@"isOnline",
//            @"isLive":@"isLive"
//    };
}

+ (NSValueTransformer *)infoJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserInfo.class];
}

+ (NSValueTransformer *)dateOfBirthJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)stateJSONTransformer {
    return [self JSONTransformerForString];
}

-(BOOL) isBirthday:(int) timestamp {
    return [PVPTimeHelper isDate:[NSDate date] equalsToTimestamp:timestamp byEra:NO byYear:NO byMonth:YES byDay:YES];
}

-(NSString *)birthDisplayDate {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.dateOfBirth];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd/MM/yyyy"];
    
    return [format stringFromDate:date];
}

-(NSString *)genderDisplayName {

    if (self.gender == 1) {
        return [@"Male" localizedString];
    } else if (self.gender == 2) {
        return [@"Female" localizedString];
    }
    
    return @"";
}

-(BOOL) isSpecialRightsUser {
    return ([_pvpRole isEqualToString:PVP_ROLE_ADMIN]
            || [_pvpRole isEqualToString:PVP_ROLE_ROOT]
            || [_pvpRole isEqualToString:PVP_ROLE_MANAGER]);
}

- (NSDictionary *)jsonObject {
    
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithDictionary:
         @{
             @"id": self.id,
             @"firstName" : self.firstName,
             @"lastName" : self.lastName,
             @"email" : self.email,
             @"password" : self.password,
             @"role": PVP_ROLE_VIEWER,
             @"fullName" : self.fullName
         }];
    
    NSMutableDictionary *jsonInfoDict = [[NSMutableDictionary alloc] init];

    
    if( [PVPConfigHelper sharedInstance].enablePromoCode ) {
        jsonInfoDict[@"promocode"] = self.info.promocode ?: @"";
    }
    
    if( [PVPConfigHelper sharedInstance].enableGender ) {
        jsonDict[@"gender"] = @(self.gender);
    }
    
    if( [PVPConfigHelper sharedInstance].enableDOB ) {
        jsonDict[@"dateOfBirth"] = @(self.dateOfBirth);
    }
    
    if( [PVPConfigHelper sharedInstance].enablePhone ) {
        jsonDict[@"phone"] = self.phone ?: @"";
    }
    
    jsonDict[@"info"] = jsonInfoDict;
    
    return jsonDict;
}

@end
