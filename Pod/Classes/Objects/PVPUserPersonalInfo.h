//
//  PVPUserPersonalInfo.h
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

/**
 *  The PVPUserPersonalInfo object includes user's personal info. Every PVP user has this object in the following path: info -> personalInfo.
 *  @see PVPUserInfo
 */
@interface PVPUserPersonalInfo : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
 *  The enum of the user's relationship status.
 */
@property (nonatomic, assign) int relationshipStatus;

/**
 *  The user's height in centimeters.
 */
@property (nonatomic, assign) int height;

/**
 *  The user's weight in kilograms.
 */
@property (nonatomic, assign) int weight;

/**
 *  An numeric value indicating if the user is a smoker (0 for no, 1 for yes).
 */
@property (nonatomic, assign) int isSmoker;

/**
 *  The user's policital view.
 */
@property (nonatomic, copy) NSString *politicalView;

/**
 *  The user's religious view.
 */
@property (nonatomic, copy) NSString *religionView;

/**
 *  The enum value of the user's education level.
 */
@property (nonatomic, assign) int education;

/**
 *  The user's job.
 */
@property (nonatomic, assign) int job;

/**
 *  The user's interests.
 */
@property (nonatomic, copy) NSString *interests;

/**
 *  The user's biography.
 */
@property (nonatomic, copy) NSString *bio;

/**
 *  A short description of the user.
 */
@property (nonatomic, copy) NSString *aboutMe;

/**
 *  A short description of the ideal partner for the user.
 */
@property (nonatomic, copy) NSString *partnerDescription;

@end
