//
//  PVPRestorePurchases.h
//  Pods
//
//  Created by Oren Kosto,  on 7/13/17.
//
//

#import "PVPBaseObject.h"

@interface PVPEncryptedData : PVPBaseObject <PVPBaseObjectProtocol>

@property(nonatomic, strong) NSArray<NSString *> *data;

- (PVPEncryptedData *)initWithLocalPurchases;
- (PVPEncryptedData *)initWithObjects:(NSArray <NSDictionary *>*)objects;

@end
