//
//  PVPFlavorAsset.h
//  Pods
//
//  Created by Oren Kosto,  on 6/27/16.
//
//

#import <Foundation/Foundation.h>
#import "KalturaClient.h"
#import "PVPBaseObject.h"

/**
 *  The PVPFlavorAsset object represents a media flavor asset on the Bamboo platform. 
 */
@interface PVPFlavorAsset : KalturaFlavorAsset

/**
 *  The URL of the media file for this flavor.
 */
@property(nonatomic, copy, readonly) NSString *url;

@end
