//
//  ObjectsClass.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/29/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "ObjectClasses.h"

@implementation ObjectClasses

+(NSArray *)getObjectClasses
{
    return @[@"PVPUser", @"PVPLogin", @"PVPEntry", @"PVPCategory", @"PVPLiveEntry", @"PVPLiveEntryPublish", @"PVPUploadToken", @"PVPConfig", @"PVPMeta", @"PVPSubscription",@"PVPLike", @"PVPPushNotificationRequestObject", @"PVPOpenTok", @"PVPResult", @"PVPConversation", @"PVPMessage", @"PVPEvent", @"PVPFlavorAsset", @"PVPPlaylist", @"PVPChannelSchedule", @"PVPPackage", @"PVPEncryptedData", @"PVPNode", @"PVPNodeCategory"];
}

@end
