//
//  PVPBaseObject.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"
#import <objc/runtime.h>

@interface PVPBaseObject ()

/**
 *  @name Private Properties
 */

/**
 *  The MongoId object of the object, in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *mongoIdObj;

@end

@implementation PVPBaseObject

- (NSString *)mongoId
{
    if (self.mongoIdObj) {
        return [PVPMongoHelper mongoIdStringFromObject:self.mongoIdObj];
    }
    return nil;
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    NSArray *propertyNames = [self allPropertyNames];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:propertyNames.count];
    for (NSString *name in propertyNames) {
        [dict setValue:name forKey:name];
    }
    if (dict[@"mongoIdObj"]) {
        dict[@"mongoIdObj"] = @"_id";
    }
    [dict removeObjectsForKeys:@[@"superclass", @"description", @"debugDescription", @"hash", @"allProperties"]];
    return [NSDictionary dictionaryWithDictionary:dict];
}

-(void)setNilValueForKey:(NSString *)key
{
    @try {
        [super setNilValueForKey:key];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)updatedAtJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)JSONTransformerForInt {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSNumber numberWithInt:value.intValue];
    }];
}

+ (NSValueTransformer *)JSONTransformerForBool {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSNumber numberWithInt:value.boolValue];
    }];
}

+ (NSValueTransformer *)JSONTransformerForString {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        if (value) {
            if (![value isKindOfClass:[NSString class]]) {
                value = [NSString stringWithFormat:@"%@", value];
            }
        } else {
            value = @"";
        }
        return value;
    }];
}

+ (NSValueTransformer *)JSONTransformerForDictionary {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        if (value && [value isKindOfClass:[NSDictionary class]]) {
            return value;
        }
        return @{};
    }];
}

+ (NSArray *)allPropertyNames {
    NSMutableArray *result = [NSMutableArray array];
    Class observed = self;

    while ([observed isSubclassOfClass:PVPBaseObject.class]) {
        unsigned count;
        objc_property_t *properties = class_copyPropertyList(observed, &count);

        unsigned i;
        for (i = 0; i < count; i++)
        {
            objc_property_t property = properties[i];
            NSString *name = [NSString stringWithUTF8String:property_getName(property)];
            [result addObject:name];
        }

        free(properties);
        observed = observed.superclass;
    }

    return result;
}

@end
