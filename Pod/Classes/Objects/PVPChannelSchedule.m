//
//  PVPChannelSchedule.m
//  Pods
//
//  Created by Oren Kosto,  on 1/16/17.
//
//

#import "PVPChannelSchedule.h"

@implementation PVPChannelSchedule

-(id)init
{
    self = [super init];
    if (self) {
        self.scheduleItems = [NSArray array];
    }
    return self;
}

+ (NSValueTransformer *)scheduleItemsJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPChannelScheduleItem.class];
}

@end
