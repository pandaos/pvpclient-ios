//
//  PVPLogin.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/30/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

/**
 *  The PVP login object is used to log in to the platform, either with a user id and password, or with a social network access token.
 */
@interface PVPLogin : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The ID of the user we wish to log in.
 */
@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *accountId; // iid

/**
 *  The password of the user we wish to log in.
 */
@property (nonatomic, copy) NSString *password;

/**
 *  The PVP session token returned from the response of a successful login.
 */
@property (nonatomic, copy) NSString *token;

/**
 *  A valid Facebook access token for the user we with to log in.
 */
@property (nonatomic, copy) NSString *facebookToken;

/**
 *  A valid OAuth access token for the user we with to log in.
 */
@property (nonatomic, copy) NSString *oAuthToken;

/**
 *  A valid OAuth token secret for the user we with to log in.
 */
@property (nonatomic, copy) NSString *oAuthTokenSecret;

/**
 *  @name Public Methods
 */

/**
 *  Initializes an instance of this object.
 *
 *  @return A new initialized instance of the PVPLogin object.
 */
-(PVPLogin *)init;

/**
 *  Initializes an instance of this object, pre-set with a user id and password.
 *
 *  @param userId   The user id we wish to use.
 *  @param password The password for the user id.
 *
 *  @return A new initialized instance of the PVPLogin object, containing the given user id and password.
 */
-(PVPLogin *)initWithUserId:(NSString *)userId withPassword:(NSString *)password;

@end
