//
//  PVPMeta.h
//  PVPClient
//
//  Created by Oren Kosto,  on 9/4/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

/**
 *  The PVPMeta object represents the metadata of a response that has been returned from the server.
 *  The response metadata will contain general information about the response (currently only contains the amount of objects returned).
 */
@interface PVPMeta : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The total amount of objects found in the query (regardless of the number of objects returned in the response).
 */
@property (nonatomic, copy) NSString *total;

/**
 *  The token of the next page (in case we're dealing with Youtube data api for example)
 */
@property (nonatomic, copy) NSString *nextPageToken;

/**
 *  The token of the previous page (in case we're dealing with Youtube data api for example)
 */
@property (nonatomic, copy) NSString *prevPageToken;


/**
 *  @name Public Methods
 */

/**
 *  Initializes a PVPMeta obbect with a given value in the "total" field.
 *
 *  @param total the value to assign the "total" field.
 *
 *  @return A new PVPMeta object with the given value in the "total" field.
 */
-(instancetype)initWithTotal:(int)total;

/**
 *  Initializes a PVPMeta obbect with a given value in the "total" field.
 *
 *  @param total the value to assign the "total" field.
 *
 *  @param prevPageToken the value to assign the "prevPageToken" field.
 *
 *  @param nextPageToken the value to assign the "nextPageToken" field.
 *
 *  @return A new PVPMeta object with the given values.
 */
-(instancetype)initWithTotal:(int)total withPrevPageToken:(NSString *)prevPageToken withNextPageToken:(NSString *)nextPageToken;

@end
