//
//  PVPPlaylist.h
//  Pods
//
//  Created by Oren Kosto,  on 1/16/17.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"
#import "KalturaClient.h"
#import "PVPEntry.h"

@interface PVPPlaylist : KalturaPlaylist <PVPBaseObjectProtocol>

@property (nonatomic, copy) NSArray<PVPEntry *> *entries;

@end
