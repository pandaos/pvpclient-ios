//
//  PVPEntryInfo.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/31/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"
#import "IAPHelper.h"

/**
 *  The PVPEntryInfo object represents the additional custom metadata we store on a PVPEntry or PVPLiveEntry object.
 */
@interface PVPEntryInfo : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The price in USD as a string.
 */
@property (nonatomic, strong) NSString *price;

/**
 *  The package ID as a string.
 */
@property (nonatomic, strong) NSString *package;

/**
 *  The package entry ID as a string.
 */
@property (nonatomic, strong) NSString *packageEntryId;

/**
 *  The Apple IAP product for this entry. This will be used by the app for paid entries.
 */
@property (nonatomic, strong) NSString *appleEventProduct;

/**
 *  A flag indicating if the entry is private or not (1 for private, 0 for public).
 */
@property (nonatomic, assign) int isPrivate;

/**
 *  A flag indicating entry playback state.
 */
@property (nonatomic, assign) int watchState;
/**
 *  A flag indicating entry playback time.
 */
@property (nonatomic, assign) int watchTime;

/**
 * The player logo URL.
 */
@property (strong, nonatomic) NSString *playerLogoUrl;

/**
 *  @name Public Methods
 */

/**
 *  Sets the given Apple IAP product's identifier as the appleEventProduct attribute for this entry.
 *
 *  @param product The Apple IAP product which we would like to use for this entry.
 */
-(void) setEntryProduct:(SKProduct *)product;

/**
 *  Gets the corresponding Apple IAP product currently set for this entry.
 *
 *  @return The Apple IAP product currently set for this entry.
 */
-(SKProduct *)entryProduct;

/**
 *  The localized price string for this entry's IAP product.
 *
 *  @return The localized price string for this entry's IAP product.
 *
 *  @warning This function will return the price in the local currency of the App Store used by the device (i.e. New Israeli Shekels for an Israeli device).
 *  Due to different tax regulations between countries, there is no way of getting the price in any other currency.
 *  Once an object is purchased, the server converts the paid amount to USD before saving the purchase in the database.
 */
-(NSString *)localizedPriceString;

@end
