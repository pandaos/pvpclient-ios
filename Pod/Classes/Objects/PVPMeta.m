//
//  PVPMeta.m
//  PVPClient
//
//  Created by Oren Kosto,  on 9/4/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPMeta.h"

@implementation PVPMeta

-(instancetype)initWithTotal:(int)total {
    
    self = [super init];
    if (self) {
        self.total = [NSString stringWithFormat:@"%d", total];
    }
    return self;
}

-(instancetype)initWithTotal:(int)total withPrevPageToken:(NSString *)prevPageToken withNextPageToken:(NSString *)nextPageToken
{
    self = [self initWithTotal:total];
    if(self) {
        self.prevPageToken = prevPageToken;
        self.nextPageToken = nextPageToken;
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"total": @"total",
             @"prevPageToken": @"prevPageToken",
             @"nextPageToken": @"nextPageToken"};
}

@end
