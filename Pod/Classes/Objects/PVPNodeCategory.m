//
//  PVPNodeCategory.m
//  Pods
//
//  Created by  on 09/07/18.
//

#import "PVPNodeCategory.h"
#import "PVPNode.h"

@implementation PVPNodeCategory

-(id)init
{
    self = [super init];
    if (self) {
        self.content = [NSArray array];
        self.items = [NSArray array];
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"content": @"content",
             @"type": @"type",
             @"title": @"title",
             @"link": @"link",
             @"link_title": @"link_title",
             @"icon": @"icon",
             @"categoryReverseColors": @"categoryReverseColors",
             @"categoryNameIcon": @"categoryNameIcon",
             @"categoryLayoutHeight": @"categoryLayoutHeight",
             @"category_name": @"category_name",
             @"items": @"items",
             @"overrideAppLogo": @"info"
             };
}

+ (NSValueTransformer *)contentJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPNode.class];
}

+ (NSValueTransformer *)itemsJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPNodeCategory.class];
}

+ (NSValueTransformer *)overrideAppLogoJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSDictionary *values) {
        return values[@"overrideAppLogo"];
    } reverseBlock:^(NSString *str) {
        return @[@{@"overrideAppLogo" : str}];
    }];
}

+ (NSValueTransformer *)categoryReverseColorsJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSNumber numberWithInt:value.boolValue];
    }];
}

+ (NSValueTransformer *)categoryLayoutHeightJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSNumber numberWithFloat:value.floatValue];
    }];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [self JSONTransformerForString];
}

@end
