//
//  PVPPlaylist.m
//  Pods
//
//  Created by Oren Kosto,  on 1/16/17.
//
//

#import "PVPPlaylist.h"

@implementation PVPPlaylist

-(id)init
{
    self = [super init];
    if (self) {
        self.entries = [NSArray array];
    }
    return self;
}

+ (NSValueTransformer *)entriesJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPEntry.class];
}

+ (NSValueTransformer *)filtersJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if (!value) {
            value = @[];
        }
        return [value isKindOfClass:[NSMutableArray class]] ? value : [NSMutableArray arrayWithArray:value];
    }];
}

@end
