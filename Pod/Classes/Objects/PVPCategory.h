//
//  PVPCategory.h
//  Pods
//
//  Created by Roni Cohen on 07/10/2016.
//
//

#import "PVPBaseObject.h"
#import "PVPEntry.h"
#import "PVPPackage.h"

/**
 * The PVPCategory object represents a media category on the Bamboo platform.
 */
@interface PVPCategory : PVPBaseObject<PVPBaseObjectProtocol>

/**
 * @name Public Properties
 */

/**
 * The ID of the Category.
 */
@property (nonatomic,assign,readonly) int id;

/**
 * The parent ID of the category.
 */
@property (nonatomic,assign) int parentId;

/**
 * The depth of the category.
 */
@property (nonatomic,assign,readonly) int depth;

/**
 * The custom metadata of the category.
 */
@property (strong, nonatomic) NSDictionary *info;

/**
 * The partner ID.
 */
@property (nonatomic,assign,readonly) int partnerId;

/**
 * The name of the Category.
 *
 * @warning The following characters are not allowed: '<', '>', ','
 */
@property (nonatomic,copy) NSString* name;


/**
 * The display name of the Category "label" key.
 *
 * @warning The following characters are not allowed: '<', '>', ','
 */
- (NSString *)label;

/**
 * The full name of the Category.
 */
@property (nonatomic,copy,readonly) NSString* fullName;

/**
 * The full IDs of the Category.
 */
@property (nonatomic,copy,readonly) NSString* fullIds;

/**
 * Number of entries in this Category (including child categories).
 */
@property (nonatomic,assign,readonly) int entriesCount;

/**
 * Creation date as Unix timestamp (In seconds).
 */
@property (nonatomic,assign,readonly) int createdAt;

/**
 * Update date as Unix timestamp (In seconds).
 */
@property (nonatomic,assign,readonly) int updatedAt;

/**
 * Category description.
 */
@property (nonatomic,copy) NSString* categoryDescription;

/**
 * Category tags.
 */
@property (nonatomic,copy) NSString* tags;

/**
 * KalturaAppearInListType enum indicating if category will be returned for list action.
 */
@property (nonatomic,assign) int appearInList;

/**
 * KalturaPrivacyType enum indicating the privacy of the entries that assigned to this category.
 */
@property (nonatomic,assign) int privacy;

/**
 * KalturaInheritanceType enum indicating if Category members are inherited from parent category or set manualy.
 */
@property (nonatomic,assign) int inheritanceType;

/**
 * KalturaUserJoinPolicyType enum indicating who can ask to join this category.
 */
@property (nonatomic,assign,readonly) int userJoinPolicy;

/**
 * KalturaCategoryUserPermissionLevel enum indicating the default permissionLevel for new users.
 */
@property (nonatomic,assign) int defaultPermissionLevel;

/**
 * Category Owner (User id).
 */
@property (nonatomic,copy) NSString* owner;

/**
 * Number of entries that belong to this category directly.
 */
@property (nonatomic,assign,readonly) int directEntriesCount;

/**
 * Category external id, controlled and managed by the partner.
 */
@property (nonatomic,copy) NSString* referenceId;

/**
 * KalturaContributionPolicyType enum indicating who can assign entries to this category.
 */
@property (nonatomic,assign) int contributionPolicy;

/**
 * Number of active members for this category.
 */
@property (nonatomic,assign,readonly) int membersCount;

/**
 * Number of pending members for this category.
 */
@property (nonatomic,assign,readonly) int pendingMembersCount;

/**
 * Set privacy context for search entries that assigned to private and public categories. The entries will be private if the search context is set with those categories.
 */
@property (nonatomic,copy) NSString* privacyContext;

/**
 * Comma separated parents that define a privacyContext for search.
 */
@property (nonatomic,copy,readonly) NSString* privacyContexts;

/**
 * KalturaCategoryStatus enum indicating the category Status.
 */
@property (nonatomic,assign,readonly) int status;

/**
 * The category id that this category inherit its members and members permission (for contribution and join).
 */
@property (nonatomic,assign,readonly) int inheritedParentId;

/**
 * Can be used to store various partner related data as a numeric value.
 */
@property (nonatomic,assign) int partnerSortValue;

/**
 * Can be used to store various partner related data as a string.
 */
@property (nonatomic,copy) NSString* partnerData;

/**
 * KalturaCategoryOrderBy enum enabling client side applications to define how to sort the category child categories.
 */
@property (nonatomic,copy) NSString* defaultOrderBy;

/**
 * Number of direct children categories.
 */
@property (nonatomic,assign,readonly) int directSubCategoriesCount;

/**
 * KalturaNullableBoolean enum indicating the moderation to add entries to this category by users that are not of permission level Manager or Moderator.
 */
@property (nonatomic,assign) int moderation;

/**
 * Number of pending moderation entries.
 */
@property (nonatomic,assign,readonly) int pendingEntriesCount;

/**
 * Children categories.
 */
@property (nonatomic, strong) NSArray <PVPCategory*> *children;

/**
 * The entries in the category.
 */
@property (nonatomic, strong) NSArray <PVPEntry*> *entries;

/**
* The entries in the category.
*/
@property (nonatomic,strong,readonly,nullable) NSArray *accessPackages;

@property (nonatomic, assign) int nextEntriesPage;
@property (nonatomic, assign) BOOL hasMoreItems;
@property (nonatomic, assign) BOOL isLoading;

- (NSString *)sortByString;

- (BOOL)isSlider;
- (BOOL)isPosterAvailable;
- (NSString * _Nullable)posterUrlString;

- (BOOL)isListView;
- (BOOL)isTwoColEntriesGallery;
- (BOOL)isHorizontalCards;
- (BOOL)isVerticalCards;
- (BOOL)isPosterNetflix;

- (NSString *_Nullable)broadcasterName;
- (NSString *_Nullable)broadcastTime;
- (UIColor *_Nullable)colorPallet;

- (nullable NSArray<PVPPackage *> *)categoryPackages;

- (BOOL)hasPurchases;
- (BOOL)isPurchased;

@end
