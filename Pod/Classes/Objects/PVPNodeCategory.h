//
//  PVPNodeCategory.h
//  Pods
//
//  Created by  on 09/07/2018.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"
#import "PVPNode.h"

/**
 *  The PVPNodeCategory object represents a channel on the Bamboo platform.
 */
@interface PVPNodeCategory : PVPBaseObject<PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 * The node ID
 */
@property (strong, nonatomic) NSString *id;

/**
 * The view type of the node
 */
@property (strong, nonatomic) NSString *type;

/**
 * The node category node content
 */
@property (copy, nonatomic) NSArray<PVPNode *> *content;
@property (strong, nonatomic) NSString *overrideAppLogo;



@property (copy, nonatomic) NSArray<PVPNodeCategory *> *items;

/**
 * The instance ID of the channel.
 */
@property (strong, nonatomic) NSString *title;

/**
 * The instance ID of the channel.
 */
@property (strong, nonatomic) NSString *link;

@property (strong, nonatomic) NSString *link_title;

@property (strong, nonatomic) NSString *icon;
@property (nonatomic) bool categoryReverseColors;
@property (strong, nonatomic) NSString *categoryNameIcon;
@property ( nonatomic ) float categoryLayoutHeight;
@property (strong, nonatomic) NSString *category_name;

@end
