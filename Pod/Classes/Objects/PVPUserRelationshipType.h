//
//  PVPUserRelationshipType.h
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

/**
 *  The user's "Relationship type" info - This object is under the PVPUserSearchInfo object which includes the filter settings set by the user for the required type of relationship.
 *
 *  @see PVPUserSearchInfo
 */
@interface PVPUserRelationshipType : PVPBaseObject

/**
 *  @name Public Methods
 */

/**
*  A boolean attribute which indicates if the user is interested in a realtionship.
*/
@property (nonatomic, assign) BOOL relationship;

/**
 *  A boolean attribute which indicates if the user is interested in marriage.
 */
@property (nonatomic, assign) BOOL marriage;

/**
 *  A boolean attribute which indicates if the user is interested in a relation.
 */
@property (nonatomic, assign) BOOL relation;

/**
 *  A boolean attribute which indicates if the user is interested in networking.
 */
@property (nonatomic, assign) BOOL networking;

/**
 *  A boolean attribute which indicates if the user is interested in dating.
 */
@property (nonatomic, assign) BOOL dating;

/**
 *  A boolean attribute which indicates if the user is interested in friendship.
 */
@property (nonatomic, assign) BOOL friendship;

/**
 *  A boolean attribute which indicates if the user is interested in a casual relationship.
 */
@property (nonatomic, assign) BOOL casual;

@end
