//
//  PVPUser.h
//  rest-clieny
//
//  Created by Oren Kosto,  on 6/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KalturaClient.h"
#import "PVPUserInfo.h"
#import "PVPMongoHelper.h"
#import "PVPTimeHelper.h"

/**
 *  The PVPUser object represents a user on the Bamboo platform. This object inherits most properties from the KalturaUser object.
 *
 *  @warning Notice that this object inherits from KalturaUser and *NOT* from PVPBaseObject, therefore we needed to implement the addDescriptorsToObjectManager: method separately here.
 */
@interface PVPUser : KalturaUser <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

//@property (nonatomic, copy) NSString *password;

/**
 *  The user's role for this instance of the platform (PVP_ROLE_VIEWER, PVP_ROLE_CONTRIBUTOR, or PVP_ROLE_ADMIN).
 */
@property (nonatomic, copy) NSString *pvpRole;

/**
 *  The user's living area.
 */
@property (nonatomic, copy) NSString *area;

@property (nonatomic, copy) NSString *phone;

/**
 *  The user's additional info object, containing additional custom user metadata.
 *
 *  @see PVPUserInfo
 */
@property (nonatomic, strong) PVPUserInfo *info;

/**
 *  A boolean field which states if the user is currently online.
 */
@property (nonatomic, assign) BOOL isOnline;

/**
 *  Indication of whether the user is currently live or not.
 */
@property (nonatomic, assign) BOOL isLive;

/**
 *  @name Public Methods
 */

/**
 *  Extracts and returns the MongoId of the user object in string form.
 *
 *  @return The MongoId of the user object in string form.
 */
-(NSString *) mongoId;

-(NSString *)userFullName;

-(BOOL)bypassCountryRestriction;
-(BOOL)bypassSchedulingRestriction;
-(BOOL)bypassPurchaseRestriction;
/**
 *  Checks whether today is the user's birthday.
 *
 *  @param timestamp The user's date of birth in Unix timestamp.
 *
 *  @return Returns YES if today is the user's birthday, otherwise returns NO;
 */
-(BOOL) isBirthday:(int) timestamp;
-(NSString *)birthDisplayDate;
-(NSString *)genderDisplayName;

-(BOOL) isSpecialRightsUser;

- (NSDictionary *)jsonObject;

@end
