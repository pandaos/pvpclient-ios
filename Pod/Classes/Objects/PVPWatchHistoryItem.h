//
//  PVPWatchHistoryItem.h
//  Pods
//
//  Created by Oren Kosto,  on 2/9/17.
//
//

#import "PVPBaseObject.h"
#import "PVPEntry.h"

/**
 * The PVPWatchHistoryItem object represents a watch history item on the Bamobo Platform.
 * While a user watches an entry, the player sends events indicating that it is either being watched, or has finished watching.
 * This way, the Bamboo platform is able to show a user's watch history (currently watching AND previously watched, separately if needed).
 */
@interface PVPWatchHistoryItem : PVPBaseObject <PVPBaseObjectProtocol>

/**
 * The entry.
 */
@property (nonatomic, copy, setter=setEntry:) PVPEntry *entry;

/**
 * The current time being watched.
 */
@property (nonatomic, assign, setter=setTime:) int time;

/**
 * The state (watched or watching).
 *
 * @see PVPWatchHistoryItemState
 */
@property (nonatomic, assign, readonly) PVPWatchHistoryItemState state;

@end
