//
//  PVPLike.h
//  Pods
//
//  Created by Jacob Barr on 17/04/2016.
//
//

#import "PVPBaseObject.h"

#define OBJECT_TYPE_ENTRY 1
#define OBJECT_TYPE_CATEGORY 2
#define OBJECT_TYPE_USER 3

/**
 *  The PVPLike object represents a "like" action between a user and an object in the Bamboo platform.
 */
@interface PVPLike : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  The ID of the object, regardless of what object type this is.
 */
@property(nonatomic, strong) NSString *objectId;

/**
 *  The enum of the object type (OBJECT_TYPE_ENTRY, OBJECT_TYPE_USER, etc...).
 */
@property(nonatomic, assign) int type;

/**
 *  initialize a PVPLike object
 *
 *  @param objectId The id (mongoId) of the object that receives the like
 *  @param type     The object's type enum
 *
 *  @return PVPLike object
 */
-(PVPLike *)initWithObjectId:(NSString *)objectId withObjectType:(int)type;

@end
