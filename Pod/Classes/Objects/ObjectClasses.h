//
//  ObjectsClass.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/29/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVPUser.h"
#import "PVPLogin.h"
#import "PVPEntry.h"
#import "PVPCategory.h"
#import "PVPLiveEntry.h"
#import "PVPChannel.h"
#import "PVPLiveEntryPublish.h"
#import "PVPUploadToken.h"
#import "PVPConfig.h"
#import "PVPMeta.h"
#import "PVPSubscription.h"
#import "PVPPushNotification.h"
#import "PVPOpenTok.h"
#import "PVPResult.h"
#import "PVPConversation.h"
#import "PVPMessage.h"
#import "PVPLike.h"
#import "PVPEvent.h"
#import "PVPFlavorAsset.h"
#import "PVPCuePoint.h"
#import "PVPPlaylist.h"
#import "PVPChannelSchedule.h"
#import "PVPWatchHistoryItem.h"
#import "PVPPackage.h"
#import "PVPEncryptedData.h"
#import "PVPNode.h"
#import "PVPNodeCategory.h"

/**
 *  The ObjectClasses class provides a centralized interface for all of the PVP object classes.
 *  This class is only used to configure the API manager when the app starts.
 *  
 *  @warning Importing this class is likely to cause circular imports, use carefully or not at all.
 */
@interface ObjectClasses : NSObject

/**
 *  Gathers and returns the names of all the object class.
 *
 *  @return The names of all of the PVP object classes, in an array of strings.
 */
+(NSArray *) getObjectClasses DEPRECATED_ATTRIBUTE;

@end
