//
//  PVPUserRequiredArea.m
//  Pods
//
//  Created by Jacob Barr on 07/04/2016.
//
//

#import "PVPUserRequiredArea.h"

@implementation PVPUserRequiredArea

+(NSDictionary *) JSONKeyPathsByPropertyKey
{

    return @{
            @"HaDarom": @"HaDarom",
            @"HaMerkaz": @"HaMerkaz",
            @"HaZafon": @"HaZafon",
            @"Haifa": @"Haifa",
            @"TelAviv": @"Tel Aviv",
            @"Jerusalem": @"Jerusalem",
            @"HaSharon": @"HaSharon",
            @"YehudaShomron": @"Yehuda Shomron"
    };
}

@end
