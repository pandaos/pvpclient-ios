//
//  PVPNode.h
//  Pods
//
//  Created by  on 09/07/2018.
//
//

#import "PVPBaseObject.h"
#import "PVPEntry.h"

/**
 *  The PVPNode object represents a channel on the Bamboo platform.
 */
@interface PVPNode : PVPBaseObject <PVPBaseObjectProtocol>

@property (strong, nonatomic) NSString *imageUrl;
@property (copy, nonatomic) PVPEntry *entry;
//@property (copy, nonatomic) NSMutableArray<PVPEntry *> *entry;
@property (strong, nonatomic) NSString *viewType;
@property (strong, nonatomic) NSMutableString *marqueeFullString;
@property (strong, nonatomic) NSString *instanceId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *primaryTitle;
@property (strong, nonatomic) NSString *secondaryTitle;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *videoId;
@property (strong, nonatomic) NSString *nodeTag;
@property (strong, nonatomic) NSString *authorPageUrl;
@property (strong, nonatomic) NSString *authorImageUrl;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *imageBy;
@property (strong, nonatomic) NSString *nid;
@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *created;
@property (strong, nonatomic) NSString *link;
@property (strong, nonatomic) NSString *link_title;
@property (strong, nonatomic) NSString *authorTitle;
@property (strong, nonatomic) NSArray *mainTag;
@property (strong, nonatomic) NSString *nodeTagCategory;
@property (strong, nonatomic) NSArray *tags;
@property (strong, nonatomic) NSString *overrideAppLogo;
@property (strong, nonatomic) NSString *authorDescription;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *eventLink;
@property (strong, nonatomic) NSString *eventDate;
@property (strong, nonatomic) NSString *eventDateText;
@property (strong, nonatomic) NSString *eventLocation;
@property (strong, nonatomic) NSString *registrationText;
@property (strong, nonatomic) NSString *promotionText;
@property (strong, nonatomic) NSString *promotionTextType;
@property (strong, nonatomic) NSString *icon;
@property (nonatomic) bool categoryReverseColors;
@property (strong, nonatomic) NSString *categoryNameIcon;
@property ( nonatomic ) float categoryLayoutHeight;
@property (strong, nonatomic) NSString *category_name;

@end
