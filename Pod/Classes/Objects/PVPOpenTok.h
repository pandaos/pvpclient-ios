//
//  PVPOpenTok.h
//  PVPClient
//
//  Created by Oren Kosto,  on 11/1/15.
//  Copyright © 2015 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

/**
 *  The PVPOpenTok object represents an OpenTok session. We use this object to initiate and join OpenTok calls.
 */
@interface PVPOpenTok : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
*  The OpenTok API key we wish to use for this session.
*/
@property (nonatomic, strong) NSString *apiKey;

/**
 *  The ID of the session we generated, or the session we wish to join.s
 */
@property (nonatomic, strong) NSString *sessionId;

/**
 *  The generated session token. This token is generated per-user, per-session. We use this token to identify ourselves with the OpenTok system.
 */
@property (nonatomic, strong) NSString *token;

@end
