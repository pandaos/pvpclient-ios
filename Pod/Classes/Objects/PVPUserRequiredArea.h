//
//  PVPUserRequiredArea.h
//  Pods
//
//  Created by Jacob Barr on 07/04/2016.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

/**
 *  The user's "Required Area" info - This object is under the PVPUserSearchInfo object which includes the filter settings set by the user for the areas required by the user.
 *
 *  @see PVPUserSearchInfo
 */

@interface PVPUserRequiredArea : PVPBaseObject

/**
 *  @name Public Methods
 */

/**
 *  A boolean attribute which indicates if the user is interested in users from the North.
 */
@property (nonatomic, assign) BOOL HaZafon;

/**
 *  A boolean attribute which indicates if the user is interested in users from the Center.
 */
@property (nonatomic, assign) BOOL HaMerkaz;

/**
 *  A boolean attribute which indicates if the user is interested in users from the South.
 */
@property (nonatomic, assign) BOOL HaDarom;

/**
 *  A boolean attribute which indicates if the user is interested in users from Ha Sharon.
 */
@property (nonatomic, assign) BOOL HaSharon;

/**
 *  A boolean attribute which indicates if the user is interested in users from Jerusalem.
 */
@property (nonatomic, assign) BOOL Jerusalem;

/**
 *  A boolean attribute which indicates if the user is interested in users from Haifa.
 */
@property (nonatomic, assign) BOOL Haifa;

/**
 *  A boolean attribute which indicates if the user is interested in users from Tel Aviv.
 */
@property (nonatomic, assign) BOOL TelAviv;

/**
 *  A boolean attribute which indicates if the user is interested in users from Yehuda & Shomron.
 */
@property (nonatomic, assign) BOOL YehudaShomron;

@end
