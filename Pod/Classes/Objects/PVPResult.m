//
//  PVPResult.m
//  PVPClient
//
//  Created by Oren Kosto,  on 3/23/15.
//  Copyright (c) 2015 Panda-OS. All rights reserved.
//

#import "PVPResult.h"

@implementation PVPResult

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"result": @"result",
             @"id": @"id"};
}

+ (NSValueTransformer *)resultJSONTransformer {
    return [self JSONTransformerForString];
}

@end
