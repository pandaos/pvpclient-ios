//
//  PVPCategory.m
//  Pods
//
//  Created by Roni Cohen on 07/10/2016.
//
//

#import "PVPCategory.h"
#import "PVPLocalizationHelper.h"

@implementation PVPCategory

-(id)init
{
    self = [super init];
    if (self) {
        self.nextEntriesPage = 1;
    }
    return self;
}


- (void)setHasMoreItems:(BOOL)hasMoreItems {
    _hasMoreItems = hasMoreItems;
    
    if (hasMoreItems) {
        _nextEntriesPage += 1;
    }
}


+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{
             @"id": @"id",
             @"parentId": @"parentId",
             @"depth": @"depth",
             @"partnerId": @"partnerId",
             @"name": @"name",
             @"fullName": @"fullName",
             @"fullIds": @"fullIds",
             @"entriesCount": @"entriesCount",
             @"createdAt": @"createdAt",
             @"updatedAt": @"updatedAt",
             @"categoryDescription": @"description",
             @"tags": @"tags",
             @"privacy": @"privacy",
             @"referenceId": @"referenceId",
             @"status": @"status",
             @"info": @"info",
             @"children": @"children",
             @"entries": @"entries",
             @"partnerSortValue": @"partnerSortValue"
             };
}

+ (NSValueTransformer *)infoJSONTransformer {
    return [PVPJSONAdapter transformerForModelPropertiesOfClass:NSDictionary.class];
}

+ (NSValueTransformer *)entriesJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPEntry.class];
}

+ (NSValueTransformer *)childrenJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPCategory.class];
}

- (nullable NSArray *)accessPackages {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]]) {
        NSArray *packs = [self.info[@"packages"] isEqual:[NSNull null]] ? nil : self.info[@"packages"];
        
        return packs;
    }
    
    return nil;
}

- (BOOL)isSlider {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"slider"]);
}

- (BOOL)isHorizontalCards {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"horizontal_cards"]);
}

- (BOOL)isVerticalCards {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"vertical_cards"]);
}

- (BOOL)isPosterNetflix {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"poster_carousel"]);
}

- (BOOL)isListView {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"video_list"]);
}

- (BOOL)isTwoColEntriesGallery {
    return (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"mobileViewType"] != nil && [self.info[@"mobileViewType"] isEqualToString:@"two_columns"]);
}

- (BOOL)isPosterAvailable {
    if ([self.children count] > 0) {
        return [(PVPCategory *)[self.children firstObject] posterUrlString] != nil;
    } else if ([self.entries count] > 0) {
        return [[self.entries firstObject] posterUrlString] != nil;
    }
    
    return NO;
}

- (NSString *)name {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *key = [NSString stringWithFormat:@"%@_name", language];
    
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[key] != nil && ![self.info[key] isEqual:[NSNull null]]) {
        return self.info[key];
    }
    
    return _name;
}

- (NSString *)label {
    NSString *language = [PVPLocalizationHelper getLanguage];
    NSString *key = [NSString stringWithFormat:@"%@_name", language];
    
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[key] != nil && ![self.info[key] isEqual:[NSNull null]]) {
        return self.info[key];
    } else if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"label"] != nil && ![self.info[@"label"] isEqual:[NSNull null]]) {
        return self.info[@"label"];
    }
    
    return [self name];
}

- (NSString *)sortByString {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"sortBy"] != nil && ![self.info[@"sortBy"] isEqual:[NSNull null]]) {
        return self.info[@"sortBy"];
    }
    
    return nil;
}

- (NSString *)posterUrlString {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"poster"] != nil && ![self.info[@"poster"] isEqual:[NSNull null]]) {
        return self.info[@"poster"];
    }
    
    return nil;
}

- (NSString *)broadcasterName {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"broadcaster"] != nil && ![self.info[@"broadcaster"] isEqual:[NSNull null]] && [self.info[@"broadcaster"] isKindOfClass:[NSArray class]]) {
        return [self.info[@"broadcaster"] firstObject];
    }
    
    return nil;
}

- (NSString *)broadcastTime {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"broadcastTime"] != nil && ![self.info[@"broadcastTime"] isEqual:[NSNull null]]) {
        return self.info[@"broadcastTime"];
    }
    
    return nil;
}

- (UIColor *)colorPallet {
    if (self.info != nil && [self.info isKindOfClass:[NSDictionary class]] && self.info[@"colorPallet"] != nil && ![self.info[@"colorPallet"] isEqual:[NSNull null]] && [self.info[@"colorPallet"] length] > 0) {
        NSString *hexString = self.info[@"colorPallet"];
        return [UIColor colorWithHexString:hexString];
    }
    
    return nil;
}

- (NSArray<PVPPackage *> *)categoryPackages {
    if ([[self accessPackages] count] > 0) {
        NSMutableArray *packs = [NSMutableArray new];
        
        for (NSString *packID in [self accessPackages]) {
            for (PVPPackage *pack in [[IAPHelper sharedInstance] appPackages]) {
                if ([[pack mongoId] isEqualToString:packID]) {
                    [packs addObject:pack];

                    break;
                }
            }
        }
        
        return packs;
    }
    
    return nil;
}

- (BOOL)hasPurchases {
    return ([[self accessPackages] count] > 0);
}

- (BOOL)isPurchased {
    if ([[self accessPackages] count] > 0) {
        for (NSString *packID in [self accessPackages]) {
            NSArray *purchases = [[IAPHelper sharedInstance].purchasedPackages copy];
            if ([purchases containsObject:packID]) {
                return YES;
            }
        }
    }
    
    return NO;
}

@end
