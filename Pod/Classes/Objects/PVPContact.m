//
//  PVPContact.m
//  Pods
//
//  Created by Oren Kosto,  on 5/16/16.
//
//

#import "PVPContact.h"

@implementation PVPContact

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"fullName":@"fullName",
             @"email":@"email",
             @"phone":@"phone",
             @"profilePictureUrl":@"profilePictureUrl"};
}

@end
