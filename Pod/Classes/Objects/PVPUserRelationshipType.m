//
//  PVPUserRelationshipType.m
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import "PVPUserRelationshipType.h"

@implementation PVPUserRelationshipType

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"relationship": @"relationship",
             @"marriage": @"marriage",
             @"relation": @"relation",
             @"networking": @"networking",
             @"dating": @"dating",
             @"friendship": @"friendship",
             @"casual": @"casual"
             };
}

@end
