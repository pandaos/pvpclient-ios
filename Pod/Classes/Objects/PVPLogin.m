//
//  PVPLogin.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/30/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPLogin.h"

@implementation PVPLogin

-(PVPLogin *)init
{
    self = [super init];
    self.id = @"";
    self.password = @"";
    self.accountId = @"";
    self.token = @"";
    self.oAuthToken = @"";
    self.oAuthTokenSecret = @"";
    
    return self;
}
-(PVPLogin *)initWithUserId:(NSString *)userId withPassword:(NSString *)password
{
    self = [self init];
    self.id = userId;
    self.password = password;
    return self;
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"id": @"id",
             @"password": @"password",
             @"accountId": @"accountId",
             @"token": @"token",
             @"facebookToken": @"facebookToken",
             @"oAuthToken": @"oAuthToken",
             @"oAuthTokenSecret": @"oAuthTokenSecret"};
}

@end
