//
//  PVPInAppSDKConfig.h
//  PVPClient
//
//  Created by Prajeet Shrestha on 30/11/2022.
//

#import <Foundation/Foundation.h>


@interface PVPInAppSDKConfig : NSObject

@property (nonatomic, strong) NSString *publisherId;
@property (nonatomic, strong) NSString *tagId;
@property (nonatomic, strong) NSString *testPublisherId;
@property (nonatomic, strong) NSString *testTagId;

@end

