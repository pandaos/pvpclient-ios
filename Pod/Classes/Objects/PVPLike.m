//
//  PVPLike.m
//  Pods
//
//  Created by Jacob Barr on 17/04/2016.
//
//

#import "PVPLike.h"

@implementation PVPLike

-(PVPLike *)initWithObjectId:(NSString *)objectId withObjectType:(int)type
{
    self = [super init];
    self.objectId = objectId;
    self.type = type;
    return self;
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"objectId": @"objectId",
             @"type": @"type"};
}

@end
