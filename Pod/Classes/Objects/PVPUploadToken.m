//
//  PVPUploadToken.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPUploadToken.h"

@implementation PVPUploadToken

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"id": @"id",
             @"uploadTokenId": @"uploadTokenId",
             @"partnerId": @"partnerId",
             @"userId": @"userId",
             @"status": @"status",
             @"fileName": @"fileName",
             @"fileSize": @"fileSize",
             @"uploadedFileSize": @"uploadedFileSize",
             @"createdAt": @"createdAt",
             @"updatedAt": @"updatedAt"};
}

@end
