 ////
//  PVPUserInfo.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/23/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPUserInfo.h"

@implementation PVPUserInfo

-(id)init
{
    self = [super init];
    if (self) {
//        self.searchInfo = [[PVPUserSearchInfo alloc] init];
//        self.personalInfo = [[PVPUserPersonalInfo alloc] init];
        self.contacts = [NSArray array];
    }
    return self;
}

//+ (NSValueTransformer *)personalInfoJSONTransformer {
//    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserPersonalInfo.class];
//}
//
//+ (NSValueTransformer *)searchInfoJSONTransformer {
//    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserSearchInfo.class];
//}

+ (NSValueTransformer *)contactsJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPContact.class];
}

+ (NSValueTransformer *)deviceTokensJSONTransformer {
    return [MTLValueTransformer transformerUsingReversibleBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if ([value isKindOfClass:[NSString class]]) {
            NSString *itemString = [NSString stringWithFormat:@"%@", value];
            NSDictionary *dict = [NSDictionary dictionaryFromJSONString:itemString];
            value = [NSMutableDictionary dictionaryWithDictionary:dict];
        }
        if (!value) {
            value = @[];
        }
        return [value isKindOfClass:[NSArray class]] ? value : @[value];
    }];
}

+ (NSValueTransformer *)profileViewsJSONTransformer {
    return [PVPBaseObject JSONTransformerForInt];
}

//+ (NSValueTransformer *)bypassCountryRestrictionJSONTransformer {
//    return [PVPBaseObject JSONTransformerForBool];
//}

-(void) updateCurrentDeviceToken
{
    NSMutableArray *allTokens = [NSMutableArray arrayWithArray:self.deviceTokens];
    NSString *deviceUUID = [PVPConfigHelper sharedInstance].uniqueDeviceIdentifier;
    NSMutableIndexSet *tokensToDelete = [NSMutableIndexSet indexSet];
    NSUInteger index = 0;
    
    for (__strong NSMutableDictionary *item in allTokens) {
        if ([item isKindOfClass:[NSString class]]) {
            NSString *itemString = [NSString stringWithFormat:@"%@", item];
            NSDictionary *dict = [NSDictionary dictionaryFromJSONString:itemString];
            item = [NSMutableDictionary dictionaryWithDictionary:dict];
        }
        if (item &&
            (([item valueForKey:@"udid"] && [[item valueForKey:@"udid"] isEqualToString:deviceUUID]) ||
            [item valueForKey:deviceUUID])) {
            [tokensToDelete addIndex:index];
        }
        
        index++;
    }
    [allTokens removeObjectsAtIndexes:tokensToDelete];
    
    NSDictionary *newToken = @{@"udid": deviceUUID,
                               @"platform": @"ios"};
    [allTokens addObject:newToken];
    
    self.deviceTokens = [allTokens copy];
}

- (NSString *)profilePictureUrlWithSize:(CGSize)size {
    NSString *updatedProfilePictureUrl = self.profilePictureUrl;
    int width = size.width > 0 ? size.width : 400;
    int height = size.height > 0 ? size.height : 400;
    
    if ([self.profilePictureUrl containsString:@"graph.facebook"]) {
        updatedProfilePictureUrl = [self.profilePictureUrl stringByAppendingString:[NSString stringWithFormat:@"&width=%d&height=%d", width, height]];
    } else if ([self.profilePictureUrl containsString:[NSString stringWithFormat:@"/thumbnail/"]] || [self.profilePictureUrl containsString:[NSString stringWithFormat:@"/%@/", [[PVPConfigHelper sharedInstance] partnerId]]]) {
        updatedProfilePictureUrl = [self.profilePictureUrl stringByAppendingString:[NSString stringWithFormat:@"/width/%d/height/%d", width, height]];
    }
    return updatedProfilePictureUrl;
}

-(NSArray *) photoAlbumAsArray
{
    NSMutableArray *allPhotos = [NSMutableArray arrayWithArray:self.photoAlbum];
    
    for (__strong NSMutableDictionary *item in allPhotos) {
        if ([item isKindOfClass:[NSString class]]) {
            NSString *itemString = [NSString stringWithFormat:@"%@", item];
            NSDictionary *dict = [NSDictionary dictionaryFromJSONString:itemString];
            item = [NSMutableDictionary dictionaryWithDictionary:dict];
        }
    }
    
    self.photoAlbum = [allPhotos copy];
    
    return allPhotos;
}

-(void) addPhotoToAlbum:(NSDictionary *)photoObject
{
    NSMutableArray *allPhotos = [NSMutableArray arrayWithArray:[self photoAlbumAsArray]];
    [allPhotos addObject:photoObject];
    self.photoAlbum = [allPhotos copy];
}

-(void) removePhotoFromAlbumById:(NSString *)photoId {
    [self removePhotoFromAlbumByKey:@"id" andValue:photoId];
}

-(void) removePhotoFromAlbumByUrl:(NSString *)photoUrl {
    [self removePhotoFromAlbumByKey:@"url" andValue:photoUrl];
}

-(void) removePhotoFromAlbumByKey:(NSString *)key andValue:(NSString *)value {
    if (!key || !key.length || !value || !value.length) {
        return;
    }
    
    NSMutableIndexSet *photosToDelete = [NSMutableIndexSet indexSet];
    NSUInteger index = 0;
    NSMutableArray *allPhotos = [NSMutableArray arrayWithArray:[self photoAlbumAsArray]];
    for (__strong NSMutableDictionary *item in allPhotos) {
        if ([item isKindOfClass:[NSString class]]) {
            NSString *itemString = [NSString stringWithFormat:@"%@", item];
            NSDictionary *dict = [NSDictionary dictionaryFromJSONString:itemString];
            item = [NSMutableDictionary dictionaryWithDictionary:dict];
        }
        if (item &&
            [item valueForKey:key] && [[item valueForKey:key] isEqualToString:value]) {
                [photosToDelete addIndex:index];
            }
        
        index++;
    }
    
    [allPhotos removeObjectsAtIndexes:photosToDelete];
    self.photoAlbum = [allPhotos copy];
}

@end
