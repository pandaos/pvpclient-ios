//
//  PVPUserInterestedInGender.m
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import "PVPUserInterestedInGender.h"

@implementation PVPUserInterestedInGender

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"male": @"male",
             @"female": @"female"
             };
}

@end
