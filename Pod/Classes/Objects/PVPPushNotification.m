//
//  PVPPushNotification.m
//  PVPClient
//
//  Created by Oren Kosto,  on 11/2/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPPushNotification.h"

#pragma mark the content section of a push notification
@implementation PVPPushNotificationContent

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{@"en": @"en"};
}

//-(RKObjectManager *)addDescriptorsToObjectManager:(RKObjectManager *)objectManager
//{
//    return objectManager;
//}

@end

#pragma mark push notification object
@implementation PVPPushNotification

-(NSDictionary *) mappingDictionary
{
    return @{@"data": @"data",
             @"content": @"content"};
}

+ (NSValueTransformer *)contentJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPPushNotificationContent.class];
}

@end

#pragma mark object to send that contains a push notification
@implementation PVPPushNotificationRequestObject

-(id)init
{
    self = [super init];
    if (self) {
        self.notifications = [[PVPPushNotification alloc] init];
        self.notifications.content = [[PVPPushNotificationContent alloc] init];
        self.notifications.content.en = @""; //set empty message

    }
    return self;
}

-(id)initWithMessage:(NSString *)message
{
    self = [self init];
    if (self) {
        self.notifications.content.en = message; //set message
        self.notifications.data = @"";
    }
    return self;
}

-(id)initWithMessage:(NSString *)message withData:(NSString *)data
{
    self = [self initWithMessage:message];
    if (self) {
        self.notifications.data = data; //set data
    }
    return self;
}

-(id)initWithMessage:(NSString *)message withDataDict:(NSDictionary *)dataDict
{
    self = [self initWithMessage:message];
    if (self) {
        self.notifications.data = [NSString JSONStringFromDictionary:dataDict]; //set data
    }
    return self;
}

-(NSDictionary *) mappingDictionary
{
    return @{@"data": @"data",
             @"notifications": @"notifications"};
}

+ (NSValueTransformer *)notificationsJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPPushNotification.class];
}

@end
