//
//  PVPSubscribe.h
//  PVPClient
//
//  Created by Oren Kosto,  on 9/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"
#import "IAPHelper.h"

/**
 *  The PVPSubscription object represents a subscription or purchase made by the user, in the format in which it will be evntually saved in the PVP database.
 *  When sent to the PVP server, this object can also contain 2 additional optional fields, which are sent with the API request as URL params:
 *
 *      - info - A dictionary containing any additional info needed for this purchase (in the case of a Apple IAP, we will need to include the purchase receipt string in a string named "receipt").
 *      - price - The purchase object's price (the amount of money that was actually paid in this transaction), in the device's local currency, in string format, with the currency symbol (i.e. $3.99). The PVP server will convert this value to the value in USD before saving this object to the database.
 *
 */
@interface PVPSubscription : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  The ID of the object, regardless of what object type this is.
 */
@property(nonatomic, strong) NSString *objectId;

/**
 *  The enum of the object type (OBJECT_TYPE_ENTRY, OBJECT_TYPE_USER, etc...).
 */
@property(nonatomic, assign) int type;

/**
 *  The enum of the service used to make the purchase. This field is initialized as PURCHASE_SERVICE_APPLE, and should remain in this value.
 */
@property(nonatomic, assign) int service;

@property(nonatomic, assign) int status;

/**
 *  Initializes an instance of this object with a given object ID and type.
 *
 *  @param objectId The ID of the object, regardless of what object type this is.
 *  @param type     The enum of the object type (OBJECT_TYPE_ENTRY, OBJECT_TYPE_USER, etc...).
 *
 *  @return A newly created instance of this object, set up with the given values.
 */
-(PVPSubscription *)initWithObjectId:(NSString *)objectId withObjectType:(int)type;

/**
 *  Processes the given purchase objects and saves them in the app for later use.
 *  This method will be called from the user model as part of the user model's getPurchasesForUser: method callback. No reason for ever calling this method manually.
 *
 *  @param purchases The purchase objects, as received by the user model's getPurchasesForUser: method.
 *
 *  @see [PVPUserModel getMyPurchases]
 *  @see [PVPUserModel getPurchasesForUser:]
 */
+(void)processUserPurchases:(NSArray *)purchases;

/**
 *  Removes all of the purchase objects currently stored locally in the app.
 *  This method will be called from the user model as part of the login model's removeLoggedInUser method callback. No reason for ever calling this method manually.
 *
 *  @see [PVPLoginModel removeLoggedInUser]
 */
+(void)emptyUserPurchases;

@end
