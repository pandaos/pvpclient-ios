//
//  PVPLiveEntryPublish.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/29/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"
#import "PVPLiveEntry.h"

/**
 *  The PVPLiveEntryPublish object is used to publish or unpublish live entried on Wowza.
 */
@interface PVPLiveEntryPublish : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
 *  The entry we wish ot publish or unpublish.
 *
 *  @see PVPLiveEntry
 */
@property(nonatomic, retain) PVPLiveEntry *entry;

/**
 *  The result of the API request, as returned from the server.
 */
@property(nonatomic, retain) NSString *result;

/**
 *  @name Public Methods
 */

/**
 *  Initializes an instance of this object with a given live entry.
 *
 *  @param liveEntry The live entry we wish to use for this object.
 *
 *  @return An initialized instance of this object, with the given live entry object.
 */
-(id) initWithLiveEntry:(PVPLiveEntry *)liveEntry;

@end
