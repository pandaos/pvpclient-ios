//
//  PVPPushNotification.h
//  PVPClient
//
//  Created by Oren Kosto,  on 11/2/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

#pragma mark the content section of a push notification

/**
 *  The PVPPushNotificationContent object represents the content of a single push notification to be sent.
 */
@interface PVPPushNotificationContent : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
 *  The message we would like to send with the notification, in English.
 */
@property (nonatomic, copy) NSString *en;

@end

#pragma mark push notification object

/**
 *  The PVPPushNotification object represents a single push notification to be sent.
 */
@interface PVPPushNotification : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
 *  The message content of the push notification.
 *
 *  @see PVPPushNotificationContent
 */
@property (nonatomic, strong) PVPPushNotificationContent *content;

/**
 *  The custom data of the push notification. This string needs to be in JSON format.
 */
@property (nonatomic, strong) NSString *data;

@end

#pragma mark object to send that contains a push notification

/**
 *  The PVPPushNotificationRequestObject is the object we send to the server in order to send a push notification.
 */
@interface PVPPushNotificationRequestObject : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The content of the push notification.
 *
 *  @see PVPPushNotification
 */
@property (nonatomic, strong) PVPPushNotification *notifications;

/**
 *  @warning Unused.
 */
@property (nonatomic, copy) NSString *data;

/**
 *  @name Public Methods
 */

/**
 *  Initiates a new instance of this object with a pre-set notification message.
 *
 *  @param message The message we would like to send with the push notification (in English).
 *
 *  @return A new instance of this object, with the given message set into it.
 *
 * @see [PVPPushNotificationRequestObject initWithMessage:withData:]
 * @see [PVPPushNotificationRequestObject initWithMessage:withDataDict:]
 */
-(id)initWithMessage:(NSString *)message;

/**
 *  Initiates a new instance of this object with a pre-set notification message and custom data.
 *
 *  @param message The message we would like to send with the push notification (in English).
 *  @param data    The custom data we would like to send with the push notification. This string needs to be in JSON format.
 *
 *  @return A new instance of this object, with the given message and data set into it.
 *
 * @see [PVPPushNotificationRequestObject initWithMessage:withDataDict:]
 */
-(id)initWithMessage:(NSString *)message withData:(NSString *)data;

/**
 *  Initiates a new instance of this object with a pre-set notification message and custom data.
 *
 *  @param message  The message we would like to send with the push notification (in English).
 *  @param dataDict The custom data we would like to send with the push notification. This dictionary is automatically converted to a string in JSON format.
 *
 *  @return A new instance of this object, with the given message and data set into it.
 *
 * @see [PVPPushNotificationRequestObject initWithMessage:withData:]
 */
-(id)initWithMessage:(NSString *)message withDataDict:(NSDictionary *)dataDict;

@end
