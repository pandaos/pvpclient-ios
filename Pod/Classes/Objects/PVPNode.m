//
//  PVPNode.m
//  Pods
//
//  Created by  on 09/07/18.
//
//
#import "PVPEntry.h"
#import "PVPNode.h"

@implementation PVPNode

-(id)init
{
    self = [super init];
    if (self) {
        self.entry = [NSMutableDictionary dictionary];
//        self.entry = [NSMutableArray array];
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"uid": @"uid",
             @"nid": @"nid",
             @"created": @"created",
             @"type": @"type",
             @"viewType": @"viewType",
             @"author": @"author",
             @"authorImageUrl": @"authorImageUrl",
             @"authorPageUrl": @"authorPageUrl",
             @"authorTitle": @"authorTitle",
             @"nodeTag": @"nodeTag",
             @"mainTag": @"mainTag",
             @"text": @"text",
             @"title": @"title",
             @"primaryTitle": @"primaryTitle",
             @"authorDescription": @"authorDescription",
             @"secondaryTitle": @"secondaryTitle",
             @"imageUrl": @"imageUrl",
             @"imageBy": @"imageBy",
             @"url": @"url",
             @"link": @"link",
             @"link_title": @"link_title",
             @"nodeTagCategory": @"nodeTagCategory",
             @"tags": @"tags",
             @"overrideAppLogo": @"overrideAppLogo",
             @"videoId": @"videoId",
             @"eventLink": @"eventLink",
             @"eventDate": @"eventDate",
             @"eventDateText": @"eventDateText",
             @"eventLocation": @"eventLocation",
             @"registrationText": @"registrationText",
             @"promotionText": @"promotionText",
             @"promotionTextType": @"promotionTextType",
             @"icon": @"icon",
             @"entry": @"entry",
             @"categoryReverseColors": @"categoryReverseColors",
             @"categoryNameIcon": @"categoryNameIcon",
             @"category_name": @"category_name",
             @"categoryLayoutHeight": @"categoryLayoutHeight",
             @"marqueeFullString": @"marqueeFullString"
             };
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [self JSONTransformerForString];
}

+ (NSValueTransformer *)entryJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPEntry.class];
//    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPEntry.class];
}

@end
