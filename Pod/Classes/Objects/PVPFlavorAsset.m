//
//  PVPFlavorAsset.m
//  Pods
//
//  Created by Oren Kosto,  on 6/27/16.
//
//

#import "PVPFlavorAsset.h"

@implementation PVPFlavorAsset

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"id": @"id",
             @"flavorParamsId": @"flavorParamsId",
             @"width": @"width",
             @"height": @"height",
             @"bitrate": @"bitrate",
             @"frameRate": @"frameRate",
             @"isOriginal": @"isOriginal",
             @"isWeb": @"isWeb",
             @"containerFormat": @"containerFormat",
             @"videoCodecId": @"videoCodecId",
             @"status": @"status",
             @"entryId": @"entryId",
             @"partnerId": @"partnerId",
             @"version": @"version",
             @"size": @"size",
             @"tags": @"tags",
             @"fileExt": @"fileExt",
             @"createdAt": @"createdAt",
             @"updatedAt": @"updatedAt",
             @"deletedAt": @"deletedAt",
             @"description": @"description",
             @"partnerData": @"partnerData",
             @"partnerDescription": @"partnerDescription",
             @"actualSourceAssetParamsIds": @"actualSourceAssetParamsIds",
             @"url": @"url"};
}

@end
