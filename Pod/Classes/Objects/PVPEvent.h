//
//  PVPEvent.h
//  Pods
//
//  Created by Jacob Barr on 10/04/2016.
//
//

#import "PVPBaseObject.h"
#import "PVPUser.h"

/**
 *  The PVPEvent object represents an news feed event between two users in the Bamboo platform.
 */
@interface PVPEvent : PVPBaseObject<PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  A Unix timestamp which represents the time in which the event has been created
 */
@property (nonatomic, assign) int createdAt;

/**
 *  A Unix timestamp which represents the time in which the event has been updated
 */
@property (nonatomic, assign) int updatedAt;

/**
 *  The mongoId of the user who created the event 
 */
@property (nonatomic, copy) NSString *creator;

/**
 *  The creator of the event.
 *
 *  @warning This object gets only a small portion of the PVPUser fields.
 *
 *  @see PVPUser
 */
@property (nonatomic, strong) PVPUser *creatorExpand;

/**
 *  The mongoId of the user who received the event
 */
@property (nonatomic, copy) NSString *receiver;

/**
 *  The receiver of the event.
 *
 *  @warning This object gets only a small portion of the PVPUser fields.
 *
 *  @see PVPUser
 */
@property (nonatomic, strong) PVPUser *receiverExpand;

/**
 *  The type of the event
 */
@property (nonatomic, assign) int type;

/**
 *  The status of the event
 */
@property (nonatomic, assign) int status;

/**
 *  Initializes a PVPEvent object with a given receiver ID and type.
 *  Typically, the creator of the event will be the current logged in user.
 *
 *  @param receiverId - The receiver of the event
 *  @param type       - The type of the event
 *
 *  @return PVPEvent object.
 */
- (id)initWithReceiverId:(NSString *)receiverId WithType:(int)type;

/**
 *  Get the object's mongoId
 *
 *  @return MongoId string
 */
- (NSString *) mongoId;

@end
