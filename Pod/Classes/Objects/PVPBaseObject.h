//
//  PVPBaseObject.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <Overcoat/Overcoat.h>
#import "PVPJSONAdapter.h"
#import "AppUtils.h"
#import "NSDictionary+PVPClient.h"
#import "NSString+PVPClient.h"
#import "UILabel+PVPClient.h"
#import "UIColor+PVPClient.h"
#import "UIImage+PVPClient.h"
#import "PVPMongoHelper.h"

/**
 *  The base protocol for any PVPClient object.
 *  Every PVPClient object must implement the required methods in this protocol.
 *  Not implementing a method from this protocol might result in the app not working properly.
 */
@protocol PVPBaseObjectProtocol <NSObject>
@required
/**
 *  Generates and provides the object mapping.
 *
 *  @return The object mapping as a dictionary.
 */
//-(NSDictionary *) mappingDictionary;

@end

/**
 *  The PVP object is a mapped Objective-C object used by the PVP client and server.
 *  All objects and API routes must be mapped in advance in order for the PVP client to be able to handle them.
 *  The base PVP object class provides basic functionality that is a-must for any PVP object.
 */
@interface PVPBaseObject : MTLModel<MTLJSONSerializing>

/**
 *  @name Public Methods
 */

/**
 *  The original JSON dictionary that has been returned from the API.
 */
@property (nonatomic, strong) NSDictionary *allProperties;

/**
 *  @name Public Methods
 */

/**
 *  Extracts and returns the MongoId of the object in string form.
 *
 *  @return The MongoId of the object in string form.
 */
- (NSString *) mongoId;

/**
 *  Creates and returns a custom JSON transformer for int values.
 *
 *  @return A custom JSON transformer for int values.
 */
+ (NSValueTransformer *)JSONTransformerForInt;

/**
 *  Creates and returns a custom JSON transformer for boolean values.
 *
 *  @return A custom JSON transformer for boolean values.
 */
+ (NSValueTransformer *)JSONTransformerForBool;

/**
 *  Creates and returns a custom JSON transformer for string values.
 *
 *  @return A custom JSON transformer for string values.
 */
+ (NSValueTransformer *)JSONTransformerForString;

/**
 *  Creates and returns a custom JSON transformer for dictionary values.
 *
 *  @return A custom JSON transformer for dictionary values.
 */
+ (NSValueTransformer *)JSONTransformerForDictionary;

@end
