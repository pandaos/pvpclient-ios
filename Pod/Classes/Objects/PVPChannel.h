//
//  PVPChannel.h
//  Pods
//
//  Created by Oren Kosto,  on 9/13/16.
//
//

#import "PVPBaseObject.h"
#import "PVPLiveEntry.h"
#import "PVPChannelSchedule.h"

typedef NS_ENUM(NSInteger, PVPChannelType) {
    PVPChannelTypeBambooNative = 0,
    PVPChannelTypeYoutube = 1
};

/**
 *  The PVPChannel object represents a channel on the Bamboo platform.
 */
@interface PVPChannel : PVPBaseObject<PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 * The ID of the channel.
 */
@property (strong, nonatomic) id id;

/**
 * The type of the channel.
 */
@property (assign, nonatomic, readonly) int type;

/**
 * The instance ID of the channel.
 */
@property (strong, nonatomic) NSString *instanceId;

/**
 * The live entry ID of the channel.
 */
@property (strong, nonatomic) PVPLiveEntry *entry;

@property (nonatomic, copy) NSArray <PVPChannelSchedule *>*schedules;

/**
 * The external channel name of the channel.
 */
@property (strong, nonatomic) NSString *externalChannelName;

/**
 * The category IDs of the channel.
 */
@property (strong, nonatomic) NSArray *categoriesIds;

/**
 * The mosaic of the channel.
 */
@property (strong, nonatomic) NSString *mosaic;

/**
 * The custom metadata of the channel.
 */
@property (strong, nonatomic) NSDictionary *info;

/**
 * The Wowza app of the channel.
 */
@property (strong, nonatomic) NSString *wowzaApp;

/**
 * The HLS URL of the channel.
 */
@property (strong, nonatomic) NSString *hlsUrl;

/**
 * The status of the channel.
 */
@property (assign, nonatomic, readonly) int status;

/**
 * The name of the channel.
 */
@property (strong, nonatomic) NSString *name;

/**
 * The description of the channel.
 */
@property (strong, nonatomic) NSString *channelDescription;

/**
 * The creator of the channel.
 */
@property (strong, nonatomic) NSString *creator;

/**
 * The owner of the channel.
 */
@property (strong, nonatomic) NSString *owner;

/**
 * The player logo URL.
 */
@property (strong, nonatomic) NSString *playerLogoUrl;

/**
 * The create time of the channel.
 */
@property (assign, nonatomic, readonly) int createdAt;

/**
 * The update time of the channel.
 */
@property (assign, nonatomic, readonly) int updatedAt;

-(NSArray<PVPChannelScheduleItem *> *) getScheduleItems;
-(NSArray<PVPChannelScheduleItem *> *) getScheduleItems:(BOOL)includeAds;

/**
*Filtered EPG Channel Schedule items
*/
@property (nonatomic, copy) NSArray<PVPChannelScheduleItem *> *epgSchedule;

- (PVPChannelScheduleItem *)currentLiveScheduleItem;
- (PVPChannelScheduleItem *)nextScheduleItemAfter:(PVPChannelScheduleItem *)item;
- (CGFloat)currentTimeOffsetForScheduleItem:(PVPChannelScheduleItem *)item;
- (BOOL)isYoutubeChannel;

/**
 * The display epg label of the channel.
 */
- (NSString *)epgLabel;
- (NSString *)eventIDString;

@end
