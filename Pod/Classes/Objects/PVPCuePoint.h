//
// Created by Oren Kosto,  on 8/16/16.
//

#import <Foundation/Foundation.h>
#import "KalturaCuePointClientPlugin.h"
#import "PVPBaseObject.h"

@interface PVPCuePoint : KalturaCuePoint <PVPBaseObjectProtocol>

@property (nonatomic,copy) NSString* text;
@property (nonatomic,copy) NSString* parentId;
@property (nonatomic, assign) int endTime;
@property (nonatomic, assign) int duration;
@property (nonatomic, assign) int depth;
@property (nonatomic, assign) int childrenCount;
@property (nonatomic, assign) int directChildrenCount;

@end
