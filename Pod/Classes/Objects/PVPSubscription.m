//
//  PVPSubscribe.m
//  PVPClient
//
//  Created by Oren Kosto,  on 9/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#define OBJECT_TYPE_ENTRY 1
#define OBJECT_TYPE_CATEGORY 2
#define OBJECT_TYPE_USER 3
#define OBJECT_TYPE_PACKAGE 8

#import "PVPSubscription.h"

@implementation PVPSubscription

-(PVPSubscription *)initWithObjectId:(NSString *)objectId withObjectType:(int)type
{
    self = [super init];
    self.objectId = objectId ? objectId : @"";
    self.service = PURCHASE_SERVICE_APPLE;
    self.type = type;
    return self;
}

+(void)processUserPurchases:(NSArray *)purchases
{
    [[IAPHelper sharedInstance].purchasedEntries removeAllObjects];
    [[IAPHelper sharedInstance].purchasedUsers removeAllObjects];
    [[IAPHelper sharedInstance].purchasedPackages removeAllObjects];

    [purchases enumerateObjectsUsingBlock:^(PVPSubscription *purchase, NSUInteger idx, BOOL *stop) {
        if ([purchase isKindOfClass:[PVPSubscription class]] && purchase.objectId && [purchase status] == 1) {
            if (purchase.type == OBJECT_TYPE_ENTRY) {
                [[IAPHelper sharedInstance].purchasedEntries addObject:purchase.objectId];
            } else if (purchase.type == OBJECT_TYPE_USER) {
                [[IAPHelper sharedInstance].purchasedUsers addObject:purchase.objectId];
            } else if (purchase.type == OBJECT_TYPE_PACKAGE) {
                [[IAPHelper sharedInstance].purchasedPackages addObject:purchase.objectId];
            }
        }
    }];
}

+(void)emptyUserPurchases
{
    [[IAPHelper sharedInstance].purchasedEntries removeAllObjects];
    [[IAPHelper sharedInstance].purchasedUsers removeAllObjects];
    [[IAPHelper sharedInstance].restoreDict removeAllObjects];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)serviceJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [self JSONTransformerForInt];
}

@end
