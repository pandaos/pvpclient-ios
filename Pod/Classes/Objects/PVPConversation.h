//
//  PVPConversation.h
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPBaseObject.h"
#import "PVPMessage.h"
#import "PVPUser.h"

/**
 *  The PVPConversation object represents a conversation between two users on the Bamboo platform.
 *  Typically, a conversation will be between the current logged in user and another user. For this reason, there is only one user object in the conversation (the other user).
 */
@interface PVPConversation : PVPBaseObject<PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  userDetails is a PVPUser object and represents the other user in the conversation.
 *
 *  @warning This object gets only a small portion of the PVPUser fields.
 *
 *  @see PVPUser
 */
@property (nonatomic, strong) PVPUser *userDetails;

/**
 *  conversation PVPMessage object, the most recent message that has been sent (or received) in the conversation.
 *
 *  @see PVPMessage
 */
@property (nonatomic, strong) PVPMessage *conversation;

@end
