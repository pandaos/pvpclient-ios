//
//  PVPRestorePurchases.m
//  Pods
//
//  Created by Oren Kosto,  on 7/13/17.
//
//

#import "PVPEncryptedData.h"
#import "PVPConfigHelper.h"
//#import <RNCryptor-objc/RNEncryptor.h>
#import "RNEncryptor.h"

@implementation PVPEncryptedData

+ (NSValueTransformer *)dataJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:NSString.class];
}

- (PVPEncryptedData *)initWithLocalPurchases {
    self = [super init];
    NSDictionary <NSString *, NSString *>*purchases = [AppUtils getDefaultDictionaryForKey:PURCHASES_DICT_KEY];
    NSMutableArray<NSString *> *encryptedPurchases = [NSMutableArray arrayWithCapacity:purchases.count];
    NSString *udid = [PVPConfigHelper sharedInstance].uniqueDeviceIdentifier;
    
    for (NSString *purchase in [purchases allKeys]) {
        NSDictionary *dict = @{
                               @"productId": purchase,
                               @"userId": udid,
                               @"service": @(PURCHASE_SERVICE_APPLE),
                               @"info": @{@"receipt": purchases[purchase]}
                               };
        
        NSString *result = [self encryptedDict:dict];
        [encryptedPurchases addObject:result];
    }
    
    self.data = [encryptedPurchases mutableCopy];
    
    return self;
}

- (PVPEncryptedData *)initWithObjects:(NSArray <NSDictionary *>*)objects {
    self = [super init];
    NSMutableArray<NSString *> *encryptedObjects = [NSMutableArray arrayWithCapacity:objects.count];
    for (NSDictionary *object in objects) {
        NSString *result = [self encryptedDict:object];
        [encryptedObjects addObject:result];
    }
    
    self.data = [encryptedObjects mutableCopy];
    
    return self;
}

- (NSString *)encryptedDict:(NSDictionary *)dict {
    NSAssert([PVPConfigHelper encryptPurchases], @"Cannot encrypt purchases, no encryption key set");
    NSError *error;
    NSString *jsonStr = [NSString JSONStringFromDictionary:dict];
    NSString *key = [PVPConfigHelper getLocalConfiguration:@"purchasesSharedSecret"];
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipher = [RNEncryptor encryptData:data withSettings:kRNCryptorAES256Settings password:key error:&error];
    return [cipher base64EncodedStringWithOptions:0];
}

@end
