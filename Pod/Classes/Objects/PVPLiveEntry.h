//
//  PVPLiveEntry.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/13/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KalturaClient.h"
#import "PVPEntry.h"
#import "PVPBaseObject.h"

/**
 *  The PVPLiveEntry object represents a live video entry on the Bamboo platform (for other media types see PVPEntry).
 *
 *  @warning This class inherits from KalturaLiveStreamEntry, and *NOT* from PVPBaseObject, therefore we needed to implement the mappingDictionary, addDescriptorsToObjectManager: and other basic methods methods separately here.
 */
@interface PVPLiveEntry : PVPEntry <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

//KalturaLiveEntry Properties

/**
 *  The message to be presented when the stream is offline.
 */
@property (nonatomic,copy) NSString* offlineMessage;

//KalturaLiveStreamEntry PropertiesX

/**
 *  The stream id as provided by the provider.
 */
@property (nonatomic,copy,readonly) NSString* streamRemoteId;

/**
 *  The backup stream id as provided by the provider.
 */
@property (nonatomic,copy,readonly) NSString* streamRemoteBackupId;

/**
 *  Array of supported bitrates.
 *  @see [KalturaLiveStreamBitrate](https://www.kaltura.com/api_v3/testmeDoc/objects/KalturaLiveStreamBitrate.html)
 */
//@property (nonatomic,retain) NSMutableArray* bitrates;	// of KalturaLiveStreamBitrate elements

/**
 *  The primary URL for broadcasting.
 */
@property (nonatomic,copy) NSString* primaryBroadcastingUrl;

/**
 *  The secondary URL for broadcasting.
 */
@property (nonatomic,copy) NSString* secondaryBroadcastingUrl;

/**
 *  The stream name.
 */
@property (nonatomic,copy) NSString* streamName;

/**
 *  The stream url.
 */
@property (nonatomic,copy) NSString* streamUrl;

/**
 *  HLS URL - URL for live stream playback on mobile device.
 */
@property (nonatomic,copy) NSString* hlsStreamUrl;

/**
 *  The DVR status of the entry.
 *  @see [KalturaDVRStatus](https://www.kaltura.com/api_v3/testmeDoc/enums/KalturaDVRStatus.html)
 */
@property (nonatomic,assign) int dvrStatus;	// enum KalturaDVRStatus, insertonly

/**
 *  Window of time which the DVR allows for backwards scrubbing (in minutes).
 */
@property (nonatomic,assign) int dvrWindow;	// insertonly

/**
 *  URL Manager to handle the live stream URL (for instance, add token).
 */
@property (nonatomic,copy) NSString* urlManager;

/**
 *  Array of key value protocol->live stream url objects.
 *  @see [KalturaLiveStreamConfiguration](https://www.kaltura.com/api_v3/testmeDoc/objects/KalturaLiveStreamConfiguration.html)
 */
@property (nonatomic, retain) NSMutableArray* liveStreamConfigurations;	// of KalturaLiveStreamConfiguration elements

/**
 *  Indication of whether the entry is currently live or not.
 */
@property (nonatomic, assign) BOOL isLive;

/*
// TODO: We need to see if we don't need these fields and remove them if neededLeaving them commented for now...
 
// Recording Status Enabled/Disabled
@property (nonatomic,assign) int recordStatus;	// enum KalturaRecordStatus, insertonly
// DVR Status Enabled/Disabled
// Elapsed recording time (in msec) up to the point where the live stream was last stopped (unpublished).
@property (nonatomic,assign) int lastElapsedRecordingTime;
// Recorded entry id
@property (nonatomic,copy) NSString* recordedEntryId;
// Flag denoting whether entry should be published by the media server
@property (nonatomic,assign) int pushPublishEnabled;	// enum KalturaLivePublishStatus
// Array of publish configurations
@property (nonatomic,retain) NSMutableArray* publishConfigurations;	// of KalturaLiveStreamPushPublishConfiguration elements
// The first time in which the entry was broadcast
@property (nonatomic,assign,readonly) int firstBroadcast;
// The Last time in which the entry was broadcast
@property (nonatomic,assign,readonly) int lastBroadcast;
// The time (unix timestamp in milliseconds) in which the entry broadcast started or 0 when the entry is off the air
@property (nonatomic,assign) double currentBroadcastStartTime;
@property (nonatomic,retain) KalturaLiveEntryRecordingOptions* recordingOptions;	// insertonly

@property (nonatomic,copy) NSString* primaryRtspBroadcastingUrl;
@property (nonatomic,copy) NSString* secondaryRtspBroadcastingUrl;
// The broadcast primary ip
@property (nonatomic,copy) NSString* encodingIP1;
// The broadcast secondary ip
@property (nonatomic,copy) NSString* encodingIP2;
// The broadcast password
@property (nonatomic,copy) NSString* streamPassword;
// The broadcast username
@property (nonatomic,copy,readonly) NSString* streamUsername;

//@property (nonatomic, copy) NSString *lastPlayedAt;
*/

/**
 *  @name Public Methods
 */

@end
