//
//  PVPUserSearchInfo.m
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import "PVPUserSearchInfo.h"

@implementation PVPUserSearchInfo

-(id)init
{
    self = [super init];
    if (self) {
        self.relationshipType = [[PVPUserRelationshipType alloc] init];
        self.interestedInGender = [[PVPUserInterestedInGender alloc] init];
        self.requiredSexualPreference = [[PVPUserInterestedInGender alloc] init];
    }
    return self;
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{//Dating-IL params
             @"interestedInAgeMax": @"interestedInAgeMax",
             @"interestedInAgeMin": @"interestedInAgeMin",
             @"interestedInStatus": @"interestedInStatus",
             
             @"interestedInGender": @"interestedInGender",
             @"relationshipType": @"relationshipType",
             @"requiredSexualPreference": @"requiredSexualPreference",
             @"requiredArea": @"requiredArea"
             };
}

+ (NSValueTransformer *)interestedInAgeMaxJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)interestedInAgeMinJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)interestedInStatusJSONTransformer {
    return [self JSONTransformerForInt];
}

+ (NSValueTransformer *)interestedInGenderJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserInterestedInGender.class];
}

+ (NSValueTransformer *)relationshipTypeJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserRelationshipType.class];
}

+ (NSValueTransformer *)requiredSexualPreferenceJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserInterestedInGender.class];
}

+ (NSValueTransformer *)requiredAreaJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUserRequiredArea.class];
}

@end
