//
//  PVPEntry.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KalturaClient.h"
#import "PVPBaseObject.h"
#import "PVPEntryInfo.h"
#import "PVPPackage.h"
/**
 *  The PVPEntry object represents a media entry on the Bamboo platform, which can be of any media type except live video (for live video see PVPLiveEntry).
 *  
 *  @warning This class inherits from KalturaMediaEntry, and *NOT* from PVPBaseObject.
 */
@interface PVPEntry : KalturaMediaEntry <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

//@property (nonatomic, copy) NSString *lastPlayedAt;
//@property (nonatomic, copy) NSString *redirectEntryId;

/**
 *  Additional info about the entry, saved as custom metadata.
 */

/**
 *  The entry's objectType
 */
@property (nonatomic, copy) NSString *objectType;

@property (nonatomic, strong) PVPEntryInfo *info;

/**
 *  @name Public Methods
 */


/**
 Inits an instance of this object with a given custom entry ID.

 @param entryId The entry ID to assign to this entry.

 @return A new PVPEntry instance, with the given entry ID.
 */
- (id)initWithId:(NSString *)entryId;

/**
 * Checks whether the entry is an ad.
 * This is decided according to whether the entry is in a "Ad" or "Ads" category.
 * @return YES or NO, depending on whether the entry is an ad.
 */
-(BOOL)isAd;

/**
 * Generates a PVP download URL for the entry. This uses the "source" flavor param ID.
 *
 * @discussion Example:
 * <pre>
 * <code>
 * PVPEntry *entry = ...; //retrieve the required entry from somewhere
 * NSString *downloadUrl = [entry pvpDownloadUrl]; //use 0 as the flavor param ID to retrieve the source file (full resolution).
 * </code>
 * </pre>
 *
 * @return A newly generated PVP download URL for the entry.
 */
- (NSString *)pvpDownloadUrl;

/**
 * Generates a PVP download URL for the entry with a given flavor param ID.
 *
 * @discussion Example:
 * <pre>
 * <code>
 * PVPEntry *entry = ...; //retrieve the required entry from somewhere
 * NSString *downloadUrl = [entry pvpDownloadUrlWithFlavorParamId:0]; //use 0 as the flavor param ID to retrieve the source file (full resolution).
 * </code>
 * </pre>
 *
 * @param flavorParamId The flavor param ID.
 * @return A newly generated PVP download URL for the entry.
 */
- (NSString *)pvpDownloadUrlWithFlavorParamId:(int)flavorParamId;

/**
 * Generates the adaptive HLS playback URL for the entry.
 * @return The adaptive HLS playback URL for the entry.
 * @warning Will only return a valid M3U8 if the instance is hosting the content on S3.
 */
- (NSString *)pvpAdaptiveHLSPlaybackUrl;

/**
 * Generates a thumbnail URL for the entry with the PVP API.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry pvpThumbnailUrlWithTime:]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:withType:]
 */
- (NSString *)pvpThumbnailUrl;

/**
 * Generates a thumbnail URL for the entry with the PVP API from a custom time.
 *
 * @param time The desired frame time.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry pvpThumbnailUrl]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:withType:]
 */
- (NSString *)pvpThumbnailUrlWithTime:(int)time;

/**
 * Generates a thumbnail URL for the entry with the PVP API with a custom time and size.
 *
 * @param time The desired frame time.
 * @param size The desired size.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry pvpThumbnailUrl]
 * @see [PVPEntry pvpThumbnailUrlWithTime:]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:withType:]
 */
- (NSString *)pvpThumbnailUrlWithTime:(int)time withSize:(CGSize)size;

/**
 * Generates a thumbnail URL for the entry with the PVP API with a custom time, size, and crop type.
 *
 * @param time The desired frame time.
 * @param size The desired size.
 * @param type The desired crop type.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry pvpThumbnailUrl]
 * @see [PVPEntry pvpThumbnailUrlWithTime:]
 * @see [PVPEntry pvpThumbnailUrlWithTime:withSize:]
 */
- (NSString *)pvpThumbnailUrlWithTime:(int)time withSize:(CGSize)size withType:(NSString *)type;

/**
 * Generates a thumbnail URL for the entry with the Kaltura API.
 *
 * @param entryId The entry ID to generate the thumbnail URL for.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:withSize:]
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:withSize:withType:]
 */
+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId;

/**
 * Generates a thumbnail URL for the entry with the Kaltura API with a custom size.
 *
 * @param entryId The entry ID to generate the thumbnail URL for.
 * @param size The desired thumbnail size.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:]
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:withSize:withType:]
 */
+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId withSize:(CGSize)size;

/**
 * Generates a thumbnail URL for the entry with the Kaltura API with a custom size and crop type.
 *
 * @param entryId The entry ID to generate the thumbnail URL for.
 * @param size The desired thumbnail size.
 * @param type The desired crop type.
 *
 * @return The entry's thumbnail URL, generated with the PVP API.
 *
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:withSize:]
 * @see [PVPEntry kalturaThumbnailUrlForEntryId:]
 */
+ (NSString *)kalturaThumbnailUrlForEntryId:(NSString *)entryId withSize:(CGSize)size withType:(NSString *)type;

/**
 * Generates HLS url from entryId going through Bamboo play manifest url.
 *
 * @param entryId the entry ID we wish to generate the HLS manifest URL.
 * @return The HLS URL for the given entry ID.
 */
+ (NSString *)hlsManifestUrlWithId:(NSString*)entryId;
+ (NSString *)hlsManifestUrlWithIdFromKaltura:(NSString*)entryId;

/**
* Checks entry info YouTube
*/

- (BOOL)isYouTubeEntry;
- (NSString *)youTubeID;

/**
* Checks entry info hlsUrl
*/

- (BOOL)isExternalEntry;
- (NSString *)externalHLSUrl;
/**
* Video Trailer id
*/

- (NSString *)trailerID;

/**
* Slider Poster Url String
*/

- (NSString *)posterUrlString;


- (NSString *)articleLink;

- (NSString *)eventIDString;

- (NSArray<PVPPackage *> *)entryPackages;

- (BOOL)hasPurchases;
// Call only if purchases available. Use - hasPurchases to check.
- (BOOL)isPurchased;

@end
