//
//  PVPUploadToken.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

/**
 *  The PVPUploadToken object is used for uploading files to Kaltura, and binding an uploaded file to an entry.
 */
@interface PVPUploadToken : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The object ID.
 */
@property (nonatomic, copy) NSString *id;

/**
 *  The generated upload token. This property is used to bind between the uploaded file and the PVPEntry created for it.
 */
@property (nonatomic, copy) NSString *uploadTokenId;

/**
 *  The Kaltura partner ID.
 */
@property (nonatomic, assign) int partnerId;

/**
 *  The ID of the user uploading the file.
 */
@property (nonatomic, copy) NSString *userId;

/**
 *  The status of the process.
 */
@property (nonatomic, copy) NSString *status;

/**
 *  The name we are giving to the uploaded file.
 */
@property (nonatomic, copy) NSString *fileName;

/**
 *  The size of the file we wish to upload, in bytes.
 */
@property (nonatomic, assign) NSUInteger fileSize;

/**
 *  The size of the file that was actually uploaded.
 */
@property (nonatomic, assign) int uploadedFileSize;

/**
 *  Unix timestamp indicating when we have created this object.
 */
@property (nonatomic, assign) int createdAt;

/**
 *  Unix timestamp indicating when we last updated this object.
 */
@property (nonatomic, assign) int updatedAt;

@end
