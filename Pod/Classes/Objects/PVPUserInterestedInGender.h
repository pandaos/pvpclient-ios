//
//  PVPUserInterestedInGender.h
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//
#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"

/**
 *  The user's "interested in gender" info - This object is under the PVPUserSearchInfo object which includes the filter settings set by the user for the required gender.
 *
 *  @see PVPUserSearchInfo
 */
@interface PVPUserInterestedInGender : PVPBaseObject

/**
 *  male - a boolean which states if the user is interested in a relationship with a male.
 */
@property (nonatomic, assign) BOOL male;

/**
 *  female - a boolean which states if the user is interested in a relationship with a female.
 */
@property (nonatomic, assign) BOOL female;

@end
