//
// Created by Oren Kosto,  on 8/16/16.
//

#import "PVPCuePoint.h"


@implementation PVPCuePoint

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
            @"parentId": @"parentId",
            @"text": @"text",
            @"endTime": @"endTime",
            @"duration": @"duration",
            @"depth": @"depth",
            @"childrenCount": @"childrenCount",
            @"directChildrenCount": @"directChildrenCount",
            @"id": @"id",
            @"cuePointType": @"cuePointType",
            @"status": @"status",
            @"entryId": @"entryId",
            @"partnerId": @"partnerId",
            @"createdAt": @"createdAt",
            @"updatedAt": @"updatedAt",
            @"tags": @"tags",
            @"startTime": @"startTime",
            @"userId": @"userId",
            @"partnerData": @"partnerData",
            @"partnerSortValue": @"partnerSortValue",
            @"forceStop": @"forceStop",
            @"thumbOffset": @"thumbOffset",
            @"systemName": @"systemName"
    };
}

+ (NSValueTransformer *)startTimeJSONTransformer {
    return [self JSONTransformerForInt];
}
@end
