//
//  PVPUserSearchInfo.h
//  Pods
//
//  Created by Jacob Barr on 2/18/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPBaseObject.h"
#import "PVPUserInterestedInGender.h"
#import "PVPUserRelationshipType.h"
#import "PVPUserRequiredArea.h"

/**
 *  The PVPUserSearchInfo object provides info about the user's default search filter.
 */
@interface PVPUserSearchInfo : PVPBaseObject

/**
 *  The user's search info - This is a subclass of PVPUserPersonalInfo which includes the filter settings set by the user for the search functionality.
 *
 *  @see PVPUserPersonalInfo
 */

/**
*  Maximum age search filter
*/
@property (nonatomic, assign) int interestedInAgeMax;

/**
 *  Minimum age search filter
 */
@property (nonatomic, assign) int interestedInAgeMin;

/**
 *  The status in which the user is interested.
 */
@property (nonatomic, assign) int interestedInStatus;

/**
 *  Gender search filter object
 *
 *  @see PVPUserInterestedInGender
 */
@property (nonatomic, strong) PVPUserInterestedInGender *interestedInGender;

/**
 *  The sexual preference of the other side.
 *  
 *  @see PVPUserInterestedInGender
 */
@property (nonatomic, strong) PVPUserInterestedInGender *requiredSexualPreference;

/**
 *  Relationship type filter object
 *
 *  @see PVPUserRelationshipType
 */
@property (nonatomic, strong) PVPUserRelationshipType *relationshipType;

/**
 *  Required Area filter object
 *
 *  @see PVPUserRequiredArea
 */
@property (nonatomic, strong) PVPUserRequiredArea *requiredArea;

@end
