//
//  PVPChannel.m
//  Pods
//
//  Created by Oren Kosto,  on 9/13/16.
//
//

#import "PVPChannel.h"
#import "NSDate+PVPClient.h"

@implementation PVPChannel

-(NSArray<PVPChannelScheduleItem *> *) getScheduleItems {
    return [self getScheduleItems:NO];
}

-(NSArray<PVPChannelScheduleItem *> *) getScheduleItems:(BOOL)includeAds {
    NSMutableArray<PVPChannelScheduleItem *> *result = [NSMutableArray arrayWithCapacity:100];
    if (self.schedules && self.schedules.count) {
        for (PVPChannelSchedule *schedule in self.schedules) {
            if (schedule.scheduleItems && schedule.scheduleItems.count) {
                for (PVPChannelScheduleItem *scheduleItem in schedule.scheduleItems) {
                    if (includeAds || (!includeAds && ![scheduleItem.entry isAd])) {
                        scheduleItem.startTime += schedule.date;
                        scheduleItem.endTime += schedule.date;
                        [result addObject:scheduleItem];
                    }
                }
            }
        }
    }
    
    if (self.epgSchedule == nil) {
        self.epgSchedule = [result copy];
    }
    
    return [result copy];
}

- (PVPChannelScheduleItem *)currentLiveScheduleItem {
    for (PVPChannelScheduleItem *item in _epgSchedule) {
        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970WithServerOffset:[item startTime]];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970WithServerOffset:[item endTime]];
        if ([startDate compare:[NSDate date]] == NSOrderedAscending && [endDate compare:[NSDate date]] == NSOrderedDescending) {
            return item;
        }
    }
    
    return nil;
}

- (PVPChannelScheduleItem *)nextScheduleItemAfter:(PVPChannelScheduleItem *)item {
    NSUInteger index = [_epgSchedule indexOfObject:item];
    if (index != NSNotFound && _epgSchedule.count > (index + 1)) {
        return _epgSchedule[(index + 1)];
    }
    
    return nil;
}

- (CGFloat)currentTimeOffsetForScheduleItem:(PVPChannelScheduleItem *)item {
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970WithServerOffset:[item startTime]];

    return [[NSDate date] timeIntervalSinceDate:startDate];
}

- (BOOL)isYoutubeChannel {
    return self.type == PVPChannelTypeYoutube;
}

- (NSString *)epgLabel {
    if ([self info] != nil && [[self info] isKindOfClass:[NSDictionary class]] && [self info][@"epgLabel"] != nil) {
        return [self info][@"epgLabel"];
    }
    
    return nil;
}

- (NSString *)eventIDString {
    return [NSString stringWithFormat:@"%@|%@", self.id ?: self.entry.id, self.name ?: @""];
}

+ (NSValueTransformer *)infoJSONTransformer {
    return [PVPJSONAdapter transformerForModelPropertiesOfClass:NSDictionary.class];
}

+ (NSValueTransformer *)channelDescriptionJSONTransformer {
    return [PVPBaseObject JSONTransformerForString];
}

+ (NSValueTransformer *)entryJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPLiveEntry.class];
}

+ (NSValueTransformer *)schedulesJSONTransformer {
    return [PVPJSONAdapter arrayTransformerWithModelClass:PVPChannelSchedule.class];
}

@end
