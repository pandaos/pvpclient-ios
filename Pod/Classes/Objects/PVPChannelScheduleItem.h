//
//  PVPChannelScheduleItem.h
//  Pods
//
//  Created by Oren Kosto,  on 1/17/17.
//
//

#import "PVPBaseObject.h"
#import "PVPEntry.h"

@interface PVPChannelScheduleItem : PVPBaseObject <PVPBaseObjectProtocol>

@property (nonatomic, assign) int type;
@property (nonatomic, assign) int startTime;
@property (nonatomic, assign) int endTime;
@property (nonatomic, copy) NSString *entryId;
@property (nonatomic, copy) PVPEntry *entry;

- (NSString *)youtubeID;

@end
