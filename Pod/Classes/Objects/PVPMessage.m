//
//  PVPMessage.m
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPMessage.h"

@interface PVPMessage ()

/**
 *  @name Private Properties
 */

/**
 *  The MongoId object of the message, in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *mongoIdObj;

@end

@implementation PVPMessage

- (id)initWithReceiver:(NSString *)receiver withContent:(NSString *)content {
    self = [super init];
    if (self) {
        self.creator = @"0";
        self.receiver = receiver;
        self.content = content;
    }
    return self;
}

- (NSString *)mongoId {
    return [PVPMongoHelper mongoIdStringFromObject:self.mongoIdObj];
}

+ (NSDictionary *) JSONKeyPathsByPropertyKey {
    return @{@"mongoIdObj": @"_id",
             @"createdAt": @"createdAt",
             @"creator": @"creator",
             @"receiver": @"receiver",
             @"content": @"content",
             @"read": @"read"
             };
}

@end
