//
//  PVPLiveEntryPublish.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/29/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPLiveEntryPublish.h"

@implementation PVPLiveEntryPublish

-(id) init
{
    self = [super init];
    if (self) {
        self.entry = [[PVPLiveEntry alloc] init];
    }
    return self;
}

-(id) initWithLiveEntry:(PVPLiveEntry *)liveEntry
{
    self = [self init];
    if (self) {
        self.entry = liveEntry;
    }
    return self;
}

+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"result": @"result",
             @"entry": @"entry"};
}

+ (NSValueTransformer *)entryJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPLiveEntry.class];
}

@end
