//
//  PVPInstance.h
//  Pods
//
//  Created by  on 17/2/19.
//
//

#import "PVPBaseObject.h"

/**
 *  The PVPContact object represents a user's contact list entry on the Bamboo platform. This object inherits most properties from the PVPBaseObject object.
 */
@interface PVPInstance : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *iconURL;

@property int type;

@property (nonatomic, copy) NSArray<NSString *> *hosts;

-(NSString *)mongoId;


@end
