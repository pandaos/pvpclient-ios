//
//  PVPChannelScheduleItem.m
//  Pods
//
//  Created by Oren Kosto,  on 1/17/17.
//
//

#import "PVPChannelScheduleItem.h"

@implementation PVPChannelScheduleItem

-(id)init
{
    self = [super init];
    if (self) {
        self.entry = [[PVPEntry alloc] init];
    }
    return self;
}

+ (NSValueTransformer *)entryJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPEntry.class];
}

- (NSString *)youtubeID {
    return [_entry youTubeID] ?: @"";
}

@end
