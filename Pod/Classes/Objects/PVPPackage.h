//
//  PVPPackage.h
//  Pods
//
//  Created by Oren Kosto,  on 6/27/17.
//
//

#import "PVPBaseObject.h"
@class SKProduct;

@interface PVPPackage : PVPBaseObject <PVPBaseObjectProtocol>

@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *packageDescription;
@property(nonatomic, strong) NSString *price;
@property(nonatomic, strong) NSDictionary *info;
@property(nonatomic, strong) NSString *thumbnailUrl;

@property(nonatomic) NSInteger purchaseType;

- (NSString *)productId;

- (void)setupProductIdentifier:(NSString *)identifier;

@end
