//
//  PVPLiveEntry.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/13/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPLiveEntry.h"

@implementation PVPLiveEntry

- (id)init {
    self = [super init];
    [self setDvrStatus:0];
    [self setDvrWindow:0];
    [self setOfflineMessage:@"Stream is currently unavailavle. Please check back later."];
    [self setMediaType:[KalturaMediaType LIVE_STREAM_FLASH]];
    return self;
}

+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    //KalturaMediaEntry -> KalturaLiveStreamEntry
    NSDictionary *entryDictionary = [PVPEntry JSONKeyPathsByPropertyKey];
    NSDictionary *liveDictionaty = @{@"offlineMessage": @"offlineMessage",
                                     @"streamRemoteId": @"streamRemoteId",
                                     @"streamRemoteBackupId": @"streamRemoteBackupId",
//                                     @"bitrates": @"bitrates",
                                     @"primaryBroadcastingUrl": @"primaryBroadcastingUrl",
                                     @"secondaryBroadcastingUrl": @"secondaryBroadcastingUrl",
                                     @"streamName": @"streamName",
                                     @"streamUrl": @"streamUrl",
                                     @"hlsStreamUrl": @"hlsStreamUrl",
                                     @"dvrStatus": @"dvrStatus",
                                     @"dvrWindow": @"dvrWindow",
                                     @"urlManager": @"urlManager",
//                                     @"liveStreamConfigurations": @"liveStreamConfigurations",
                                     @"isLive": @"isLive"};
    
    NSMutableDictionary *combinedDictionary = [entryDictionary mutableCopy];
    [combinedDictionary addEntriesFromDictionary:liveDictionaty];
    
    return combinedDictionary;
}

@end
