//
//  PVPConfig.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/21/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

/**
 *  The PVP server config object.
 *  When received from the server, this object will contain all of the configurations for this instance.
 */
@interface PVPConfig : PVPBaseObject

/**
 *  @name Public Properties
 */

/**
 *  The user KS, can be used as an access token for Kaltura-related services.
 */
@property (nonatomic, copy) NSString *userKs;

/**
 * The current instance Id of Bamboo
 */
@property (nonatomic, copy) NSString *currentInstanceId;

@property (nonatomic, copy) NSDictionary *general;

/**
 *  The Kaltura configuration for this instance. Will contain the partner ID and base URL for Kaltura services.
 */
@property (nonatomic, copy) NSDictionary *kaltura;

/**
 *  The Application configuration for this instance.
 */
@property (nonatomic, copy) NSDictionary *application;


/**
 *  The Home configuration. Will contain the entry categories.
 */
@property (nonatomic, copy) NSDictionary *home;

/**
 *  The Navigation configuration.
 */
@property (nonatomic, copy) NSDictionary *navigation;

/**
 *  The Kaltura player configuration. Will contain the uiConfId and player URL.
 */
@property (nonatomic, copy) NSDictionary *player;

/**
 *  The Live configuration. Will contain the Wowza RTMP server URL and other configurations for the instance.
 */
@property (nonatomic, copy) NSDictionary *live;

/**
 *  
 */
@property (nonatomic, copy) NSDictionary *hls;

/**
 *  The PushWoosh configuration. Will contain texts for push notifications, and rules for if and when to send a push notification.
 */
@property (nonatomic, copy) NSDictionary *pushwoosh;

/**
 *  The analytics configuration. Will contain the relevant paraneters for Piwik and Google analytics.
 */
@property (nonatomic, copy) NSDictionary *analytics;

/**
 *  Puchases configuration. Will contain information about purchase services that are configured for the instance.
 *  Apple IAP parameters and product identifiers are configured here if they are available for this instance.
 */
@property (nonatomic, copy) NSDictionary *purchases;

/**
 *  Social network configuration. Will contain info about the social networks configured for this instance, including app IDs and share texts.
 */
@property (nonatomic, copy) NSDictionary *social;

/**
 *  Entry configuration. Will contain entry configurations (i.e. whether to enable download links).
 */
@property (nonatomic, copy) NSDictionary *entry;

/**
 *  Docs URLs configuration. Will contain URLs of relevant docs for the app.
 */
@property (nonatomic, copy) NSDictionary *docs;

/**
 *  Branding
 */
@property (nonatomic, copy) NSDictionary *branding;

/**
 *  Mobile configuration. Will contain the configs related to mobile apps.
 */
@property (nonatomic, copy) NSDictionary *mobile;

/**
 *  DRM configuration.
 */
@property (nonatomic, copy) NSDictionary *drm;

/**
 *  Channels configuration.
 */
@property (nonatomic, copy) NSDictionary *channels;

/**
 *  User info
 */
@property (nonatomic, copy) NSDictionary *user;


@property (nonatomic, copy) NSArray *appList;

@end
