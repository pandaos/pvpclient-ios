//
//  PVPPackage.m
//  Pods
//
//  Created by Oren Kosto,  on 6/27/17.
//
//

#import "PVPPackage.h"
#import <StoreKit/StoreKit.h>

@implementation PVPPackage

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    dict[@"packageDescription"] = @"description";
    
    return [dict mutableCopy];
}

- (NSString *)productId {
    return self.info && self.info[@"appleProductId"] ? self.info[@"appleProductId"] : nil;
}

+ (NSValueTransformer *)priceJSONTransformer {
    return [self JSONTransformerForString];
}

- (void)setupProductIdentifier:(NSString *)identifier {
    self.info = @{@"appleProductId" : identifier};
}

@end
