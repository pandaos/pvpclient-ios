//
//  PVPResult.h
//  PVPClient
//
//  Created by Oren Kosto,  on 3/23/15.
//  Copyright (c) 2015 Panda-OS. All rights reserved.
//

#import "PVPBaseObject.h"

/**
 *  The PVPResult object represents a result returned from the PVP server.
 *  In some cases, the API request doesn't need to return an object, but does need to return some kind of indication of what happened.
 */
@interface PVPResult : PVPBaseObject <PVPBaseObjectProtocol>

/**
 *  An object ID that this result refers to, will return empty if the result refers to the entire response.
 */
@property (nonatomic, strong) NSString *id;

/**
 *  The actual result (i.e. "success" for success).
 */
@property (nonatomic, strong) NSString *result;

@end
