//
//  PVPMessage.h
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPBaseObject.h"
#import "PVPMongoHelper.h"

/**
 *  The PVPMessage object represents a message between two users in the Bamboo platform.
 */
@interface PVPMessage : PVPBaseObject<PVPBaseObjectProtocol>

/**
 *  @name Public Properties
 */

/**
 *  A Unix timestamp which represents the time in which the message has been created.
 */
@property (nonatomic, assign) int createdAt;

/**
 *  The mongoId of the user who created the message (the sender).
 */
@property (nonatomic, copy) NSString *creator;

/**
 *  The mongoId of the user who received the message.
 */
@property (nonatomic, copy) NSString *receiver;

/**
 *  The content of the message (text).
 */
@property (nonatomic, copy) NSString *content;

/**
 *  A boolean flag that represents the read status of the message. YES, if message already viewed by the receiver, otherwise, NO.
 */
@property (nonatomic, assign) BOOL read;

/**
 *  @name Public Methods
 */

/**
 *  Convenience initializer for the object, with a given receiver and message content.
 *
 *  @param receiver The MongoID of the message receiver.
 *  @param content  The message content.
 *
 *  @return A new instance of the PVPMessage object, built from the given parameters.
 */
- (id)initWithReceiver:(NSString *)receiver withContent:(NSString *)content;

/**
 *  Extracts and returns the MongoId of the user object in string form.
 *
 *  @return The MongoId of the user object in string form.
 */
- (NSString *) mongoId;

@end
