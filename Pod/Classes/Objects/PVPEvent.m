//
//  PVPEvent.m
//  Pods
//
//  Created by Jacob Barr on 10/04/2016.
//
//

#import "PVPEvent.h"

@interface PVPEvent ()

/**
 *  @name Private Properties
 */

/**
 *  The MongoId object of the event, in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 */
@property (nonatomic, copy) NSDictionary *mongoIdObj;


@end

@implementation PVPEvent

- (id)initWithReceiverId:(NSString *)receiverId WithType:(int)type {
    self = [super init];
    if (self) {
        self.receiver = receiverId;
        self.type = type;
    }
    return self;
}

- (NSString *)mongoId {
    return [PVPMongoHelper mongoIdStringFromObject:self.mongoIdObj];
}

+ (NSDictionary *) JSONKeyPathsByPropertyKey {
    return @{@"mongoIdObj": @"_id",
             @"createdAt": @"createdAt",
             @"updatedAt": @"updatedAt",
             @"creator": @"creator",
             @"receiver": @"receiver",
             @"type": @"type",
             @"status": @"status",
             @"creatorExpand": @"creatorExpand",
             @"receiverExpand": @"receiverExpand"
             };
}

+ (NSValueTransformer *)creatorExpandJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUser.class];
}

+ (NSValueTransformer *)receiverExpandJSONTransformer {
    return [PVPJSONAdapter dictionaryTransformerWithModelClass:PVPUser.class];
}

@end
