//
//  PVPConfigHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//  Updated by  01/07/2018.
//

#import "PVPConfigHelper.h"
#import <AVFoundation/AVFoundation.h>
#import "PVPFlutterHelper.h"


@import AdSupport;

@implementation PVPConfigHelper

static BOOL isSetup = NO;
static PVPConfigHelper *sharedInstance = nil;
static dispatch_once_t onceToken;
static NSMutableDictionary *plistDictionary;
static NSDictionary *defaultLocalConfig;

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

- (void)setup {
    NSDictionary *currentConfigDict = [NSDictionary dictionaryFromJSONString:[AppUtils getDefaultValueFofKey:DEFAULTS_KEY_CURRENT_CONFIG]];
    NSError *e;
    
    if (currentConfigDict && [currentConfigDict isKindOfClass:[NSDictionary class]]) {
        PVPConfig *configObj = [PVPJSONAdapter modelOfClass:[PVPConfig class] fromJSONDictionary:currentConfigDict error:&e];
        if (configObj && !e) {
            [self setCurrentConfig:configObj];
        }
    }
    
    self.backHistoryStack = [NSMutableArray arrayWithCapacity:30]; // 30 items max in history
}

- (NSString *)getCurrentConfigString {
    return [AppUtils getDefaultValueFofKey:DEFAULTS_KEY_CURRENT_CONFIG];
}

- (void)setCurrentConfig:(PVPConfig *)currentConfig {
    NSError *e;
    NSDictionary *currentConfigDict = [PVPJSONAdapter JSONDictionaryFromModel:currentConfig error:&e];
    
    if (currentConfigDict && !e) {
        [AppUtils saveDefaultValue:[NSString JSONStringFromDictionary:currentConfigDict] forKey:DEFAULTS_KEY_CURRENT_CONFIG];
    }
    
    _currentConfig = currentConfig;
    [self initServerConfig];
}

- (void) initServerConfig {}

- (void) initLocalConfig {
    // Load configuration from settings.plist
    NSString *pathToPlist = [[NSBundle mainBundle] pathForResource:@"PVPSettings" ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToPlist]) {
        NSLog(@"Local configuration file PVPSettings.plist found in the main app bundle.");
        plistDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:pathToPlist];
    } else {
        plistDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    
    defaultLocalConfig = @{@"pvpServerDomain": @"https://www.bamboo-video.com",
                           @"pvpServerPrefix": @"/api",
                           @"pvpServerSuffix": @"?format=json",
                           @"tokenField": @"token",
                           @"debugEnabled": @(NO),
                           @"pushwooshEnabled": @(NO),
                           @"pageSize": @"20",
                           @"monitorReachability": @(YES),
                           @"defaultLanguage": @"en",
                           DEFAULTS_KEY_HAS_VISITED_APP: @([[AppUtils getDefaultValueFofKey:DEFAULTS_KEY_HAS_VISITED_APP] boolValue]),
                           @"serverTimeOffset": @"0"
                           };
    
    [AppUtils saveDefaultValue:@"NO" forKey:DEFAULTS_KEY_HAS_VISITED_APP];
}

- (BOOL)isParamInMobileConfigDefined:(NSString *)param {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[param] && ![self.currentConfig.mobile[param] isEqual:[NSNull null]];
}

- (BOOL)isParamInUserConfigDefined:(NSString *)param {
    return self.currentConfig && self.currentConfig.user && self.currentConfig.user[param] && ![self.currentConfig.user[param] isEqual:[NSNull null]];
}

- (BOOL)isParamInChannelsConfigDefined:(NSString *)param {
    return self.currentConfig && self.currentConfig.channels && self.currentConfig.channels[param] && ![self.currentConfig.channels[param] isEqual:[NSNull null]];
}

- ( BOOL ) useCustomFont {
    return [self isParamInMobileConfigDefined:@"enableCustomFonts"] ? [self.currentConfig.mobile[@"enableCustomFonts"] boolValue] : NO;
}

- (BOOL)showNoContent {
    return [self isParamInMobileConfigDefined:@"showNoContent"] ? [self.currentConfig.mobile[@"showNoContent"] boolValue] : NO;
}

- (UIFont *)mainCustomFontWithSize:(CGFloat)size {
    if ([PVPConfigHelper getLocalConfiguration:@"mainFont"] != nil) {
        UIFont *font = [UIFont fontWithName:[PVPConfigHelper getLocalConfiguration:@"mainFont"] size:size];
        return font;
    }
    
    return nil;
}

- (UIFont *)secondaryCustomFontWithSize:(CGFloat)size {
    if ([PVPConfigHelper getLocalConfiguration:@"secondaryFont"] != nil) {
        UIFont *font = [UIFont fontWithName:[PVPConfigHelper getLocalConfiguration:@"secondaryFont"] size:size];
        return font;
    }
    
    return nil;
}

- (UIFont *)menuFont {
    NSInteger size = 18;
    if ([[PVPConfigHelper getLocalConfiguration:@"menuFontSize"] integerValue] > 0) {
        size = [[PVPConfigHelper getLocalConfiguration:@"menuFontSize"] integerValue];
    }
    
    if ([[PVPConfigHelper sharedInstance] useCustomFont] && [PVPConfigHelper getLocalConfiguration:@"menuFont"] != nil) {
        UIFont *font = [UIFont fontWithName:[PVPConfigHelper getLocalConfiguration:@"menuFont"] size:size];
        return font;
    } else if ([[PVPConfigHelper sharedInstance] useCustomFont] && [self secondaryCustomFontWithSize:size] != nil) {
        return [self secondaryCustomFontWithSize:size];
    } else {
        return [UIFont fontWithName:@"Lato" size:size];
    }    
}

- (UIFont *)topBarFont {
    NSInteger size = 22;
    if ([PVPConfigHelper getLocalConfiguration:@"topFont"] != nil) {
        UIFont *font = [UIFont fontWithName:[PVPConfigHelper getLocalConfiguration:@"topFont"] size:size];
        return font;
    }
    
    return nil;
}

- (BOOL)isMenuFontWideSpacing {
    return [[PVPConfigHelper getLocalConfiguration:@"menuFontWideSpacing"] boolValue];
}

- (NSString *)getParamHex:( NSString * )param {
    return [self isParamInMobileConfigDefined:param] ? self.currentConfig.mobile[param] : nil;
}

- ( UIColor * ) getColorByString:( NSString * )neededColor {
    NSString *hexString = [self getParamHex:neededColor];

    if ( hexString ) {
        return [UIColor colorWithHexString:hexString];
    } else if ([neededColor isEqualToString:@"playPagePrimaryTextColor"] ||
               [neededColor isEqualToString:@"secondaryTextColor"] ||
               [neededColor isEqualToString:@"toolbarBackgroundColor"]) {
        return [self primaryTextColor];
    } else if ([neededColor isEqualToString:@"toolbarTextColor"] ||
               [neededColor isEqualToString:@"radioPrimaryColor"]) {
        return [self primaryColor];
    } else if ([neededColor isEqualToString:@"radioBackgroundColor"] ||
               [neededColor isEqualToString:@"secondaryBackgroundColor"] ||
               [neededColor isEqualToString:@"buttonSecondaryColor"] ||
               [neededColor isEqualToString:@"buttonTertiaryColor"] ||
               [neededColor isEqualToString:@"menuBackgroundColor"]) {
        return [self secondaryColor];
    } else if ([neededColor isEqualToString:@"menuSeparatorColor"] ||
               [neededColor isEqualToString:@"menuIconColor"]) {
        return [self buttonPrimaryColor];
    } else if ([neededColor isEqualToString:@"menuPrimaryTextColor"] ||
               [neededColor isEqualToString:@"loginPageSecondaryTextColor"] ||
               [neededColor isEqualToString:@"loginPagePrimaryTextColor"]) {
        return [self buttonTextPrimaryColor];
    } else if ([neededColor isEqualToString:@"playPageSecondaryTextColor"] ||
               [neededColor isEqualToString:@"buttonTextSecondaryColor"] ||
               [neededColor isEqualToString:@"buttonTextTertiaryColor"] ||
               [neededColor isEqualToString:@"packageTextColor"]) {
        return [self secondaryTextColor];
    }
    // default color values by case
    else if (// case light gray
            [neededColor isEqualToString:@"primaryColor"] ||
            [neededColor isEqualToString:@"primaryHighlightColor"] ||
            [neededColor isEqualToString:@"buttonPrimaryColor"] ||
             [neededColor isEqualToString:@"drawerSelectorColor"])
    {
        return [UIColor lightGrayColor];
    }
    else if (// case black
            [neededColor isEqualToString:@"secondaryColor"] ||
            [neededColor isEqualToString:@"primaryTextColor"] ||
            [neededColor isEqualToString:@"buttonTextPrimaryColor"])
    {
        return [UIColor blackColor];
    }
    else if (// case white
            [neededColor isEqualToString:@"backgroundColor"])
    {
        return [UIColor whiteColor];
    }
    return [UIColor lightGrayColor];
}

- ( UIColor * ) primaryColor {
    return [self getColorByString:@"primaryColor"];
}

- ( UIColor * ) secondaryColor {
    return [self getColorByString:@"secondaryColor"];
}

- (UIColor *)packageTextColor {
    return [self getColorByString:@"packageTextColor"];
}

- (UIColor *)playPagePrimaryTextColor {
    return [self getColorByString:@"playPagePrimaryTextColor"];
}

- (UIColor *)playPageSecondaryTextColor {
    return [self getColorByString:@"playPageSecondaryTextColor"];
}

- (UIColor *)loginPagePrimaryTextColor {
    return [self getColorByString:@"loginPagePrimaryTextColor"];
}

- (UIColor *)loginPageSecondaryTextColor {
    return [self getColorByString:@"loginPageSecondaryTextColor"];
}

- (UIColor *)primaryTextColor {
    return [self getColorByString:@"primaryTextColor"];
}

- (UIColor *)secondaryTextColor {
    return [self getColorByString:@"secondaryTextColor"];
}

- (UIColor *)buttonPrimaryColor {
    return [self getColorByString:@"buttonPrimaryColor"];
}

- (UIColor *)buttonSecondaryColor {
    return [self getColorByString:@"buttonSecondaryColor"];
}

- (UIColor *)buttonTextPrimaryColor {
    return [self getColorByString:@"buttonTextPrimaryColor"];
}

- (UIColor *)buttonTextSecondaryColor {
    return [self getColorByString:@"buttonTextSecondaryColor"];
}

- (UIColor *)buttonTertiaryColor {
    return [self getColorByString:@"buttonTertiaryColor"];
}

- (UIColor *)buttonTextTertiaryColor {
    return [self getColorByString:@"buttonTextTertiaryColor"];
}


- (UIColor *)backgroundColor {
    return [self getColorByString:@"backgroundColor"];
}

- (UIColor *)secondaryBackgroundColor {
    return [self getColorByString:@"secondaryBackgroundColor"];
}

- (UIColor *)toolbarTextColor {
    return [self getColorByString:@"toolbarTextColor"];
}

- (UIColor *)toolbarBackgroundColor {
    return [self getColorByString:@"toolbarBackgroundColor"];
}

- (UIColor *)radioBackgroundColor {
    return [self getColorByString:@"radioBackgroundColor"];
}

- (UIColor *)radioPrimaryColor {
    return [self getColorByString:@"radioPrimaryColor"];
}

- (UIColor *)menuBackgroundColor {
    return [self getColorByString:@"menuBackgroundColor"];
}

- (UIColor *)menuPrimaryTextColor {
    return [self getColorByString:@"menuPrimaryTextColor"];
}

- (UIColor *)menuIconColor {
    return [self getColorByString:@"menuIconColor"];
}

- (UIColor *)menuSeparatorColor {
    return [self getColorByString:@"menuSeparatorColor"];
}

- (UIColor *)primaryHighlightColor {
    return [self getColorByString:@"primaryHighlightColor"];
}

- (UIColor *)epgPrimaryColor {
    return [self getColorByString:@"epgPrimaryColor"];
}

- (UIColor *)epgHighlightColor {
    return [self getColorByString:@"epgHighlightColor"];
}

- (UIColor *)epgBackgroundColor {
    return [self getColorByString:@"epgBackgroundColor"];
}

- (UIColor *)epgTextColor {
    return [self getColorByString:@"epgTextColor"];
}

- (UIColor *)drawerSelectorColor {
    return [self getColorByString:@"drawerSelectorColor"];
}

- (UIColor *)spinnerColor {
    if ([self getParamHex:@"buttonPrimaryColor"] != nil) {
        return [self getColorByString:@"buttonPrimaryColor"]; // remote
    } else if ([PVPConfigHelper getLocalConfiguration:@"loadingSpinnerColor"] != nil) {
        return [UIColor colorWithHexString:[PVPConfigHelper getLocalConfiguration:@"loadingSpinnerColor"]]; // local
    }
    
    return [UIColor colorWithHexString:[PVPConfigHelper getLocalConfiguration:@"#FFFFFF"]]; // default white color
}

- (NSString *)logoUrl {
    return self.currentConfig && self.currentConfig.branding && self.currentConfig.branding[@"logoUrl"] ? self.currentConfig.branding[@"logoUrl"] : @"";
}

- (NSString *)newsletterUrl {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"newsletterUrl"] ? self.currentConfig.mobile[@"newsletterUrl"] : @"";
}

- (NSString *)bgImageUrl {
    
    if (self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"backgroundImages"]) {
        NSArray *urls = [self.currentConfig.mobile[@"backgroundImages"] componentsSeparatedByString:@","];
        
        uint32_t randomIndex = arc4random_uniform((uint32_t)[urls count]);

        return [urls objectAtIndex:randomIndex];
        
    } else if (self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"backgroundImage"]) {
        return self.currentConfig.mobile[@"backgroundImage"];
    }
    
    return @"";
}

- (NSString *)bgImageUrlTv {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"backgroundImageTV"] ? self.currentConfig.mobile[@"backgroundImageTV"] : @"";
}

- (NSString *)bgImageUrlLogin {
    return self.currentConfig && self.currentConfig.mobile && [self.currentConfig.mobile[@"backgroundImageLogin"] length] > 0 ? self.currentConfig.mobile[@"backgroundImageLogin"] : @"";
}

- (NSString *)bgImageUrlRegistration {
    return self.currentConfig && self.currentConfig.mobile && [self.currentConfig.mobile[@"backgroundImageRegistration"] length] > 0 ? self.currentConfig.mobile[@"backgroundImageRegistration"] : @"";
}

- (NSString *)bgImageUrlLoginTv {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"backgroundImageLoginTV"] ? self.currentConfig.mobile[@"backgroundImageLoginTV"] : @"";
}

- (NSString *)bgImageUrlRegistrationTv {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"backgroundImageRegistrationTV"] ? self.currentConfig.mobile[@"backgroundImageRegistrationTV"] : @"";
}
    
- (NSString *)externalPaidUrl {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"externalPaidUrl"] ? self.currentConfig.mobile[@"externalPaidUrl"] : @"https://panda-os.com";
}

- (NSString *)paidRequiredMessage {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"paidRequiredMessage"] ? self.currentConfig.mobile[@"paidRequiredMessage"] : @"";
}

- (NSString *)themeTypeID {
    if ( self.currentConfig.mobile[@"theme"] && [self.currentConfig.mobile[@"theme"] isKindOfClass:[NSDictionary class]] && self.currentConfig.mobile[@"theme"][@"type"] ) {
         return [self.currentConfig.mobile[@"theme"][@"type"] stringValue];
    }
    return @"1";
}

- (NSString *)cmsBaseURL {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"cmsBaseURL"] ? self.currentConfig.mobile[@"cmsBaseURL"] : @"";
}

- (NSString *)shareText {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"shareText"] ? self.currentConfig.mobile[@"shareText"] : @"";
}

- (NSString *)shareSubject {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"shareSubject"] ? self.currentConfig.mobile[@"shareSubject"] : @"";
}

- (NSString *)copyrightText {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"copyrightText"] ? self.currentConfig.mobile[@"copyrightText"] : @"";
}

- (nonnull NSString *)signUpButtonTitle {
    return self.currentConfig && self.currentConfig.mobile && [self.currentConfig.mobile[@"changeSignUpTitle"] length] > 0 ? self.currentConfig.mobile[@"changeSignUpTitle"] : @"Sign Up";
}

- (nonnull NSString *)loginButtonTitle {
    return self.currentConfig && self.currentConfig.mobile && [self.currentConfig.mobile[@"changeLoginTitle"] length] > 0 ? self.currentConfig.mobile[@"changeLoginTitle"] : @"Login";
}

- (BOOL)hideLogin {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"hideLogin"] ? [self.currentConfig.mobile[@"hideLogin"] boolValue] : NO;
}

- (BOOL)hideMenuSettings {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"hideMenuSettings"] ? [self.currentConfig.mobile[@"hideMenuSettings"] boolValue] : NO;
}

- (BOOL)showToolbarTitle {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"showToolbarTitle"] ? [self.currentConfig.mobile[@"showToolbarTitle"] boolValue] : YES;
}

- (BOOL)enablePoweredBy {
    return [self isParamInMobileConfigDefined:@"poweredBy"] ? [self.currentConfig.mobile[@"poweredBy"] boolValue] : NO;
}

- (BOOL)enableSwitchAccount {
    return [self isParamInMobileConfigDefined:@"enableSwitchAccount"] ? [self.currentConfig.mobile[@"enableSwitchAccount"] boolValue] : NO;
}

- (BOOL)enablePromoCode {
    return [self isParamInMobileConfigDefined:@"allowPromoCode"] ? [self.currentConfig.mobile[@"allowPromoCode"] boolValue] : NO;
}

- (BOOL)enableGender {
    return [self isParamInUserConfigDefined:@"requireGender"] ? [self.currentConfig.user[@"requireGender"] boolValue] : NO;
}

- (BOOL)enablePhone {
    return [self isParamInUserConfigDefined:@"requirePhone"] ? [self.currentConfig.user[@"requirePhone"] boolValue] : NO;
}

- (BOOL)enableDOB {
    return [self isParamInUserConfigDefined:@"requireDateOfBirth"] ? [self.currentConfig.user[@"requireDateOfBirth"] boolValue] : NO;
}


- (BOOL)showScheduledEntries {
    return [self isParamInMobileConfigDefined:@"showScheduledEntries"] ? [self.currentConfig.mobile[@"showScheduledEntries"] boolValue] : NO;
}

- (BOOL)useCdnApi {
    return [self isParamInMobileConfigDefined:@"useCdnApi"] ? [self.currentConfig.mobile[@"useCdnApi"] boolValue] : NO;
}

- (NSString *)wowzaRtmpServer {
    return self.currentConfig && self.currentConfig.live && self.currentConfig.live[@"wowzaRtmpServer"] ? self.currentConfig.live[@"wowzaRtmpServer"] : @"";
}

- (NSString *)wowzaAppName {
    return self.currentConfig && self.currentConfig.live && self.currentConfig.live[@"appName"] ? self.currentConfig.live[@"appName"] : @"";
}

- ( NSString * ) piwikBaseUrl {
    return self.currentConfig && self.currentConfig.analytics && self.currentConfig.analytics[@"panda"] &&self.currentConfig.analytics[@"panda"][@"url"] ? self.currentConfig.analytics[@"panda"][@"url"] : @"";
}

- ( NSString * ) termsOfServiceUrl {
    NSString *path = [self isParamInMobileConfigDefined:@"terms"] ? self.currentConfig.mobile[@"terms"] : @"";
    if (![path containsString:@"http://"] && ![path containsString:@"https://"]) {
        path = [[PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"] stringByAppendingString:path];
    }
    return path;
}

- ( NSString * ) privacyPolicyUrl {
    NSString *path = [self isParamInMobileConfigDefined:@"privacyPolicy"] ? self.currentConfig.mobile[@"privacyPolicy"] : @"";
    if (![path containsString:@"http://"] && ![path containsString:@"https://"]) {
        path = [[PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"] stringByAppendingString:path];
    }
    return path;
}

- ( NSString * ) currentInstanceId {
    return self.currentConfig && self.currentConfig.currentInstanceId ? self.currentConfig.currentInstanceId : @"";
}

- ( NSString * ) kalturaServiceUrl {
    return self.currentConfig && self.currentConfig.kaltura && self.currentConfig.kaltura[@"serviceUrl"] ? self.currentConfig.kaltura[@"serviceUrl"] : @"";
}

- ( NSString * ) kalturaCDNUrl {
    NSString *cdnUrl;
    if (self.currentConfig) {
        if ([self isParamInMobileConfigDefined:@"cdnUrl"]) {
            cdnUrl = self.currentConfig.mobile[@"cdnUrl"];
        } else if (self.currentConfig.kaltura && self.currentConfig.kaltura[@"cdnUrl"]) {
            cdnUrl = self.currentConfig.kaltura[@"cdnUrl"];
        }
    }
    
    if (!cdnUrl) {
        cdnUrl = [self kalturaServiceUrl];
    }
    
    return cdnUrl;
}

- ( NSString * ) kalturaStatsUrl {
    return self.currentConfig && self.currentConfig.kaltura && self.currentConfig.kaltura[@"statsUrl"] ? self.currentConfig.kaltura[@"statsUrl"] : @"https://videostats.panda-os.com/api_v3/index.php";
}

- ( NSString * ) kalturaApiUploadUrl {
    return [NSString stringWithFormat:@"%@api_v3/?service=uploadToken&action=upload", [self kalturaServiceUrl]];
}

- ( NSString * ) partnerId {
    return self.currentConfig && self.currentConfig.kaltura && self.currentConfig.kaltura[@"partnerId"] ? self.currentConfig.kaltura[@"partnerId"] : @"";
}

- ( NSString * ) timezoneString {
    return self.currentConfig && self.currentConfig.general && self.currentConfig.general[@"timezoneString"] ? self.currentConfig.general[@"timezoneString"] : @"utc";
}

- ( NSString * ) playerUiConfId {
    NSString *uiconfId = @"";
    if (self.currentConfig && self.currentConfig.player) {
        uiconfId = self.currentConfig.player[@"uiconfIdMobile"];
        if (!uiconfId || uiconfId.length == 0) {
            uiconfId = self.currentConfig.player[@"uiconfId"]; //fallback, in case mobile player is not available
        }
    }

    BOOL isString = [uiconfId isKindOfClass:[NSString class]];
    if ( !isString ) {
        NSString *stringFormat = [uiconfId isKindOfClass:[NSNumber class]] ? @"%@" : @"%d";
        uiconfId = [NSString stringWithFormat: stringFormat, uiconfId];
    }

    return uiconfId;
}

- ( BOOL ) upNextEnabled {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"upNextEnabled"] boolValue] : NO;
}

- ( BOOL ) googleIMAEnabled {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"googleIMAEnabled"] boolValue] : NO;
}

- ( BOOL ) googleMobileAdsEnabled {
  // before the server was not returning actual value so forcefully set to true, uncomment the code 
//    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"googleMobileAdsEnabled"] boolValue] : NO;
    return true;
}

- ( BOOL ) googleMobileAdsEnabledAndSet {

    NSString *googleMobileAdsID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GADApplicationIdentifier"];

    BOOL set = googleMobileAdsID != (id)[NSNull null] && [googleMobileAdsID length] > 0;
    BOOL enabled = [self googleMobileAdsEnabled];
    BOOL enabledAndSet = enabled && set;

    return enabledAndSet;
}

- ( NSString * ) googleMobileAdsAdUnitID {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"googleMobileAdsAdUnitID_ios"] : @"";
}


- ( BOOL ) googleMobileAdsTagForChildDirectedTreatment {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"googleMobileAdsTagForChildDirectedTreatmentEnabled"] boolValue] : NO;
}

- ( BOOL ) awesomeAdsEnabled {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"awesomeAdsEnabled"] boolValue] : NO;
}

- ( BOOL ) awesomeAdsCloseButtonEnabled {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? [ self.currentConfig.mobile[@"player"][@"awesomeAdsCloseButtonEnabled"] boolValue] : NO;
}


- ( NSString * ) awesomeAdsBannerPlacementID {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"awesomeAdsBannerPlacementID_ios"] : @"";
}

- ( NSString * ) awesomeAdsPrerollPlacementID {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"awesomeAdsPrerollPlacementID_ios"] : @"";
}

- ( NSString * ) playerTemplateLocation {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"templateLocation"] : @"";
}

- ( NSString * ) playerEmbedSrc {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"embedSrc"] : @"";
}

- ( NSString * ) appstoreURL {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"appstoreURL"] : @"";
}

- ( NSString * ) userIDFA {

  //  [[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]  was returning null so we used "00000000-0000-0000-0000-000000000000" value to check Advertising Tracking Enabled
  /// In case of isAdvertisingTrackingEnabled false idfa value  00000000-0000-0000-0000-000000000000
  NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
  if ([idfaString isEqualToString:@"00000000-0000-0000-0000-000000000000"] || idfaString == nil) {
    return [[PVPConfigHelper sharedInstance] uniqueDeviceIdentifier];
  } else {
    return idfaString;
  }
}

- ( NSString * ) baseDomainEmbedSrc {
    return self.currentConfig.mobile && [self.currentConfig.mobile isKindOfClass:NSDictionary.class] && self.currentConfig.mobile[@"player"] && [ self.currentConfig.mobile[@"player"] isKindOfClass:NSDictionary.class ] ? self.currentConfig.mobile[@"player"][@"baseUrl"] : @"";
}

- ( NSString * ) playerUrl {
    NSString *playerUrl = self.currentConfig && self.currentConfig.player ? self.currentConfig.player[@"playerUrl"] : @"";
    playerUrl = [playerUrl stringByReplacingOccurrencesOfString:@"mwEmbedLoader.php" withString:@"mwEmbedFrame.php"];
    
    if([playerUrl hasPrefix:@"//"]) {
        playerUrl = [NSString stringWithFormat:@"http:%@", playerUrl];
    }
    
    return playerUrl;
}

- ( NSString * ) playerBeaconUrl {
    NSString *beaconUrl;
    if (self.currentConfig && self.currentConfig.player && self.currentConfig.player[@"beaconEnabled"] && [self.currentConfig.player[@"beaconEnabled"] boolValue]) {
        beaconUrl = self.currentConfig && self.currentConfig.player && self.currentConfig.player[@"beaconUrl"] ? self.currentConfig.player[@"beaconUrl"] : nil;
        if (beaconUrl) {
            if (![beaconUrl containsString:@"http://"] && ![beaconUrl containsString:@"https://"]) {
                beaconUrl = [@"https:" stringByAppendingString:beaconUrl];
            }
        }
    }
    return beaconUrl;
}

- (NSArray *)eventProductIdentifiers {
    return self.currentConfig.purchases && self.currentConfig.purchases[@"apple"] && self.currentConfig.purchases[@"apple"][@"products"] && self.currentConfig.purchases[@"apple"][@"products"][@"events"] ? self.currentConfig.purchases[@"apple"][@"products"][@"events"] : @[];
}

- (nonnull NSArray *)ppvProductIdentifiers {
    return [self.currentConfig.purchases safeValueForKeyPath:@"apple.products.ppv" withDefaultValue:@[]];
}

- (NSArray *)subscriptionPlanProductIdentifiers {
    return [self.currentConfig.purchases safeValueForKeyPath:@"apple.products.subscriptionPlans" withDefaultValue:@[]];
}

- (void)goBack {
    NSLog(@"Go Back triggered");
    if([self.backHistoryStack count] == 0) {
        return;
    }
    
    NSInteger currentCategoryIndex = [self.backHistoryStack count] - 1;// where I am
    
    if (currentCategoryIndex >= 1) {
        NSDictionary *targetPastCategory = self.backHistoryStack[currentCategoryIndex - 1];// Get previous item
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
            [self.backHistoryStack removeLastObject];// we remove only current back item and keeping target as target won't be re-added in categoryChanged
 
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ROUTER_LOAD object:self userInfo:targetPastCategory];
        }];
    } else { //Single item go back to HOME, don't pop the history item
        NSDictionary *targetPastCategory = [self.backHistoryStack firstObject];// going back to main
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ROUTER_LOAD object:self userInfo:targetPastCategory];
//            [self.backHistoryStack removeLastObject]; //No need to remove when doing 1 object
        }];
    }
}

-(void)addBackItem:(NSDictionary *)userInfo withId:(NSString *)notificationID {
    NSString *stackID = [NSString stringWithFormat:@"%@", [self.backHistoryStack lastObject][@"id"] ];
    
    if ( ![ notificationID isEqualToString:stackID ] ) {
        [self.backHistoryStack addObject:userInfo];
    }
}

- (NSArray *)sideNavItems {
    if (!self.currentConfig.mobile || ![self.currentConfig.mobile isKindOfClass:NSDictionary.class]) {
        self.currentConfig.mobile = @{};
    }
    NSArray *navItems;
    if (self.currentConfig.mobile[@"navMenu"] && [self.currentConfig.mobile[@"navMenu"] isKindOfClass:[NSArray class]]) {
        navItems = self.currentConfig.mobile[@"navMenu"];
    } else {
        navItems = @[];
    }

    return navItems;
}

- (NSDictionary *)firstNativeSideNavItem {
    for (NSDictionary *item in [self sideNavItems]) {
        if ([[PVPFlutterHelper sharedInstance] isFlutterPage:item[@"type"]]) {
            continue;
        }
        
        return item;
    }
        
    return nil;
}


- (NSDictionary *) handleFilter: (NSMutableDictionary*)filter {
    //Migrate all now parameters to real time for advanced queries
    if(filter != nil) {
        for (NSString* key in filter) {
            NSString* value = [NSString stringWithFormat:@"%@", filter[key]];
            if([value isEqualToString:@"now()"]) {
                NSDate *now = [NSDate date];
                NSTimeInterval nowEpochSeconds = [now timeIntervalSince1970];
                NSNumber *nowEpochSecondsNumber = [[NSNumber alloc] initWithDouble:nowEpochSeconds];
                [filter setObject:nowEpochSecondsNumber forKey:key];
            }
        }
    }
    
    return filter;
}

- (NSArray *)bottomNavItems {
    if (!self.currentConfig.mobile || ![self.currentConfig.mobile isKindOfClass:NSDictionary.class]) {
        self.currentConfig.mobile = @{};
    }
    
    NSArray *navItems;
    if (self.currentConfig.mobile[@"bottomNavMenu"] && [self.currentConfig.mobile[@"bottomNavMenu"] isKindOfClass:[NSArray class]]) {
        navItems = self.currentConfig.mobile[@"bottomNavMenu"];
    } else {
        navItems = @[];
    }
    
    return navItems;
}

- (NSArray *) entryCategories {
    if (!self.currentConfig.home || ![self.currentConfig.home isKindOfClass:NSDictionary.class]) {
        self.currentConfig.home = @{};
    }
    NSMutableArray *categories = [NSMutableArray arrayWithCapacity:5];
    if (self.currentConfig.home[@"featuredCategory"] && [self.currentConfig.home[@"featuredCategory"] isKindOfClass:[NSString class]]) {
        [categories addObject:self.currentConfig.home[@"featuredCategory"]];
    }
    if (self.currentConfig.home[@"categories"] && [self.currentConfig.home[@"categories"] isKindOfClass:[NSArray class]]) {
        [categories addObjectsFromArray:self.currentConfig.home[@"categories"]];
    }
    if ([self isOfflineSupportEnabled] && ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad || [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)) {
        [categories addObject:CATEGORY_OFFLINE_TITLE];
    }
    return [categories copy];
}

- (NSString *) uniqueDeviceIdentifier {
    return [[AppUtils getDefaultValueFofKey:IS_SIMULATOR_KEY] boolValue] ? @"simulator" : [[UIDevice currentDevice] identifierForVendor].UUIDString;
}

- (BOOL) hlsEnabled {
    return self.currentConfig && self.currentConfig.hls && self.currentConfig.hls[@"enable"] ? [self.currentConfig.hls[@"enable"] boolValue] : YES;
}

- (BOOL) hasApplePurchases {
    return self.currentConfig.purchases && self.currentConfig.purchases[@"apple"];
}

- (BOOL) enableTvPurchases {
    return [self isParamInMobileConfigDefined:@"enableTvPurchases"] ? [self.currentConfig.mobile[@"enableTvPurchases"] boolValue] : NO;
}

- (BOOL) useNativePlayer {
    return [self isParamInMobileConfigDefined:@"nativePlayer"] ? [self.currentConfig.mobile[@"nativePlayer"] boolValue] : NO;
}

- (BOOL) drmEnabled {
    BOOL drmEnabled = [self.currentConfig.drm[@"enabled"] boolValue];

    return self.currentConfig && self.currentConfig.drm && drmEnabled && [self drmFairplayCertificatePath] && [self drmServiceUrlFairplay] ? YES : NO ;
}

- (BOOL) tokenizationEnabled {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"enableTokenization"] ? self.currentConfig.mobile[@"enableTokenization"] : NO;
}

- (NSString *)websiteText {
    if ([self isParamInMobileConfigDefined:@"websiteUrl"]) {
        return self.currentConfig.mobile[@"websiteUrl"] ?: @"";
    } else {
        return [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"] ?: @"";
    }
}

- (NSString *) drmServiceUrlFairplay {
    return self.currentConfig && self.currentConfig.drm && self.currentConfig.drm[@"drmServiceUrlFairplay"] && [self.currentConfig.drm[@"drmServiceUrlFairplay"] length] > 0 ? self.currentConfig.drm[@"drmServiceUrlFairplay"] : nil;
}

- (NSString *) drmFairplayCertificatePath {
    return self.currentConfig && self.currentConfig.drm && self.currentConfig.drm[@"drmFairplayCertificatePath"] && [self.currentConfig.drm[@"drmFairplayCertificatePath"] length] > 0 ? self.currentConfig.drm[@"drmFairplayCertificatePath"] : nil;
}

- (NSString *) playerVideoGravity {
    NSString *defaultGravity = @"aspectFit";
    NSDictionary *gravityDict = @{
                                  @"aspectFit": AVLayerVideoGravityResizeAspect,
                                  @"aspectFill": AVLayerVideoGravityResizeAspectFill,
                                  @"stretch": AVLayerVideoGravityResize
                                  };
    NSString *gravity = [self.currentConfig.mobile safeValueForKeyPath:@"player.videoGravity" withDefaultValue:defaultGravity];
    
    return gravityDict[gravity] ? gravityDict[gravity] : gravityDict[defaultGravity];
}

- (NSString *)defaultPlayerLogoUrl {
    return [self.currentConfig.mobile safeValueForKeyPath:@"player.defaultLogoUrl" withDefaultValue:nil];
}

- (BOOL)galleryStickyBannerGoogleAdsEnabled {
    return [[self.currentConfig.mobile safeValueForKeyPath:@"galleryStickyBanner.googleMobileAdsEnabled" withDefaultValue:nil] boolValue];
}

- (NSString *)galleryStickyBannerInternalBannerImage {
    return [self.currentConfig.mobile safeValueForKeyPath:@"galleryStickyBanner.internalBannerImage" withDefaultValue:nil];
}

- (NSString *)galleryStickyBannerinternalBannerLink {
    return [self.currentConfig.mobile safeValueForKeyPath:@"galleryStickyBanner.internalBannerLink" withDefaultValue:nil];
}

- (BOOL)galleryStickyBannerCloseButtonEnabled {
    return [[self.currentConfig.mobile safeValueForKeyPath:@"galleryStickyBanner.internalBannerCloseButton" withDefaultValue:nil] boolValue];
}

- (NSString *) adTagUrl {
    NSString *tag = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomTV) {
        tag = [self.currentConfig.mobile safeValueForKeyPath:@"player.adTagUrlTV" withDefaultValue:nil];
    }
    if (tag == nil) {
//      tag =  @"https://pubads.g.doubleclick.net/gampad/ads?iu=/21775744923/external/single_preroll_skippable&sz=640x480&ciu_szs=300x250%2C728x90&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator=";
     tag = [self.currentConfig.mobile safeValueForKeyPath:@"player.adTagUrl" withDefaultValue:nil];
     
    }
    return tag;
}

- (BOOL) isLoginRequired {
    return [self isParamInMobileConfigDefined:@"loginRequired"] ? [self.currentConfig.mobile[@"loginRequired"] boolValue] : NO;
}

- (BOOL) isLoginWithUdid {
    return [self isParamInMobileConfigDefined:@"loginWithUdid"] ? [self.currentConfig.mobile[@"loginWithUdid"] boolValue] : NO;
}

- (BOOL)isPaidSubscriptionRequired {
    return [self isParamInMobileConfigDefined:@"paidSubscriptionRequired"] ? [self.currentConfig.mobile[@"paidSubscriptionRequired"] boolValue] : NO;
}

- (BOOL) isShareButtonEnabled {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"allowShare"] ? [self.currentConfig.mobile[@"allowShare"] boolValue] : NO;
}

- (BOOL) isCastButtonEnabled {
    return self.currentConfig && self.currentConfig.mobile && self.currentConfig.mobile[@"allowCast"] ? [self.currentConfig.mobile[@"allowCast"] boolValue] : NO;
}

- (BOOL) isDownloadLinkEnabled {
    return self.currentConfig && self.currentConfig.entry && self.currentConfig.entry[@"enableDownloadLink"] ? [self.currentConfig.entry[@"enableDownloadLink"] boolValue] : NO;
}

- (BOOL) isOfflineSupportEnabled {
    return self.currentConfig && [self isParamInMobileConfigDefined:@"offlineMode"]? [self.currentConfig.mobile[@"offlineMode"] boolValue] : NO;
}

- (BOOL) showChannelTitles {
    return [self isParamInMobileConfigDefined:@"showChannelTitles"] ? [self.currentConfig.mobile[@"showChannelTitles"] boolValue] : YES;
}

- (BOOL) isSearchEnabled {
    return [self isParamInMobileConfigDefined:@"enableSearch"] ? [self.currentConfig.mobile[@"enableSearch"] boolValue] : YES;
}

- (BOOL) isMenuSearchEnabled {
    return [self isParamInMobileConfigDefined:@"enableMenuSearch"] ? [self.currentConfig.mobile[@"enableMenuSearch"] boolValue] : NO;
}

- (BOOL) isHistoryEnabled {
    return [self isParamInMobileConfigDefined:@"historyEnabled"] ? [self.currentConfig.mobile[@"historyEnabled"] boolValue] : NO;
}

- (BOOL) allowSkip {
    return [self isParamInMobileConfigDefined:@"allowSkip"] ? [self.currentConfig.mobile[@"allowSkip"] boolValue] : NO;
}

- (BOOL) disableRegistration {
    return [self isParamInMobileConfigDefined:@"disableRegistration"] ? [self.currentConfig.mobile[@"disableRegistration"] boolValue] : NO;
}

- (BOOL) disableGeoBlockOnTV {
    return [self isParamInMobileConfigDefined:@"disableGeoBlockOnTV"] ? [self.currentConfig.mobile[@"disableGeoBlockOnTV"] boolValue] : NO;
}

- (BOOL)isCutomHomeIcon {
    return [[PVPConfigHelper getLocalConfiguration:@"customHomeIcon"] boolValue];
}

- (BOOL) isCountryAllowed {
    
    BOOL ignoreGeoBlock = NO;
    
    #if TARGET_OS_TV
    ignoreGeoBlock = [self disableGeoBlockOnTV];
    #endif
    
    if (ignoreGeoBlock) {
        return YES;
    }

    NSString *currentCountry = [PVPConfigHelper getLocalConfiguration:@"X-Bamboo-Country"];
    NSString *currentGpsCountry = [AppUtils getDefaultValueFofKey:@"currentCountry"];
    if (currentCountry && [currentCountry length] > 0 && self.currentConfig && self.currentConfig.application && self.currentConfig.application[@"blockedCountries"] && ![self.currentConfig.application[@"blockedCountries"] isEqual:[NSNull null]]) {
        for (NSString *blockedCountry in self.currentConfig.application[@"blockedCountries"]) {
            if (currentGpsCountry == nil || [ currentGpsCountry isEqualToString:@"" ] ) {
                //If no GPS country we only check by IP
                if ([blockedCountry caseInsensitiveCompare:currentCountry] == NSOrderedSame) {
                    return NO;
                }
            } else {
                //If we have GPS we check both GPS and IP are from forbidden country
                if ([blockedCountry caseInsensitiveCompare:currentCountry] == NSOrderedSame && [blockedCountry caseInsensitiveCompare:currentGpsCountry] == NSOrderedSame) {
                    return NO;
                }
            }
        }
    }
    return YES;
}

- (NSString *)getMinimumVersion {
    NSString *os = @"ios";
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomTV) {
        os = @"tvos";
    }
    NSString *path = [NSString stringWithFormat:@"minimumVersion.%@", os];
    return self.currentConfig && self.currentConfig.mobile ? [self.currentConfig.mobile safeValueForKeyPath:path withDefaultValue:@"0"] : @"0";
}

- (NSString *)appStoreLink {
    NSString *link = nil;
    if (self.currentConfig && self.currentConfig.mobile) {
        link = [self.currentConfig.mobile safeValueForKeyPath:@"appStoreLink.ios" withDefaultValue:nil]; //check for specific iOS link
        if (link == nil) {
            link = [self.currentConfig.mobile safeValueForKeyPath:@"appStoreLink.universal" withDefaultValue:nil]; //try using the universal link as fallback
        }
    }
    return link;
}

- (BOOL) catchupEnabled {
    return [self isParamInMobileConfigDefined:@"catchUp"] ? [self.currentConfig.mobile[@"catchUp"] boolValue] : NO;
}

- (NSString *)googleAnalyticsTrackingId {
    return self.currentConfig && self.currentConfig.mobile ? [self.currentConfig.mobile safeValueForKeyPath:@"analytics.google.trackingId" withDefaultValue:nil] : nil;
}

- (BambooGalleryType)playPageRelatedEntriesViewType {
    NSString *typeString = [self isParamInMobileConfigDefined:@"playPageRelatedEntriesViewType"] ? self.currentConfig.mobile[@"playPageRelatedEntriesViewType"]: nil;
    
    return [self galleryTypeFromString:typeString defaultType:BambooGalleryTypeTwoCol];
}

- (BambooGalleryType)searchViewType {
    NSString *typeString = [self isParamInMobileConfigDefined:@"searchViewType"] ? self.currentConfig.mobile[@"searchViewType"]: nil;
    
    return [self galleryTypeFromString:typeString defaultType:BambooGalleryTypeSearchEntry];
}

- (nullable NSString *)blockingImage {
    return [self isParamInMobileConfigDefined:@"blockingImage"] ? self.currentConfig.mobile[@"blockingImage"]: nil;
}

- (BOOL)disableMenuIcons {
    return [self isParamInMobileConfigDefined:@"disableMenuIcons"] ? [self.currentConfig.mobile[@"disableMenuIcons"] boolValue] : NO;
}

- (BOOL) forceRTL {
    return [self isParamInMobileConfigDefined:@"forceRtl"] ? [self.currentConfig.mobile[@"forceRtl"] boolValue] : NO;
}

- (BOOL) enableTvRegistration {
    return [self isParamInMobileConfigDefined:@"enableTvRegistration"] ? [self.currentConfig.mobile[@"enableTvRegistration"] boolValue] : NO;
}

- (BOOL)showTvTrailers {
    return [self isParamInMobileConfigDefined:@"enableTrailer"] ? [self.currentConfig.mobile[@"enableTrailer"] boolValue] : NO;
}

- (BOOL)isAmericanFormat {
    return self.currentConfig && self.currentConfig.mobile ? [self.currentConfig.mobile safeValueForKeyPath:@"americanDate" withDefaultValue:@NO] : NO;
}

- (BOOL)enableHomepageHistory {
    return [self isParamInMobileConfigDefined:@"enableHomepageHistory"] ? [self.currentConfig.mobile[@"enableHomepageHistory"] boolValue] : NO;

}

- (BOOL)allowBackgroundPlayback {
    return [self isParamInMobileConfigDefined:@"allowBackgroundPlayback"] ? [self.currentConfig.mobile[@"allowBackgroundPlayback"] boolValue] : NO;
}

- (BOOL)showSliderTitle {
    return [self isParamInMobileConfigDefined:@"showSliderTitle"] ? [self.currentConfig.mobile[@"showSliderTitle"] boolValue] : NO;
}

+ (NSString *)getLocalConfiguration:(NSString *)name {
    if ( !plistDictionary ) { 
        [[self sharedInstance] initLocalConfig];
    }
    return plistDictionary[name] ? plistDictionary[name] : defaultLocalConfig[name];
}

+ (void)setLocalConfiguration:(NSString *)configuration forKey:(NSString *)key {
    if (!plistDictionary) {
        [[self sharedInstance] initLocalConfig];
    }
    plistDictionary[key] = configuration;
}

+ (void)setLocalDictionary:(NSDictionary *)configuration forKey:(NSString *)key {
    if (!plistDictionary) {
        [[self sharedInstance] initLocalConfig];
    }
    plistDictionary[key] = configuration;
}

+ (NSDictionary *)getLocalConfigurationDict:(NSString *)name {
    if ( !plistDictionary ) {
        [[self sharedInstance] initLocalConfig];
    }
    return plistDictionary[name] ? plistDictionary[name] : defaultLocalConfig[name];
}

+ (BOOL)alertsEnabled {
    return [[self getLocalConfiguration:@"alertsEnabled"] boolValue];
}
+ (BOOL)userIDFAEnabled {
  return [[self getLocalConfiguration:@"userIDFAEnabled"] boolValue];
}

+ (CGFloat)menuWidthPercent {
    if ([PVPConfigHelper getLocalConfiguration:@"menuWidthPercents"] != nil) {
        return [[self getLocalConfiguration:@"menuWidthPercents"] floatValue];
    }
    return 0.7;
}

+ (BOOL)isCustomMenuIcon {
    return [[self getLocalConfiguration:@"customMenuIcon"] boolValue];
}

+ (BOOL)monitorReachability {
    return [[self getLocalConfiguration:@"monitorReachability"] boolValue];
}

+ (NSString *)pvpServerPrefix {
    return [PVPConfigHelper getLocalConfiguration:@"pvpServerPrefix"];
}

+ (NSString *)pvpServerSuffix {
    return [PVPConfigHelper getLocalConfiguration:@"pvpServerSuffix"];
}

- ( BOOL ) pushwooshEnabled {
    return [[PVPConfigHelper getLocalConfiguration:@"pushwooshEnabled"] boolValue];
}

- (BOOL) oneSignalEnabled {
    return [[PVPConfigHelper getLocalConfiguration:@"oneSignalEnabled"] boolValue];
}

- (NSString *) oneSignalKey {
    return [PVPConfigHelper getLocalConfiguration:@"oneSignalKey"];
}

- ( BOOL ) branchIoEnabled {
    return [[PVPConfigHelper getLocalConfiguration:@"branchIoEnabled"] boolValue];
}

- (NSString *)appsFlyerDevKey {
    return [PVPConfigHelper getLocalConfiguration:@"appsFlyerDevKey"];
}

- (NSString *)appleAppID {
    return [PVPConfigHelper getLocalConfiguration:@"appleAppID"];
}

+ (NSString *)tokenField {
    return [PVPConfigHelper getLocalConfiguration:@"tokenField"];
}

+ (BOOL) debugEnabled {
    return [[PVPConfigHelper getLocalConfiguration:@"debugEnabled"] boolValue];
}

+ (BOOL)hasVisitedApp {
    return [[self getLocalConfiguration:DEFAULTS_KEY_HAS_VISITED_APP] boolValue];
}

+ (BOOL)encryptPurchases {
    return [PVPConfigHelper getLocalConfiguration:@"purchasesSharedSecret"] != nil;
}

- (BOOL)showStillWatchingDialog {
    return [self showStillWatchingDialogDelay] > 0;
}

- (int)showStillWatchingDialogDelay {
    return [self isParamInMobileConfigDefined:@"stillWatchingDialogDelay"] ? [self.currentConfig.mobile[@"stillWatchingDialogDelay"] intValue] : 0;
}

- (NSString *)offlineDownloadPackage {
    return self.currentConfig && [self isParamInMobileConfigDefined:@"offlinePackage"]? self.currentConfig.mobile[@"offlinePackage"] : nil;
}

- (NSInteger )epgTimeSlot {
    
#if TARGET_OS_IOS
    return self.currentConfig && [self isParamInChannelsConfigDefined:@"epgTimeSlot"] ? [self.currentConfig.channels[@"epgTimeSlot"] intValue] : 900;
#else
    if (self.currentConfig) {
        if ([self isParamInChannelsConfigDefined:@"tvEpgTimeSlot"]) {
            return [self.currentConfig.channels[@"tvEpgTimeSlot"] intValue];
        } else {
            if ([self isParamInChannelsConfigDefined:@"epgTimeSlot"]) {
                return [self.currentConfig.channels[@"epgTimeSlot"] intValue];
            } else {
                return 1800;
            }
        }
    } else {
        return 1800;
    }
#endif
}

- (BOOL) hideChannelName {
    return [self isParamInMobileConfigDefined:@"hideChannelName"] ? [self.currentConfig.mobile[@"hideChannelName"] boolValue] : NO;
}

- (BambooGalleryType) galleryTypeFromString:(NSString *)typeString defaultType:(BambooGalleryType)defaultType {
    
    return ((BambooGalleryType (^)(void))@{
        @"two_columns" : ^{
        return BambooGalleryTypeTwoCol;
    },
        @"one_column" : ^{
        return BambooGalleryTypeOneCol;
    },
        @"channels" : ^{
        return BambooGalleryTypeChannels;
    },
        @"offline" : ^{
        return BambooGalleryTypeOffline;
    },
        @"video_list" : ^{
        return BambooGalleryTypeList;
    },
        @"live" : ^{
        return BambooGalleryTypeLive;
    },
        @"history" : ^{
        return BambooGalleryTypeWatchHistory;
    },
        @"search_entry" : ^{
        return BambooGalleryTypeSearchEntry;
    },
        @"category": ^{
        return BambooGalleryTypeTwoCol;
    }}[[typeString lowercaseString]] ?: ^{ // default
        return defaultType;
    })();
}

- (BOOL) isOnboardingAlwaysEnabled {
    if ([self isParamInMobileConfigDefined:@"onboarding"]) {
        NSDictionary *onboardingDict = self.currentConfig.mobile[@"onboarding"];
        
        if ([[onboardingDict allKeys] count] > 0) {
            return [onboardingDict[@"alwaysVisible"] boolValue];
        }
    }
    
    return NO;
}

- (BOOL) isOnboardingButtonsEnabled {
    if ([self isParamInMobileConfigDefined:@"onboarding"]) {
        NSDictionary *onboardingDict = self.currentConfig.mobile[@"onboarding"];
        
        if ([[onboardingDict allKeys] count] > 0) {
            return [onboardingDict[@"onboardingButtons"] boolValue];
        }
    }
    
    return NO;
}

- (NSArray <PVPOnboardingItem *>* _Nullable)onboardingItems {
    if ([self isParamInMobileConfigDefined:@"onboarding"]) {
        NSDictionary *onboardingDict = self.currentConfig.mobile[@"onboarding"];
        NSArray *jsonArray = onboardingDict[@"onboardingValues"];
        NSMutableArray *items = [NSMutableArray new];
        
        for (NSDictionary *itemDict in jsonArray) {
            PVPOnboardingItem *item = [PVPOnboardingItem new];
            item.title = itemDict[@"title"];
            item.itemDescription = itemDict[@"description"];
            item.imageUrl = itemDict[@"imageUrl"];
            item.order = [itemDict[@"arrayOrder"] integerValue];
            
            [items addObject:item];
        }
        
        [items sortUsingComparator:^NSComparisonResult(PVPOnboardingItem *  _Nonnull obj1, PVPOnboardingItem * _Nonnull obj2) {
            return obj1.order > obj2.order;
        }];
        
        return items;
    }
    
    return nil;
}
// "omidPartnerName": "",
// "omidPartnerVersion": "",
// "omidVersion": ""
- ( PVPPalConfig * ) getPalSDKConfigs {
    if ([self isParamInMobileConfigDefined:@"player"]) {
        
        NSDictionary *value = self.currentConfig.mobile[@"player"];
        NSString *name = value[@"omidPartnerName"];
        NSString *partnerVersion = value[@"omidPartnerVersion"];
        NSString *version = value[@"omidVersion"];
        
        if (name != nil
            && partnerVersion != nil
            && version != nil) {
            PVPPalConfig *palconfig = [[PVPPalConfig alloc] init];
            palconfig.omidVersion = version;
            palconfig.omidPartnerName = name;
            palconfig.omidPartnerVersion = partnerVersion;
            return palconfig;
        }
    }
    return nil;
}

- ( PVPInAppSDKConfig * ) getInAppSDKConfig {
    if ([self isParamInMobileConfigDefined:@"inAppAdSdk"]) {
        NSDictionary *value = self.currentConfig.mobile[@"inAppAdSdk"];
        NSString *publisherId = value[@"publisherId"];
        NSString *tagId = value[@"tagId"];
        NSString *testPublisherId = value[@"publisherId_bak"];
        NSString *testTagId = value[@"tagId_bak"];
        
        if (publisherId != nil
            && tagId != nil
            && testPublisherId != nil
            && testTagId != nil) {
            PVPInAppSDKConfig *config = [[PVPInAppSDKConfig alloc] init];
            config.publisherId = publisherId;
            config.tagId = tagId;
            config.testPublisherId = testPublisherId;
            config.testTagId = testTagId;
            return config;
        }
    }
    return nil;
}

@end
