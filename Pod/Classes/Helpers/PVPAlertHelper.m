//
//  PVPAlertHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 1/29/16.
//
//

#import "PVPAlertHelper.h"

@interface PVPAlertHelper ()

@property (strong, nonatomic) UIWindow *alertWindow;

@end

@implementation PVPAlertHelper

static BOOL isSetup = NO;
static PVPAlertHelper *sharedInstance = nil;
static dispatch_once_t onceToken;

+(instancetype)sharedInstance
{
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup
{
    if (self.alertWindow == nil) {
        self.alertWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.alertWindow.rootViewController = [UIViewController new];
        self.alertWindow.windowLevel = UIWindowLevelAlert + 1;
    }
}

#pragma show an alert dialog
+(void) showAlertWithDefaultErrorMessage
{
    [self showAlertWithTitle:ALERT_ERROR_TITLE andMessage:ALERT_ERROR_MESSAGE];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    [self showAlertWithTitle:title andMessage:message withHandler:nil];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withHandler:(void (^)(UIAlertController *alert, BOOL confirmed))handler
{
    [self showAlertWithTitle:title andMessage:message withCancelButtonTitle:ALERT_CANCEL withOtherButtonTitle:ALERT_OK withHandler:handler];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withCancelButtonTitle:(NSString *)cancelButtonTitle withOtherButtonTitle:(NSString *)otherButtonTitle withHandler:(void (^ __nullable)(UIAlertController *alert, BOOL confirmed))handler
{
    [self showAlertWithTitle:title andMessage:message withCancelButtonTitle:cancelButtonTitle withOtherButtonTitle:otherButtonTitle withTextFieldType:-1 withPlaceholder:nil withInitialText:nil withHandler:handler];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withButtonTitle:(NSString *)buttonTitle withHandler:(void (^ __nullable)(UIAlertController *alert, BOOL confirmed))handler
{
    [self showAlertWithTitle:title andMessage:message withCancelButtonTitle:buttonTitle withOtherButtonTitle:nil withTextFieldType:-1 withPlaceholder:nil withInitialText:nil withHandler:handler];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withCancelButtonTitle:(NSString *)cancelButtonTitle withOtherButtonTitle:(NSString *)otherButtonTitle withTextFieldType:(UIKeyboardType)keyboardType withPlaceholder:(NSString *)placeholder withInitialText:(NSString *)text withHandler:(void (^ __nullable)(UIAlertController * _Nonnull alert, BOOL confirmed))handler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    if (keyboardType > -1) {
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.keyboardType = keyboardType;
            if (placeholder != nil && placeholder.length > 0) {
                textField.placeholder = placeholder;
            }
            if (text != nil && text.length > 0) {
                textField.text = text;
            }
        }];
    }
    
    if (handler && otherButtonTitle) {
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [self destroyAlertWindow];
                                                                  handler(alert, YES);
                                                              }];
        [alert addAction:defaultAction];
    }
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:handler ? cancelButtonTitle : otherButtonTitle
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [self destroyAlertWindow];
                                                             if (handler) {
                                                                 handler(alert, NO);
                                                             }
                                                         }];
    
    [alert addAction:cancelAction];
    
    [[self sharedInstance] showAlert:alert];
}

#pragma show an action sheet
+(void) showActionSheetWithTitle:(NSString *)title andMessage:(NSString *)message withCancelButtonTitle:(NSString *)cancelButtonTitle withDestructiveButtonTitles:(NSArray<NSString *> *)destructiveButtonTitles withOtherButtonTitles:(NSArray<NSString *> *)otherButtonTitles withHandler:(void (^ __nullable)(UIAlertController *alert, BOOL confirmed, NSString *buttonTitle))handler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.popoverPresentationController.sourceView = [[self sharedInstance] alertWindow].rootViewController.view;
        alert.popoverPresentationController.sourceRect = CGRectMake(0, 0, 0, 0);
    }
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [self destroyAlertWindow];
                                                             if (handler) {
                                                                 handler(alert, NO, action.title);
                                                             }
                                                         }];
    [alert addAction:cancelAction];
    
    if (destructiveButtonTitles) {
        for (NSString *title in destructiveButtonTitles) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:title
                                                             style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self destroyAlertWindow];
                                                               if (handler) {
                                                                   handler(alert, YES, action.title);
                                                               }
                                                           }];
            [alert addAction:action];
        }
    }
    
    if (otherButtonTitles) {
        for (NSString *title in otherButtonTitles) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:title
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self destroyAlertWindow];
                                                               if (handler) {
                                                                   handler(alert, YES, action.title);
                                                               }
                                                           }];
            
            [alert addAction:action];
        }
        
    }
    
    [[self sharedInstance] showAlert:alert];
}

-(void) showAlert:(UIAlertController *)alert
{
    self.alertWindow.hidden = NO;
    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}

+(void)destroyAlertWindow
{
    [[self sharedInstance] alertWindow].hidden = YES;
}

@end
