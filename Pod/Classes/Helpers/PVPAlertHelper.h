//
//  PVPAlertHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 1/29/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppUtils.h"
#import "NSString+PVPClient.h"

/**
 *  The PVPAlertHelper provides generic convenience functions for building, showing and handling UIAlertControllers.
 */
@interface PVPAlertHelper : NSObject

/**
 *  Get singleton of the PVPAlertHelper
 *
 *  @return PVPAlertHelper singleton instance
 */
+(nonnull instancetype)sharedInstance;

/**
 *  Shows an alert with a generic error message
 */
+(void) showAlertWithDefaultErrorMessage;

/**
 *  Shows an alert with given texts for the title and message.
 *
 *  @param title   The title for the alert.
 *  @param message The message for the alert.
 */
+(void) showAlertWithTitle:(nonnull NSString *)title andMessage:(nonnull NSString *)message;

/**
 *  Shows an alert with given texts for the title and message, and a handler function for the "OK" button.
 *
 *  @param title   The title for the alert.
 *  @param message The message for the alert.
 *  @param handler The handler function for the "OK" button.
 */
+(void) showAlertWithTitle:(nonnull NSString *)title andMessage:(nonnull NSString *)message withHandler:(void (^ __nullable)( UIAlertController * _Nonnull alert, BOOL confirmed))handler;

/**
 *  Shows an alert with given texts for the title, message and buttons, and a handler function for the "OK" button.
 *
 *  @param title       The title for the alert.
 *  @param message     The message for the alert.
 *  @param buttonTitle The title for the "OK" button.
 *  @param handler     The handler function for the "OK" button.
 */
+(void) showAlertWithTitle:(nonnull NSString *)title andMessage:(nonnull NSString *)message withButtonTitle:(nonnull NSString *)buttonTitle withHandler:(void (^ __nullable)(UIAlertController * _Nonnull alert, BOOL confirmed))handler;

/**
 *  Shows an alert with given texts for the title, message and buttons, and a handler function for the "OK" button.
 *
 *  @param title             The title for the alert.
 *  @param message           The message for the alert.
 *  @param cancelButtonTitle The title for the "cancel" button.
 *  @param otherButtonTitle  The title for the "OK" button.
 *  @param handler           The handler function for the "OK" button.
 */
+(void) showAlertWithTitle:(nonnull NSString *)title andMessage:(nonnull NSString *)message withCancelButtonTitle:(nonnull NSString *)cancelButtonTitle withOtherButtonTitle:(nonnull NSString *)otherButtonTitle withHandler:(void (^ __nullable)(UIAlertController * _Nonnull alert, BOOL confirmed))handler;

/**
 *  Shows an alert with given texts for the title, message and buttons, a text field, and a handler function for the "OK" button.
 *
 *  @param title             The title for the alert.
 *  @param message           The message for the alert.
 *  @param cancelButtonTitle The title for the "cancel" button.
 *  @param otherButtonTitle  The title for the "OK" button.
 *  @param keyboardType      The keyboard type for the text field.
 *  @param placeholder       The placeholder text for the text field. nil would result in an empty string.
 *  @param text              The initial text for the text field. nil would result in an empty string.
 *  @param handler           The handler function for the "OK" button.
 */
+(void) showAlertWithTitle:(nonnull NSString *)title andMessage:(nonnull NSString *)message withCancelButtonTitle:(nonnull NSString *)cancelButtonTitle withOtherButtonTitle:(nullable NSString *)otherButtonTitle withTextFieldType:(UIKeyboardType)keyboardType withPlaceholder:(nullable NSString *)placeholder withInitialText:(nullable NSString *)text withHandler:(void (^ __nullable)(UIAlertController * _Nonnull alert, BOOL confirmed))handler;

/**
 *  Shows an action sheet alert with given texts for the title, message and buttons, and a handler function for the buttons.
 *
 *  @param title                   The title for the alert.
 *  @param message                 The message for the alert.
 *  @param cancelButtonTitle       The title for the "cancel" button.
 *  @param destructiveButtonTitles The titles for the destructive (red) buttons.
 *  @param otherButtonTitles       The titles for the other (default style) buttons.
 *  @param handler                 A handler for all of the buttons.
 */
+(void) showActionSheetWithTitle:(nullable NSString *)title andMessage:(nullable NSString *)message withCancelButtonTitle:(nullable NSString *)cancelButtonTitle withDestructiveButtonTitles:(nullable NSArray<NSString *> *)destructiveButtonTitles withOtherButtonTitles:(nullable NSArray<NSString *> *)otherButtonTitles withHandler:(void (^ __nullable)(UIAlertController * _Nonnull alert, BOOL confirmed, NSString * _Nonnull buttonTitle))handler;


@end
