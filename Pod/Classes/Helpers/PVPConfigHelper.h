//
//  PVPConfigHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 3/9/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPConfig.h"
#import "PVPOnboardingItem.h"
#import "PVPPalConfig.h"
#import "PVPInAppSDKConfig.h"
/**
 *  The PVPConfigHelper provides an interface for managing the current configuration of the app and environment.
 *  Once the PVPClient instance is initialized, the singleton instance of this class will contain the instance config.
 *  We can use the singleton instance of the class to access the current instance config directly, or (better) use the convenienct methods to safely access the properties we need.
 *  This class also manages the app's local configuration, determined by the PVPSettings.plist file.
 */
@interface PVPConfigHelper : NSObject

/**
 *  @name Public Methods
 */

- (nullable NSString *)getCurrentConfigString;
/**
* Checks whether param is defined in !mobile! config.
*
* @return YES or NO, indicating whether param is defined in mobile config.
*/
- (BOOL)isParamInMobileConfigDefined:(NSString *)param;

/**
 *  Generates and returns the param color configured in the instance, as a hex string.
 *
 *  @return The param color configured in the instance, as a hex string.
 */
- (nullable NSString *)getParamHex : (nonnull NSString *)param;

/**
 *  Generates and returns the needed color configured in the instance.
 *
 *  @return The needed color configured in the instance.
 */
- (nonnull UIColor *)getColorByString : (nonnull NSString *)neededColor;

- (nonnull UIColor *)primaryColor;
- (nonnull UIColor *)secondaryColor;

- (nonnull UIColor *)playPagePrimaryTextColor;
- (nonnull UIColor *)playPageSecondaryTextColor;

- (nonnull UIColor *)loginPagePrimaryTextColor;
- (nonnull UIColor *)loginPageSecondaryTextColor;

- (nonnull UIColor *)packageTextColor;

- (nonnull UIColor *)primaryTextColor;
- (nonnull UIColor *)secondaryTextColor;

- (nonnull UIColor *)buttonPrimaryColor;
- (nonnull UIColor *)buttonSecondaryColor;
- (nonnull UIColor *)buttonTextPrimaryColor;
- (nonnull UIColor *)buttonTextSecondaryColor;
- (nonnull UIColor *)buttonTertiaryColor;
- (nonnull UIColor *)buttonTextTertiaryColor;

- (nonnull UIColor *)backgroundColor;
- (nonnull UIColor *)secondaryBackgroundColor;

- (nonnull UIColor *)toolbarTextColor;
- (nonnull UIColor *)toolbarBackgroundColor;
- (nonnull UIColor *)radioPrimaryColor;
- (nonnull UIColor *)radioBackgroundColor;

- (nonnull UIColor *)menuBackgroundColor;
- (nonnull UIColor *)menuPrimaryTextColor;
- (nonnull UIColor *)menuIconColor;
- (nonnull UIColor *)menuSeparatorColor;

- (nonnull UIColor *)primaryHighlightColor;

- (nonnull UIColor *)spinnerColor;

- (UIColor *)drawerSelectorColor;

- (UIColor *)epgPrimaryColor;
- (UIColor *)epgHighlightColor;
- (UIColor *)epgBackgroundColor;
- (UIColor *)epgTextColor;

/**
 *  Get singleton of the PVPConfigHelper
 *
 *  @return PVPConfigHelper singleton instance
 */
+ (nonnull instancetype)sharedInstance;

- (nonnull NSString *)logoUrl;
- (nonnull NSString *)newsletterUrl;
- (nonnull NSString *)themeTypeID;
- (nonnull NSString *)bgImageUrl;
- (nonnull NSString *)bgImageUrlTv;
- (nonnull NSString *)externalPaidUrl;
- (nonnull NSString *)paidRequiredMessage;
- (nonnull NSString *)cmsBaseURL;
- (nonnull NSString *)shareText;
- (nonnull NSString *)shareSubject;
- (nonnull NSString *)copyrightText;
- (nonnull NSString *)bgImageUrlLogin;
- (nonnull NSString *)bgImageUrlRegistration;

- (nonnull NSString *)signUpButtonTitle;
- (nonnull NSString *)loginButtonTitle;

- (BOOL)hideLogin;
- (BOOL)hideMenuSettings;
- (BOOL)showToolbarTitle;
- (BOOL)enablePoweredBy;
- (BOOL)enableSwitchAccount;
- (BOOL)enablePromoCode;
- (BOOL)showScheduledEntries;
- (BOOL)useCustomFont;
- (BOOL)showNoContent;
- (BOOL)useCdnApi;
- (BOOL)enableTvRegistration;
- (BOOL)showTvTrailers;

- (BOOL)enableGender;
- (BOOL)enablePhone;
- (BOOL)enableDOB;

- (BOOL)isCutomHomeIcon;

/**
*  Gets custom fonts
*
*/
- (UIFont *)mainCustomFontWithSize:(CGFloat)size;
- (UIFont *)secondaryCustomFontWithSize:(CGFloat)size;
- (UIFont *)menuCustomFontWithSize:(CGFloat)size;

- (UIFont *)menuFont;
- (UIFont *)topBarFont;
- (BOOL)isMenuFontWideSpacing;

/**
 *  Gets the Wowza RTMP server URL.
 *
 *  @return NSString Wowza RTMP server URL.
 */
- (nonnull NSString *)wowzaRtmpServer;

/**
 *  Gets the Wowza app name.
 *
 *  @return NSString Wowza app name
 */
- (nonnull NSString *)wowzaAppName;

/**
 *  Gets the terms of service document URL.
 *
 *  @return NSString Terms of service URL
 */
- (nonnull NSString *)termsOfServiceUrl;

/**
 *  Get the Kaltura CDN URL for this instance.
 *
 *  @return The Kaltura CDN URL.
 */
- (nonnull NSString *)kalturaCDNUrl;

/**
 *  Get the Kaltura stats URL for this instance.
 *
 *  @return The Kaltura stats URL.
 */
-(nonnull NSString *)kalturaStatsUrl;

/**
 *  Get the privacy policy URL.
 *
 *  @return NSString Privacy policy URL.
 */
- (nonnull NSString *)privacyPolicyUrl;

/**
 * Get the current Instance Id
 *
 * @return NSString currentInstanceId
 */
-(nonnull NSString *)currentInstanceId;


/**
 *  Get the Kaltura service URL for this instance.
 *
 *  @return The Kaltura service URL.
 */
- (nonnull NSString *)kalturaServiceUrl;

/**
 *  Get the Kaltura upload service URL for this instance.
 *
 *  @return The Kaltura upload service URL.
 */
- (nonnull NSString *)kalturaApiUploadUrl;

/**
 *  Get the instance partner id
 *
 *  @return NSString The instance partner ID.
 */
- (nonnull NSString *)partnerId;

- (nonnull NSString *)timezoneString;

/**
 *  Get the player uiconfId.
 *
 *  @return NSString The player uiconfId.
 */
- (nonnull NSString *)playerUiConfId;

/**
 *  Get Boolean for player auto play next video if available in related items.
 *
 *  @return BOOL for player auto play next video (if available in related items).
 */
- ( BOOL ) upNextEnabled;

/**
 *  Get the player URL.
 *
 *  @return NSString The player URL.
 */
- (nonnull NSString *)playerUrl;

- (nullable NSString *)playerBeaconUrl;

/**
 *  Get the player template location - back when we used aniview.
 *
 *  @return NSString The player template location on the web (Back when we used Aniview).
 */
- ( nonnull NSString * ) playerTemplateLocation;
/**
 *  Get the player direct location (e.g. embed.avplayer.com) - back when we used aniview.
 *
 *  @return NSString The player direct location (e.g. embed.avplayer.com) before we used 'playerTemplateLocation' Template location from the web (Back when we used Aniview).
 */
- ( nonnull NSString * ) playerEmbedSrc;
/**
 *  Get appstore url for playerEmbedSrc - back when we used aniview.
 *
 *  @return NSString Get appstore url for playerEmbedSrc (Back when we used Aniview).
 */
- ( nonnull NSString * ) appstoreURL;
/**
 *  Get user IDFA url for playerEmbedSrc - back when we used aniview.
 *
 *  @return NSString Get user IDFA for playerEmbedSrc (Back when we used Aniview).
 */
- ( nonnull NSString * ) userIDFA;
/**
 *  Get baseUrl url for playerEmbedSrc - back when we used aniview.
 *
 *  @return NSString Get baseUrl url for playerEmbedSrc (Back when we used Aniview).
 */
- ( nonnull NSString * ) baseDomainEmbedSrc;


- ( nonnull NSString * ) googleMobileAdsAdUnitID;
- ( BOOL ) googleMobileAdsTagForChildDirectedTreatment;
- ( BOOL ) googleIMAEnabled;
- ( BOOL ) googleMobileAdsEnabled;
- ( BOOL ) googleMobileAdsEnabledAndSet;

- ( BOOL ) awesomeAdsEnabled;
- ( BOOL ) awesomeAdsCloseButtonEnabled;
- ( nonnull NSString * ) awesomeAdsBannerPlacementID;
- ( nonnull NSString * ) awesomeAdsPrerollPlacementID;

/**
 *  Get the Apple IAP product identifiers for events.
 *
 *  @return NSArray The Apple IAP product identifiers for events.
 */
- (nonnull NSArray *)eventProductIdentifiers;

/**
*  Get the Apple IAP product identifiers for Pay-Per-View.
*
*  @return NSArray The Apple IAP product identifiers for Pay-Per-View.
*/
- (nonnull NSArray *)ppvProductIdentifiers;

/**
 *  Get the Apple IAP product identifiers for subscription plans.
 *
 *  @return NSArray The Apple IAP product identifiers for subscription plans.
 */
- (nonnull NSArray *)subscriptionPlanProductIdentifiers;

/**
 *  Get the side menu navigation items for this instance.
 *
 *  @return NSArray The side menu navigation items for this instance.
 */
- (nonnull NSArray *)sideNavItems;
- (NSDictionary *)firstNativeSideNavItem;
- (nonnull NSArray *)bottomNavItems;
/**
 *  Get the list of available entry categories.
 *
 *  @return NSArray the list of available entry categories.
 */
- (nonnull NSArray *)entryCategories;

/**
 *  Get the current device's UDID.
 *
 *  @return The UDID for a real device, or the work "simulator" for a simulator (because simulators don't have a UDID).
 */
- (nonnull NSString *)uniqueDeviceIdentifier;

/**
 * Checks whether HLS is enabled for entries on this instance.
 *
 * @return YES or NO, indicating whether HLS is enabled for entries on this instance.
 */
- (BOOL) hlsEnabled;

- (BOOL) hasApplePurchases;
- (BOOL) enableTvPurchases;

- (NSString *)websiteText;

- (NSString * _Nonnull) playerVideoGravity;

- (nullable NSString *) defaultPlayerLogoUrl;

- (BOOL) galleryStickyBannerGoogleAdsEnabled;

- (nullable NSString *) galleryStickyBannerInternalBannerImage;

- (nullable NSString *) galleryStickyBannerinternalBannerLink;

- (BOOL) galleryStickyBannerCloseButtonEnabled;

/**
 * Gets the ad tag (i.e. VAST) for the player on this instance.
 *
 * @return The ad tag for the player on this instance.
 */
- (nullable NSString *) adTagUrl;

/**
 Returns whether to use the native iOS AVPlayer to play video.

 @return true if we should use the native iOS player, or false if we should use the Kaltura player.
 */
- (BOOL) useNativePlayer;

- (BOOL) drmEnabled;

- (BOOL) tokenizationEnabled;

- (NSString * _Nullable) drmServiceUrlFairplay;
- (NSString * _Nullable) drmFairplayCertificatePath;

/**
 *  Returns whether to require a login to enter the app.
 *
 *  @return YES if a user login is required to use the app, and NO otherwise.
 */
- (BOOL) isLoginRequired;

/**
 *  Returns whether to login with UDID
 *
 *  @return YES if a user login is required to use the app, and NO otherwise.
 */
- (BOOL) isLoginWithUdid;

/**
 *  Returns whether to require a paid subscription to enter the app.
 *
 *  @return YES if a paid subscription is required to use the app, and NO otherwise.
 */
- (BOOL) isPaidSubscriptionRequired;

/**
 *  Returns whether to show a share button on the play page.
 *
 *  @return YES or NO, indicating if we should show a share button on the play page.
 */
- (BOOL) isShareButtonEnabled;

/**
 *  Returns whether to show a cast button on the play page.
 *
 *  @return YES or NO, indicating if we should show a share button on the play page.
 */
- (BOOL) isCastButtonEnabled;

/**
 *  Returns whether to show a download button on the play page.
 *
 *  @return YES or NO, indicating if we should show a download button on the play page.
 */
- (BOOL) isDownloadLinkEnabled;
- (BOOL) isOfflineSupportEnabled;
- (nullable NSString *)offlineDownloadPackage;

- (NSInteger )epgTimeSlot;
- (BOOL) hideChannelName;

- (BOOL) showChannelTitles;
- (BOOL) isSearchEnabled;
- (BOOL) isMenuSearchEnabled;
- (BOOL) isHistoryEnabled;
- (BOOL) disableRegistration;
- (BOOL) allowSkip;
- (BOOL) isCountryAllowed;
- (BOOL) catchupEnabled;

- (nonnull NSString *)getMinimumVersion;

- (nullable NSString *)appStoreLink;
- (nullable NSString *)googleAnalyticsTrackingId;

- (BambooGalleryType)playPageRelatedEntriesViewType;
- (BambooGalleryType)searchViewType;

- (nullable NSString *)blockingImage;
/**
 *  @name Local Configuration
 */

/**
 *  Get a property from the local PVPSettings.plist file.
 *
 *  @param name The name of the property to retrieve.
 *
 *  @return The value of the property with the given name, or nil if doesn't exist.
 */
+ (nullable NSString *)getLocalConfiguration:(nonnull NSString *)name;
+ (NSDictionary *)getLocalConfigurationDict:(NSString *)name;

/**
 *  Set a property in the local app configuration.
 *
 *  @param configuration The value to assign to the configuration.
 *  @param key           The key to which we want to assign the configuration.
 */
+ (void)setLocalConfiguration:(nullable NSString *)configuration forKey:(nonnull NSString *)key;
+ (void)setLocalDictionary:(NSDictionary *)configuration forKey:(NSString *)key;
/**
 *  Indicates whether alerts from the SDK are enabled.
 *
 *  @return YES or NO, depending if alerts from the SDK are enabled.
 */
+ (BOOL)alertsEnabled;
+ (BOOL)userIDFAEnabled;
+ (CGFloat)menuWidthPercent;
/**
 *  Indicates whether to show original menu icon or apply backend colors
 *
 *  @return YES or NO, depending if menu icon is custom.
 */
+ (BOOL)isCustomMenuIcon;

/**
 *  Indicates whether monitoring of internet reachability by the SDK is enabled.
 *
 *  @return YES or NO, depending if monitoring of internet reachability by the SDK is enabled.
 */
+ (BOOL)monitorReachability;

/**
 *  The PVP server prefix.
 */
+ (nonnull NSString *)pvpServerPrefix;

/**
 *  The PVP server suffix.
 */
+ (nonnull NSString *)pvpServerSuffix;

- ( BOOL ) pushwooshEnabled;
- ( BOOL ) branchIoEnabled;
- (nonnull NSString *)appsFlyerDevKey;
- (nonnull NSString *)appleAppID;

- (BOOL) oneSignalEnabled;
- (NSString *) oneSignalKey;

/**
 *  The PVP token field.
 */
+ (nonnull NSString *)tokenField;

/**
 *  The PVP American Time Format.
 */
- (BOOL)isAmericanFormat;

/**
 *  Should show history on homepage.
 */
- (BOOL)enableHomepageHistory;

/**
*  Allows playback in background
*/
- (BOOL)allowBackgroundPlayback;

/**
*  Should show title for slider
*/
- (BOOL)showSliderTitle;
/**
 *  Indicates whether the debug cookie is sent in API calls.
 */
+ (BOOL)debugEnabled;

+ (BOOL)hasVisitedApp;

+ (BOOL)encryptPurchases;

- (BOOL) showStillWatchingDialog;

- (int) showStillWatchingDialogDelay;

- (nonnull NSDictionary *) handleFilter:(NSDictionary *)filter;
    
- (BOOL) disableMenuIcons;

- (BOOL) forceRTL;

- (void) goBack;

-(void) addBackItem:(NSDictionary *)userInfo withId:(NSString *)notificationId;


- (BOOL) isOnboardingAlwaysEnabled;
- (BOOL) isOnboardingButtonsEnabled;

- (NSArray <PVPOnboardingItem *> *_Nullable)onboardingItems;

- ( PVPPalConfig * _Nullable ) getPalSDKConfigs;
- ( PVPInAppSDKConfig * _Nullable) getInAppSDKConfig;
/**
 *  @name Public Properties
 */

/**
 *  Current global instance config.
 */
@property (strong, nonatomic, setter=setCurrentConfig:) PVPConfig * _Nullable currentConfig;

@property (nonatomic, retain) NSMutableArray<NSDictionary *> *backHistoryStack;

@end
