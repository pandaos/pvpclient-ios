//
//  PVPBeaconHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 8/3/17.
//
//

#import <PVPClient/PVPClient.h>

@interface PVPBeaconHelper : NSObject

/**
 *  @name Public Methods
 */

/**
 *  Get singleton of the PVPBeaconHelper
 *
 *  @return PVPBeaconHelper singleton instance
 */
+ (nonnull instancetype)sharedInstance;


/**
 *  Start sending beacon requests
 */
-(void)start;

/**
 *  Stop sending beacon requests
 */
-(void)stop;

@end
