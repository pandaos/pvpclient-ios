//
//  PVPLocalizationHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 6/13/17.
//
//

#import "PVPLocalizationHelper.h"
#import "PVPConfigHelper.h"
#import "PVPAlertHelper.h"

@implementation PVPLocalizationHelper

+ (NSString *)getLanguage {
    NSString *language = [AppUtils getDefaultValueFofKey:DEFAULTS_KEY_USER_LANGUAGE];
//    if (!language || language.length == 0) {
//        language = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
//    }
    return language ? language : [PVPConfigHelper getLocalConfiguration:@"defaultLanguage"];
}

+ (NSString *)getLanguageName {
    return [self getLanguageName:[self getLanguage]];
}

+ (NSString *)getLanguageName:(NSString *)language {
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:language];
    NSString *languageName = [locale displayNameForKey:NSLocaleIdentifier value:language];
    
    return languageName ? languageName : @"";
}

+ (NSArray <NSString *>*)availableLanguages {
    NSMutableArray<NSString *> *languages = [NSMutableArray arrayWithCapacity:5];
    
    NSURL *bundleRoot = [[NSBundle mainBundle] bundleURL];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray * dirContents =
    [fm contentsOfDirectoryAtURL:bundleRoot
      includingPropertiesForKeys:@[]
                         options:NSDirectoryEnumerationSkipsHiddenFiles
                           error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"pathExtension='lproj'"]; //search for .proj folders in the bundle directory
    NSArray<NSURL *> *onlyLproj = [dirContents filteredArrayUsingPredicate:fltr];
    for (NSURL *proj in onlyLproj) {
        NSString *lang = [[proj lastPathComponent] stringByReplacingOccurrencesOfString:@".lproj" withString:@""];
        if (![lang isEqualToString:@"Base"]) { //exclude Base.jproj
            [languages addObject:lang]; //gather languages
        }
    }
    
    return [languages mutableCopy];
}

+ (NSArray <NSString *>*)availableLanguageNames {
    NSMutableArray<NSString *> *languageNames = [NSMutableArray arrayWithCapacity:5];
    NSArray<NSString *> *languages = [self availableLanguages];
    
    for (NSString *lang in languages) {
        [languageNames addObject:[self getLanguageName:lang]]; //gather language names
    }
    
    return [languageNames mutableCopy];
}

+ (BOOL)isRTLEnabled {
    return [[PVPConfigHelper getLocalConfiguration:@"rtlEnabled"] boolValue];// && [UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomTV;
}

+ (BOOL)isRTL {
    NSString *language = [self getLanguage];
    return [language rangeOfString:@"he"].location != NSNotFound || [language rangeOfString:@"ar"].location != NSNotFound;
}

+ ( BOOL ) canUseString: ( NSString * ) stringToTest {
    return stringToTest != (id)[NSNull null] && [stringToTest length] > 0 ? YES : NO;
}

+ (void)setLanguage:(NSString *)language {
    [AppUtils saveDefaultValue:language forKey:DEFAULTS_KEY_USER_LANGUAGE];
    [self setRTLIfNeeded];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_SETTINGS_CHANGED object:nil userInfo:@{@"key": DEFAULTS_KEY_USER_LANGUAGE}];
}

+ (void)setRTLIfNeeded {
    if ([PVPLocalizationHelper isRTL] && [PVPLocalizationHelper isRTLEnabled]) {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UINavigationBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        [[UITabBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    } else {
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UINavigationBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITabBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
}

@end
