//
//  PVPAppUpdateHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 6/5/17.
//
//

#import <Foundation/Foundation.h>

@interface PVPAppUpdateHelper : NSObject

#if TARGET_OS_IOS
+(void)checkForUpdates;
#endif

@end
