//
//  ReachabilityHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 5/23/16.
//
//


#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>

#define REACHABILITY_CONNECTED_NOTIFICATION @"reachabilityConnectedNotification"
#define REACHABILITY_DISCONNECTED_NOTIFICATION @"reachabilityDisonnectedNotification"

/**
 *  The ReachabilityHelper provides an interface for detecting and notifying about the device's internet connectivity status.
 *  To activate internet connectivity monitoring, you must add the "monitorReachability" key to the PVPClient.plist file, and set it to "YES".
 *  Once activated, this class will monitor the internet connectivity of the device, posting a NSNotification on every change:
 *
 *  - REACHABILITY_CONNECTED_NOTIFICATION - When the device is connected to the internet.
 *  - REACHABILITY_DISCONNECTED_NOTIFICATION - When the device is not connected to the internet.
 *  
 *  @see [PVPConfigHelper monitorReachability]
 *
 */
@interface ReachabilityHelper : NSObject

/**
 *  @name Public Methods
 */

/**
 *  Get singleton of the ReachabilityHelper
 *
 *  @return ReachabilityHelper singleton instance
 */
+ (nonnull instancetype)sharedInstance;

/**
 *  Start monitoring internet reachability.
 */
-(void) start;

/**
 *  Stop monitoring internet reachability.
 */
-(void) stop;

/**
 *  Checks if an internet connection is currently reachable.
 *
 *  @return YES if an internet connection is currently reachable, NO otherwise.
 */
-(BOOL) isInternetReachable;

+ (NSString * _Nullable)getIPAddress;

@end
