//
//  PVPMongoHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import <Foundation/Foundation.h>

/**
 *  Enums representing the order in which we would like to get the results from Mongo.
 */
typedef NS_ENUM(NSInteger, PVPMongoOrder) {
    /**
     *  Ascending order
     */
    PVPMongoOrderAscending = 1,
    /**
     *  Descending order
     */
    PVPMongoOrderDescending = -1,
};

/**
 *  The PVPMongoHelper class provides convenience functions for creating and manipulating MongoDB filters and objects.
 */
@interface PVPMongoHelper : NSObject

/**
 *  @name Public Methods
 */

/**
 *  Extracts and returns the MongoId in string form, from a given MongoId object.
 *
 *  @param mongoIdObj The MongoId object in its original form, as it comes from MongoDB.
 *  The object usually comes as a single key-value pair:
 *
 *  ````
 *  {"_id": "<object-mongo-id>"}
 *  ````
 *
 *  @return The MongoId in string form if exists, or an empty string otherwise.
 */
+(NSString *)mongoIdStringFromObject:(NSDictionary *)mongoIdObj;

/**
 *  Builds a search query for a given value using $regex.
 *
 *  @param value         The value to be searched.
 *  @param caseSensitive Wether or not to use the "i" option (case insensitive) or not.
 *
 *  @return A dictionary for a search query of a given value.
 */
+(NSDictionary *)regexForValue:(NSString *)value caseSensitive:(BOOL)caseSensitive;

/**
 *  Builds a sort dictionary from a given field path.
 *
 *  @param field The full field path, in the form of field1.field2... for nested fields.
 *  @param order PVPMongoOrder representing whether the order should be descending or ascending.
 *
 *  @return A dictionary for the sort field.
 */
+(NSDictionary *)sortByField:(NSString *)field order:(PVPMongoOrder)order;

@end
