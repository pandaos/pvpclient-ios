//
//  IAPHelper.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/24/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "IAPHelper.h"
#import "PVPClient.h"

@implementation IAPHelper

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductCanceledNotification = @"IAPHelperProductCanceledNotification";
NSString *const IAPHelperProductFailedNotification = @"IAPHelperProductFailedNotification";
NSString *const IAPHelperProductsRestoredNotification = @"IAPHelperProductsRestoredNotification";

NSMutableDictionary *restoreDict;

static BOOL isSetup = NO;

+(IAPHelper *)sharedInstance
{
    static dispatch_once_t once;
    static IAPHelper *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    self.eventProducts = [NSMutableArray arrayWithCapacity:10];
    self.purchasedEntries = [NSMutableArray arrayWithCapacity:10];
    self.purchasedUsers = [NSMutableArray arrayWithCapacity:10];
    self.purchasedPackages = [NSMutableArray arrayWithCapacity:10];
    self.restoreDict = [NSMutableDictionary dictionaryWithCapacity:10];
}

-(void)initWithEventProducts
{
    if (self) {
        // store product identifiers
        _productIdentifiers = [NSMutableSet set];
        [_productIdentifiers addObjectsFromArray:[PVPConfigHelper sharedInstance].eventProductIdentifiers];
        [_productIdentifiers addObjectsFromArray:[PVPConfigHelper sharedInstance].subscriptionPlanProductIdentifiers];
        
        // check for previously purchased products
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString *productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            } else {
                NSLog(@"Not purchased: %@", productIdentifier);
            }
        }
        
        //[self requestAllEventProducts];
    }
}

- (void)requestPayPerViewProducts {
    if ([self.ppvProducts count] > 0) {
        return;
    }
    
    NSSet *productIdentifiers = [NSSet setWithArray:[[PVPConfigHelper sharedInstance] ppvProductIdentifiers]];
    if (productIdentifiers.count > 0) {
        [[IAPHelper sharedInstance] requestProductsWithIdentifiers:productIdentifiers WithCompletionHandler:^(BOOL success, NSArray *products) {
            
            self.ppvProducts = [products sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                return [[(SKProduct *)obj1 price] compare:[(SKProduct *)obj2 price]];
            }];
        }];
    }
}

- (void)requestAllEventProducts {
    NSSet *productIdentifiers = [NSSet setWithArray:[PVPConfigHelper sharedInstance].eventProductIdentifiers];
    if (productIdentifiers.count > 0) {
        [[IAPHelper sharedInstance] requestProductsWithIdentifiers:productIdentifiers WithCompletionHandler:^(BOOL success, NSArray *products) {
            [self addToEventProducts:products];
        }];
    }
}

- (void)addToEventProducts:(NSArray<SKProduct *> *)products {
    for (SKProduct *product in products) {
        if (![self.eventProducts containsObject:product]) {
            [self.eventProducts addObject:product];
        }
    }
}

- (void)requestAllAppPackages {
    [self requestAllAppPackagesWithCompletionHandler:^(BOOL success, NSArray * _Nullable products) {
    }];
}

- (void)requestAllAppPackagesWithCompletionHandler:(_Nonnull requestProductsCompletionHandler)completion {
    if (self.appPackages.count > 0) {
        completion(YES, self.appPackages);
        return;
    }
    
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"package"]
                    params:params
                   success:^(NSArray * _Nonnull result) {
                        self.appPackages = result;
                        completion(YES, self.appPackages);
                   } failure:^(NSError * _Nonnull e) {
                       self.appPackages = @[];
                       completion(NO, self.appPackages);
    }];
}

- (PVPPackage *)packageForProductID:(NSString *)productID {
    for (PVPPackage *pack in self.appPackages) {
        if ([[pack productId] isEqualToString:productID]) {
            return pack;
        }
    }
    
    return nil;
}

- (void)verifyLoginPurchaseWithCompletionHandler:(_Nonnull loginPurchaseCompletionHandler)completionHandler {
    if ([self.appPackages count] > 0) {
        completionHandler([self isNeededLoginPackPurchased]);
    } else {
        [self requestAllAppPackagesWithCompletionHandler:^(BOOL success, NSArray * _Nullable products) {
            completionHandler([self isNeededLoginPackPurchased]);
        }];
    }
}

- (BOOL)isNeededLoginPackPurchased {
    for (NSString *subID in self.purchasedPackages) {
        for (PVPPackage *pack in self.appPackages) {
            if ([[[PVPConfigHelper sharedInstance] subscriptionPlanProductIdentifiers] containsObject:pack.productId] && [subID isEqualToString:pack.mongoId]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)isPackagePurchased:(NSString *)packageString {
    NSArray *purchases = [[IAPHelper sharedInstance].purchasedPackages copy];
    NSArray *packageIDS = [packageString componentsSeparatedByString:@","];
    for (NSString *packID in packageIDS) {
        if ([purchases containsObject:packID]) {
            return YES;
        }
    }
    
    return NO;
}

- ( NSDictionary * ) addIIDtoParams: ( NSDictionary * ) origParams {

    NSMutableDictionary *newParams = origParams ? [NSMutableDictionary dictionaryWithDictionary:origParams] : [NSMutableDictionary dictionaryWithCapacity:3];
    newParams[@"iid"] = [[PVPConfigHelper sharedInstance] currentInstanceId];
    NSDictionary *params = [newParams copy];

    return params;
}

#pragma mark issue a new product request
-(void)requestAllProductsWithCompletionHandler:(requestProductsCompletionHandler)completionHandler
{
    _completionHandler = [completionHandler copy];
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

-(void)requestProductWithIdentifier:(NSString *)identifier WithCompletionHandler:(requestProductsCompletionHandler)completionHandler
{
    _completionHandler = [completionHandler copy];
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObjects:identifier, nil]];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

-(void)requestProductsWithIdentifiers:(NSSet *)identifiers WithCompletionHandler:(requestProductsCompletionHandler)completionHandler
{
    _completionHandler = [completionHandler copy];
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:identifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

#pragma mark SKProductsRequestDelegate methods
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    NSArray *skProducts = [response.products sortedArrayUsingComparator:^NSComparisonResult(SKProduct *product1, SKProduct *product2) {
        return [product1.price compare:product2.price];
    }];
    for (SKProduct *skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    if (_completionHandler) {
        @try {
            _completionHandler(YES, skProducts);
            //        _completionHandler = nil;
        } @catch (NSException *exception) {}
    }
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    if (_completionHandler) {
        @try {
            _completionHandler(NO, nil);
            //    _completionHandler = nil;
        } @catch (NSException *exception) {}
    }
}

-(BOOL)productPurchased:(NSString *)productIdentifier
{
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

-(void)buyProduct:(SKProduct *)product
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
#pragma mark restore completed transatcions
-(void)restoreCompletedTransactions
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"Restored %lu transactions", (unsigned long)queue.transactions.count);
    NSMutableDictionary *purchasesDict = [NSMutableDictionary dictionaryWithCapacity:queue.transactions.count];
    for (SKPaymentTransaction *transaction in queue.transactions) {
        [_purchasedProductIdentifiers addObject:transaction.payment.productIdentifier];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:transaction.payment.productIdentifier];
        NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        NSString *receiptStr = [receiptData base64EncodedStringWithOptions:0];
        purchasesDict[transaction.payment.productIdentifier] = receiptStr;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:purchasesDict forKey:PURCHASES_DICT_KEY];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductsRestoredNotification object:nil userInfo:nil];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"transaction error: %@", error.localizedDescription);
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductFailedNotification object:error.localizedDescription userInfo:nil];
}

#pragma mark new or incomplete transaction handling
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
            BOOL productRelevant = [_productIdentifiers containsObject: transaction.payment.productIdentifier] || [[self ppvProductIDs] containsObject: transaction.payment.productIdentifier];
            if (productRelevant) {
                switch (transaction.transactionState) {
                    case SKPaymentTransactionStatePurchased:
                        [self completeTransaction:transaction];
                        break;
                    case SKPaymentTransactionStateFailed:
                        [self failedTransaction:transaction];
                        break;
                    case SKPaymentTransactionStateRestored:
                        [self restoreTransaction:transaction];
                        break;
                    default:
                        break;
                }
            }
        
    }
}
-(void)completeTransaction:(SKPaymentTransaction *)transaction
{
#if TARGET_OS_TV
// no support fo firebase analytics yet
#else
    [FIRAnalytics logEventWithName:kFIREventEcommercePurchase
                        parameters:@{
                                     kFIRParameterItemID: transaction.payment.productIdentifier
                                     }];
#endif


    NSLog(@"completing transaction for product ID: %@", transaction.payment.productIdentifier);
    NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier withReceipt:receiptData]; //provide the content
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

-(void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    if(!self.restoreDict[transaction.payment.productIdentifier]) {
        NSLog(@"restoring transaction for product ID: %@", transaction.payment.productIdentifier);
        NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        [self provideContentForProductIdentifier:transaction.payment.productIdentifier withReceipt:receiptData]; //provide the content
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        
        //Lock only if we have logged in user to save this on and no allow annonymous
        self.restoreDict[transaction.payment.productIdentifier] = @"1";
    }
}

-(void)failedTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"transaction failed for product ID: %@", transaction.payment.productIdentifier);
    if (transaction.error.code != SKErrorPaymentCancelled) {
        NSLog(@"transaction error: %@", transaction.error.localizedDescription);
        [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductFailedNotification object:transaction.error.localizedDescription userInfo:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductCanceledNotification object:nil userInfo:nil];
    }
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

#pragma mark after purchase - mark product identifier as purchased in user defaults, and send a notification
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier withReceipt:(NSData *)receipt
{
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:productIdentifier
                                                      userInfo:receipt ? @{@"receipt": [receipt base64EncodedStringWithOptions:0]} : nil];
}

#pragma mark product price
-(NSString *) localizedPriceStringForProduct:(SKProduct *)product
{
    [priceFormatter setLocale:product.priceLocale];
    return [priceFormatter stringFromNumber:product.price];
}

- (NSString *)localizedPriceStringForEventProductWithIdentifier:(NSString *)identifier {
    for (SKProduct *product in [self eventProducts]) {
        if ([product.productIdentifier isEqualToString:identifier]) {
            return [[IAPHelper sharedInstance] localizedPriceStringForProduct:product];
        }
    }
    return @"";
}

-(NSString *) currencyCodeForProduct:(SKProduct *)product
{
    [priceFormatter setLocale:product.priceLocale];
    return [priceFormatter currencyCode];
}

- (NSArray *)ppvProductIDs {
    if ([_ppvProducts count] > 0) {
        return [_ppvProducts valueForKey:@"productIdentifier"];
    }
    
    return @[];
}

-(SKProduct * _Nullable) ppvProductForPrice:(NSString *)price {
    if ([_ppvProducts count] == 0) {
        return nil;
    }
    
    SKProduct *targetProduct = nil;
    
    for (SKProduct *product in _ppvProducts) {
        if ([[product.productIdentifier componentsSeparatedByString:@"_"] lastObject].doubleValue >= [price doubleValue]) {
            targetProduct = product;
            break;
        }
    }
    
    if (targetProduct == nil) {
        targetProduct = [_ppvProducts lastObject];
    }
    
    return targetProduct;
}

@end
