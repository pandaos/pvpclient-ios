//
//  PVPFlutterHelper.m
//  PVPClient
//
//  Created by Kovtun Vladimir on 07.07.2021.
//

#import "PVPFlutterHelper.h"

static NSString *ROUTE_HOME = @"/sportHomepage";
static NSString *ROUTE_SCORES = @"/gamesAndScores";
static NSString *ROUTE_TEAMS = @"/teamPage";
static NSString *ROUTE_SOCIAL = @"/socialPage";
static NSString *ROUTE_GAMES = @"/gamePage";
static NSString *ROUTE_FANCLUB = @"/fanClub";

@implementation PVPFlutterHelper

static PVPFlutterHelper *sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (FlutterScreenType)screenTypeFromString:(NSString *)typeString {
    if ([typeString isEqualToString:@"sportHomepage"]) {
        return FlutterScreenTypeHome;
    } else if ([typeString isEqualToString:@"gamesAndScores"]) {
        return FlutterScreenTypeGamesAndScores;
    }  else if ([typeString isEqualToString:@"teamPage"]) {
        return FlutterScreenTypeTeams;
    } else if ([typeString isEqualToString:@"socialPage"]) {
        return FlutterScreenTypeSocial;
    } else if ([typeString isEqualToString:@"gamePage"]) {
        return FlutterScreenTypeGames;
    } else if ([typeString isEqualToString:@"fanClub"]) {
        return FlutterScreenTypeFanclub;
    }
    
    return FlutterScreenTypeUndefined;
}

- (NSString *)routeForType:(FlutterScreenType)screenType {
    switch (screenType) {
        case FlutterScreenTypeHome:
            return ROUTE_HOME;
        case FlutterScreenTypeGamesAndScores:
            return ROUTE_SCORES;
        case FlutterScreenTypeTeams:
            return ROUTE_TEAMS;
        case FlutterScreenTypeSocial:
            return ROUTE_SOCIAL;
        case FlutterScreenTypeGames:
            return ROUTE_GAMES;
        case FlutterScreenTypeFanclub:
            return ROUTE_FANCLUB;
        default:
            return @"";
    }
}

- (BOOL)isFlutterPage:(NSString *)page {
    return ([self screenTypeFromString:page] != FlutterScreenTypeUndefined);
}

@end
