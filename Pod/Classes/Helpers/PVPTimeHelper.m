//
//  PVPTimeHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 12/9/15.
//
//

#import "PVPTimeHelper.h"
#import "NSString+PVPClient.h"

@implementation PVPTimeHelper

+(NSString *)formattedTimeForTotalSeconds:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours) {
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    } else {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }

}

+(NSNumber *)timeStampForAge:(int)age
{
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear fromDate:currentDate]; // Get necessary date components
    
    int currentYear = (int) [components year];
    int yearOfBirth = currentYear - age;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT];
    
    NSDate *resultDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"01-01-%d", yearOfBirth]];
    
    return @([resultDate timeIntervalSince1970WithServerOffset]);
}

+(int)ageFromTimeStamp:(int)timeStamp
{
    NSDate *currentDate = [NSDate date];
    NSDate *timeStampDate = [NSDate dateWithTimeIntervalSince1970WithServerOffset:timeStamp];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSInteger currentYear = [cal component:NSCalendarUnitYear fromDate:currentDate];
    NSInteger timeStampYear = [cal component:NSCalendarUnitYear fromDate:timeStampDate];
    
    NSNumber *ageNumber = [NSNumber numberWithInteger:currentYear - timeStampYear];
    return [ageNumber intValue];
}

+(BOOL)isDate:(NSDate *)date equalsToTimestamp:(int)timeStamp byEra:(BOOL)compareEras byYear:(BOOL)compareYears byMonth:(BOOL)compareMonths byDay:(BOOL)compareDays
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsDate = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    
    NSDateComponents *componentsTimeStamp = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate dateWithTimeIntervalSince1970WithServerOffset:timeStamp]];
    
    if(compareEras && componentsDate.era != componentsTimeStamp.era) {
        return NO;
    }
    if(compareYears && componentsDate.year != componentsTimeStamp.year) {
        return NO;
    }
    if(compareMonths && componentsDate.month != componentsTimeStamp.month) {
        return NO;
    }
    if(compareDays && componentsDate.day != componentsTimeStamp.day) {
        return NO;
    }
    return YES;
}

+(NSString *)dateStringFromTimeStamp:(int)timeStamp {
    return [self dateStringFromTimeStamp:timeStamp withFormat:DATE_FORMAT];
}

+(NSString *)dateStringFromTimeStamp:(int)timeStamp withFormat:(NSString *)format
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970WithServerOffset:timeStamp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

+(int)timeStampFromDateString:(NSString *)dateString
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:DATE_FORMAT];
    
    NSDate *date = [format dateFromString:dateString];
    
    return [date timeIntervalSince1970WithServerOffset];
}

+(NSString *)zodiacSignFromTimeStamp:(int)timeStamp
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970WithServerOffset:timeStamp];
    NSString *result = @"N/A";
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComps = [gregorianCal components: (NSCalendarUnitDay | NSCalendarUnitMonth)
                                                  fromDate: date];
    // Then use it
    int month = (int) [dateComps month];
    int days = (int) [dateComps day];
    
    //there are two zodiac signs possilble for each month
    switch (month) {
            //compare the dates
        case 1:
            if (days <= 19) {
                result = @"Capricorn";
            } else {
                result = @"Aquarius";
            }
            break;
        case 2:
            if (days <= 18) {
                result = @"Aquarius";
            } else {
                result = @"Pisces";
            }
            break;
        case 3:
            if (days <= 20) {
                result = @"Pisces";
            } else {
                result = @"Aries";
            }
            break;
        case 4:
            if (days <= 19) {
                result = @"Aries";
            } else {
                result = @"Taurus";
            }
            break;
        case 5:
            if (days <= 20) {
                result = @"Taurus";
            } else {
                result = @"Gemini";
            }
            break;
        case 6:
            if (days <= 20) {
                result = @"Gemini";
            } else {
                result = @"Cancer";
            }
            break;
        case 7:
            if (days <= 22) {
                result = @"Cancer";
            } else {
                result = @"Leo";
            }
            break;
        case 8:
            if (days <= 22) {
                result = @"Leo";
            } else {
                result = @"Virgo";
            }
            break;
        case 9:
            if (days <= 22) {
                result = @"Virgo";
            } else {
                result = @"Libra";
            }
            break;
        case 10:
            if (days <= 22) {
                result = @"Libra";
            } else {
                result = @"Scorpio";
            }
            break;
        case 11:
            if (days <= 21) {
                result = @"Scorpio";
            } else {
                result = @"Sagittarius";
            }
            break;
        case 12:
            if (days <= 20) {
                result = @"Sagittarius";
            } else {
                result = @"Capricorn";
            }
            break;
        default:
            break;
    }
    return result;
}

+(NSString *)zodiacSymbolFromTimeStamp:(int)timeStamp
{
    NSDictionary *zodiacSymbols = @{@"Capricorn": @"♑︎",
                                    @"Aquarius": @"♒︎",
                                    @"Pisces": @"♓︎",
                                    @"Aries": @"♈︎",
                                    @"Taurus": @"♉︎",
                                    @"Gemini": @"♊︎",
                                    @"Cancer": @"♋︎",
                                    @"Leo": @"♌︎",
                                    @"Virgo": @"♍︎",
                                    @"Libra": @"♎︎",
                                    @"Scorpio": @"♏︎",
                                    @"Sagittarius": @"♐︎",
                                    @"N/A": @"-"};
    NSString *zodiacSign = [self zodiacSignFromTimeStamp:timeStamp];
    
    return zodiacSymbols[zodiacSign];
}

+(NSArray <NSString *> *) getAllMonths:(BOOL)shortened {
    return @[
             [shortened ? @"Jan" : @"January" localizedString],
             [shortened ? @"Feb" : @"Febuary" localizedString],
             [shortened ? @"Mar" : @"March" localizedString],
             [shortened ? @"Apr" : @"April" localizedString],
             [@"May" localizedString],
             [shortened ? @"Jun" : @"June" localizedString],
             [shortened ? @"Jul" : @"July" localizedString],
             [shortened ? @"Aug" : @"August" localizedString],
             [shortened ? @"Sep" : @"September" localizedString],
             [shortened ? @"Oct" : @"October" localizedString],
             [shortened ? @"Nov" : @"November" localizedString],
             [shortened ? @"Dec" : @"December" localizedString]
             ];
}

+(NSArray <NSString *> *) getAllDaysOfWeek:(BOOL)shortened {
    return @[
             [shortened ? @"Sun" : @"Sunday" localizedString],
             [shortened ? @"Mon" : @"Monday" localizedString],
             [shortened ? @"Tue" : @"Tuesday" localizedString],
             [shortened ? @"Wed" : @"Wednesday" localizedString],
             [shortened ? @"Thu" : @"Thursday" localizedString],
             [shortened ? @"Fri" : @"Friday" localizedString],
             [shortened ? @"Sat" : @"Saturday" localizedString]
             ];
}

+(NSString *) localizedFullDateForTimeStamp:(int)timestamp shortened:(BOOL)shortened {
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [cal components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:[NSDate dateWithTimeIntervalSince1970:timestamp]];
    return [NSString stringWithFormat:@"%@, %ld %@ %ld", [self getAllDaysOfWeek:shortened][components.weekday - 1], (long) components.day, [self getAllMonths:shortened][components.month - 1], components.year];
}

@end
