//
//  PVPFlutterHelper.h
//  PVPClient
//
//  Created by Kovtun Vladimir on 07.07.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FlutterScreenType) {
    FlutterScreenTypeUndefined = -1,
    FlutterScreenTypeHome,
    FlutterScreenTypeGamesAndScores,
    FlutterScreenTypeTeams,
    FlutterScreenTypeSocial,
    FlutterScreenTypeGames,
    FlutterScreenTypeFanclub
};

@interface PVPFlutterHelper : NSObject

+ (nonnull instancetype)sharedInstance;

- (FlutterScreenType)screenTypeFromString:(NSString *)typeString;
- (NSString *)routeForType:(FlutterScreenType)screenType;
- (BOOL)isFlutterPage:(NSString *)page;

@end

NS_ASSUME_NONNULL_END
