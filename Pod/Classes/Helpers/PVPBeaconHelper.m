//
//  PVPBeaconHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 8/3/17.
//
//

#import "PVPBeaconHelper.h"
#import "PVPConfigHelper.h"

#define MAX_CONNECTIONS 10
#define BEACON_TIMER_INTERVAL 30

@interface PVPBeaconHelper ()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSURLSessionDataTask *> *tasksDict;
@property (nonatomic, strong) NSTimer *beaconRequestTimer;

@end

@implementation PVPBeaconHelper

static BOOL isSetup = NO;
static PVPBeaconHelper *sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void) setup {
    self.session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    self.baseUrl = [[PVPConfigHelper sharedInstance] playerBeaconUrl];
    self.params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                  @"sessionId":[[PVPConfigHelper sharedInstance] uniqueDeviceIdentifier],
                                                                  @"iid": [[PVPConfigHelper sharedInstance] currentInstanceId],
                                                                  }];
}

-(void)start {
    if (self.beaconRequestTimer) {
        [self.beaconRequestTimer invalidate];
    }
    
    if (self.baseUrl) {
        self.beaconRequestTimer = [NSTimer scheduledTimerWithTimeInterval:BEACON_TIMER_INTERVAL target:self selector:@selector(handleBeaconTimer:) userInfo:nil repeats:YES];
        [self sendBeaconRequest:YES];
    } else {
        NSLog(@"Beacon not enabled for this app.");
    }
}

-(void)stop {
    if (self.beaconRequestTimer) {
        [self.beaconRequestTimer invalidate];
    }
}

-(NSURL *) beaconUrlWithParams:(NSDictionary *)eventParams {
    NSURLComponents *components = [NSURLComponents componentsWithString:self.baseUrl];
    NSMutableArray *queryItems = [NSMutableArray arrayWithCapacity:MAX_CONNECTIONS];
    NSString *userToken = [[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY] stringByReplacingOccurrencesOfString:@"token=" withString:@""];
    self.params[@"token"] = userToken ? userToken : @"";
    NSDictionary *finalParams = [self.params dictionaryByMergingWithDictionary:eventParams];
    for (NSString *key in finalParams) {
        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:finalParams[key]]];
    }
    components.queryItems = queryItems;
    
    return components.URL;
}

-(void)handleBeaconTimer:(NSTimer *)timer {
    [self sendBeaconRequest];
}

-(void)sendBeaconRequest {
    [self sendBeaconRequest:NO];
}

-(void)sendBeaconRequest:(BOOL)set {
    NSURL *url = [self beaconUrlWithParams:set ? @{@"set": @"1"} : @{}];
    NSLog(@"sending beacon: %@", url.absoluteString);
    NSURLSessionDataTask *task = [self.session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if ([responseStr isEqualToString:@"1"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_BEACON_SUCCESS object:nil userInfo:nil];
                NSLog(@"Beacon success");
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_BEACON_FAILURE object:nil userInfo:nil];
                NSLog(@"Beacon failed");
            }
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_BEACON_FAILURE object:nil userInfo:nil];
            NSLog(@"Beacon failed");
        }
    }];
    [task resume];
}

@end
