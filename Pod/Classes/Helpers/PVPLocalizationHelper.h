//
//  PVPLocalizationHelper.h
//  Pods
//
//  Created by Oren Kosto,  on 6/13/17.
//
//

#import <Foundation/Foundation.h>

@interface PVPLocalizationHelper : NSObject

+ (NSString *)getLanguage;
+ (NSString *)getLanguageName;
+ (NSString *)getLanguageName:(NSString *)language;
+ (NSArray <NSString *>*)availableLanguages;
+ (NSArray <NSString *>*)availableLanguageNames;
+ (BOOL)isRTLEnabled;
+ (BOOL)isRTL;
+ (BOOL)canUseString:(NSString *)stringToTest;
+ (void)setLanguage:(NSString *)language;
+ (void)setRTLIfNeeded;

@end
