//
//  PVPMongoHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import "PVPMongoHelper.h"

@implementation PVPMongoHelper

+(NSString *)mongoIdStringFromObject:(NSDictionary *)mongoIdObj
{
    if ([mongoIdObj isKindOfClass:[NSDictionary class]] && mongoIdObj[@"$id"]) {
        return mongoIdObj[@"$id"];
    }
    return @"";
}

+(NSDictionary *)regexForValue:(NSString *)value caseSensitive:(BOOL)caseSensitive
{
    if (caseSensitive) {
        return @{@"$regex": value};
    } else {
        return @{@"$regex": value,
                 @"$options": @"i"};
    }
}

+(NSDictionary *)sortByField:(NSString *)field order:(PVPMongoOrder)order
{
    return @{field: @(order)};
}

@end
