//
//  PVPAppUpdateHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 6/5/17.
//
//

#import "PVPAppUpdateHelper.h"
#import "PVPConfigHelper.h"
#import "PVPAlertHelper.h"

@implementation PVPAppUpdateHelper

#if TARGET_OS_IOS
+(void)checkForUpdates {
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *minimumVersion = [[PVPConfigHelper sharedInstance] getMinimumVersion];
    if ([minimumVersion compare:appVersion options:NSNumericSearch] == NSOrderedDescending) {
        NSString *appStoreLink = [[PVPConfigHelper sharedInstance] appStoreLink];
        if (appStoreLink && [appStoreLink length] > 0) {
            [PVPAlertHelper showAlertWithTitle:[@"App Update Available" localizedString]
                                    andMessage:[@"A newer version of the app is now available for download. Please update the app via the App Store" localizedString]
//                         withCancelButtonTitle:[@"Maybe Later" localizedString]
                          withButtonTitle:[@"Update Now" localizedString]
                                   withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
//                                       if (confirmed) {
                                           [AppUtils openUrlWithString:appStoreLink];
//                                       }
                                   }];
        }
    }
}
#endif

@end
