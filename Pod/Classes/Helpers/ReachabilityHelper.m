//
//  ReachabilityHelper.m
//  Pods
//
//  Created by Oren Kosto,  on 5/23/16.
//
//

#import "ReachabilityHelper.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation ReachabilityHelper

static BOOL isSetup = NO;
static ReachabilityHelper *sharedInstance = nil;
static dispatch_once_t onceToken;
static NSMutableDictionary *plistDictionary;

+ (instancetype)sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void) setup {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        NSString *notificatonString;
        if ([self isInternetReachable]) {
            notificatonString = REACHABILITY_CONNECTED_NOTIFICATION;
        } else {
            notificatonString = REACHABILITY_DISCONNECTED_NOTIFICATION;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:notificatonString
                                                            object:[AFNetworkReachabilityManager sharedManager]];
    }];
}

-(void) start {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

-(void) stop {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

-(BOOL) isInternetReachable {
    return [AFNetworkReachabilityManager sharedManager].isReachable;
}

+ (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

@end
