//
//  IAPHelper.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/24/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#if TARGET_OS_TV
#import <FirebaseCore/FirebaseCore.h>
#else
#import <Firebase/Firebase.h>
#import <FirebaseCore/FirebaseCore.h>
#endif

#import "PVPConfigHelper.h"



UIKIT_EXTERN NSString * _Nonnull const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString * _Nonnull const IAPHelperProductCanceledNotification;
UIKIT_EXTERN NSString * _Nonnull const IAPHelperProductFailedNotification;
UIKIT_EXTERN NSString * _Nonnull const IAPHelperProductsRestoredNotification;

@class PVPPackage;
/**
 *  A completion block for IAP product requests. This block, if provided, will be executed once the request finishes.
 *
 *  @param success  An indication if the request succeeded or failed.
 *  @param products An array of the IAP products returned from the request.
 *
 *  @see [IAPHelper requestAllProductsWithCompletionHandler:]
 *  @see [IAPHelper requestProductWithIdentifier:WithCompletionHandler:]
 *  @see [IAPHelper requestProductsWithIdentifiers:WithCompletionHandler:]
 */
typedef void (^requestProductsCompletionHandler)(BOOL success, NSArray * _Nullable products);
typedef void (^loginPurchaseCompletionHandler)(BOOL success);

/**
 The IAPHelper provides an interface for executing Apple in-app purchase requests.
 This class handles ONLY the process of the purchase itself, using Apple's in-app purchase service.
 After a purchase is finished, you must call the "finalizePurchase" method in the relevant model class to register the purchase on the PVP.
 
 @see [PVPEntryModel finalizePurchaseForEntry:withProduct:withRequestInfo:]
 @see [PVPUserModel finalizePurchaseForUser:withProduct:withRequestInfo:]
 */
@interface IAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {    
    requestProductsCompletionHandler _completionHandler;
    SKProductsRequest *_productsRequest;
    NSMutableSet *_productIdentifiers;
    NSMutableSet *_purchasedProductIdentifiers;
    NSNumberFormatter *priceFormatter;
}

/**
 *  @name Public Properties
 */

/**
 *  The array of StoreKit products for all of the availenle events.
 */
@property (nonatomic, strong) NSMutableArray * _Nullable eventProducts;

/**
 *  The array of PVPSubscription objects, representing the entry products that were purchased by the user.
 */
@property (nonatomic, strong) NSMutableArray * _Nullable purchasedEntries;

/**
 *  The array of PVPSubscription objects, representing the user products that were purchased by the user.
 */
@property (nonatomic, strong) NSMutableArray * _Nullable purchasedUsers;

@property (nonatomic, strong) NSMutableArray * _Nullable purchasedPackages;
/**
 *  The array of PVPSubscription objects, representing the user products that were purchased by the user.
 */
@property (nonatomic, strong) NSMutableDictionary * _Nullable restoreDict;

@property (nonatomic, strong) NSArray * _Nullable ppvProducts;
@property (nonatomic, strong) NSArray * _Nullable appPackages;

/**
 *  @name Public Methods
 */

/**
 * Get singleton instance of the IAPHelper.
 *
 * @return singleton instance of IAPHelper
 */
+ (IAPHelper * _Nonnull) sharedInstance;

/**
 *  Init IAPHelper with a set of product identifiers.
 *  This function requests the product info for all of the provided products, and stores it locally for faster retrieval.
 *  You can still purchase a product that doesn't exist in the set after calling this function, but you will have to issue a separate request for it before you can do so.
 *
 *  @see -requestAllProductsWithCompletionHandler:
 *  @see -requestProductWithIdentifier:WithCompletionHandler:
 *  @see -requestProductsWithIdentifiers:WithCompletionHandler:
 */
-(void)initWithEventProducts;
-(void)requestPayPerViewProducts;
-(void)requestAllAppPackages;
- (PVPPackage * _Nullable)packageForProductID:(NSString * _Nullable)productID;
- (void)verifyLoginPurchaseWithCompletionHandler:(_Nonnull loginPurchaseCompletionHandler)completionHandler;
- (BOOL)isPackagePurchased:(NSString *)packageString;

/**
 *  Fetches all of the products in _productIdentifiers.
 *
 *  @param completionHandler the block to execute when the request if complete.
 */
- (void)requestAllProductsWithCompletionHandler:(_Nonnull requestProductsCompletionHandler)completionHandler;

/**
 *  Fetches the product with a given product identifier.
 *
 *  @param identifier        the desired product identifier.
 *  @param completionHandler the block to execute when the request if complete.
 *
 *  @see -requestProductWithIdentifier:WithCompletionHandler:
 *  @see -requestProductsWithIdentifiers:WithCompletionHandler:
 */
- (void)requestProductWithIdentifier:(NSString * _Nonnull)identifier WithCompletionHandler:(_Nonnull requestProductsCompletionHandler)completionHandler;

/**
 *  Fetches the product with a given set of product identifiers.
 *
 *  @param identifiers       the desired product identifiers.
 *  @param completionHandler the block to execute when the request if complete.
 *
 *  @see -requestAllProductsWithCompletionHandler:
 *  @see -requestProductsWithIdentifiers:WithCompletionHandler:
 */
- (void)requestProductsWithIdentifiers:(NSSet * _Nonnull)identifiers WithCompletionHandler:(_Nonnull requestProductsCompletionHandler)completionHandler;

/**
 *  Executes a request to purchase a given product.
 *
 *  @param product the product we would like to purchase.
 *
 *  @see -requestAllProductsWithCompletionHandler:
 *  @see -requestProductWithIdentifier:WithCompletionHandler:
 */
- (void)buyProduct:(SKProduct * _Nonnull)product;

/**
 * Issues a request to restore all completed IAP transactions under the current Apple ID.
 * The user may have to authenticate the account to complete the process.
 */
-(void)restoreCompletedTransactions;

/**
 *  Indicates if the product with a given product identifier has already been purchased before.
 *
 *  @param productIdentifier the product identifier.
 *
 *  @return YES if the product has been purchased before, and NO otherwise.
 */
- (BOOL)productPurchased:(NSString * _Nonnull)productIdentifier;

/**
 *  Extracts the localized price of a given StoreKit product, in NSString format.
 *  The locale is determined by the given product. In most cases it will be the same as the device locale.
 *
 *  @param product The StoreKit product we would like to extract the price from.
 *
 *  @return The localized price for the given product.
 */
- (NSString * _Nonnull) localizedPriceStringForProduct:(SKProduct * _Nonnull)product;

/**
 *  Extracts the localized price of an event product with a given identifier, in NSString format.
 *  The locale is determined by the given product. In most cases it will be the same as the device locale.
 *
 *  @param identifier The identifier of the StoreKit product we would like to extract the price from.
 *
 *  @return The localized price for the given product.
 */
- (NSString * _Nonnull)localizedPriceStringForEventProductWithIdentifier:(NSString * _Nonnull)identifier;

/**
 *  Extracts the localized currency symbol of a given StoreKit product, in NSString format.
 *  The locale is determined by the given product. In most cases it will be the same as the device locale.
 *
 *  @param product The StoreKit product we would like to extract the currency symbol from.
 *
 *  @return The localized currency symbol for the given product.
 */
-(NSString * _Nonnull) currencyCodeForProduct:(SKProduct * _Nonnull)product;

-(SKProduct * _Nullable) ppvProductForPrice:(NSString *)price;

@end
