//
//  PVPClient.h
//  PVPClient
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "PVPConfigHelper.h"
#import "ReachabilityHelper.h"
#import "PVPViewController.h"
#import "PVPConfigModel.h"
#import "PVPLoginModel.h"
#import "PVPEntryModel.h"
#import "PVPLiveEntryModel.h"
#import "PVPOpenTokModel.h"
#import "PVPPushNotificationModel.h"
#import "PVPUserModel.h"
#import "PVPEventModel.h"
#import "PVPAlertHelper.h"
#import "ViewControllerSwitch.h"
#import "PVPTimeHelper.h"
#import "PVPLocalizationHelper.h"
#import "PVPMessageModel.h"
#import "PVPConversationModel.h"
#import "PVPFlavorModel.h"
#import "PVPContactUsModel.h"
#import "PVPChannelModel.h"
#import "PVPCategoryModel.h"
#import "FileDownloadManager.h"
#import "PVPAppUpdateHelper.h"
#import "PVPNodeModel.h"
#import "PVPNodeCategoryModel.h"

/**
 *  The PVPClient provides a single centralized interface to access all parts of the PVPClient library.
 *  This class is also in charge of initializing the PVPClient library and getting the initial server configuration.
 *
 *  Usage:
 *  To initialize and be able to use the PVPClient library in your project, run:
 *
 *  ````
 *  [[PVPClient sharedInstance] start]; //to use the default Bamboo domain.
 *  [[PVPClient sharedInstance] startWithServerDomain:<bamboo-server-domain>]; //to use the a custom Bamboo domain.
 *  ````
 *
 *  In the "application:didFinishLaunchingWithOptions:" of your app delegate class.
 */
@interface PVPClient : NSObject<PVPConfigModelDelegate, PVPLoginModelDelegate, CLLocationManagerDelegate> {
    PVPConfigModel *configModel;
    PVPLoginModel *loginModel;
}

@property (strong, nonatomic) CLLocationManager * _Nonnull locationManager;

/**
 *  Get singleton of the PVPClient
 *
 *  @return PVPClient singleton instance
 */
+ (_Nonnull instancetype)sharedInstance;
+ ( void ) InitSharedInstance;

/**
 *  Initial configuration and activation, get initial server config.
 */
- (void)start;

/**
 *  Initial configuration and activation with a custom server domain, get initial server config.
 *
 *  @param serverDomain The PVP server domain to use for the client.
 */
- (void)startWithServerDomain:(NSString * _Nonnull)serverDomain;

/**
 *  Checks if the client is currently logged in.
 *
 *  @return YES if the client is currently logged in, and NO otherwise.
 */
- (BOOL)isLoggedIn;

- (NSString *)userToken;

@end
