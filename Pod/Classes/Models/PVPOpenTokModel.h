//
//  PVPOpenTokModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 11/1/15.
//  Copyright © 2015 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"
#import "PVPOpenTok.h"

/**
 *  The PVPOpenTok delegate provides an interface for responding to events from the PVPOpenTokModel.
 */
@protocol PVPOpenTokDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful call API request.
 *
 *  @param openTok The result of the API request.
 *
 *  @see [PVPOpenTokModel initiateCallWithUser:]
 */
-(void)callRequestSuccessWithResult:(PVPOpenTok *)openTok;

/**
 *  Triggered upon a successful call API request.
 *
 *  @see [PVPOpenTokModel initiateCallWithUser:]
 */
-(void)callRequestFail;

@end

/**
 *  The PVPOpenTokModel provides an interface for using the PVP opentok service, for initiating OpenTok video calls from within the app.
 */
@interface PVPOpenTokModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPOpenTokDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPOpenTokDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Initiates a one-on-one video call with a given user.
 *
 *  @param user The user we would like to call.
 */
-(void)initiateCallWithUser:(PVPUser *)user;

@end
