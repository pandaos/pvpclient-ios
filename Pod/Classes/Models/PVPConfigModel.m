//
//  PVPConfigModel.m
//  Pods
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import "PVPConfigModel.h"

@implementation PVPConfigModel

-(void)getInitialServerConfig
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"vcode"] = version;
    
    [[ApiManager sharedInstance] getRequest: @"config"
                    params:params
                   success:^(NSArray *result) {
                       //get all info we need from the config
                       PVPConfig *config = [result firstObject];
                       [[PVPConfigHelper sharedInstance] setCurrentConfig:config];
                       
                       [[IAPHelper sharedInstance] initWithEventProducts];
                       [[IAPHelper sharedInstance] requestPayPerViewProducts];
                       [[IAPHelper sharedInstance] requestAllAppPackages];

                       if (config.social[@"twitter"] && [config.social[@"twitter"] isKindOfClass:[NSDictionary class]] &&
                           config.social[@"twitter"][@"consumerKey"] &&
                           config.social[@"twitter"][@"consumerSecret"]) {
                           [AppUtils saveDefaultValue:config.social[@"twitter"][@"consumerKey"] forKey:TWITTER_CONSUMER_KEY];
                           [AppUtils saveDefaultValue:config.social[@"twitter"][@"consumerSecret"] forKey:TWITTER_CONSUMER_SECRET];
                       }
                       
                       if (config.social[@"facebook"] && [config.social[@"facebook"] isKindOfClass:[NSDictionary class]] &&
                           config.social[@"facebook"][@"applicationId"]) {
                           [AppUtils saveDefaultValue:config.social[@"facebook"][@"applicationId"] forKey:FACEBOOK_APP_ID];
                       }
                       
                       if (config.analytics[@"panda"] &&
                           [config.analytics[@"panda"] isKindOfClass:[NSDictionary class]] &&
                           config.analytics[@"panda"][@"url"] &&
                           config.analytics[@"panda"][@"siteId"]) {
//                           [[AppUtils sharedUtils] setPiwikBaseUrl:config.analytics[@"panda"][@"url"]];
//                           [[AppUtils sharedUtils] setPiwikSiteId:[NSString stringWithFormat:@"%d", [config.analytics[@"panda"][@"siteId"] intValue]]];
                       }

                       [AppUtils performSelector:@selector(initialConfigRequestSuccess) on:self.delegate];
                   }
                   failure:^(NSError *e) {
                       NSLog(@"%@", e);
                       [AppUtils performSelector:@selector(configRequestFail) on:self.delegate];
                   }];
}

-(void)getServerConfig
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"vcode"] = version;
    [[ApiManager sharedInstance] getRequest:@"config"
                    params:params
                   success:^(NSArray *result) {
                       PVPConfig *config = [result firstObject];
                       [[PVPConfigHelper sharedInstance] setCurrentConfig:config];
                       [[IAPHelper sharedInstance] requestPayPerViewProducts];
                       [[IAPHelper sharedInstance] requestAllAppPackages];

                       if (!config.userKs ||
                           !config.live[@"chunkInterval"] ||
                           !config.live[@"wowzaRtmpServer"] ||
                           !config.live[@"appName"] ||
                           !config.kaltura[@"partnerId"] ||
                           !config.kaltura[@"serviceUrl"] ||
                           !config.player[@"playerUrl"]) {
                           NSLog(@"items missing in config! aborting login process...");
                           [AppUtils performSelector:@selector(configRequestFail) on:self.delegate];
                           return;
                       }
                       NSLog(@"got partner ID: %@ and KS: %@", config.kaltura[@"partnerId"], config.userKs);
                       [[AppUtils sharedUtils] setChunkSize:config.live[@"chunkInterval"]];
                       [[AppUtils sharedUtils] setWowzaRtmpServer:config.live[@"wowzaRtmpServer"]];
                       [[AppUtils sharedUtils] setAppName:config.live[@"appName"]];
                       [[AppUtils sharedUtils] setBroadcastTime:config.live[@"broadcastTime"] ? [config.live[@"broadcastTime"] doubleValue] : 3600];
                       [[AppUtils sharedUtils] setUserKS:config.userKs];
                       [[AppUtils sharedUtils] setUserPartnerId:config.kaltura[@"partnerId"]];
                       [[AppUtils sharedUtils] setKalturaServiceUrl:config.kaltura[@"serviceUrl"]];
                       [[AppUtils sharedUtils] setPlayerUrl:config.player[@"playerUrl"]];
                       [[AppUtils sharedUtils] setSendPushOnEventCreated:[config.pushwoosh[@"sendOnEventCreated"] boolValue]];
                       [[AppUtils sharedUtils] setSendPushOnEventStarted:[config.pushwoosh[@"sendOnEventStarted"] boolValue]];
                       [[AppUtils sharedUtils] setEventCreatedText:config.pushwoosh[@"eventCreatedText"] ? config.pushwoosh[@"eventCreatedText"] : @""];
                       [[AppUtils sharedUtils] setEventStartedText:config.pushwoosh[@"eventStartedText"] ? config.pushwoosh[@"eventStartedText"] : @""];
                       [[AppUtils sharedUtils] setSocialStreamShareText:config.social[@"socialStreamShareText"] ? config.social[@"socialStreamShareText"] : @""];
                       [[AppUtils sharedUtils] setSocialWatchVODShareText:config.social[@"socialWatchVODShareText"] ? config.social[@"socialWatchVODShareText"] : @""];
                       [[AppUtils sharedUtils] setSocialWatchLiveShareText:config.social[@"socialWatchLiveShareText"] ? config.social[@"socialWatchLiveShareText"] : @""];
                       
                       NSString *uiconfId = config.player[@"uiconfIdMobile"];
                       if (!uiconfId || uiconfId.length == 0) {
                           uiconfId = config.player[@"uiconfId"]; //fallback, in case mobile player is not available
                       }
                       [[AppUtils sharedUtils] setPlayerUiConfId:uiconfId];
                       
                       [AppUtils performSelector:@selector(configRequestSuccess) on:self.delegate];
                   }
                   failure:^(NSError *e) {
                       NSLog(@"%@", e);
                       [AppUtils performSelector:@selector(configRequestFail) on:self.delegate];
                   }];
}

-(void)initConfig
{
    [self getInitialServerConfig];
}

@end
