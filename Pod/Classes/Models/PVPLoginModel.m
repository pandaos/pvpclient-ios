//
//  PVPLoginModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/30/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPLoginModel.h"

#define TWITTER_ALERT_TAG 1
#define PASSWORD_ALERT_TAG 2

@implementation PVPLoginModel

-(PVPUserModel *)userModel
{
    if (!userModel) {
        userModel = [[PVPUserModel alloc] init];
        userModel.delegate = self;
    }
    return userModel;
}

-(PVPConfigModel *)configModel
{
    if (!configModel) {
        configModel = [[PVPConfigModel alloc] init];
        configModel.delegate = self;
    }
    return configModel;
}

-(void)loginUser:(PVPLogin *)login
{
    login.accountId = [[PVPConfigHelper sharedInstance] currentInstanceId];
//    login.id = [login.id stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    login.password = [login.password stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self deleteTokenFromDefaults];
    [[ApiManager sharedInstance] postRequest:@"token"
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        if (result.count > 0) {
                            [self saveTokenToDefaults:[result firstObject]];
                            [self.userModel getLoggedInUser];
                        }
                        else {
                            if ([PVPConfigHelper alertsEnabled]) {
                                [PVPAlertHelper showAlertWithTitle:@"Login Failed!" andMessage:@"Invalid username and/or password. Please check your credentials and try again."]; //notify the user
                            }
                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                        }
                    } failure:^(NSError *e, NSString *bambooError) {
                        if ([PVPConfigHelper alertsEnabled]) {
                            NSString *alertMessage = bambooError.length > 0 ? bambooError : @"Invalid username and/or password. Please check your credentials and try again.";
                            [PVPAlertHelper showAlertWithTitle:@"Login Failed!" andMessage:alertMessage]; //notify the user
                        }
                        [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                    }];
}

-(void) loginFacebookUser:(PVPLogin *)login
{
    [self deleteTokenFromDefaults];
    [[ApiManager sharedInstance] postRequest:@"facebookuser"
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        if (result.count > 0) {
                            [self saveTokenToDefaults:[result firstObject]];
                            [self.userModel getLoggedInUser];
                        }
                        else {
                            if ([PVPConfigHelper alertsEnabled]) {
                                [PVPAlertHelper showAlertWithTitle:@"Facebook Login Failed!" andMessage:@"There was a problem logging you in with your Facebook account. Please make sure your Facebook account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                            }
                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                        }
                    } failure:^(NSError *e, NSString *bambooError) {
                        if ([PVPConfigHelper alertsEnabled]) {
                            [PVPAlertHelper showAlertWithTitle:@"Facebook Login Failed!" andMessage:@"There was a problem logging you in with your Facebook account. Please make sure your Facebook account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                        }
                        [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                    }];
}

-(void) loginTwitterUser:(PVPLogin *)login
{
    [self deleteTokenFromDefaults];
    [[ApiManager sharedInstance] postRequest:@"twitteruser"
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        if (result.count > 0) {
                            [self saveTokenToDefaults:[result firstObject]];
                            [self.userModel getLoggedInUser];
                        } else {
                            if ([PVPConfigHelper alertsEnabled]) {
                                [PVPAlertHelper showAlertWithTitle:@"Twitter Login Failed!" andMessage:@"There was a problem logging you in with your Twitter account. Please make sure your Twitter account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                            }
                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                        }
                    } failure:^(NSError *e, NSString *bambooError) {
                        if (![login.id length]) {
                            [PVPAlertHelper showAlertWithTitle:@"Twitter Email" andMessage:@"Please enter your email address for this user" withCancelButtonTitle:ALERT_CANCEL withOtherButtonTitle:ALERT_OK withTextFieldType:UIKeyboardTypeEmailAddress withPlaceholder:@"Email address" withInitialText:nil withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                                if (confirmed) {
                                    login.id = alert.textFields[0].text;
                                    [self loginTwitterUser:login];
                                } else {
                                    [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                                }
                            }];
                        } else {
                            if ([PVPConfigHelper alertsEnabled]) {
                                [PVPAlertHelper showAlertWithTitle:@"Twitter Login Failed!" andMessage:@"There was a problem logging you in with your Twitter account. Please make sure your Twitter account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                            }
                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                        }
                    }];
}

-(void) loginSavedUser {
    NSDictionary *savedCredentials = [self getUserCredentials];
    if (savedCredentials[@"user"] != nil && savedCredentials[@"pass"] != nil) {
        PVPLogin *login = [[PVPLogin alloc] initWithUserId:savedCredentials[@"user"] withPassword:savedCredentials[@"pass"]];
        [self loginUser:login];
    } else {
        [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
    }
}

-(void) convertUserByUDIDToPVPUser:(PVPUser *)user
{
    __block PVPLogin *login = [[PVPLogin alloc] initWithUserId:user.id withPassword:user.password];
    NSDictionary *params = [[ApiManager sharedInstance] loggedInUser] ? @{@"convertFromUserId": [[ApiManager sharedInstance] loggedInUser].mongoId} : nil;
    
//    NSString *currentUserToken = [NSString stringWithString:[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY]]; //save the current token for later
    
    [[ApiManager sharedInstance] postRequest:@"user" //create the new PVP user
                     object:user
                     params:params
                    success:^(NSArray *result) {
                        [self loginUser:login];
                        //TODO: remove this part in the FB and Twitter conversions
//                        user = [result firstObject];
//                        [apiManager postRequest:@"token" //log in as the new user to receive a token
//                                         object:login
//                                         params:nil
//                                        success:^(NSArray *result) {
//                                            [apiManager updateUserToken:currentUserToken]; //switch back to the current token
//                                            PVPLogin *newLogin = [result firstObject];
//                                            
//                                            [self updateNewUserWithInfo:user withToken:newLogin.token];
//                                        } failure:^(NSError *e) {
//                                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
//                                        }];
                    } failure:^(NSError *e, NSString *bambooError) {
                        if ([PVPConfigHelper alertsEnabled]) {
                            [PVPAlertHelper showAlertWithDefaultErrorMessage];
                        }
                        [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                    }];
}

-(void) convertUserByUDIDToFacebookUser:(PVPLogin *)login
{
    NSString *currentUserToken = [[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY] copy]; //save the current token for later
    [[ApiManager sharedInstance] postRequest:@"facebookuser" //log in as the new user to receive a token
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        PVPLogin *newLogin = [result firstObject];
                        [[ApiManager sharedInstance] updateUserToken:newLogin.token];
                        
                        [[ApiManager sharedInstance] getRequest:@"user/0" //get the new user
                                        params:nil
                                       success:^(NSArray *result) {
                                           [[ApiManager sharedInstance] updateUserToken:currentUserToken]; //switch back to the current token
                                           PVPUser *user = [result firstObject];
                                           
                                           [self updateNewUserWithInfo:user withToken:newLogin.token];
                                       } failure:^(NSError *e) {
                                           [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                                       }];
                    } failure:^(NSError *e, NSString *bambooError) {
                        if ([PVPConfigHelper alertsEnabled]) {
                            [PVPAlertHelper showAlertWithTitle:@"Facebook Login Failed!" andMessage:@"There was a problem logging you in with your Facebook account. Please make sure your Facebook account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                        }
                        [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                    }];
}

-(void) convertUserByUDIDToTwitterUser:(PVPLogin *)login
{
    NSString *currentUserToken = [NSString stringWithString:[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY]]; //save the current token for later
    [[ApiManager sharedInstance] updateUserToken:@""];
    [[ApiManager sharedInstance] postRequest:@"twitteruser"
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        PVPLogin *newLogin = [result firstObject];
                        [[ApiManager sharedInstance] updateUserToken:newLogin.token];
                        
                        [[ApiManager sharedInstance] getRequest:@"user/0" //get the new user
                                        params:nil
                                       success:^(NSArray *result) {
                                           [[ApiManager sharedInstance] updateUserToken:currentUserToken]; //switch back to the current token
                                           PVPUser *user = [result firstObject];
                                           
                                           [self updateNewUserWithInfo:user withToken:newLogin.token];
                                       } failure:^(NSError *e) {
                                           [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                                       }];
                    } failure:^(NSError *e, NSString *bambooError) {
                        if (![login.id length]) {
                            [PVPAlertHelper showAlertWithTitle:@"Twitter Email" andMessage:@"Please enter your email address for this user" withCancelButtonTitle:ALERT_CANCEL withOtherButtonTitle:ALERT_OK withTextFieldType:UIKeyboardTypeEmailAddress withPlaceholder:@"Email address" withInitialText:nil withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                                if (confirmed) {
                                    login.id = alert.textFields[0].text;
                                    [self convertUserByUDIDToTwitterUser:login];
                                } else {
                                    [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                                }
                            }];
                        } else {
                            if ([PVPConfigHelper alertsEnabled]) {
                                [PVPAlertHelper showAlertWithTitle:@"Twitter Login Failed!" andMessage:@"There was a problem logging you in with your Twitter account. Please make sure your Twitter account is active and that you have provided the neseccary permissions, and try again."]; //notify the user
                            }
                            [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                        }
                    }];
}

-(void) updateNewUserWithInfo:(PVPUser *)newUser withToken:(NSString *)newUserToken
{
    NSString *currentUserToken = [NSString stringWithString:[AppUtils getDefaultValueFofKey:USER_TOKEN_KEY]]; //save the current token for later
    [[ApiManager sharedInstance] updateUserToken:newUserToken]; //change to the new token
    
    newUser.info.subscription = [[ApiManager sharedInstance] loggedInUser].info.subscription;
    newUser.info.deviceTokens = [[ApiManager sharedInstance] loggedInUser].info.deviceTokens;
    
    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"user/%@", newUser.mongoId]
                    object:newUser
                    params:nil
                   success:^(NSArray *result) {
                       [[ApiManager sharedInstance] updateUserToken:currentUserToken];
                       [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"user/%@", [[ApiManager sharedInstance] loggedInUser].mongoId]
                                          object:nil
                                          params:nil
                                         success:^(NSArray *result) {
                                             [[ApiManager sharedInstance] updateUserToken:newUserToken]; //change to the new token
                                             [[ApiManager sharedInstance] setLoggedInUser:newUser];
                                             [AppUtils saveDefaultValue:@"YES" forKey:LOGGED_IN_KEY];
                                             [AppUtils performSelector:@selector(loginRequestSuccess) on:self.delegate]; //successful login flow
                                         } failure:^(NSError *e) {
                                             [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                                         }];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                   }];
}

-(void) restoreCurrentSession
{
    [self.userModel getLoggedInUser];
}

- (void)clearUser {
    [self deleteTokenFromDefaults];
    [self deleteUserCredentials];
    [PVPSubscription emptyUserPurchases];
    [[ApiManager sharedInstance] setLoggedInUser:nil];
}

-(void) removeLoggedInUser
{
#if TARGET_OS_IOS
    [FIRAnalytics logEventWithName:kFIREventLogout parameters: nil ];
#endif

    [self deleteTokenFromDefaults];
    
    [PVPSubscription emptyUserPurchases];
    [[ApiManager sharedInstance] setLoggedInUser:nil];
    
    [AppUtils performSelector:@selector(logoutRequestSuccess) on:self.delegate];
    
    //DELETE TOKEN FROM SERVER
//    [apiManager deleteRequest:@"token"
//                       object:login
//                       params:nil
//                      success:^(NSArray *result) {
//                          [self deleteTokenFromDefaults];
//                          [self.delegate logoutRequestSuccess];
//                      } failure:^(NSError *e) {
//                          [self.delegate logoutRequestFail];
//                      }];
}

-(void)showForgotPasswordDialog
{
    [PVPAlertHelper showAlertWithTitle:[@"Forgot Password" localizedString]
                            andMessage:[@"Please enter your email address" localizedString]
                 withCancelButtonTitle:ALERT_CANCEL
                  withOtherButtonTitle:ALERT_OK
                     withTextFieldType:UIKeyboardTypeEmailAddress
                       withPlaceholder:[@"Email address" localizedString]
                       withInitialText:nil
                           withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                               if (confirmed) {
                                   [AppUtils performSelector:@selector(forgotPasswordSending) on:self.delegate];
                                   [self sendForgotPassword:alert.textFields[0].text];
                               }
                           }];
}

-(void)sendForgotPassword:(NSString *)email
{
    [[ApiManager sharedInstance] postRequest:[NSString stringWithFormat:@"user/%@/password", email]
                     object:nil
                     params:nil
                    success:^(NSArray *result) {
                        [PVPAlertHelper showAlertWithTitle:@"Email Sent" andMessage:@"An email has been sent to your account, containing instructions to reset your password."];
                        [AppUtils performSelector:@selector(forgotPasswordFinished) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [PVPAlertHelper showAlertWithTitle:@"User Not Found" andMessage:@"The email address you provided doesn't exist. Please correct it and try again."];
                        [AppUtils performSelector:@selector(forgotPasswordFailed) on:self.delegate];
                    }];
}

-(void) changePasswordForUser:(NSString *)userId withCurrentPassword:(NSString *)currentPassword andNewPassword:(NSString *)newPassword
{
    PVPLogin *login = [[PVPLogin alloc] initWithUserId:userId withPassword:currentPassword]; //try to log in with the current password (to check if its correct)
    [[ApiManager sharedInstance] updateUserToken:@""];
    [[ApiManager sharedInstance] postRequest:@"token"
                     object:login
                     params:nil
                    success:^(NSArray *result) {
                        [self saveTokenToDefaults:[result firstObject]];
                        PVPUser *updated = [[ApiManager sharedInstance] loggedInUser];
                        updated.password = newPassword;
                        [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"user/%@", [userId stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] //current password correct, update new password
                                        object:updated
                                        params:@{@"password_verify": newPassword}
                                       success:^(NSArray *result) {
                                           [AppUtils performSelector:@selector(changePasswordSuccess) on:self.delegate];
                                       } failure:^(NSError *e) {
                                           [AppUtils performSelector:@selector(changePasswordFailed) on:self.delegate];
                                       }];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [[ApiManager sharedInstance] updateUserTokenFromDefaults];
                        [AppUtils performSelector:@selector(changePasswordFailed) on:self.delegate];
                    }];
}

-(void) saveTokenToDefaults:(PVPLogin *)login
{
    NSLog(@"setting cookie...");
    if (login && [login isKindOfClass:[PVPLogin class]] && login.token) {
        [[ApiManager sharedInstance] updateUserToken:login.token];
        [AppUtils saveDefaultValue:[ [[ApiManager sharedInstance] loggedInUser].id isEqualToString:[PVPConfigHelper sharedInstance].uniqueDeviceIdentifier ] ? @"NO" : @"YES" forKey:LOGGED_IN_KEY];
    }
}

//+(PVPLogin *) getTokenFromDefaults
//{
//    PVPLogin *login = [[PVPLogin alloc] init];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    login.token = [defaults stringForKey:@"loggedInUserToken"];
//    return login;
//}

-(void) deleteTokenFromDefaults
{
    [AppUtils saveDefaultValue:@"NO" forKey:LOGGED_IN_KEY];
    [[ApiManager sharedInstance] updateUserToken:@""];
}


/**
 *  Saves user credentials for persistence
 */
-(void) saveUserCredentials:(NSString*)user withPassword:(NSString*)pass {
    [AppUtils saveDefaultDictionary:@{@"user": user, @"pass": pass} forKey:CREDENTIALS_DICT_KEY];
}

/**
 * Deletes user credentials
 */
-(void) deleteUserCredentials {
    [AppUtils deleteDefaultValueWithKey:CREDENTIALS_DICT_KEY];
}

/**
 * Gets user credentials
 */

- (NSDictionary*) getUserCredentials {
    return [AppUtils getDefaultDictionaryForKey:CREDENTIALS_DICT_KEY];
}



#pragma TODO replace with PVPAlertHelper
#if TARGET_OS_TV
#else
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case TWITTER_ALERT_TAG:
            switch (buttonIndex) {
                case 0: //cancel
                    [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
                    break;
                case 1: //ok
                    currentLogin.id = [alertView textFieldAtIndex:0].text;
                    [self loginTwitterUser:currentLogin];
                    break;
                default:
                    break;
            }
            break;
        case PASSWORD_ALERT_TAG:
            switch (buttonIndex) {
                case 0: //cancel
                    break;
                case 1: //ok
                    [AppUtils performSelector:@selector(forgotPasswordSending) on:self.delegate];
                    [self sendForgotPassword:[alertView textFieldAtIndex:0].text];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
}
#endif

#pragma mark user model delegate methods
-(void)userRequestSuccessWithUser:(PVPUser *)user
{
    [user.info updateCurrentDeviceToken];
    [[ApiManager sharedInstance] setLoggedInUser:user];
    [self userRequestSuccess];
    //user.id will be changed later (when the server is updated and ready) to user.mongoId
    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"user/0"]
                    object:user
                    params:nil
                   success:^(NSArray *result) {}
                   failure:^(NSError *e) {}];
}

-(void)userRequestSuccess
{
    [self.configModel getServerConfig];
}
-(void)userRequestFail:(NSString *)message
{
    [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
}
#pragma mark user model delegate methods
-(void)configRequestSuccess
{
    [AppUtils saveDefaultValue:[[[ApiManager sharedInstance] loggedInUser].id isEqualToString:[PVPConfigHelper sharedInstance].uniqueDeviceIdentifier] ? @"NO" : @"YES" forKey:LOGGED_IN_KEY];
    [AppUtils performSelector:@selector(loginRequestSuccess) on:self.delegate];
}
-(void)configRequestFail
{
    [AppUtils performSelector:@selector(loginRequestFail) on:self.delegate];
}

@end
