//
//  PVPBaseModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/8/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiManager.h"

#define OBJECT_TYPE_ENTRY 1
#define OBJECT_TYPE_USER 3
#define OBJECT_TYPE_INSTANCE 6
#define OBJECT_TYPE_PACKAGE 8

/**
 *  The base protocol for any PVPClient model.
 */
@protocol PVPBaseModelProtocol <NSObject>
@optional

/**
 *  Retrieves a list of objects with a given page index.
 *
 *  @param page The page number we would like to retrieve.
 */
-(void) listWithPage:(int)page;

/**
 *  Retrieves a PVP object with a given id.
 *
 *  @param idString The id of the object we wish to retrieve.
 */
-(void) get:(NSString *)idString;

@end

/**
 *  The PVPBaseModel provides an interface for using a service on the PVP system. Each model needs to inherit from this model and implement its own methods.
 */
@interface PVPBaseModel : NSObject

/**
 *  @name Public Methods
 */

/**
 *  Retrieves the first page of a list of objects.
 *  This function doesn't send any params, and therefore the server will always retrieve the first page, and use the default page size.
 */
-(void) list;

/**
 *  Retrieves a list of objects with a given page index.
 *
 *  @param page The page number we would like to retrieve.
 */
-(void) listWithPage:(int)page;

/**
 *  Retrieves a list of objects with a given page index and size.
 *
 *  @param page The page number we would like to retrieve.
 *  @param size The maximum amount of objects we would like to retrieve.
 */
-(void) listWithPage:(int)page withPageSize:(int)size;

/**
 *  Retrieves a list of objects with a given page index, size, and sort type.
 *
 *  @param page The page number we would like to retrieve.
 *  @param size The maximum amount of objects we would like to retrieve.
 *  @param sort Kaltura sort string
 */
-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort;

/**
 *  Retrieves a list of objects with a given page index, size, sort type, and search text.
 *
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 */
-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;
-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types;

/**
 *  Retrieves a list of the objects created by the user, with a given page index, size, sort type, and search text.
 *
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 */
-(void) myListWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of the objects created by a given user ID, with a given page index, size, sort type, and search text.
 *
 *  @param userId     The ID of the user that created the objects we want to retrieve.
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 *
 *  This method retrieves objects of media type VIDEO.
 *
 * @see [KalturaMediaType](http://www.kaltura.com/api_v3/testmeDoc/?object=KalturaMediaType)
 */
-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of the objects created by a given user ID, with a given page index, size, sort type, search text, and media type.
 *
 *  @param userId     The ID of the user that created the objects we want to retrieve.
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 *  @param mediaType  The media type
 *
 *  @see [KalturaMediaType](http://www.kaltura.com/api_v3/testmeDoc/?object=KalturaMediaType)
 */
-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types;

/**
 *  Retrieves a list of the objects created by a given user ID, with a given page index, size, sort type, and search text.
 *  This method also retrieves the PVPMeta object from the API call.
 *
 *  @param userId     The ID of the user that created the objects we want to retrieve.
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kalura sort string
 *  @param searchText The search text.
 *
 *  This method retrieves objects of media type VIDEO.
 *
 * @see [KalturaMediaType](http://www.kaltura.com/api_v3/testmeDoc/?object=KalturaMediaType)
 */
-(void) listWithMetaWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of purchased objects with page index, size, sort and search phrase.
 *
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 */
-(void) listMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of purchased objects with page index, size, sort and search phrase.
 *  This method will also return the PVPMeta object that comes with the response.
 *
 *  @param page       The page number we would like to retrieve.
 *  @param size       The maximum amount of objects we would like to retrieve.
 *  @param sort       Kaltura sort string
 *  @param searchText The search text.
 */
-(void) listMyPurchasedWithMetaWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of objects with a given page index, size, and MongoDB filter.
 *
 *  @param page The page number we would like to retrieve.
 *  @param size The maximum amount of objects we would like to retrieve.
 *  @param filter The MongoDB filter to apply to the search.
 *
 *  @see [MongoDB db.collection.find()](https://docs.mongodb.org/v3.0/reference/method/db.collection.find/)
 */
-(void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter;

/**
 *  Retrieves a list of objects with a given page index, size, MongoDB filter, and details to expand.
 *
 *  @param page   The page number we would like to retrieve.
 *  @param size   The maximum amount of objects we would like to retrieve.
 *  @param filter The MongoDB filter to apply to the search.
 *  @param sort   The parameters to sort by.
 *
 *  @see [MongoDB db.collection.find()](https://docs.mongodb.org/v3.0/reference/method/db.collection.find/)
 */
-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort;

/**
 *  Retrieves a list of objects with a given page index, size, MongoDB filter, and details to expand.
 *
 *  @param page   The page number we would like to retrieve.
 *  @param size   The maximum amount of objects we would like to retrieve.
 *  @param filter The MongoDB filter to apply to the search.
 *  @param expand The details to expand.
 *
 *  @see [MongoDB db.collection.find()](https://docs.mongodb.org/v3.0/reference/method/db.collection.find/)
 */
- (void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpand:(NSDictionary *)expand;

/**
 *  Retrieves a list of objects with a given page index, size, MongoDB filter, and details to expand.
 *
 *  @param page   The page number we would like to retrieve.
 *  @param size   The maximum amount of objects we would like to retrieve.
 *  @param filter The MongoDB filter to apply to the search.
 *  @param expand The details to expand.
 *  @param sort   The way we want to sort the results.
 *
 *  @see [MongoDB db.collection.find()](https://docs.mongodb.org/v3.0/reference/method/db.collection.find/)
 */
- (void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpand:(NSDictionary *)expand withSort:(NSDictionary *)sort;

/**
 *  Retrieves a list of objects with a given category name.
 *
 *  @param category The name of the category we would like to retrieve.
 *
 *  @see listWithCategory:withPage:
 *  @see listWithCategory:withPage:withPageSize:
 */
-(void) listWithCategory:(NSString*)category;

/**
 *  Retrieves a list of objects with a given category name and page number.
 *
 *  @param category The name of the category we would like to retrieve.
 *  @param page     The page number we would like to retrieve.
 *
 *  @see listWithCategory:
 *  @see listWithCategory:withPage:withPageSize:
 */
-(void) listWithCategory:(NSString*)category withPage:(int)page;

/**
 *  Retrieves a list of objects with a given category name, page number, and page size.
 *
 *  @param category The name of the category we would like to retrieve.
 *  @param page     The page number we would like to retrieve.
 *  @param size     The size of the page we would like to retrieve.
 *
 *  @see listWithCategory:
 *  @see listWithCategory:withPage:
 */
-(void) listWithCategory:(NSString*)category withPage:(int)page withPageSize:(int)size;
/**
 *
 *  Retrieves a list of objects with a given category name, page number, page size, and sort.
 *
 *  @param category The name of the category we would like to retrieve.
 *  @param page     The page number we would like to retrieve.
 *  @param size     The size of the page we would like to retrieve.
 *  @param sort     The sort type.
 *
 *  @see listWithCategory:
 *  @see listWithCategory:withPage:
 *  @see listWithCategory:withPage:withPageSize:
 */
-(void) listWithCategory:(NSString*)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)params;

/**
 *  Retrieves a PVP object with a given id.
 *
 *  @param idString The id of the object we wish to retrieve.
 */
-(void) get:(NSString *)idString;

/**
 *  Getter method for the APIManager singleton instance.
 *
 *  @return The APIManager singleton instance.
 */
-(ApiManager *) getApiManager;
-(ApiManager *) getCDNApiManager;

-(NSDictionary *)addIIDtoParams:(NSDictionary *)origParams;
-(NSDictionary *)addNoContentFilterToParamsIfNeeded:(NSDictionary *)origParams;

/**
*  Methods for the search API filter with support video and audio.
*/
- (NSString *)searchAPIFilterFrom:(NSArray *)filters;
- (NSArray *)searchVideoAudioFilterArray;

@end
