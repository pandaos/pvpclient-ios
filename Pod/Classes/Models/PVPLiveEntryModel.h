//
//  PVPLiveEntryModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/14/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"

@class PVPLiveEntry;
@class PVPLiveEntryPublish;

/**
 *  The PVPLiveEntryModel delegate provides an interface for responding to events from the PVPLiveEntryModel.
 */
@protocol PVPLiveEntryModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPLiveEntryModel update:to:]
 *  @see [PVPLiveEntryModel remove:]
 */
-(void)liveEntryRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPLiveEntry object has to be returned.
 *
 *  @param entry The PVPLiveEntry object returned from the API request.
 *
 *  @see [PVPLiveEntryModel get:]
 *  @see [PVPLiveEntryModel add:]
 */
-(void)liveEntryRequestSuccessWithLiveEntry:(PVPLiveEntry *)entry;

/**
 *  Triggered upon a successful API request where an array of PVPLiveEntry objects has to be returned.
 *
 *  @param entriesArray The array of PVPLiveEntry objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
-(void)liveEntryRequestSuccessWithLiveEntriesArray:(NSArray *)entriesArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 *  @see [PVPLiveEntryModel get:]
 *  @see [PVPLiveEntryModel add:]
 *  @see [PVPLiveEntryModel update:to:]
 *  @see [PVPLiveEntryModel remove:]
 *  @see [PVPLiveEntryModel publish:]
 *  @see [PVPLiveEntryModel unpublish:createVODEntry:]
 *  @see [PVPLiveEntryModel unpublishAndRemove:createVODEntry:]
 *  @see [PVPLiveEntryModel livetovod:]
 */
-(void)liveEntryRequestFail;

/**
 *  Triggered upon a successful viewers API request.
 *
 *  @param results The array of PVPUser objects which represent the requested viewers.
 *
 *  @see [PVPLiveEntryModel getEntryViewers:]
 */
-(void)liveEntryViewersSuccess:(NSArray *)results;

/**
 *  Triggered upon a failed viewers API request.
 *
 *  @see [PVPLiveEntryModel getEntryViewers:]
 */
-(void)liveEntryViewersFail;

/**
 *  Triggered upon a successful publish API request.
 *
 *  @see [PVPLiveEntryModel publish:]
 */
-(void)liveEntryPublishSuccess;

/**
 *  Triggered upon a successful unpublish API request.
 *
 *  @see [PVPLiveEntryModel publish:]
 */
-(void)liveEntryUnpublishSuccess;

/**
 *  Triggered upon a successful live-to-vod API request.
 *
 *  @see [PVPLiveEntryModel livetovod:]
 */
-(void)liveToVodSuccess;

/**
 *  Triggered upon a successful start-recording API request.
 *
 *  @see [PVPLiveEntryModel startrecording:]
 */
-(void)liveStartRecordingSuccess;

/**
 *  Triggered upon a successful purchase-related API request.
 *
 *  @see [PVPLiveEntryModel finalizePurchaseForEntry:withProduct:withRequestInfo:]
 *  @see [PVPLiveEntryModel verifyPurchaseForEntry:]
 */
-(void)liveEntryPurchaseSuccess;

/**
 *  Triggered upon a failed purchase-related API request.
 *
 *  @see [PVPLiveEntryModel finalizePurchaseForEntry:withProduct:withRequestInfo:]
 *  @see [PVPLiveEntryModel verifyPurchaseForEntry:]
 */
-(void)liveEntryPurchaseFail;

/**
 *  Triggered upon a successful is-live API request.
 *
 *  @param resultArray An array of PVPResult objects, representing the is-live status of each entry ID we sent in the request.
 */
-(void)liveEntryIsLiveSuccessWithArray:(NSArray *)resultArray;

@end


/**
 *  The PVPLiveEntryModel provides an interface for using the PVP live service.
 */
@interface PVPLiveEntryModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The PVPLiveEntryModel delegate provides an interface for responding to events from the PVPLiveEntryModel.
 */
@property (nonatomic, weak) id<PVPLiveEntryModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Adds a new PVPLiveEntry to the database.
 *
 *  @param entry The entry we wish to add.
 */
-(void) add:(PVPLiveEntry *)entry;

/**
 *  Publishes the PVPLiveEntry.
 *  This method creates this entry on Wowza, publishes it as "live", and starts the "please wait" video loop.
 *
 *  @param entry The PVPLiveEntry we wish to publish.
 */
-(void) publish:(PVPLiveEntryPublish *)entry;

/**
 *  Updates an existing PVPLiveEntry in the database.
 *
 *  @param entryId The ID of the PVPLiveEntry we wish to update.
 *  @param updated An updated version of the PVPLiveEntry we wish to update.
 */
-(void) update:(NSString *)entryId to:(PVPLiveEntry *)updated;

/**
 *  Deletes a PVPLiveEntry from the database.
 *
 *  @param entryId The ID of the PVPLiveEntry we wish to delete.
 */
-(void) remove:(NSString *)entryId;

/**
 *  Retrieves the viewers for a given live entry.
 *
 *  @param entryId The ID of the live entry that we would like to retrieve the viewers for.
 */
-(void) getEntryViewers:(NSString *)entryId;

/**
 *  Sets a new image for an entry.
 *
 *  @param imageUrl The URL of the image we would like to set for the entry.
 *  @param entryId  The ID of the entry we would like to set the image for.
 */
-(void) setImagelUrl:(NSString *)imageUrl forEntryId:(NSString *)entryId;

/**
 *  Starts recording video for a given entry.
 *  This makes Wowza switch the "please wait" loop with the actual captured video from the entry.
 *
 *  @param entry The entry we would like to start recording.
 */
-(void) startrecording:(PVPLiveEntryPublish *)entry;

/**
 *  Deletes the live entry and turns it into a VOD entry.
 *
 *  @param entryId The ID of the live entry we wish to turn into a VOD entry.
 */
-(void) livetovod:(NSString *)entryId;

/**
 *  Unpublishes the PVPLiveEntry.
 *  This switches the captured video with the "please wait" loop.
 *
 *  @param entryId   The ID of the PVPLiveEntry we wish to unpublish.
 *  @param createVOD A flag indicating wether to create a VOD entry with the video that was recorded so far.
 */
-(void) unpublish:(NSString *)entryId createVODEntry:(BOOL)createVOD;

/**
 *  Unpublishes the PVPLiveEntry, and deletes it.
 *  This switches the captured video with the "please wait" loop before deleting it.
 *
 *  @param entryId   The ID of the PVPLiveEntry we wish to unpublish and delete.
 *  @param createVOD A flag indicating wether to create a VOD entry with the video that was recorded so far.
 */
-(void) unpublishAndRemove:(NSString *)entryId createVODEntry:(BOOL)createVOD;

/**
 *  Checks wether an entry is currently live or not.
 *  This method can also work for multiple entries.
 *
 *  @param entryId the ID or IDs of the entry or entries we wish to check. You can send either one entry ID, or multiple entry IDs separated by a comma (<entryId1>,<entryId2>,....).
 */
-(void) checkIfLive:(NSString *)entryId;

/**
 *  Finalizes a purchase process for an entry, adds the purchased object to the list us the user's purchases.
 *
 *  @param entry   the entry we have purchased
 *  @param product the product that was purchased
 *  @param info    the purchase info (in this case, the receipt data)
 */
-(void) finalizePurchaseForEntry:(PVPLiveEntry *)entry withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info;

/**
 *  Verifies if the user has a valid receipt for this purchased entry.
 *
 *  @param entry the entry we wish to verify.
 */
-(void) verifyPurchaseForEntry:(PVPLiveEntry *)entry;
-(void) verifyPurchaseForEntry:(PVPLiveEntry *)entry withCompletion:(void(^)(BOOL isPurchased, NSError * error))completionBlock;

/**
 *  Indicates wether an entry is free or not.
 *
 *  @param entry The entry in question.
 *
 *  @return YES if free, NO otherwise.
 */
-(BOOL) isEntryFree:(PVPLiveEntry *)entry;

/**
 *  Indicates wether the entry has been previously purchased by the current user.
 *
 *  @param entryId The ID of the entry in question.
 *
 *  @return YES if purchased, NO otherwise.
 */
-(BOOL) isEntryPurchased:(NSString *)entryId;

/**
 *  Adds the current entry to the list of locally stored purchases.
 *
 *  @param entryId The ID of the entry that we wish to add.
 */
-(void) saveToPurchasedEntries:(NSString *)entryId;

@end
