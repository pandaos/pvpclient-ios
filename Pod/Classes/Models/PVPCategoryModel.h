//
//  PVPCategoryModel.h
//  Pods
//
//  Created by Roni Cohen on 7/10/16.
//
//

#import "PVPBaseModel.h"
#import "PVPCategory.h"

/**
 *  The PVPCategoryModelDelegate provides an interface for responding to events from the PVPCategoryModel.
 */
@protocol PVPCategoryModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a failed API request.
 */
- (void) categoryPurchaseValidationSuccess:(PVPCategory *)category;

- (void) entryRequestSuccessWithEntriesArray:(NSArray *)entriesArray;

/**
 *  Triggered upon a successful API request that returned an array of PVPCategory objects.
 *
 *  @param categoriesArray The array of PVPCategory objects returned from the API request.
 */
- (void) categoryPurchaseValidationFailed:(PVPCategory *)category;


/**
 *  Triggered upon a successful API request that returned an array of PVPCategory objects.
 *
 *  @param categoriesArray The array of PVPCategory objects returned from the API request.
 */
- (void) categoryRequestSuccessWithCategories:(NSArray <PVPCategory *>*)categoriesArray;

/**
 *  Triggered upon a failed API request.
 */
- (void) categoryRequestFail;

@end

/**
 *  The PVPCategoryModel provides an interface for using the PVP category service.
 */
@interface PVPCategoryModel : PVPBaseModel

/**
 *  Verifies if the user has a valid receipt for this purchased entry.
 *
 *  @param category the entry we wish to verify
 */

- (void) verifyPurchaseForCategory:(PVPCategory *)category;
- (void) verifyPurchaseForCategory:(PVPCategory *)category withSuccess:(void (^)(BOOL success))successBlock;

/**
 *  @name Public Methods
 */

/**
 * Retrieves a category tree with a given root category
 *
 * @param name The name of the root category for the tree we wish to retrieve.
 *
 * @see [PVPCategoryModel treeWithName:withParams:]
 */
- (void) treeWithName:(NSString*)name;

/**
 * Retrieves a category tree with a given root category and params
 *
 * @param name The name of the root category for the tree we wish to retrieve.
 * @param params Additional params we wish to pass.
 *
 * @see [PVPCategoryModel treeWithName:]
 */
- (void) treeWithName:(NSString*)name withParams:(NSDictionary*)params;

/**
 * Retrieves a category tree with a given root category and params
 *
 * @param name The name of the root category for the tree we wish to retrieve.
 * @param params Additional params we wish to pass.
 *
 * @see [PVPCategoryModel treeWithName:]
 */
- (void) treeWithName:(NSString*)name withParams:(NSDictionary*)params withSort:(NSDictionary*)sort;

- (void) getWatchHistoryTree;
- (void) getWatchHistoryTree:(NSString *)userId withCompletion:(void(^)(NSArray <PVPCategory *> *categories))completionBlock;

- (void) getPurchasehHistory;

- (void) getCategoryForPurchaseValidation:(NSString *)idString;
- (void) getCategoryWithID:(NSString *)idString withSuccess:(void (^)(PVPCategory *success))successBlock;
/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPCategoryModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPCategoryModelDelegate> delegate;

@end
