//
//  PVPUserModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/26/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPUserModel.h"
#import "IAPHelper.h"

@implementation PVPUserModel

-(void) list
{    
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user"]
                    params:nil
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

-(void)listWithPage:(int)page withPageSize:(int)size
{
    [self listWithPage:page withPageSize:size withFilter:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSearchText:(NSString *)searchText
{
    [self listWithPage:page withPageSize:size withRole:role withSort:nil withSearchText:searchText];
}

-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSort:(NSDictionary *)sort withSearchText:(NSString *)searchText
{
    NSDictionary *mongoFilter = searchText && searchText.length ? @{@"fullName" : @{@"$regex": searchText,
                                                                                    @"$options": @"i"},
                                                                    @"role": role} : @{@"role": role};
    NSDictionary *params = [self paramsWithPage:page withPageSize:size withFilter:mongoFilter withSort:sort];
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter
{
    NSDictionary *params = [self paramsWithPage:page withPageSize:size withFilter:filter];
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

-(void) get:(NSString *)idString
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@", idString]
                           params:nil
                          success:^(NSArray *result) {
                              [AppUtils performSelector:@selector(userRequestSuccessWithUser:) on:self.delegate withObject:result.firstObject];
                          } failure:^(NSError *e) {
                              [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                          }];
}

-(void) getLoggedInUser
{
    [[ApiManager sharedInstance] getRequest:@"user/0"
                    params:nil
                   success:^(NSArray *result) {
                       [[ApiManager sharedInstance] setLoggedInUser:result.firstObject];
                       [AppUtils performSelector:@selector(userRequestSuccessWithUser:) on:self.delegate withObject:result.firstObject];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

- (void) getLoggedInUserInstances
{
    [[ApiManager sharedInstance] getRequest:@"user/0/instances"
                    params:nil
                   success:^(NSArray *result) {
                       [[ApiManager sharedInstance] setLoggedInUserInstances:result];
                       [AppUtils performSelector:@selector(userRequestSuccessWithInstances) on:self.delegate];
                   }
                   failure:^(NSError *e) {
                       NSLog(@"%@", e);
                       [AppUtils performSelector:@selector(userRequestFailWithInstances) on:self.delegate];
                   }];
}

-(void) getFollowersForUser:(NSString *)idString
{
    [self getFollowersForUser:idString withPage:0 withPageSize:0 withMeta:YES];
}

-(void) getFollowersForUser:(NSString *)idString withPage:(int)page withPageSize:(int)size withMeta:(BOOL)withMeta
{
    [self getFollowersForUser:idString withPage:page withPageSize:size withMeta:withMeta withSearchText:nil];
    
}

-(void) getFollowersForUser:(NSString *)idString withPage:(int)page withPageSize:(int)size withMeta:(BOOL)withMeta withSearchText:(NSString *)searchText
{
    NSDictionary *mongoFilter = searchText && searchText.length ? @{@"fullName" : [PVPMongoHelper regexForValue:searchText caseSensitive:NO]} : nil;
    NSDictionary *params = [self paramsWithPage:page withPageSize:size withFilter:mongoFilter];
    if (withMeta) {
        [[ApiManager sharedInstance] getRequestWithMeta:[NSString stringWithFormat:@"user/%@/followers", idString]
                                params:params
                               success:^(NSArray *result) {
                                   [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                               } failure:^(NSError *e) {
                                   [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                               }];
    }
    else {
        [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/followers", idString]
                        params:params
                       success:^(NSArray *result) {
                           [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                       } failure:^(NSError *e) {
                           [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                       }];
    }
    
}

-(void) getFollowingForUser:(PVPUser *)user withPage:(int)page withPageSize:(int)size
{
    [self getFollowingForUser:user withPage:page withPageSize:size withSearchText:nil];
}

-(void) getFollowingForUser:(PVPUser *)user withPage:(int)page withPageSize:(int)size withSearchText:(NSString *)searchText
{
    NSDictionary *mongoFilter = searchText && searchText.length ? @{@"fullName" : @{@"$regex": searchText,
                                                                                    @"$options": @"i"}} : nil;
    NSDictionary *params = [self paramsWithPage:page withPageSize:size withFilter:mongoFilter];
    NSString *userId = (user == [[ApiManager sharedInstance] loggedInUser] || [user.mongoId isEqualToString:[[ApiManager sharedInstance] loggedInUser].mongoId]) ? @"0" : user.mongoId;
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/following", userId]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userRequestSuccessWithUsersArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

-(void) getSubscribersForUser:(PVPUser *)user
{
    [[ApiManager sharedInstance] getRequestWithMeta:@"user"
                            params:[self paramsSubscribersWithObjectId:user.mongoId
                                                        withObjectType:OBJECT_TYPE_USER withPage:0 withPageSize:0]
                           success:^(NSArray *result) {
                               if ([self.delegate respondsToSelector:@selector(userSubscribersMetaRequestSuccess:)]) {
                                   for (PVPBaseObject *object in result) {
                                       if ([object isKindOfClass:[PVPMeta class]]) {
                                           [AppUtils performSelector:@selector(userSubscribersMetaRequestSuccess:) on:self.delegate withObject:(PVPMeta *)object];
                                       }
                                   }
                               }
                           } failure:^(NSError *e) {
                               [AppUtils performSelector:@selector(userSubscribersRequestFail) on:self.delegate];
                           }];
}

-(void)getMyPurchases
{
    [self getPurchasesForUser:@"0"];
}

-(void) getPurchasesForUser:(NSString *)idString
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/purchase", idString]
                    params:nil
                   success:^(NSArray *result) {
                       [PVPSubscription processUserPurchases:result];
                       [AppUtils performSelector:@selector(userPurchasesRequestSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userPurchasesRequestFailed) on:self.delegate];
                   }];
}

-(BOOL) isUserPurchased:(NSString *)userId
{
    NSArray *purchases = [[IAPHelper sharedInstance].purchasedUsers copy];
    if ([purchases containsObject:userId]) {
        return YES;
    }
    return NO;
}

-(void)restorePurchases {
    PVPEncryptedData *restore = [[PVPEncryptedData alloc] initWithLocalPurchases];
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:nil
                     params:@{@"action": @"restore",
                              @"data": restore.data}
                    success:^(NSArray * _Nonnull result) {
                        [AppUtils performSelector:@selector(userRestorePurchasesSuccess) on:self.delegate];
                    } failure:^(NSError * _Nonnull e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userRestorePurchasesFail) on:self.delegate];
                    }];
}

-(void)restorePurchasesWithPlanProducts:(NSMutableArray <SKProduct *> *)products
{
    NSDictionary <NSString *, NSString *>*purchases = [AppUtils getDefaultDictionaryForKey:PURCHASES_DICT_KEY];
    NSMutableArray<NSDictionary *> *dictPurchases = [NSMutableArray new];
    
    for (NSString *purchase in [purchases allKeys]) {
    
        SKProduct *restoredProduct = nil;
        for (SKProduct *product in products) {
            if ([product.productIdentifier isEqualToString:purchase]) {
                restoredProduct = product;
                break;
            }
        }
        
        PVPPackage *pack = [[IAPHelper sharedInstance] packageForProductID:restoredProduct.productIdentifier];
        
        PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:pack.mongoId withObjectType:OBJECT_TYPE_PACKAGE];
        //NSString *price = restoredProduct ? [[IAPHelper sharedInstance] localizedPriceStringForProduct:restoredProduct] : @"";
        NSString *currency = restoredProduct ? [[IAPHelper sharedInstance] currencyCodeForProduct:restoredProduct] : @"";

        NSDictionary *dict = @{
                                @"objectId": subscription.objectId,
                                @"type": @(subscription.type),
                                @"service": @(subscription.service),
                                @"price": restoredProduct.price,
                                @"info": purchases[purchase] ? @{@"receipt": purchases[purchase]} : @{},
                                @"name": pack.name ?: @"",
                                @"currency": currency
                               };
        
        [dictPurchases addObject:dict];
    }
    
    if ([dictPurchases count] == 0) {
        [AppUtils performSelector:@selector(userRestorePurchasesSuccess) on:self.delegate];
        return;
    }
    
    PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:dictPurchases];

    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:nil
                     params:@{@"action": @"restore",
                              @"data": encryptedData.data}
                    success:^(NSArray * _Nonnull result) {
                        [AppUtils performSelector:@selector(userRestorePurchasesSuccess) on:self.delegate];
                    } failure:^(NSError * _Nonnull e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userRestorePurchasesFail) on:self.delegate];
                    }];
}

#pragma mark purchases
-(void) getUserSubscriptionProductForUser:(PVPUser *)user
{
    __block NSString *productId = nil;
    if (user.info.products && [user.info.products isKindOfClass:[NSDictionary class]] && [user.info.products count]) {
        if ([[user.info.products allKeys] containsObject:@"apple"]) {
            productId = [user.info.products valueForKey:@"apple"];
        }
    }
    if (productId) {
        [[IAPHelper sharedInstance] requestProductWithIdentifier:productId WithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success && [products count]) {
                [AppUtils performSelector:@selector(userSubscriptionProductRequestSuccess:) on:self.delegate withObject:products.firstObject];
            } else {
                [AppUtils performSelector:@selector(userSubscriptionProductRequestFail) on:self.delegate];
            }
        }];
    }
}

-(void) finalizePurchaseForUser:(PVPUser *)user withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info
{
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:user.mongoId withObjectType:OBJECT_TYPE_USER];
    NSString *price = [[AppUtils sharedUtils] localizedPriceStringForProduct:product];
    NSDictionary *params = nil;
    if ([PVPConfigHelper encryptPurchases]) {
        NSDictionary *dict = @{
                               @"objectId": subscription.objectId,
                               @"type": @(subscription.type),
                               @"service": @(subscription.service),
                               @"price": price,
                               @"info": info
                               };
        PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:@[dict]];
        params = @{
                   @"action": @"purchase",
                   @"data": encryptedData.data[0]
                   };
        subscription = nil;
    } else {
        params = @{
                   @"info": info,
                   @"price": price
                   };
    }
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:subscription
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(userSubscriptionPurchaseSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userSubscriptionPurchaseFail) on:self.delegate];
                    }];
}

- (void) finalizePurchaseForPlanWithProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info {
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:product.productIdentifier withObjectType:OBJECT_TYPE_PACKAGE];
    NSString *price = [[AppUtils sharedUtils] localizedPriceStringForProduct:product];
    
    NSDictionary *params = nil;
    if ([PVPConfigHelper encryptPurchases]) {
        NSDictionary *dict = @{
                               @"objectId": subscription.objectId,
                               @"type": @(subscription.type),
                               @"service": @(subscription.service),
                               @"price": price,
                               @"info": info
                               };
        PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:@[dict]];
        params = @{
                   @"action": @"purchase",
                   @"data": encryptedData.data[0]
                   };
        subscription = nil;
    } else {
        params = @{
                   @"info": info,
                   @"price": price
                   };
    }
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:subscription
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(userSubscriptionPurchaseSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userSubscriptionPurchaseFail) on:self.delegate];
                    }];
}

-(void) verifyPurchaseForUser:(PVPUser *)user
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", user.mongoId]
                    params:@{@"type": @(OBJECT_TYPE_USER)}
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userSubscriptionPurchaseSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userSubscriptionPurchaseFail) on:self.delegate];
                   }];
}

-(void) verifyPurchaseForLogin
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase"]
                                     params:@{@"iid" : [[PVPConfigHelper sharedInstance] currentInstanceId]}
                   success:^(NSArray *result) {
        
        NSArray *filteredResult = [result filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            if ([evaluatedObject isKindOfClass:[PVPSubscription class]]) {
                return  [(PVPSubscription *)evaluatedObject status] == 1;
            }
            return false;
        }]];
        
        [PVPSubscription processUserPurchases:filteredResult];
        
        for (PVPSubscription *purchase in filteredResult) {
            if (purchase.type == OBJECT_TYPE_INSTANCE && purchase.status == 1) {
                [AppUtils performSelector:@selector(userLoginPurchaseRequestSuccess:) on:self.delegate withObject:@(YES)];
                return;
            }
        }
        
        [[IAPHelper sharedInstance] verifyLoginPurchaseWithCompletionHandler:^(BOOL success) {
            [AppUtils performSelector:@selector(userLoginPurchaseRequestSuccess:) on:self.delegate withObject:@(success)];
        }];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userLoginPurchaseRequestFailed) on:self.delegate];
                   }];
}

-(void) add:(PVPUser *)user {
    [self add:user withParams:nil];
}

-(void) add:(PVPUser *)user withPrivateInfo:(NSDictionary *)privateInfo {
    [self add:user withParams:@{@"privateInfo": privateInfo}];
}

-(void) add:(PVPUser *)user withParams:(NSDictionary *)params
{
    NSDictionary *userDict = [user jsonObject];
    
    if ([params count] > 0) {
        userDict = [userDict dictionaryByMergingWithDictionary:params];
    }
    
    [[ApiManager sharedInstance] postRequest:@"user"
                     object:nil
                     params:userDict
                    success:^(NSArray *result) {
                        [AppUtils saveDefaultValue:@"YES" forKey:@"socialAutoPost"];
                        [AppUtils performSelector:@selector(userRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:bambooError];
                    }];
}

- (void) updateTo:(PVPUser *)updated {
    [self update:@"0" to:updated];
}

-(void) update:(NSString *)userId to:(PVPUser *)updated
{
    [self update:userId to:updated withPrivateInfo:nil];
}

-(void) update:(NSString *)userId to:(PVPUser *)updated withPrivateInfo:(NSDictionary *)privateInfo {
    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"user/%@", userId]
                    object:updated
                    params:privateInfo ? @{@"privateInfo": privateInfo} : nil
                   success:^(NSArray *result) {
                        [[ApiManager sharedInstance] setLoggedInUser:[result firstObject]];
                       [AppUtils performSelector:@selector(userRequestSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                   }];
}

-(void) remove:(NSString *)userId
{
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"user/%@", userId]
                       object:nil
                       params:nil
                      success:^(NSArray *result) {
                          [AppUtils performSelector:@selector(userRequestSuccess) on:self.delegate];
                      } failure:^(NSError *e) {
                          [AppUtils performSelector:@selector(userRequestFail:) on:self.delegate withObject:nil];
                      }];
 
}

-(void) followUser:(NSString *)userId
{
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:userId withObjectType:OBJECT_TYPE_USER];
    [[ApiManager sharedInstance] postRequest:@"user/0/following"
                     object:subscription
                     params:nil
                    success:^(NSArray *result) {
                        [[ApiManager sharedInstance] setLoggedInUser:[result firstObject]];
                        [AppUtils performSelector:@selector(followRequestSuccess) on:self.delegate];
                     } failure:^(NSError *e, NSString *bambooError) {
                         [AppUtils performSelector:@selector(followRequestFail) on:self.delegate];
                     }];
}

-(void) unfollowUser:(NSString *)userId
{
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"user/0/following/%@", userId]
                     object:nil
                     params:nil
                    success:^(NSArray *result) {
                        if (result && [result count] && [[result firstObject] isKindOfClass:[PVPUser class]]) {
                            [[ApiManager sharedInstance] setLoggedInUser:[result firstObject]];
                        }
                        [AppUtils performSelector:@selector(followRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e) {
                        [AppUtils performSelector:@selector(followRequestFail) on:self.delegate];
                    }];
}

-(BOOL) isFollowingUser:(NSString *)userId
{
    NSArray *followingUsers = [[ApiManager sharedInstance] loggedInUser].info.subscription;
    if (!followingUsers || ![followingUsers isKindOfClass:[NSArray class]]) {
        return NO;
    }
    for (__strong NSDictionary *user in followingUsers) {
        if ([user isKindOfClass:[NSString class]]) {
            NSString *userString = [NSString stringWithFormat:@"%@", user];
            NSData *data = [userString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            user = [NSDictionary dictionaryWithDictionary:dict];
        }
        if (user && [user objectForKey:@"id"] && [[user objectForKey:@"id"] isEqualToString:userId]) {
            return YES;
        }
    }
    return NO;
}

-(void) likeUser:(NSString *)userId
{
    PVPLike *like = [[PVPLike alloc] initWithObjectId:userId withObjectType:OBJECT_TYPE_USER];
    [[ApiManager sharedInstance] postRequest:@"user/0/like"
                     object:like
                     params:nil
                    success:^(NSArray *result) {
                        [[ApiManager sharedInstance] setLoggedInUser:[result firstObject]];
                        [AppUtils performSelector:@selector(likeRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(likeRequestFail) on:self.delegate];
                    }];
}

-(BOOL) didLikeUser:(NSString *)userId
{
    NSArray *userLikes = [[ApiManager sharedInstance] loggedInUser].info.like;
    if (!userLikes || ![userLikes isKindOfClass:[NSArray class]]) {
        return NO;
    }
    for (__strong NSDictionary *user in userLikes) {
        if ([user isKindOfClass:[NSString class]]) {
            NSString *userString = [NSString stringWithFormat:@"%@", user];
            NSData *data = [userString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            user = [NSDictionary dictionaryWithDictionary:dict];
        }
        if (user && [user objectForKey:@"id"] && [[user objectForKey:@"id"] isEqualToString:userId]) {
            return YES;
        }
    }
    return NO;
}

-(void) getWatchHistory {
    [self getWatchHistory:@"0"];
}

-(void) getWatchHistory:(NSString *)userId {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
            @"sort": [PVPMongoHelper sortByField:@"name" order:PVPMongoOrderAscending],
            @"expand": @[@"entry"],
    }];
    for (NSString *key in [params allKeys]) {
        if ([params[key] isKindOfClass:NSDictionary.class]) {
            params[key] = [NSString JSONStringFromDictionary:params[key]];
        }
    }

    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/history", userId]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(userWatchHistorySuccessWithHistory:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(userWatchHistoryFail) on:self.delegate];
                   }];
}

-(void)updateUserWatching:(PVPWatchHistoryItem *)watchingItem {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"entry": watchingItem.entry.id,
                                                                                  @"time": @(watchingItem.time),
                                                                                  @"state": @(watchingItem.state),
                                                                                  @"expand": @[@"entry"]
                                                                                  }];
    [[ApiManager sharedInstance] postRequest:@"user/0/history"
                     object:nil
                     params:params
                    success:^(NSArray * _Nonnull result) {
                        [AppUtils performSelector:@selector(userWatchHistorySuccess) on:self.delegate];
                    } failure:^(NSError * _Nonnull e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userWatchHistoryFail) on:self.delegate];
                    }];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter
{
    return [self paramsWithPage:page withPageSize:size withFilter:filter withSort:nil];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:5];
    if (!filter) {
        if ( [[ApiManager sharedInstance] loggedInUser] ) {
            filter = @{@"_id": @{@"$nin": @[[[ApiManager sharedInstance] loggedInUser].mongoId]}}; //we do not want to include the logged in user in the results
        }
    }
    else {
        NSMutableDictionary *mutableFilter = [NSMutableDictionary dictionaryWithDictionary:filter];
        if (!mutableFilter[@"_id"]) {
            mutableFilter[@"_id"] = [NSMutableDictionary dictionaryWithCapacity:1];
        }
        if ( [[ApiManager sharedInstance] loggedInUser] ) {
            mutableFilter[@"_id"][@"$nin"] = @[[[ApiManager sharedInstance] loggedInUser].mongoId]; //we do not want to include the logged in user in the results
        }
        filter = mutableFilter;
    }
    
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}

-(NSDictionary *)paramsSubscribersWithObjectId:(NSString *)objectId withObjectType:(int)objectType withPage:(int)page withPageSize:(int)size
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:5];
    NSDictionary *purchaseFilter = @{@"purchases": @{@"objectId": objectId, @"type": @(objectType)}};
    if (size > 0) {
        params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    }
    params[@"filter"] = [NSString JSONStringFromDictionary:purchaseFilter];
    
    return params;
}

#pragma mark message and conversation related methods
- (void) getConverstations:(PVPUser *)user {
    [[ApiManager sharedInstance] getRequestWithMeta:@"user"
                            params:[self paramsSubscribersWithObjectId:user.mongoId
                                                        withObjectType:OBJECT_TYPE_USER withPage:0 withPageSize:0]
                           success:^(NSArray *result) {
                               if ([self.delegate respondsToSelector:@selector(userSubscribersMetaRequestSuccess:)]) {
                                   for (PVPBaseObject *object in result) {
                                       if ([object isKindOfClass:[PVPMeta class]]) {
                                           [AppUtils performSelector:@selector(userSubscribersMetaRequestSuccess:) on:self.delegate withObject:(PVPMeta *)object];
                                       }
                                   }
                               }
                           } failure:^(NSError *e) {
                               [AppUtils performSelector:@selector(userSubscribersRequestFail) on:self.delegate];
                           }];
}
#pragma mark blocking related methods

- (void) blockUser:(NSString *)userId WithReason:(int)reason {
    
    [[ApiManager sharedInstance] postRequest:@"user/0/block"
                     object:nil
                     params:[self paramsBlockWithUserId:@"0" WithObjectId:userId withObjectType:OBJECT_TYPE_USER withBlockReason:reason]
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(userBlockRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(userBlockRequestFail) on:self.delegate];
                    }];
}

- (NSDictionary *)paramsBlockWithUserId:(NSString *)userId WithObjectId:(NSString *)objectId withObjectType:(int)objectType  withBlockReason:(int)blockReason {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"id"] = userId;
    params[@"objectId"] = objectId;
    params[@"objectType"] = @(objectType);
    params[@"blockReason"] = @(blockReason);
    
    return params;
}


@end
