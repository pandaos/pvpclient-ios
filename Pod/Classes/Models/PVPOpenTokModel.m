//
//  PVPOpenTokModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 11/1/15.
//  Copyright © 2015 Panda-OS. All rights reserved.
//

#import "PVPOpenTokModel.h"

@implementation PVPOpenTokModel

-(void)initiateCallWithUser:(PVPUser *)user
{
    [[ApiManager sharedInstance] getRequest:@"opentok"
                    params:@{@"userIdsIn": user.id}
                   success:^(NSArray *result) {
                       PVPOpenTok *openTok = [result firstObject];
                       [AppUtils performSelector:@selector(callRequestSuccessWithResult:) on:self.delegate withObject:openTok];
                   } failure:^(NSError *e) {
                       NSLog(@"boo!!!");
                       [AppUtils performSelector:@selector(callRequestFail) on:self.delegate];
                   }];
}

@end
