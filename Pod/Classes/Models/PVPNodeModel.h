//
//  PVPNodeModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"
#import "PVPNode.h"

/**
 *  The PVPNodeModel delegate provides an interface for responding to events from the PVPNodeModel.
 */
@protocol PVPNodeModelDelegate <NSObject>

@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPNodeModel update:to:]
 *  @see [PVPNodeModel remove:]
 */
-(void)nodeRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPNode object has to be returned.
 *
 *  @param node The PVPNode object returned from the API request.
 *
 *  @see [PVPNodeModel get:]
 *  @see [PVPNodeModel add:]
 */
-(void)nodeRequestSuccessWithNode:(PVPNode *)node;

/**
 *  Triggered upon a successful API request where an array of PVPNode objects has to be returned.
 *
 *  @param nodesArray The array of PVPNode objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
-(void)nodeRequestSuccessWithNodeArray:(NSArray *)nodesArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 *  @see [PVPNodeModel get:]
 *  @see [PVPNodeModel add:]
 *  @see [PVPNodeModel update:to:]
 *  @see [PVPNodeModel remove:]
 *  @see [PVPNodeModel uploadImage:]
 */
-(void)nodeRequestFail;

@end

/**
 *  The PVPNodeModel provides an interface for using the PVP node service.
 */
@interface PVPNodeModel : PVPBaseModel <PVPBaseModelProtocol, ASIHTTPRequestDelegate>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPNodeModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPNodeModelDelegate> delegate;


- ( void ) getNodeByUrl: ( NSString * ) idString;
- ( void ) get: ( NSString * ) idString;

/**
 *  Adds a new PVPNode to the database.
 *
 *  @param node The node we wish to add.
 */
-(void) add:(PVPNode *)node;

/**
 *  Updates an existing PVPNode in the database.
 *
 *  @param nodeId The ID of the PVPNode we wish to update.
 *  @param updated An updated version of the PVPNode we wish to update.
 */
-(void) update:(NSString *)nodeId to:(PVPNode *)updated;

/**
 *  Deletes a PVPNode from the database.
 *
 *  @param nodeId The ID of the PVPNode we wish to delete.
 */
-(void) remove:(NSString *)nodeId;

@end
