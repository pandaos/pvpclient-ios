//
//  PVPConfigModel.h
//  Pods
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import "PVPBaseModel.h"
#import "IAPHelper.h"

@class PVPConfig;

/**
 *  The PVPConfigModelDelegate protocol provides an interface for responding to events from the PVPConfigModel.
 *  Each method is triggered for the delegate, if it implements it.
 */
@protocol PVPConfigModelDelegate <NSObject>
@optional

/**
 *  Triggered on success of retrieving the server config.
 *
 *  @see [PVPConfigModel getServerConfig]
 */
-(void)configRequestSuccess;

/**
 *  Triggered on success of retrieving the initial server config.
 *
 *  @see [PVPConfigModel getInitialServerConfig]
 */
-(void)initialConfigRequestSuccess;

/**
 *  Triggered on failure of a config API call.
 */
-(void)configRequestFail;

@end

/**
 *  The PVPConfigModel provides an interface for using the PVP config service, and handling the PVP server config.
 */
@interface PVPConfigModel : PVPBaseModel

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPConfigModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPConfigModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Gets the initial server config (The config you get when you're not logged in)
 *  This method also uses the retrieved config to initialize different variables needed for the app when it starts.
 *
 */
-(void)getInitialServerConfig; //DEPRECATED_ATTRIBUTE_MESSAGE("This method has been deprecated. Please use: initConfig")

/**
 *  Gets the initial server config (The config you get when you're logged in)
 *  This method also uses the retrieved config to initialize different variables needed for the app when it starts.
 */
-(void) getServerConfig;

/**
 *  Gets the initial server config (The config you get when you're not logged in)
 *  This method also uses the retrieved config to initialize different variables needed for the app when it starts.
 */
-(void)initConfig;


@end
