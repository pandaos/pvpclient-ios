//
//  PVPCategoryModel.m
//  Pods
//
//  Created by Roni Cohen,  on 7/10/16.
//
//

#import "PVPCategoryModel.h"

@implementation PVPCategoryModel

-(void)listWithPage:(int)page withPageSize:(int)size {
    [self listWithPage:page withPageSize:size withFilter:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    [self listWithPage:page withPageSize:size withFilter:filter withSort:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter withSort:sort]];
    
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];
    
    [api getRequest:[NSString stringWithFormat:@"category"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(categoryRequestSuccessWithCategories:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:nil] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@/entries", category]
             params:params
            success:^(NSArray *result) {
                [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
            } failure:^(NSError *e) {
                [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
            }];
}

-(void)treeWithName:(NSString*)name {
    [self treeWithName:name withParams:nil];
}

-(void)treeWithName:(NSString*)name withParams:(NSDictionary*)params {
    NSDictionary *newParams = [self addIIDtoParams:params];
    newParams = [self addNoContentFilterToParamsIfNeeded:newParams];
    
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];
    NSString *url = [NSString stringWithFormat:@"categorytree/%@", name];
    [api getRequest:url
                    params:newParams
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(categoryRequestSuccessWithCategories:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
                   }];
}

-(void)treeWithName:(NSString*)name withParams:(NSDictionary*)params withSort:(NSDictionary*)sort {
    NSDictionary *newParams = [self addIIDtoParams:params];
    newParams = [self addNoContentFilterToParamsIfNeeded:newParams];

    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    NSString *url = [NSString stringWithFormat:@"categorytree/%@", name];
    [api getRequest:url
                    params:newParams
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(categoryRequestSuccessWithCategories:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
                   }];
}

-(void) getWatchHistoryTree {// user for viewing "My History"
    [self getWatchHistoryTree:@"0" withCompletion:nil];
}

-(void) getWatchHistoryTree:(NSString *)userId withCompletion:(void(^)(NSArray <PVPCategory *> *categories))completionBlock { // user for viewing "My History"
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"tree": @"true"
                                                                                  }];
    for (NSString *key in [params allKeys]) {
        if ([params[key] isKindOfClass:NSDictionary.class]) {
            params[key] = [NSString JSONStringFromDictionary:params[key]];
        }
    }
    
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/historytree", userId]
                    params:params
                   success:^(NSArray *result) {
      NSArray* reversedResult = [[result reverseObjectEnumerator] allObjects];
                        if (completionBlock == nil) {
                            [AppUtils performSelector:@selector(categoryRequestSuccessWithCategories:) on:self.delegate withObject: reversedResult];
                        } else {
                            completionBlock(result);
                        }
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       if (completionBlock == nil) {
                           [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
                       } else {
                           completionBlock(nil);
                       }
                   }];
    
}

- (void) getCategoryWithID:(NSString *)idString withSuccess:(void (^)(PVPCategory *success))successBlock {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@", idString]
             params:params
            success:^(NSArray *result) {
                successBlock(result.firstObject);
            } failure:^(NSError *e) {
                successBlock(nil);
            }];
}

-(void) getCategoryForPurchaseValidation:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@", idString]
             params:params
            success:^(NSArray *result) {
                [self verifyPurchaseForCategory:result.firstObject];
            } failure:^(NSError *e) {
                [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
            }];
}

-(void) verifyPurchaseForCategory:(PVPCategory *)category {

    if ( [category.info isEqual: @(YES)] ) { // if we have a package we need to check if the user has watching rights
        if( [category.info objectForKey:@"packages"] ){
            NSDictionary *params = @{@"type": @(PURCHASE_TYPE_CATEGORY)};

            [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%d", category.id]
                            params:params
                           success:^(NSArray *result) {
                               if (result) { // if result == true
                                   if(self.delegate != nil) {
                                       [self.delegate categoryPurchaseValidationSuccess:category];
                                   }
                               } else { // positive validation fail
                                   if(self.delegate != nil) {
                                       [self.delegate categoryPurchaseValidationFailed:category];
                                   }
                               }
                           } failure:^(NSError *e) { // HTTP fail not validation fail (false positive)
                               if(self.delegate != nil) {
                                   [self.delegate categoryPurchaseValidationFailed:category];
                               }
                           }];
        } else {
            if(self.delegate != nil) {
                [self.delegate categoryPurchaseValidationSuccess:category];
            }
        }
    } else { // else it's open
        if(self.delegate != nil) {
            [self.delegate categoryPurchaseValidationSuccess:category];
        }
    }
}

- (void)verifyPurchaseForCategory:(PVPCategory *)category withSuccess:(void (^)(BOOL success))successBlock{

    if ([category.accessPackages count] > 0) {
        NSDictionary *params = @{@"type": @(PURCHASE_TYPE_CATEGORY)};
        [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%d", category.id]
                        params:params
                       success:^(NSArray *result) {
                            successBlock((result != nil));
                       } failure:^(NSError *e) {
                           successBlock(NO);
                       }];
    }
}


-(void) getPurchasehHistory {// user for viewing "My Purchases"
    [self getPurchasehHistory:@"0"];
}

-(void) getPurchasehHistory:(NSString *)userId { // user for viewing "My Purchases"
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"expand": @[@"entries"],
                                                                                  @"purchasesTree": @"true"
                                                                                  }];
    for (NSString *key in [params allKeys]) {
        if ([params[key] isKindOfClass:NSDictionary.class]) {
            params[key] = [NSString JSONStringFromDictionary:params[key]];
        }
    }

    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/purchasetree", userId]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(categoryRequestSuccessWithCategories:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(categoryRequestFail) on:self.delegate];
                   }];
    
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    return [self paramsWithPage:page withPageSize:size withFilter:filter withSort:nil];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:3];
    
    NSDictionary *finalFilter = [self addNoContentFilterToParamsIfNeeded:filter];
    
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:finalFilter];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}

@end
