//
//  PVPEventModel.m
//  Pods
//
//  Created by Jacob Barr on 10/04/2016.
//
//

#import "PVPEventModel.h"

@implementation PVPEventModel

- (void) listWithPage:(int)page withPageSize:(int)size {
    [self listWithPage:page withPageSize:size withFilter:nil];
}

- (void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpand:(NSDictionary *)expand withSort:(NSDictionary *)sort {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithUserId:@"0" withPage:page withPageSize:size withFilter:filter withExpandUserDetails:expand withSort:sort] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"event"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(eventRequestSuccessWithEventsArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(eventRequestFail) on:self.delegate];
                   }];
}

- (NSDictionary *)paramsWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpandUserDetails:(NSDictionary *)expandUserDetails withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"userId"] = userId;
    if (expandUserDetails) {
        params[@"expand"] = [NSString JSONStringFromDictionary:expandUserDetails];
        params[@"useRedis"] = @(NO);
    }
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}

- (void) fire:(PVPEvent *)event {
    [[ApiManager sharedInstance] postRequest:@"event"
                     object:event
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(eventRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(eventRequestFail) on:self.delegate];
                    }];
}

@end
