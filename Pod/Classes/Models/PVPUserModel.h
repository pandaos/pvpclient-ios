//
//  PVPUserModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/26/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVPBaseModel.h"
#import "PVPSubscription.h"
#import "PVPMongoHelper.h"
#import "PVPLike.h"

@class PVPUser;

/**
 *  The PVPUserModelDelegate protocol provides an interface for responding to events from the PVPUserModel.
 */
@protocol PVPUserModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPUserModel add:]
 *  @see [PVPUserModel update:to:]
 *  @see [PVPUserModel remove:]
 */
-(void)userRequestSuccess;

/**
 *  Triggered upon a failed API request.
 */
-(void)userRequestFail:(NSString *)message;

/**
 *  Triggered upon a successful API request that returned a PVPUser object.
 *
 *  @param user The PVPUser object that has returned from the API request.
 *
 *  @see [PVPUserModel get:]
 */
-(void)userRequestSuccessWithUser:(PVPUser *)user;

/**
 *  Triggered upon a successful API request that returned an array of PVPUser objects.
 *
 *  @param usersArray The array of PVPUser objects that has returned from the API request.
 *
 *  @see [PVPUserModel list]
 *  @see [PVPUserModel listWithPage:withPageSize:]
 *  @see [PVPUserModel listWithPage:withPageSize:withFilter:]
 */
-(void)userRequestSuccessWithUsersArray:(NSArray *)usersArray;

/**
 *  Triggered upon a successful follow or unfollow API request.
 *
 *  @see [PVPUserModel followUser:]
 *  @see [PVPUserModel unfollowUser:]
 */
-(void)followRequestSuccess;

/**
 *  Triggered upon a failed follow or unfollow API request.
 *
 *  @see [PVPUserModel followUser:]
 *  @see [PVPUserModel unfollowUser:]
 */
-(void)followRequestFail;

/**
 *  Triggered upon a successful subscribers API request, in which we wished to retrieve the PVPMeta object.
 *
 *  @param meta The PVPMeta object returned from the API request.
 *
 *  @see [PVPUserModel getSubscribersForUser:]
 */
-(void)userSubscribersMetaRequestSuccess:(PVPMeta *)meta;

/**
 *  Triggered upon a successful subscribers API request.
 *
 *  @see [PVPUserModel getSubscribersForUser:]
 */
-(void)userSubscribersRequestFail;

/**
 *  Triggered upon a successful purchases API request.
 *  This function also saves the retrieved purchases locally on the app.
 *
 *  @see [PVPUserModel getMyPurchases]
 *  @see [PVPUserModel getPurchasesForUser:]
 */
-(void)userPurchasesRequestSuccess;
-(void)userPurchasesRequestFailed;

-(void)userLoginPurchaseRequestSuccess:(NSNumber *)result;
-(void)userLoginPurchaseRequestFailed;

/**
 *  Triggered upon a successful purchase process, or after a successful validation with the server.
 *
 *  @see [PVPUserModel finalizePurchaseForUser:withProduct:withRequestInfo:]
 *  @see [PVPUserModel verifyPurchaseForUser:]
 */
-(void)userSubscriptionPurchaseSuccess;

/**
 *  Triggered upon a failed purchase process, or after a failed validation with the server.
 *
 *  @see [PVPUserModel finalizePurchaseForUser:withProduct:withRequestInfo:]
 *  @see [PVPUserModel verifyPurchaseForUser:]
 */
-(void)userSubscriptionPurchaseFail;

/**
 *  Triggered upon a successful subscription IAP product request for a user.
 *
 *  @param product The IAP product linked to the user.
 *
 *  @see [PVPUserModel getUserSubscriptionProductForUser:]
 */
-(void)userSubscriptionProductRequestSuccess:(SKProduct *)product;

/**
 *  Triggered upon a failed subscription IAP product request for a user.
 *
 *  @see [PVPUserModel getUserSubscriptionProductForUser:]
 */
-(void)userSubscriptionProductRequestFail;

/**
 *  Triggered upon a successful API request where no object has to be returned.
 */
-(void)userRestorePurchasesSuccess;

/**
 *  Triggered upon a failed API request.
 */
-(void)userRestorePurchasesFail;

/**
 *  Triggered upon a successful block API request.
 */
- (void)userBlockRequestSuccess;

/**
 *  Triggered upon a failed block API request.
 */
- (void)userBlockRequestFail;

/**
 *  Triggered upon a successful like API request.
 */
-(void)likeRequestSuccess;

/**
 *  Triggered upon a failed like API request.
 */
-(void)likeRequestFail;

/**
 *  Triggered upon a successful API request that returned an array of PVPWatchHistoryItem objects.
 *
 *  @param historyArray The array of PVPWatchHistoryItem objects that has returned from the API request.
 */
-(void)userWatchHistorySuccessWithHistory:(NSArray <PVPWatchHistoryItem *>*)historyArray;

/**
 *  Triggered upon a successful API request.
 */
-(void)userWatchHistorySuccess;

/**
 *  Triggered upon a failed watch history API request.
 *
 */
-(void)userWatchHistoryFail;

@end

/**
 *  The PVPUserModel provides an interface for using the PVP user service.
 */
@interface PVPUserModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPUserModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPUserModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Retrieves a list of users with a given page index, page size, user role, and search text (case insensitive)
 *
 *  @param page            The page index
 *  @param size            The page size
 *  @param role            The user role
 *  @param searchText      The search text
 */
-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSearchText:(NSString *)searchText;

/**
 *  Retrieves a list of users with a given page index, page size, user role, sort, and search text (case insensitive)
 *
 *  @param page       The page index
 *  @param size       The page size
 *  @param role       The user role
 *  @param sort       The sort parameters
 *  @param searchText The search text
 */
-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSort:(NSDictionary *)sort withSearchText:(NSString *)searchText;

/**
 *  Retrieves the currently logged in user.
 */
-(void) getLoggedInUser;

/**
 *  Retrieves the currently logged in user IIDs.
 */
- ( void ) getLoggedInUserInstances;
- ( void ) userRequestSuccessWithInstances;
- ( void ) userRequestFailWithInstances;

/**
 *  Retrieves the list of followers for a given user.
 *
 *  @param idString The ID of the user whos followers we wish to retrieve.
 */
-(void) getFollowersForUser:(NSString *)idString;

/**
 *  Retrieves the list of followers for a given user, with a page index, size, and optionaly the PVPMeta object.
 *
 *  @param idString The ID of the user whos followers we wish to retrieve.
 *  @param page     The page index.
 *  @param size     The page size.
 *  @param withMeta A flag that indicates whether to include the PVPMeta object or not.
 */
-(void) getFollowersForUser:(NSString *)idString withPage:(int)page withPageSize:(int)size withMeta:(BOOL)withMeta;

/**
 *  Retrieves the list of followers for a given user, with a page index, size, optionaly the PVPMeta object, and a search expression.
 *
 *  @param idString The ID of the user whos followers we wish to retrieve.
 *  @param page     The page index.
 *  @param size     The page size.
 *  @param withMeta A flag that indicates whether to include the PVPMeta object or not.
 *  @param searchText The search expression.
 */
-(void) getFollowersForUser:(NSString *)idString withPage:(int)page withPageSize:(int)size withMeta:(BOOL)withMeta withSearchText:(NSString *)searchText;

/**
 *  Retrieves the list of the users that a given user is following, with a page index and size.
 *
 *  @param user The user we wish to retrieve the list for.
 *  @param page The page index.
 *  @param size The page size.
 */
-(void) getFollowingForUser:(PVPUser *)user withPage:(int)page withPageSize:(int)size;

/**
 *  Retrieves the list of the users that a given user is following, with a page index, size, and search expression.
 *
 *  @param user The user we wish to retrieve the list for.
 *  @param page The page index.
 *  @param size The page size.
 *  @param searchText The search expression.
 */
-(void) getFollowingForUser:(PVPUser *)user withPage:(int)page withPageSize:(int)size withSearchText:(NSString *)searchText;

/**
 *  Retrieves the number of subscribers for a given user.
 *
 *  @param user The user in question.
 *
 *  @warning This method will only return the PVPMeta object.
 */
-(void) getSubscribersForUser:(PVPUser *)user;

/**
 *  Retrieves the list of objects the logged in user has purchased.
 *  This method retrieves the purchase objects, not the purchased objects themselves.
 */
-(void)getMyPurchases;

/**
 *  Retrieves the list of objects the user has purchased.
 *  This method retrieves the purchase objects, not the purchased objects themselves.
 *
 *  @param idString The ID of the user whos purchases we wish to retrieve.
 *
 *  @deprecated This method will be removed soon. Please use: getMyPurchases.
 */
-(void) getPurchasesForUser:(NSString *)idString DEPRECATED_MSG_ATTRIBUTE("Please use: getMyPurchases");

-(void)restorePurchases;
-(void)restorePurchasesWithPlanProducts:(NSMutableArray <SKProduct *> *)products;

/**
 *  Indicates whether the user has subscribed to a user with a given ID.
 *
 *  @param userId The ID of the user in question.
 *
 *  @return YES if purchased, NO otherwise.
 */
-(BOOL) isUserPurchased:(NSString *)userId;

/**
 *  Retrieves the list of users who have subscribed to the user with a given ID.
 *
 *  @param user The ID of the user in question.
 */
-(void) getUserSubscriptionProductForUser:(PVPUser *)user;

/**
 *  Finalizes a purchase process for a user subscription, adds the purchased object to the list us the user's purchases.
 *
 *  @param user    the user we have purchased a subscription to
 *  @param product the product that was purchased
 *  @param info    the purchase info (in this case, the receipt data)
 */
-(void) finalizePurchaseForUser:(PVPUser *)user withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info;

/**
 *  Verifies if the user has a valid receipt for this purchased user subscription (i.e. if the user didn't cancel the subscription).
 *
 *  @param user the user we wish to verify
 */
-(void) verifyPurchaseForUser:(PVPUser *)user;
-(void) verifyPurchaseForLogin;

/**
 *  Adds a PVPUser to the database.
 *  This method is used for registering a new account.
 *
 *  @param user The PVPUser we wish to add.
 */
-(void) add:(PVPUser *)user;

/**
 *  Adds a PVPUser to the database.
 *  This method is used for registering a new account, and applying additional private info to the user upon registration.
 *
 *  @param user The PVPUser we wish to add.
 *  @param privateInfo The additional private info we wish to apply to the user.
 */
-(void) add:(PVPUser *)user withPrivateInfo:(NSDictionary *)privateInfo;

/**
 *  Adds a PVPUser to the database.
 *  This method is used for registering a new account, and applying additional params to the user upon registration.
 *
 *  @param user The PVPUser we wish to add.
 *  @param params The additional params we wish to apply to the user (will arrive to the server as part of the request body).
 */
-(void) add:(PVPUser *)user withParams:(NSDictionary *)params;

- (void) updateTo:(PVPUser *)updated;
/**
 *  Updates an existing user in the database.
 *
 *  @param userId  The ID of the user we wish to update.
 *  @param updated The updated version of the PVPUser.
 */
-(void) update:(NSString *)userId to:(PVPUser *)updated;
-(void) update:(NSString *)userId to:(PVPUser *)updated withPrivateInfo:(NSDictionary *)privateInfo;

/**
 *  Deletes a PVPUser from the database.
 *
 *  @param userId The ID of the user we wish to delete.
 */
-(void) remove:(NSString *)userId;

/**
 *  Starts following a user.
 *
 *  @param userId The ID of the user we wish to follow.
 */
-(void) followUser:(NSString *)userId;

/**
 *  Unfollows a user.
 *
 *  @param userId The ID of the user we wish to unfollow.
 */
-(void) unfollowUser:(NSString *)userId;

/**
 *  Indicates whether the current user is following a certain user.
 *
 *  @param userId The ID of the user in question.
 *
 *  @return YES if following, NO otherwise.
 */
-(BOOL) isFollowingUser:(NSString *)userId;

/**
 *  Likes a user.
 *
 *  @param userId The ID of the user we wish to like.
 */
-(void) likeUser:(NSString *)userId;

/**
 *  Indicates whether the current user likes a certain user.
 *
 *  @param userId The ID of the user in question.
 *
 *  @return YES if likes, NO otherwise.
 */
-(BOOL) didLikeUser:(NSString *)userId;

-(void) updateUserWatching:(PVPWatchHistoryItem *)watchingItem;
-(void) getWatchHistory;

/**
 *  Blocks a user.
 *  The current user won't be able to view receive or send messages to the user he's blocking.
 *  Nor will the user be able to view the blocked user profile anywhere in the app.
 *
 *  @param userId - The ID of the user we wish to block.
 *  @param reason - an enum which represents the reason of blocking.
 *
 *  @warning Blocking is permanent and there's no way for the user to unblock except contacting support.
 *
 */
- (void) blockUser:(NSString *)userId WithReason:(int)reason;

@end
