//
//  PVPChannelModel.h
//  Pods
//
//  Created by Oren Kosto,  on 9/13/16.
//
//

#import "PVPBaseModel.h"
#import "PVPChannel.h"

/**
 *  The PVPChannelModelDelegate provides an interface for responding to events from the PVPFlavorModel.
 */
@protocol PVPChannelModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request that returned an array of PVPChannel objects.
 *
 *  @param channelsArray The array of PVPChannel objects returned from the API request.
 */
-(void)channelRequestSuccessWithChannels:(NSArray <PVPChannel *>*)channelsArray;

/**
 *  Triggered upon a successful API request that returned an single PVPChannel object.
 *
 *  @param channel The PVPChannel object returned from the API request.
 */
-(void)channelRequestSuccessWithChannel:(PVPChannel *)channel;

/**
 *  Triggered upon a failed API request.
 */
-(void)channelRequestFail;

/**
 *  Triggered upon a successful API request that returned an array of PVPChannelSchedule objects.
 *
 *  @param channel The channel containing the PVPChannelSchedule objects returned from the API request.
 */
-(void)channelScheduleRequestSuccessWithChannel:(PVPChannel *)channel;

/**
 *  Triggered upon a failed API request.
 */
-(void)channelScheduleRequestFail;

@end

/**
 *  The PVPChannelModel provides an interface for using the PVP channel service.
 */
@interface PVPChannelModel : PVPBaseModel

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPChannelModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPChannelModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 * Retrieves the EPG schedule for a given channel.
 *
 * @param channel The channel we wish to retrieve the EPG schedule for.
 */
-(void)getScheduleForChannel:(PVPChannel *)channel;

/**
 * Retrieves the EPG schedule for a given entry.
 *
 * @param channel The channel we wish to retrieve the EPG schedule for.
 */
-(void)getScheduleForEntry:(PVPEntry *)entry;

/**
* Retrieves the EPG schedule for a all channels.
*/

-(void)getScheduleForChannelsPage:(NSInteger)page withSuccess:(void(^)(NSArray *result, BOOL isFinished))success failure:(void(^)(NSError *e))failure;

/**
* Retrieves channel for live entry (if exist).
*/
-(void)getChannelForEntryID:(NSString *)entryID success:(void(^)(PVPChannel *result))success failure:(void(^)(NSError *e))failure;

@end
