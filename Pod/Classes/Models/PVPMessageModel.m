//
//  PVPMessageModel.m
//  Pods
//
//  Created by Jacob Barr on 3/16/16.
//
//

#import "PVPMessageModel.h"

@implementation PVPMessageModel

- (void) listWithSecondUserId:(NSString *)secondUserId WithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self paramsWithUserId:@"0" withSecondUserId:secondUserId withPrivateConversation:YES withPage:page withPageSize:size withFilter:filter];
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"message"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(messageRequestSuccessWithMessagesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(messageRequestFail) on:self.delegate];
                   }];
}

- (void) add:(PVPMessage *)message {
    [[ApiManager sharedInstance] postRequest:@"message"
                     object:message
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(messageRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(messageRequestFail) on:self.delegate];
                    }];
}

- (NSDictionary *)paramsWithUserId:(NSString *)userId withSecondUserId:(NSString *)secondUserId withPrivateConversation:(BOOL)privateConversation withPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:5];
    params[@"userId"] = userId;
    params[@"secondUserId"] = secondUserId;
    params[@"privateConversation"] = @(privateConversation);
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    return params;
}

@end
