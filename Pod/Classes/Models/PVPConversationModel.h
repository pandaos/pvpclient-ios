//
//  PVPConversationModel.h
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPBaseModel.h"

@class PVPConversation;

/**
 *  The PVPConversationModel delegate provides an interface for responding to events from the PVPConversationModel.
 */
@protocol PVPConversationModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 */
- (void)conversationRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPConversation object has to be returned.
 *
 *  @param conversation The conversation object returned from the API request.
 *
 *  @deprecated Unused, will be removed in the next version.
 */
- (void)conversationRequestSuccessWithConversation:(PVPConversation *)conversation DEPRECATED_MSG_ATTRIBUTE("Unused");

/**
 *  Triggered upon a successful API request where an array of PVPConversation objects has to be returned.
 *
 *  @param conversationsArray The array of PVPConversation objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)conversationRequestSuccessWithConversationsArray:(NSArray *)conversationsArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)conversationRequestFail;

@end


/**
 *  The PVPConversationModel provides an interface for retrieving and managing conversations in the Bamboo platform.
 *
 *  @see PVPConversation
 */
@interface PVPConversationModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPConversationModelDelegate protocol, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPConversationModelDelegate> delegate;

@end
