//
//  PVPConversationModel.m
//  Pods
//
//  Created by Jacob Barr on 3/15/16.
//
//

#import "PVPConversationModel.h"

@implementation PVPConversationModel

- (void) listWithPage:(int)page withPageSize:(int)size {
    [self listWithPage:page withPageSize:size withFilter:nil];
}

- (void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {

    NSDictionary *params = [self addIIDtoParams:[self paramsWithUserId:@"0" withPage:page withPageSize:size withFilter:filter]];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"conversation"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(conversationRequestSuccessWithConversationsArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(conversationRequestFail) on:self.delegate];
                   }];
}

- (NSDictionary *)paramsWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:3];
    params[@"userId"] = userId;
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    return params;
}

@end
