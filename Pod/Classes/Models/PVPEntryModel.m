//
//  PVPEntryModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPEntryModel.h"

@implementation PVPEntryModel

-(void) listWithMetaWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    [[ApiManager sharedInstance] getRequestWithMeta:[NSString stringWithFormat:@"entry"]
                    params:[self paramsWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"entry"]
                                     params:[self paramsWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:types]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithServiceType:(int)serviceType withPageToken:(NSString *)pageToken withPageSize:(int)size withSort:(NSDictionary *)sort withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithServiceType:serviceType withPageToken:pageToken withPageSize:size withFilter:filter withSort:sort] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequestWithMeta:[NSString stringWithFormat:@"entry"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"entry"]
                    params:[self paramsWithPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:types]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size {

    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"entry"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)filterParams {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withSort:sort params:filterParams]];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@/entries", category]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)filterParams completionBlock:(void(^)(NSArray *result, NSError *e))completion
{
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withSort:sort params:filterParams]];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@/entries", category]
                    params:params
                   success:^(NSArray *result) {
                        completion(result, nil);
                   } failure:^(NSError *e) {
                       completion(nil, e);
                   }];
}


-(void) listMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"entry"]
                    params:[self paramsMyPurchasedWithPage:page withPageSize:size withSort:sort withSearchText:searchText]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithEndpoint:(NSString *)endpoint withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:endpoint
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccessWithEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}

-(void) get:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"entry/%@", idString]
             params:params
            success:^(NSArray *result) {
                [AppUtils performSelector:@selector(entryRequestSuccessWithEntry:) on:self.delegate withObject:result.firstObject];
            } failure:^(NSError *e) {
                //                              NSLog(@"error: %@", e);
                [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
            }];
}

-(void) get:(NSString *)idString completionBlock:(void(^)(PVPEntry* entry, NSError *error))completion {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"entry/%@", idString]
             params:params
            success:^(NSArray *result) {
                completion(result.firstObject, nil);
            } failure:^(NSError *e) {
                completion(nil, e);
            }];
}

-(void) add:(PVPEntry *)entry {
    [[ApiManager sharedInstance] postRequest:@"entry"
                     object:entry
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(entryRequestSuccessWithEntry:) on:self.delegate withObject:result.firstObject];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                    }];
}

-(void) update:(NSString *)entryId to:(PVPEntry *)updated {

    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"entry/%@", entryId]
                    object:updated
                    params:nil
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryRequestSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"failure! error: %@", e);
                       [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                   }];
}
-(void) remove:(NSString *)entryId {

    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"entry/%@", entryId]
                       object:nil
                       params:nil
                      success:^(NSArray *result) {
                          [AppUtils performSelector:@selector(entryRequestSuccess) on:self.delegate];
                      } failure:^(NSError *e) {
                          //                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                      }];
    
}

#pragma mark this section handles the image upload process
-(void) uploadImage:(UIImage *)image { //image upload process step 1: request upload token

    NSData *imageData = [[NSData alloc] initWithData:UIImagePNGRepresentation(image)];
    PVPUploadToken *uploadToken = [[PVPUploadToken alloc] init];
    uploadToken.fileName = @"userPhoto.png";
    uploadToken.fileSize = imageData.length;
    [[ApiManager sharedInstance] postRequest:@"upload-token"
                     object:uploadToken
                     params:nil
                    success:^(NSArray *result) { //image upload process step 2: upload token received, upload file to Kaltura
                        PVPUploadToken *resultToken = [result firstObject];
                        [self uploadImageToKaltura:image withName:uploadToken.fileName withUploadToken:resultToken.id];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                        NSLog(@"%@", e);
                    }];
}

#pragma mark image upload to Kaltura
-(void) uploadImageToKaltura:(UIImage *)image withName:(NSString *)name withUploadToken:(NSString *)uploadTokenId {
    NSURL *fileUploadUrl = [NSURL URLWithString:[[PVPConfigHelper sharedInstance] kalturaApiUploadUrl]];
    NSData *imageData = [[NSData alloc] initWithData:UIImagePNGRepresentation(image)];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:fileUploadUrl]; //prepare upload request
    [request setDidFinishSelector:@selector(imageUploadRequestFinished:)];
    [request setDidFailSelector:@selector(imageUploadRequestFailed:)];
    [request setDelegate:self];
    [request setUploadProgressDelegate:self];
    [request setUserInfo:[NSDictionary dictionaryWithObject:name forKey:@"fileName"]];
    [request setRequestMethod:@"POST"];
    [request setData:imageData withFileName:name andContentType:@"image/png" forKey:@"fileData"];
    [request addPostValue:uploadTokenId forKey:@"uploadTokenId"];
    [request addPostValue:[[AppUtils sharedUtils] userKS] forKey:@"ks"];
    [request setTimeOutSeconds:20];
    [request startAsynchronous];
}

-(void)imageUploadRequestFinished:(ASIHTTPRequest *)request {
    NSDictionary *responseDict = [[XMLDictionaryParser alloc] dictionaryWithData:request.responseData];
    [self imageUploadSuccess:responseDict];
}

-(void)imageUploadRequestFailed:(ASIHTTPRequest *)request {
    [self imageUploadFailure];
}

-(void) setProgress:(float)newProgress {
    //do progress stuff.....
    NSLog(@"upload progress: %f", newProgress);
}

#pragma image upload delegate methods
-(void)imageUploadSuccess:(NSDictionary *)responseDict {//image upload process step 3: image uploaded successfuly to Kaltura, create empty image entry

    PVPEntry *emptyEntry = [[PVPEntry alloc] init];
    [emptyEntry setMediaType:[KalturaMediaType IMAGE]];
    emptyEntry.name = @"userPhoto.png";
    [[ApiManager sharedInstance] postRequest:@"entry"
                     object:emptyEntry
                     params:nil
                    success:^(NSArray *result) {
                        PVPEntry *newEntry = [result firstObject];
                        [self bindUploadToken:responseDict[@"result"][@"id"][@"__text"] withEntry:newEntry]; //bind entry with upload token
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                    }];
    NSLog(@"image upload finished");
}

-(void)imageUploadFailure
{
    NSLog(@"image upload failed");
    [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
}

-(void) bindUploadToken:(NSString *)uploadToken withEntry:(PVPEntry *)entry //image upload process step 4: bind new empty entry with upload token
{
    PVPUploadToken *bindUploadToken = [[PVPUploadToken alloc] init];
    bindUploadToken.uploadTokenId = uploadToken;
    [[ApiManager sharedInstance] postRequest:[NSString stringWithFormat:@"entry/%@/content", entry.id]
                     object:bindUploadToken
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(imageUploadSuccessWithEntry:) on:self.delegate withObject:entry];
                    }
                    failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(entryRequestFail) on:self.delegate];
                    }];
}

#pragma mark purchases
-(void) finalizePurchaseForEntry:(PVPEntry *)entry withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info
{
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:entry.id withObjectType:OBJECT_TYPE_ENTRY];
    NSString *price = product ? [[IAPHelper sharedInstance] localizedPriceStringForProduct:product] : @"";
    NSString *currency = product ? [[IAPHelper sharedInstance] currencyCodeForProduct:product] : @"";
    
    NSDictionary *params = nil;
    if ([PVPConfigHelper encryptPurchases]) {
        NSDictionary *dict = @{
                               @"objectId": subscription.objectId,
                               @"type": @(subscription.type),
                               @"service": @(subscription.service),
                               @"price": product.price ?: @(0),
                               @"info": info ?: @{},
                               @"name": entry.name,
                               @"currency": currency
                               };
        PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:@[dict]];
        params = @{
                   @"action": @"purchase",
                   @"data": encryptedData.data[0]
                   };
        subscription = nil;
    } else {
        params = @{
                   @"info": info,
                   @"price": price
                   };
    }
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:subscription
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(entryPurchaseSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(entryPurchaseFail) on:self.delegate];
                    }];
}

-(void) finalizePurchaseForPackageWithEntry:(PVPEntry *)entry withProduct:(SKProduct *)product withPackage:(PVPPackage *)package withRequestInfo:(NSDictionary *)info
{
    NSString *packName = package.name;
    if ([packName length] == 0) {
        packName = entry.name;
    }
    
    [self finalizePurchaseForPackage:[package mongoId] ?: entry.info.package name:packName withProduct:product withRequestInfo:info];
}

- (void)finalizePurchaseForPackage:(NSString *)packageID name:(NSString *)name withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info {
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:packageID withObjectType:OBJECT_TYPE_PACKAGE];
    NSString *price = product ? [[IAPHelper sharedInstance] localizedPriceStringForProduct:product] : @"";
    NSString *currency = product ? [[IAPHelper sharedInstance] currencyCodeForProduct:product] : @"";

    NSDictionary *params = nil;
    if ([PVPConfigHelper encryptPurchases]) {
        NSDictionary *dict = @{
                               @"objectId": subscription.objectId,
                               @"type": @(subscription.type),
                               @"service": @(subscription.service),
                               @"price": product.price ?: @(0),
                               @"info": info ?: @{},
                               @"name": name ?: @"",
                               @"currency": currency
                               };
        PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:@[dict]];
        params = @{
                   @"action": @"purchase",
                   @"data": encryptedData.data[0]
                   };
        subscription = nil;
    } else {
        params = @{
            @"info": info ?: @{},
                   @"price": price
                   };
    }
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:subscription
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(entryPurchaseSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(entryPurchaseFail) on:self.delegate];
                    }];
}

-(void) verifyPurchaseForEntry:(PVPEntry *)entry {

    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", entry.id]
                    params:@{@"type": @(OBJECT_TYPE_ENTRY)}
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryPurchaseSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       if (entry.info.package && [entry.info.package length]) {
                           [self getMyPurchasesWithCompletion:^(BOOL success) {
                               if (success) {
                                   if([[IAPHelper sharedInstance] isPackagePurchased:entry.info.package]) {
                                       [AppUtils performSelector:@selector(entryPurchaseSuccess) on:self.delegate];
                                   } else {
                                       [AppUtils performSelector:@selector(entryPurchaseFail) on:self.delegate];
                                   }
                               } else {
                                   [AppUtils performSelector:@selector(entryPurchaseFail) on:self.delegate];
                               }
                           }];
                       } else {
                           [AppUtils performSelector:@selector(entryPurchaseFail) on:self.delegate];
                       }
                   }];
}

- (void)verifyOfflineDownloadPurchaseForEntry:(PVPEntry *)entry withOfflinePackage:(NSString *)packageID completionBlock:(void(^)(BOOL result, NSError *error))success {
    
    if ([[[ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_ADMIN]
    || [[[ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_ROOT]
    || [[[ApiManager sharedInstance] loggedInUser ].pvpRole isEqualToString: PVP_ROLE_MANAGER]
    || [packageID length] == 0) {
        success(YES, nil);
        return;
    }
    
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", packageID]
                    params:@{@"type": @(OBJECT_TYPE_PACKAGE)}
                   success:^(NSArray *result) {
                        success(YES, nil);
                   } failure:^(NSError *e) {
                       success(NO, e);
                   }];
}

-(void) verifyPurchaseForEntry:(PVPEntry *)entry withCompletion:(void(^)(BOOL isPurchased, NSError *error))completionBlock {
 
     [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", entry.id]
                     params:@{@"type": @(OBJECT_TYPE_ENTRY)}
                    success:^(NSArray *result) {
                        completionBlock(YES, nil);
                    } failure:^(NSError *e) {
                        if (entry.info.package && [entry.info.package length]) {
                            [self getMyPurchasesWithCompletion:^(BOOL success) {
                                if (success) {
                                    completionBlock([[IAPHelper sharedInstance] isPackagePurchased:entry.info.package], nil);
                                } else {
                                    completionBlock(NO, nil);
                                }
                            }];
                        } else {
                            completionBlock(NO, e);
                        }
                    }];
 }

- (void)getCuepointsForEntry:(NSString *)entryId {
    NSDictionary *params = [self addIIDtoParams:@{@"filter": [NSString JSONStringFromDictionary:@{@"entryIdEqual": entryId}]} ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"cuepoint"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(entryCuepointsSuccessWithCuepoints:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                [AppUtils performSelector:@selector(entryCuepointsFail) on:self.delegate];
            }];
}

-(void)getPackageForEntry:(PVPEntry *)entry {
    if (entry.info && entry.info.package) {
        [self getPackageWithId:entry.info.package withCompletion:nil];
    }
    else {
        [AppUtils performSelector:@selector(entryPackageRequestFail) on:self.delegate];
    }
}

-(void)getPackageWithId:(NSString *)packageId withCompletion:(void(^)(NSArray<PVPPackage *> *package))completionBlock {
    
    NSArray *packageIDS = [packageId componentsSeparatedByString:@","];
    NSArray *packs = [self packagesFromList:[[IAPHelper sharedInstance] appPackages] withIDs:packageIDS];

    if (packs.count == packageIDS.count) {
        if (completionBlock == nil) {
                                   [AppUtils performSelector:@selector(entryPackageRequestSuccessWithPackages:) on:self.delegate withObject:packs];
        } else {
            completionBlock(packs);
        }

        return;
    }
    
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"package%@", (packageIDS.count > 1) ? @"" : [@"/" stringByAppendingString:[packageIDS firstObject]]]
                    params:params
                   success:^(NSArray * _Nonnull result) {
                    NSArray *packs = [self packagesFromList:result withIDs:packageIDS];

                        if (completionBlock == nil) {
                                                   [AppUtils performSelector:@selector(entryPackageRequestSuccessWithPackages:) on:self.delegate withObject:packs];
                        } else {
                            completionBlock(packs);
                        }
                   } failure:^(NSError * _Nonnull e) {
                       if (completionBlock == nil) {
                           [AppUtils performSelector:@selector(entryPackageRequestFail) on:self.delegate];
                       } else {
                           completionBlock(nil);
                       }
                   }];
}

- (NSArray *)packagesFromList:(NSArray<PVPPackage *>*)packages withIDs:(NSArray *)packageIDS {
    NSMutableArray *packs = [NSMutableArray new];

    for (NSString *packID in packageIDS) {
        for (PVPPackage *pack in packages) {
            if ([packID isEqualToString:[pack mongoId]] && ![packs containsObject:pack]) {
                [packs addObject:pack];
            }
        }
    }
    
    return packs;
}

-(BOOL) isEntryFree:(PVPEntry *)entry {
    return [[entry.info localizedPriceString] isEqualToString:@""];
}

-(BOOL) isEntryPurchased:(NSString *)entryId {
    NSArray *purchases = [[IAPHelper sharedInstance].purchasedEntries copy];
    return [purchases containsObject:entryId];
}

-(void) saveToPurchasedEntries:(NSString *)entryId {
    [[IAPHelper sharedInstance].purchasedEntries addObject:entryId];
}

-(void)getMyPurchasesWithCompletion:(void(^)(BOOL success))completionBlock
{
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"user/%@/purchase", @"0"]
                    params:nil
                   success:^(NSArray *result) {
                       [PVPSubscription processUserPurchases:result];
                        completionBlock(true);
                   } failure:^(NSError *e) {
                       completionBlock(false);
                   }];
}

#pragma mark params creation
-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size {
    return [self paramsWithPage:page withPageSize:size withSort:0 params:@{}];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)params {
    return [self paramsWithPage:page withPageSize:size withSort:sort withMediaType:-1 params:params];
}
-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withMediaType:(int)mediaType params:(NSDictionary *)filterParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    NSMutableDictionary *filter = [NSMutableDictionary dictionaryWithDictionary:filterParams ?: @{}];
    NSDictionary *pager = @{@"pageIndex": @(page), @"pageSize": @(size)};
    if (sort != nil) {
        filter[@"orderBy"] = sort;
    }

    if(mediaType != -1) {
        filter[@"mediaTypeEqual"] = @(mediaType);
    }

    if ([filter allKeys].count > 0) {
        params[@"filter"] = [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filter]];
    }
    if ([pager allKeys].count > 0) {
        params[@"pager"] = [[NSString JSONStringFromDictionary:pager] stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    return params;
}
// Does not support kaltura entries, only youtube entries (different paging). 
- (NSDictionary *)paramsWithServiceType:(int)serviceType withPageToken:(NSString *)pageToken withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"serviceType"] = @(serviceType);
    params[@"pager"] = [[NSString JSONStringFromDictionary:@{@"pageToken": pageToken, @"pageSize": @(size)}] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filter]];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}


-(NSDictionary *) paramsWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    return [self paramsWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:@[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])]];
}

-(NSDictionary *) paramsWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types
{
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    
    NSDictionary *pagerDict = @{
            @"pageIndex":@(page),
            @"pageSize":@(size)
    };
    
    NSMutableDictionary *filterDict = [@{
            @"mediaTypeIn":[self searchAPIFilterFrom:types],
            @"freeText":fixedSearchText,
            @"userIdEqual":userId
    } mutableCopy];
    
    if (sort != nil) {
        filterDict[@"orderBy"] = [KalturaBaseEntryOrderBy RECENT_DESC];
    }
    
    NSDictionary *paramsDict = @{
        @"pager" : [NSString JSONStringFromDictionary:pagerDict],
        @"filter" : [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filterDict]]
    };
    
    return paramsDict;
}

-(NSDictionary *) paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    return [self paramsWithPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:@[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])]];
}

-(NSDictionary *) paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types
{
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    
    NSDictionary *pagerDict = @{
            @"pageIndex":@(page),
            @"pageSize":@(size)
    };
    
    NSMutableDictionary *filterDict = [@{
            @"mediaTypeIn":[self searchAPIFilterFrom:types],
            @"freeText":fixedSearchText
    } mutableCopy];
    
    if (sort != nil) {
        filterDict[@"orderBy"] = [KalturaBaseEntryOrderBy RECENT_DESC];
    }
    
    NSDictionary *paramsDict = @{
        @"pager" : [NSString JSONStringFromDictionary:pagerDict],
        @"filter" : [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filterDict]]
    };
    
    return paramsDict;
}

-(NSDictionary *) paramsMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    return [self paramsMyPurchasedWithPage:page withPageSize:size withSort:sort withMediaType:[KalturaMediaType VIDEO] withSearchText:searchText];
}

-(NSDictionary *) paramsMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withMediaType:(int)mediaType withSearchText:(NSString *)searchText {
    NSArray *purchasesEntries = [[IAPHelper sharedInstance].purchasedEntries copy];
    NSString *myPurchasedEntriesString = purchasesEntries && [purchasesEntries count] ? [purchasesEntries componentsJoinedByString:@","] : @",";
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    if (sort != nil) {
        return @{@"pager": [NSString stringWithFormat:@"{\"pageIndex\":%d,\"pageSize\":%d}", page, size],
                 @"filter": [NSString stringWithFormat:@"{\"mediaTypeEqual\":%d, \"orderBy\":\"%@\", \"freeText\":\"%@\", \"idIn\":\"%@\"}", mediaType, [KalturaBaseEntryOrderBy RECENT_DESC], fixedSearchText, myPurchasedEntriesString]};
    }
   
    return @{@"pager": [NSString stringWithFormat:@"{\"pageIndex\":%d,\"pageSize\":%d}", page, size],
             @"filter": [NSString stringWithFormat:@"{\"mediaTypeEqual\":%d, \"idIn\":\"%@\"}", mediaType, myPurchasedEntriesString]};
}

- (NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    NSDictionary *defaultFilter = @{@"mediaTypeIn": @([KalturaMediaType VIDEO]),
                                    @"orderBy": [KalturaMediaEntryOrderBy CREATED_AT_DESC],
                                    @"endDateGreaterThanOrEqualOrNull": @([[NSDate date] timeIntervalSince1970WithServerOffset])};
    params[@"pager"] = [[NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:[defaultFilter dictionaryByMergingWithDictionary:filter]]];
    }
    return params;
}

@end
