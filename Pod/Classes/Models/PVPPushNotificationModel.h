//
//  PVPPushNotificationModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 11/2/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"
#import "PVPPushNotification.h"
#import "PVPLiveEntry.h"

/**
 *  The PVPPushNotificationDelegate delegate provides an interface for responding to events from the PVPPushNotificationModel.
 */
@protocol PVPPushNotificationDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful push notification API request.
 *
 *  @see [PVPPushNotificationModel send:]
 *  @see [PVPPushNotificationModel sendMessage:]
 *  @see [PVPPushNotificationModel sendLiveEventCreatedWithEntry:]
 *  @see [PVPPushNotificationModel sendLiveEventStartedWithEntry:]
 */
-(void)pushRequestSuccess;

/**
 *  Triggered upon a failed push notification API request.s
 *
 *  @see [PVPPushNotificationModel send:]
 *  @see [PVPPushNotificationModel sendMessage:]
 *  @see [PVPPushNotificationModel sendLiveEventCreatedWithEntry:]
 *  @see [PVPPushNotificationModel sendLiveEventStartedWithEntry:]
 */
-(void)pushRequestFail;

@end

/**
 *  The PVPPushNotificationModel provides an interface for using the PVP push notification service.
 *  Use this model to send push notifications from the app.
 */
@interface PVPPushNotificationModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPPushNotificationDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPPushNotificationDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Sends a push notification with a given push notification object.
 *
 *  @param notification The push notification object to send with the request.
 *
 *  @warning This method send the push notification object with the default params, which send to all of the registered devices in this instance. Use with caution.
 */
-(void)send:(PVPPushNotificationRequestObject *)notification;

/**
 *  Sends a push notification with a given message.
 *
 *  @param message The message to send in the notification.
 *  
 *  @warning This method send the push notification object with the default params, which send to all of the registered devices in this instance. Use with caution.
 */
-(void)sendMessage:(NSString *)message;

/**
 *  @brief Sends a pre-set "Live entry has been created" push notification for a given entry.
 *  The message to be sent must be set in the server config.
 *  The sent notification will also contain entry details according to the placeholders in the configured message:
 *
 *      - *<entry-name>*  - The name of the entry.
 *      - *<user-name>*   - The name of the entry creator.
 *      - *<player-url>*  - The URL of the play page for the entry.
 *      - *<store-url>*   - The URL of the app's page in the App Store, as set in the AppStoreUrl key in PVPSettings.plist.
 *
 *  @param entry The PVPLiveEntry that we wish to send a notification about.
 *
 *  @warning This method send the push notification object with the default params, which send to all of the registered devices in this instance. Use with caution.
 */
-(void)sendLiveEventCreatedWithEntry:(PVPLiveEntry *)entry;

/**
 *  @brief Sends a pre-set "Live entry has started" push notification for a given entry.
 *  The message to be sent must be set in the server config.
 *  The sent notification will also contain entry details according to the placeholders in the configured message:
 *
 *      - *<entry-name>*  - The name of the entry.
 *      - *<user-name>*   - The name of the entry creator.
 *      - *<player-url>*  - The URL of the play page for the entry.
 *      - *<store-url>*   - The URL of the app's page in the App Store, as set in the AppStoreUrl key in PVPSettings.plist.
 *
 *  @param entry The PVPLiveEntry that we wish to send a notification about.
 *
 *  @warning This method send the push notification object with the default params, which send to all of the registered devices in this instance. Use with caution.
 */
-(void)sendLiveEventStartedWithEntry:(PVPLiveEntry *)entry;

@end
