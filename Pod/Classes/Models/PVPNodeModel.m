//
//  PVPNodeModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPNodeModel.h"

@implementation PVPNodeModel

-(void) listWithServiceType:(int)serviceType withPageToken:(NSString *)pageToken withPageSize:(int)size withSort:(NSDictionary *)sort withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithServiceType:serviceType withPageToken:pageToken withPageSize:size withFilter:filter withSort:sort] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequestWithMeta:[NSString stringWithFormat:@"node"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNodeArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types {
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"node"]
                    params:[self paramsWithPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:types]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNodeArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"node"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNodeArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withSort:sort] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"category/%@/entries", category]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNodeArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

-(void) listWithEndpoint:(NSString *)endpoint withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:endpoint
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNodeArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

- ( void ) getNodeByUrl: ( NSString * ) idString {
    NSMutableDictionary * paramsInit = [NSMutableDictionary dictionaryWithCapacity:2];
    paramsInit[@"byUrl"] = @"true";

    NSDictionary *params = [self addIIDtoParams:paramsInit ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"node/%@", [ idString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLHostAllowedCharacterSet] ] ]
             params:params
            success:^(NSArray *result) {
                [AppUtils performSelector:@selector(nodeRequestSuccessWithNode:) on:self.delegate withObject:result.firstObject];
            } failure:^(NSError *e) {
                //                              NSLog(@"error: %@", e);
                [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
            }];
}

-(void) get:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"node/%@", idString]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccessWithNode:) on:self.delegate withObject:result.firstObject];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}

-(void) add:(PVPNode *)node {
    [[ApiManager sharedInstance] postRequest:@"node"
                     object:node
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(nodeRequestSuccessWithNode:) on:self.delegate withObject:result.firstObject];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                    }];
}

-(void) update:(NSString *)nodeId to:(PVPNode *)updated {
    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"node/%@", nodeId]
                    object:updated
                    params:nil
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(nodeRequestSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"failure! error: %@", e);
                       [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                   }];
}
-(void) remove:(NSString *)nodeId {
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"node/%@", nodeId]
                       object:nil
                       params:nil
                      success:^(NSArray *result) {
                          [AppUtils performSelector:@selector(nodeRequestSuccess) on:self.delegate];
                      } failure:^(NSError *e) {
                          //                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(nodeRequestFail) on:self.delegate];
                      }];
    
}
#pragma mark params creation
-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size {
    return [self paramsWithPage:page withPageSize:size withSort:0];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort {
    return [self paramsWithPage:page withPageSize:size withSort:sort withMediaType:-1];
}
-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withMediaType:(int)mediaType {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    NSMutableDictionary *filter = [NSMutableDictionary dictionaryWithCapacity:2];
    NSDictionary *pager = @{@"pageIndex": @(page), @"pageSize": @(size)};
    if (sort != nil) {
        filter[@"orderBy"] = sort;
    }

    if(mediaType != -1) {
        filter[@"mediaTypeEqual"] = @(mediaType);
    }

    if ([filter allKeys].count > 0) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    if ([pager allKeys].count > 0) {
        params[@"pager"] = [[NSString JSONStringFromDictionary:pager] stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    return params;
}
// Does not support kaltura entries, only youtube entries (different paging). 
- (NSDictionary *)paramsWithServiceType:(int)serviceType withPageToken:(NSString *)pageToken withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"serviceType"] = @(serviceType);
    params[@"pager"] = [[NSString JSONStringFromDictionary:@{@"pageToken": pageToken, @"pageSize": @(size)}] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}

-(NSDictionary *) paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    return [self paramsWithPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:@[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])]];
}

-(NSDictionary *) paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types
{
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"%@", searchText] : @"";
    if (sort != nil) {
        return @{@"pager": [NSString stringWithFormat:@"{\"pageIndex\":%d,\"pageSize\":%d}", page, size],
                 @"filter": [NSString stringWithFormat:@"{\"mediaTypeIn\":\"%@\", \"orderBy\":\"%@\", \"searchText\":\"%@\"}", [self searchAPIFilterFrom:types], [KalturaBaseEntryOrderBy RECENT_DESC], fixedSearchText]};
    }
    
    return @{@"pager": [NSString stringWithFormat:@"{\"pageIndex\":%d,\"pageSize\":%d}", page, size],
             @"filter": [NSString stringWithFormat:@"{\"mediaTypeIn\":\"%@\"}", [self searchAPIFilterFrom:types]]};
}

- (NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    NSDictionary *defaultFilter = @{@"mediaTypeIn": @([KalturaMediaType VIDEO]),
                                    @"orderBy": [KalturaMediaEntryOrderBy CREATED_AT_DESC],
                                    @"endDateGreaterThanOrEqualOrNull": @([[NSDate date] timeIntervalSince1970WithServerOffset])};
    params[@"pager"] = [[NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:[defaultFilter dictionaryByMergingWithDictionary:filter]];
    }
    return params;
}



@end
