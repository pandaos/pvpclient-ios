//
//  PVPLiveEntryModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/14/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPLiveEntryModel.h"

@implementation PVPLiveEntryModel

-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"live"]
                    params:[self paramsWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithMetaWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [[ApiManager sharedInstance] getRequestWithMeta:[NSString stringWithFormat:@"live"]
                            params:[self paramsWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText]
                           success:^(NSArray *result) {
                               [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                           } failure:^(NSError *e) {
                               //                       NSLog(@"error: %@", e);
                               [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                           }];
}

-(void) listMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"live"]
                    params:[self paramsMyPurchasedWithPage:page withPageSize:size withSort:sort withSearchText:searchText]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}

-(void) listMyPurchasedWithMetaWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [[ApiManager sharedInstance] getRequestWithMeta:[NSString stringWithFormat:@"live"]
                            params:[self paramsMyPurchasedWithPage:page withPageSize:size withSort:sort withSearchText:searchText]
                           success:^(NSArray *result) {
                               [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                           } failure:^(NSError *e) {
                               //                       NSLog(@"error: %@", e);
                               [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                           }];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"live"]
                    params:[self paramsWithPage:page withPageSize:size withSort:sort withSearchText:searchText]
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter withSort:nil] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"live"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}

-(void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter withSort:sort] ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"live"]
             params:params
            success:^(NSArray *result) {
                [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntriesArray:) on:self.delegate withObject:result];
            } failure:^(NSError *e) {
                //                       NSLog(@"error: %@", e);
                [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
            }];
}

-(void) get:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:nil ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"entry/%@", idString]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntry:) on:self.delegate withObject:result.firstObject];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}

-(void) add:(PVPLiveEntry *)entry {
    [[ApiManager sharedInstance] postRequest:@"live"
                     object:entry
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(liveEntryRequestSuccessWithLiveEntry:) on:self.delegate withObject:result.firstObject];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                    }];
}

-(void) update:(NSString *)entryId to:(PVPLiveEntry *)updated {
    [[ApiManager sharedInstance] putRequest:[NSString stringWithFormat:@"live/%@", entryId]
                    object:updated
                    params:nil
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryRequestSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       //                       NSLog(@"failure! error: %@", e);
                       [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                   }];
}
-(void) remove:(NSString *)entryId {
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"entry/%@", entryId]
                       object:nil
                       params:nil
                      success:^(NSArray *result) {
                          [AppUtils performSelector:@selector(liveEntryRequestSuccess) on:self.delegate];
                      } failure:^(NSError *e) {
//                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                      }];
}

-(void) startrecording:(PVPLiveEntryPublish *)entry {
    [[ApiManager sharedInstance] postRequest:@"livetovod"
                     object:entry
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(liveStartRecordingSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                    }];
}


-(void) livetovod:(NSString *)entryId {
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"livetovod/%@", entryId]
                       object:nil
                       params:@{@"makeVOD": @(YES)}
                      success:^(NSArray *result) {
                          [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"entry/%@", entryId]
                                             object:nil
                                             params:nil
                                            success:^(NSArray *result) {
                                                [AppUtils performSelector:@selector(liveToVodSuccess) on:self.delegate];
                                            } failure:^(NSError *e) {
                                                //                                                NSLog(@"failure! error: %@", e);
                                                [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                                            }];
                      } failure:^(NSError *e) {
                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                      }];
}



-(void) publish:(PVPLiveEntryPublish *)entry {
    [[ApiManager sharedInstance] postRequest:@"livepublish"
                     object:entry
                     params:nil
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(liveEntryPublishSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                    }];
}

-(void) unpublish:(NSString *)entryId createVODEntry:(BOOL)createVOD {
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"livepublish/%@", entryId]
                       object:nil
                       params:@{@"makeVOD": @(createVOD)}
                      success:^(NSArray *result) {
                          [AppUtils performSelector:@selector(liveEntryUnpublishSuccess) on:self.delegate];
                      } failure:^(NSError *e) {
//                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                      }];
}

-(void) unpublishAndRemove:(NSString *)entryId createVODEntry:(BOOL)createVOD {
    [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"livepublish/%@", entryId]
                       object:nil
                       params:@{@"makeVOD": @(createVOD)}
                      success:^(NSArray *result) {
                          [[ApiManager sharedInstance] deleteRequest:[NSString stringWithFormat:@"entry/%@", entryId]
                                             object:nil
                                             params:nil
                                            success:^(NSArray *result) {
                                                [AppUtils performSelector:@selector(liveEntryUnpublishSuccess) on:self.delegate];
                                            } failure:^(NSError *e) {
//                                                NSLog(@"failure! error: %@", e);
                                                [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                                            }];
                      } failure:^(NSError *e) {
                          NSLog(@"failure! error: %@", e);
                          [AppUtils performSelector:@selector(liveEntryRequestFail) on:self.delegate];
                      }];
}

-(void)checkIfLive:(NSString *)entryId {
    NSArray *idsIn = [entryId componentsSeparatedByString:@","];
    NSString *queryUrl = @"livepublish";
    NSDictionary *queryParams = nil;
    if ([idsIn count] == 1) {
        queryUrl = [queryUrl stringByAppendingString:[NSString stringWithFormat:@"/%@", entryId]];
    } else {
        queryParams = @{@"idsIn": entryId};
    }
    
    [[ApiManager sharedInstance] getRequest:queryUrl
                    params:queryParams
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryIsLiveSuccessWithArray:) on:self.delegate withObject:result];
                    } failure:^(NSError *e) {
                        
                    }];
}

-(void) getEntryViewers:(NSString *)entryId {
    [[ApiManager sharedInstance] getRequestWithMeta:[NSString stringWithFormat:@"entry/%@/viewers", entryId]
                    params:nil
                   success:^(NSArray *result) {
//                       NSLog(@"");
                       [AppUtils performSelector:@selector(liveEntryViewersSuccess:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
//                       NSLog(@"");
                       [AppUtils performSelector:@selector(liveEntryViewersFail) on:self.delegate];
                       //                              NSLog(@"error: %@", e);
                   }];
}

-(void) setImagelUrl:(NSString *)imageUrl forEntryId:(NSString *)entryId
{
    [[ApiManager sharedInstance] postRequest:[NSString stringWithFormat:@"entry/%@/thumbnail", entryId]
                     object:nil
                     params:@{@"imageUrl": imageUrl}
                    success:^(NSArray *result) {
                        NSLog(@"user profile picture attached to entry");
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                    }];
}

#pragma mark purchases
-(void) finalizePurchaseForEntry:(PVPLiveEntry *)entry withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info
{
    PVPSubscription *subscription = [[PVPSubscription alloc] initWithObjectId:entry.id withObjectType:OBJECT_TYPE_ENTRY];
    NSString *price = [[AppUtils sharedUtils] localizedPriceStringForProduct:product];
    
    NSDictionary *params = nil;
    if ([PVPConfigHelper encryptPurchases]) {
        NSDictionary *dict = @{
                               @"objectId": subscription.objectId,
                               @"type": @(subscription.type),
                               @"service": @(subscription.service),
                               @"price": price,
                               @"info": info
                               };
        PVPEncryptedData *encryptedData = [[PVPEncryptedData alloc] initWithObjects:@[dict]];
        params = @{
                   @"action": @"purchase",
                   @"data": encryptedData.data[0]
                   };
        subscription = nil;
    } else {
        params = @{
                   @"info": info,
                   @"price": price
                   };
    }
    [[ApiManager sharedInstance] postRequest:@"user/0/purchase"
                     object:subscription
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(liveEntryPurchaseSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(liveEntryPurchaseFail) on:self.delegate];
                    }];
}

-(void) verifyPurchaseForEntry:(PVPLiveEntry *)entry {
    NSDictionary *params = [self addIIDtoParams:@{@"type": @(OBJECT_TYPE_ENTRY)} ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", entry.id]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(liveEntryPurchaseSuccess) on:self.delegate];
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(liveEntryPurchaseFail) on:self.delegate];
                   }];
}

-(void) verifyPurchaseForEntry:(PVPLiveEntry *)entry withCompletion:(void(^)(BOOL isPurchased, NSError * error))completionBlock {
    NSDictionary *params = [self addIIDtoParams:@{@"type": @(OBJECT_TYPE_ENTRY)} ];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"user/0/purchase/%@", entry.id]
                    params:params
                   success:^(NSArray *result) {
                       completionBlock(YES, nil);
                   } failure:^(NSError *e) {
                       completionBlock(NO, e);
                   }];
}

-(BOOL) isEntryFree:(PVPLiveEntry *)entry {
    return [[entry.info localizedPriceString] isEqualToString:@"Free"];
}

-(BOOL) isEntryPurchased:(NSString *)entryId {
    NSArray *purchases = [[IAPHelper sharedInstance].purchasedEntries copy];
    return [purchases containsObject:entryId];
}

-(void) saveToPurchasedEntries:(NSString *)entryId {
    [[IAPHelper sharedInstance].purchasedEntries addObject:entryId];
}

-(NSDictionary *) paramsWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    
    NSDictionary *pagerDict = @{
            @"pageIndex":@(page),
            @"pageSize":@(size)
    };
    
    NSMutableDictionary *filterDict = [@{
            @"mediaTypeEqual":@([KalturaMediaType LIVE_STREAM_FLASH]),
            @"freeText":fixedSearchText
    } mutableCopy];
    
    if (sort != nil) {
        filterDict[@"orderBy"] = sort;
    }
    
    NSDictionary *paramsDict = @{
        @"pager" : [NSString JSONStringFromDictionary:pagerDict],
        @"filter" : [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filterDict]]
    };
    
    return paramsDict;
}

-(NSDictionary *) paramsWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    
    NSDictionary *pagerDict = @{
            @"pageIndex":@(page),
            @"pageSize":@(size)
    };
    
    NSMutableDictionary *filterDict = [@{
            @"mediaTypeEqual":@([KalturaMediaType LIVE_STREAM_FLASH]),
            @"freeText":fixedSearchText,
            @"userIdEqual":userId
    } mutableCopy];
    
    if (sort != nil) {
        filterDict[@"orderBy"] = sort;
    }
    
    NSDictionary *paramsDict = @{
        @"pager" : [NSString JSONStringFromDictionary:pagerDict],
        @"filter" : [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filterDict]]
    };

    return paramsDict;
}

-(NSDictionary *) paramsMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    NSArray *purchasesEntries = [[IAPHelper sharedInstance].purchasedEntries copy];
    NSString *myPurchasedEntriesString = purchasesEntries && [purchasesEntries count] ? [purchasesEntries componentsJoinedByString:@","] : @",";
    NSString *fixedSearchText = (searchText && searchText.length) ? [NSString stringWithFormat:@"*%@*", searchText] : @"";
    
    NSDictionary *pagerDict = @{
            @"pageIndex":@(page),
            @"pageSize":@(size)
    };
    
    NSMutableDictionary *filterDict = [@{
            @"mediaTypeEqual":@([KalturaMediaType LIVE_STREAM_FLASH]),
            @"freeText":fixedSearchText,
            @"idIn":myPurchasedEntriesString
    } mutableCopy];
    
    if (sort != nil) {
        filterDict[@"orderBy"] = sort;
    }
    
    NSDictionary *paramsDict = @{
        @"pager" : [NSString JSONStringFromDictionary:pagerDict],
        @"filter" : [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:filterDict]]
    };

    return paramsDict;
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    NSDictionary *defaultFilter = @{@"mediaTypeIn": @([KalturaMediaType LIVE_STREAM_FLASH])};

    params[@"pager"] = [[NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:[self addNoContentFilterToParamsIfNeeded:[defaultFilter dictionaryByMergingWithDictionary:filter]]];
    }
    if (sort) {
        params[@"sort"] = [NSString JSONStringFromDictionary:sort];
    }
    return params;
}

@end
