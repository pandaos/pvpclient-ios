//
//  PVPMessageModel.h
//  Pods
//
//  Created by Jacob Barr on 3/16/16.
//
//

#import "PVPBaseModel.h"

@class PVPMessage;

/**
 *  The PVPMessageModel delegate provides an interface for responding to events from the PVPMessageModel.
 */
@protocol PVPMessageModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 */
- (void)messageRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPMessage object has to be returned.
 *
 *  @param message The returned message in the response.
 */
- (void)messageRequestSuccessWithmessage:(PVPMessage *)message;

/**
 *  Triggered upon a successful API request where an array of PVPMessage objects has to be returned.
 *
 *  @param messagesArray The array of PVPMessage objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)messageRequestSuccessWithMessagesArray:(NSArray *)messagesArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)messageRequestFail;

@end

/**
 *  The PVPMessageModel provides an interface for sending, receiving, and managing messaging in the app.
 */
@interface PVPMessageModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPMessageModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPMessageModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Retrieves a list of PVPMessage objects, with a given receiver ID, page number, page size, and Mongo filter.
 *
 *  @param secondUserId The message reveiver ID.
 *  @param page         The page number.
 *  @param size         The page size.
 *  @param filter       The Mongo filter for filtering messages we want to receive.
 */
- (void) listWithSecondUserId:(NSString *)secondUserId WithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter;

/**
 *  Create and send a new PVPMessage object. Essentially send a message in the chat.
 *
 *  @param message The message we would like to send.
 */
- (void) add:(PVPMessage *)message;

@end
