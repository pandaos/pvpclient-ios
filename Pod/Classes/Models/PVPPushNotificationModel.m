//
//  PVPPushNotificationModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 11/2/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPPushNotificationModel.h"

@implementation PVPPushNotificationModel

-(void)send:(PVPPushNotificationRequestObject *)notification
{
    [self send:notification withParams:nil];
}

-(void)send:(PVPPushNotificationRequestObject *)notification withParams:(NSDictionary *)params
{
//    return;
    [[ApiManager sharedInstance] postRequest:@"pushnotifications"
                     object:notification
                     params:params
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(pushRequestSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        NSLog(@"failure! error: %@", e);
                        [AppUtils performSelector:@selector(pushRequestFail) on:self.delegate];
                    }];
}

-(void)sendMessage:(NSString *)message
{
    PVPPushNotificationRequestObject *pushObject = [[PVPPushNotificationRequestObject alloc] initWithMessage:message];
    [self send:pushObject];
}

-(void)sendLiveEventCreatedWithEntry:(PVPLiveEntry *)entry
{
    if ([[AppUtils sharedUtils] sendPushOnEventCreated] && !entry.info.isPrivate) {
        NSString *pushMessage = [AppUtils shareTextFrom:[[AppUtils sharedUtils] eventCreatedText] withUserName:[[ApiManager sharedInstance] loggedInUser].fullName withEntryName:entry.name withEntryId:entry.id];
        
        PVPPushNotificationRequestObject *pushObject = [[PVPPushNotificationRequestObject alloc] initWithMessage:pushMessage
                                                                                                        withData:[self notificationJSONStringForEntry:entry]];
        [self send:pushObject];
    }
}

-(void)sendLiveEventStartedWithEntry:(PVPLiveEntry *)entry
{
    if ([[AppUtils sharedUtils] sendPushOnEventStarted] && !entry.info.isPrivate) {
        NSString *pushMessage = [AppUtils shareTextFrom:[[AppUtils sharedUtils] eventStartedText] withUserName:[[ApiManager sharedInstance] loggedInUser].fullName withEntryName:entry.name withEntryId:entry.id];
        
        PVPPushNotificationRequestObject *pushObject = [[PVPPushNotificationRequestObject alloc] initWithMessage:pushMessage
                                                                                                        withData:[self notificationJSONStringForEntry:entry]];
        [self send:pushObject];
    }
}

-(NSString *)notificationJSONStringForEntry:(PVPLiveEntry *)entry
{
    return [NSString JSONStringFromDictionary:@{@"type": @(1),
                                                @"entryId": entry.id,
                                                @"entryName": entry.name,
                                                @"entryCreatorName": [[ApiManager sharedInstance] loggedInUser].fullName,
                                                @"entryCreatorId": [[ApiManager sharedInstance] loggedInUser].id}];
}

@end
