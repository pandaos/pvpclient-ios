//
//  PVPLoginModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/30/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVPUserModel.h"
#import "PVPConfigModel.h"
#import "PVPAlertHelper.h"
#import "PVPConfigHelper.h"

@class PVPLogin;

/**
 *  The PVPLoginModel delegate provides an interface for responding to events from the PVPLoginModel.
 */
@protocol PVPLoginModelDelegate <NSObject>

@optional

/**
 *  Triggered upon a successful login API request.
 *
 *  @see [PVPLoginModel loginUser:]
 *  @see [PVPLoginModel loginFacebookUser:]
 *  @see [PVPLoginModel loginTwitterUser:]
 *  @see [PVPLoginModel restoreCurrentSession]
 */
-(void)loginRequestSuccess;

/**
 *  Triggered upon a failed login API request.
 *
 *  @see [PVPLoginModel loginUser:]
 *  @see [PVPLoginModel loginFacebookUser:]
 *  @see [PVPLoginModel loginTwitterUser:]
 *  @see [PVPLoginModel restoreCurrentSession]
 */
-(void)loginRequestFail;

/**
 *  Triggered upon a successful logout API request.
 *
 *  @see [PVPLoginModel removeLoggedInUser]
 */
-(void)logoutRequestSuccess;

/**
 *  Triggered upon a failed logout API request.
 *
 *  @see [PVPLoginModel removeLoggedInUser]
 */
-(void)logoutRequestFail;

/**
 *  Triggered when the "forgot password" flow is at the phase where it sends the email address to reset the password for.
 *  This phase happens after the user has entered their email address and clicked "OK" in the dialog.
 *
 *  @see [PVPLoginModel showForgotPasswordDialog]
 */
-(void)forgotPasswordSending;

/**
 *  Triggered upon a successful "forgot password" process.
 *
 *  @see [PVPLoginModel showForgotPasswordDialog]
 */
-(void)forgotPasswordFinished;

/**
 *  Triggered upon a failed "forgot password" process.
 *
 *  @see [PVPLoginModel showForgotPasswordDialog]
 */
-(void)forgotPasswordFailed;

/**
 *  Triggered upon a successful "change password" requese.
 *
 *  @see [PVPLoginModel changePasswordForUser:withCurrentPassword:andNewPassword:]
 */
-(void)changePasswordSuccess;

/**
 *  Triggered upon a failed "change password" request.
 *
 *  @see [PVPLoginModel changePasswordForUser:withCurrentPassword:andNewPassword:]
 */
-(void)changePasswordFailed;

@end

/**
 *  The PVPLoginModel provides an interface for using the login-related services (token, facebookuser, twitteruser).
 *  The methods in this model mainly consist of initiating user sessions and generating PVP tokens for users.
 */
@interface PVPLoginModel : PVPBaseModel <
#if TARGET_OS_TV
#else
UIAlertViewDelegate,
#endif
PVPUserModelDelegate, PVPConfigModelDelegate> {
    NSHTTPCookie *cookie;
    PVPLogin *currentLogin;
    PVPUserModel *userModel;
    PVPConfigModel *configModel;
}

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPLoginModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPLoginModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Regular PVP login, using a user ID and password.
 *
 *  @param user The login object with the required credentials (user ID and password).
 */
-(void) loginUser:(PVPLogin *)user;

/**
 *  Login via Facebook, using a Facebook access token.
 *  This method can be used for both logging in and registering new users via Facebook.
 *  If the user already has an account, PVP will just log in. If the user doesn't have an account, PVP will create a new one using the retrieved Facebook profile and log it in.
 *
 *  @param login The login object with the required credentials (a valid Facebook access token).
 *
 *  @warning The PVPClient doesn't generate access tokens for you. You must integrate the Facebook SDK in the app and generate the token that needs to be provided to this method by yourself.
 *
 *  @see [Facebook iOS SDK](https://developers.facebook.com/docs/ios)
 */
-(void) loginFacebookUser:(PVPLogin *)login;

/**
 *  Login via Twitter, using a Twitter access token.
 *  This method can be used for both logging in and registering new users via Twitter.
 *  If the user already has an account, PVP will just log in. If the user doesn't have an account, PVP will create a new one using the retrieved Twitter profile and log it in.
 *
 *  @param login The login object with the required credentials (a valid Twitter access token).
 *
 *  @warning The PVPClient doesn't generate access tokens for you. You must integrate the Twitter SDK in the app and generate the token that needs to be provided to this method by yourself.
 *  @warning The Twitter API hides the user's email address, so PVP will have to request it manually from the user. The PVPClient takes care of that part, but take into account that the flow will look a bit different because of this.
 *
 *  @see [Twitter iOS SDK](https://dev.twitter.com/mopub/ios)
 */
-(void) loginTwitterUser:(PVPLogin *)login;

-(void) loginSavedUser;

/**
 *  Converts the currently logged in UDID user to a PVP user.
 *
 *  @param user New credentials to use with the new user.
 */
-(void) convertUserByUDIDToPVPUser:(PVPUser *)user;

/**
 *  Converts the currently logged in UDID user to a PVP Facebook user.
 *
 *  @param login New credentials to use with the new user.
 */
-(void) convertUserByUDIDToFacebookUser:(PVPLogin *)login;

/**
 *  Converts the currently logged in UDID user to a PVP Twitter user.
 *
 *  @param login New credentials to use with the new user.
 */
-(void) convertUserByUDIDToTwitterUser:(PVPLogin *)login;

/**
 *  Restores the current session if the app has started and the user is already logged in with a token that hasn't expired yet.
 */
-(void) restoreCurrentSession;

/**
 *  Logs out the current user and deletes the token from the cookie.
 */
-(void) removeLoggedInUser;
-(void) clearUser;

/**
 * Saves user credentials for persistence.
 * @param user The user ID to save.
 * @param pass The password to save.
 */
-(void) saveUserCredentials:(NSString*)user withPassword:(NSString*)pass;

/**
 * Deletes user credentials
 */
-(void) deleteUserCredentials;

/**
 * Gets user credentials
 */
- (NSDictionary*) getUserCredentials;

/**
 *  Initiates a "Forgot Password" flow, starting with an alert dialog asking the user to enter his/her email address.
 *  This process eventually sends an email to the user. The user eventually needs to log in to their email in order to complete the process.
 */
-(void) showForgotPasswordDialog;

/**
 *  Sends a request to change a user's password.
 *
 *  @param userId          The ID of the user we want to change the password for.
 *  @param currentPassword The user's current password.
 *  @param newPassword     The new password we wish to set.
 */
-(void) changePasswordForUser:(NSString *)userId withCurrentPassword:(NSString *)currentPassword andNewPassword:(NSString *)newPassword;
//+(PVPLogin *) getTokenFromDefaults;

@end
