//
//  PVPContactUsModel.h
//  Pods
//
//  Created by Oren Kosto,  on 8/4/16.
//
//

#import "PVPBaseModel.h"

/**
 *  The PVPContactUsModelDelegate protocol provides an interface for responding to events from the PVPContactUsModel.
 */
@protocol PVPContactUsModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPContactUsModel send:]
 */
-(void)contactUsSuccess;

/**
 *  Triggered upon a failed API request.
 */
-(void)contactUsFail;

@end

/**
 *  The PVPContactUsModel provides an interface for sending Contact Us forms.
 */
@interface PVPContactUsModel : PVPBaseModel

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPContactUsModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPContactUsModelDelegate> delegate;


/**
 *  @name Public Methods
 */

/**
 *  Sends a "Contact Us" form.
 *
 *  @param contactUsForm The form we wish to send.
 */
-(void) send:(NSDictionary *)contactUsForm;

@end
