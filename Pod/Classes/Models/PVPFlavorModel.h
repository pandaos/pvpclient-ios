//
//  PVPFlavorModel.h
//  Pods
//
//  Created by Oren Kosto,  on 6/27/16.
//
//

#import "PVPBaseModel.h"
#import "PVPFlavorAsset.h"

/**
 *  The PVPFlavorModelDelegate provides an interface for responding to events from the PVPFlavorModel. 
 */
@protocol PVPFlavorModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request that returned an array of PVPFlavorAsset objects.
 *
 *  @param flavorsArray The array of PVPFlavorAsset objects returned from the API request.
 */
-(void)flavorRequestSuccessWithFlavors:(NSArray <PVPFlavorAsset *>*)flavorsArray;

/**
 *  Triggered upon a failed API request.
 */
-(void)flavorRequestFail;

@end

/**
 *  The PVPFlavorModel provides an interface for using the PVP flavor service.
 */
@interface PVPFlavorModel : PVPBaseModel

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPFlavorModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPFlavorModelDelegate> delegate;


@end
