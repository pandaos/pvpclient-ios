//
//  PVPFlavorModel.m
//  Pods
//
//  Created by Oren Kosto,  on 6/27/16.
//
//

#import "PVPFlavorModel.h"

@implementation PVPFlavorModel

-(void)get:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:nil];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"entry/%@/flavors", idString]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(flavorRequestSuccessWithFlavors:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(flavorRequestFail) on:self.delegate];
                   }];
}

@end
