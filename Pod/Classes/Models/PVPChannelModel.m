//
//  PVPChannelModel.m
//  Pods
//
//  Created by Oren Kosto,  on 9/13/16.
//
//

#import "PVPChannelModel.h"
#import "NSDate+INSUtils.h"

@implementation PVPChannelModel

-(void)listWithPage:(int)page withPageSize:(int)size
{
    [self listWithPage:page withPageSize:size withFilter:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSearchText:(NSString *)searchText
{
    [self listWithPage:page withPageSize:size withRole:role withSort:nil withSearchText:searchText];
}

-(void)listWithPage:(int)page withPageSize:(int)size withRole:(NSString *)role withSort:(NSDictionary *)sort withSearchText:(NSString *)searchText
{
    NSDictionary *mongoFilter = searchText && searchText.length ? @{@"name" : @{@"$regex": searchText,
                                                                                    @"$options": @"i"},
                                                                    @"role": role} : @{@"role": role};
    NSDictionary *params = [self paramsWithPage:page withPageSize:size withFilter:mongoFilter withSort:sort];
    [[ApiManager sharedInstance] getRequest:[NSString stringWithFormat:@"channels/channel"]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(channelRequestSuccessWithChannels:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(channelRequestFail) on:self.delegate];
                   }];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    [self listWithPage:page withPageSize:size withFilter:filter withSort:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSDictionary *params = [self addIIDtoParams:[self paramsWithPage:page withPageSize:size withFilter:filter withSort:sort]];
  NSMutableDictionary *mutableDictionary = [params mutableCopy];     //Make the dictionary mutable to change/add
  mutableDictionary[@"filter"] = @"{\"info.isPrivate\":{\"$in\":[0,\"0\"]}}";
//    mutableDictionary[@"expand"] = @"id";
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];
    [api getRequest:[NSString stringWithFormat:@"channels/channel"]
                    params:mutableDictionary
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(channelRequestSuccessWithChannels:) on:self.delegate withObject:result];
                   } failure:^(NSError *e) {
                       // NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(channelRequestFail) on:self.delegate];
                   }];
}

-(void)get:(NSString *)idString {
    NSDictionary *params = [self addIIDtoParams:[NSMutableDictionary dictionaryWithDictionary:@{ @"expand": @"entry" }]];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"channels/channel/%@", idString]
                    params:params
                   success:^(NSArray *result) {
                       [AppUtils performSelector:@selector(channelRequestSuccessWithChannel:) on:self.delegate withObject:result.firstObject];
                   } failure:^(NSError *e) {
                       //                              NSLog(@"error: %@", e);
                       [AppUtils performSelector:@selector(channelRequestFail) on:self.delegate];
                   }];
}

-(void)getChannelForEntryID:(NSString *)entryID success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e))failure {
    NSMutableDictionary *filter = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"entry": entryID
                                                                                  }];
    
    NSMutableDictionary *filterParams = [NSMutableDictionary new];
    filterParams[@"filter"] = [NSString JSONStringFromDictionary:filter];
    filterParams[@"expand"] = @"entry";
    
    NSDictionary *params = [self addIIDtoParams:filterParams];


    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

        [api getRequest:@"channels/channel"
                    params:params
                   success:^(NSArray *result) {
                       if (result && [result count] == 1) {
                           success([result firstObject]);
                       } else {
                           failure(nil);
                       }
                   } failure:^(NSError *e) {
                       failure(e);
            }];
}
// channelExpand={"fillerPlaylist":["entries"],"entry":"entry","id":"id"}

-(void)getScheduleForChannelsPage:(NSInteger)page withSuccess:(void(^)(NSArray *result, BOOL isFinished))success failure:(void(^)(NSError *e))failure {
//    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:@{
//        @"channelExpand": @{
//            @"fillerPlaylist": @[@"entries"],
//            @"entry": @"entry",
//            @"id": @"id"
//        }}];
    
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:@{
        @"channelExpand": @"entry"}];
    
    
    
    for (NSString *key in [tempParams allKeys]) {
        if ([tempParams[key] isKindOfClass:NSDictionary.class]) {
            tempParams[key] = [NSString JSONStringFromDictionary:tempParams[key]];
        }
    }
    
    NSInteger pageSize = 15;
    
    if (page == 0) {
        page = 1;
    } else if (page == -1) {
        page = 1;
        pageSize = 1;
    }
    
    NSInteger hoursBack = [[PVPConfigHelper sharedInstance] catchupEnabled] ? 12 : 2;
     
    // 1 Hour before now.
    NSNumber *startDate = @([[[NSDate date] ins_dateWithoutMinutesAndSeconds] timeIntervalSince1970] - (1*60*60));
    // 5 Hours from now
    NSNumber *endDate = @([[[NSDate date] ins_dateRoundToNextHour] timeIntervalSince1970] + (5*60*60));
    tempParams[@"startDate"] = startDate;
    tempParams[@"endDate"] = endDate;
    
    tempParams[@"channelPager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page),@"pageSize": @(pageSize)}];
    
    NSDictionary *params = [self addIIDtoParams:tempParams];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:@"channels/epg/"
                    params:params
                   success:^(NSArray *result) {
                       if (result && [result count] > 0) {
                           success(result, (result.count != pageSize));
                       } else {
                           failure(nil);
                       }
                   } failure:^(NSError *e) {
                       NSLog(@"%@",e);
                       failure(e);
            }];
}

-(void)getScheduleForChannel:(PVPChannel *)channel {
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"channelExpand": @"entry"
                                                                                  }];
    for (NSString *key in [tempParams allKeys]) {
        if ([tempParams[key] isKindOfClass:NSDictionary.class]) {
            tempParams[key] = [NSString JSONStringFromDictionary:tempParams[key]];
        }
    }
    
    NSInteger hoursBack = [[PVPConfigHelper sharedInstance] catchupEnabled] ? 12 : 2;
    
    // 1 Hour before now
    NSNumber *startDate = @([[[NSDate date] ins_dateWithoutMinutesAndSeconds] timeIntervalSince1970] - (1*60*60));
    // 5 Hours from now
    NSNumber *endDate = @([[[NSDate date] ins_dateRoundToNextHour] timeIntervalSince1970] + (5*60*60));
    tempParams[@"startDate"] = startDate;
    tempParams[@"endDate"] = endDate;
    
    NSDictionary *params = [self addIIDtoParams:tempParams];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"channels/epg/%@", channel.mongoId]
                    params:params
                   success:^(NSArray *result) {
                       if (result && [result count] > 0) {
                           [AppUtils performSelector:@selector(channelScheduleRequestSuccessWithChannel:) on:self.delegate withObject:result[0]];
                       } else {
                           [AppUtils performSelector:@selector(channelScheduleRequestFail) on:self.delegate];
                       }
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(channelScheduleRequestFail) on:self.delegate];
            }];
}

-(void)getScheduleForEntry:(PVPEntry *)entry {
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"channelExpand": @"entry",
                                                                                  @"channelFilter": @{ @"entry": entry.id}
                                                                                  }];
    for (NSString *key in [tempParams allKeys]) {
        if ([tempParams[key] isKindOfClass:NSDictionary.class]) {
            tempParams[key] = [NSString JSONStringFromDictionary:tempParams[key]];
        }
    }

    NSDictionary *params = [self addIIDtoParams:tempParams];
    ApiManager *api = [[PVPConfigHelper sharedInstance] useCdnApi] ? [ApiManager cdnInstance] : [ApiManager sharedInstance];

    [api getRequest:[NSString stringWithFormat:@"channels/epg/"]
                    params:params
                   success:^(NSArray *result) {
                       if (result && [result count] > 0) {
                           [AppUtils performSelector:@selector(channelScheduleRequestSuccessWithChannel:) on:self.delegate withObject:result[0]];
                       } else {
                           [AppUtils performSelector:@selector(channelScheduleRequestFail) on:self.delegate];
                       }
                   } failure:^(NSError *e) {
                       [AppUtils performSelector:@selector(channelScheduleRequestFail) on:self.delegate];
                   }];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    return [self paramsWithPage:page withPageSize:size withFilter:filter withSort:nil];
}

-(NSDictionary *)paramsWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:3];
    
    NSDictionary *finalFilter = [self addNoContentFilterToParamsIfNeeded:filter];
    
    params[@"pager"] = [NSString JSONStringFromDictionary:@{@"pageIndex": @(page), @"pageSize": @(size)}];
    if (filter) {
        params[@"filter"] = [NSString JSONStringFromDictionary:filter];
    }
    
    if (sort) {
        //Doesn't work because the valeu is sent with /"-1/"
        // NSString json = [NSString JSONStringFromDictionary:sort];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        for ( NSString *key in [sort allKeys] ) {
            NSString *val = [sort objectForKey:key];
            params[@"sort"] = [NSString JSONStringFromDictionary:@{key: @([val intValue])}];
        }
    }
    params[@"expand"] = @"entry";
    return params;
}

@end
