//
//  PVPNodeCategoryModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"
#import "PVPNodeCategory.h"

/**
 *  The PVPNodeCategoryModel delegate provides an interface for responding to events from the PVPNodeCategoryModel.
 */
@protocol PVPNodeCategoryModelDelegate <NSObject>

@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPNodeCategoryModel update:to:]
 *  @see [PVPNodeCategoryModel remove:]
 */
-(void)nodeCategoryRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPNodeCategory object has to be returned.
 *
 *  @param node The PVPNodeCategory object returned from the API request.
 *
 *  @see [PVPNodeCategoryModel get:]
 *  @see [PVPNodeCategoryModel add:]
 */
-(void)nodeCategoryRequestSuccessWithNodeCategory:(PVPNodeCategory *)node;

/**
 *  Triggered upon a successful API request where an array of PVPNodeCategory objects has to be returned.
 *
 *  @param nodesArray The array of PVPNodeCategory objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
-(void)nodeCategoryRequestSuccessWithNodeCategoryArray:(NSArray *)nodesArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 *  @see [PVPNodeCategoryModel get:]
 *  @see [PVPNodeCategoryModel add:]
 *  @see [PVPNodeCategoryModel update:to:]
 *  @see [PVPNodeCategoryModel remove:]
 *  @see [PVPNodeCategoryModel uploadImage:]
 */
-(void)nodeCategoryRequestFail;

@end

/**
 *  The PVPNodeCategoryModel provides an interface for using the PVP node service.
 */
@interface PVPNodeCategoryModel : PVPBaseModel <PVPBaseModelProtocol, ASIHTTPRequestDelegate>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPNodeCategoryModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPNodeCategoryModelDelegate> delegate;

/**
 *  Adds a new PVPNodeCategory to the database.
 *
 *  @param node The node we wish to add.
 */
-(void) add:(PVPNodeCategory *)node;

/**
 *  Updates an existing PVPNodeCategory in the database.
 *
 *  @param nodeId The ID of the PVPNodeCategory we wish to update.
 *  @param updated An updated version of the PVPNodeCategory we wish to update.
 */
-(void) update:(NSString *)nodeId to:(PVPNodeCategory *)updated;

/**
 *  Deletes a PVPNodeCategory from the database.
 *
 *  @param nodeId The ID of the PVPNodeCategory we wish to delete.
 */
-(void) remove:(NSString *)nodeId;

@end
