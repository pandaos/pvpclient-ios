//
//  PVPBaseModel.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/8/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"

@implementation PVPBaseModel

-(id) init {
    self = [super init];
    if (self) {
    }
    return self;
}

-(ApiManager *) getApiManager {
    return [ApiManager sharedInstance];
}

-(ApiManager *) getCDNApiManager {
    return [ApiManager cdnInstance];
}

-(void) list {
    [self listWithPage:1];
}

-(void) listWithPage:(int)page {
    [self listWithPage:page withPageSize:[[PVPConfigHelper getLocalConfiguration:@"pageSize"] intValue]];
}

-(void) listWithPage:(int)page withPageSize:(int)size {
    [self listWithPage:page withPageSize:size withSort:0];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort{
    [self listWithPage:page withPageSize:size withSort:sort withSearchText:@""];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [self listWithPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:@[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])]];
}

-(void) listWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types {
    NSLog(@"WARNING!! Please implement at least one of the \"list\" methods in class: \"%@\"!!!", [self class]);
}

-(void)myListWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [self listWithUserId:[[ApiManager sharedInstance] loggedInUser].mongoId withPage:page withPageSize:size withSort:sort withSearchText:searchText];
}

-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    [self listWithUserId:userId withPage:page withPageSize:size withSort:sort withSearchText:searchText withMediaTypes:@[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])]];
}

-(void) listWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText withMediaTypes:(NSArray *)types {
    NSLog(@"WARNING!! Please implement at least one of the \"listWith...\" methods in class: \"%@\"!!!", [self class]);
}

-(void) listWithMetaWithUserId:(NSString *)userId withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText
{
    NSLog(@"WARNING!! Please implement at least one of the \"listWithMeta\" methods in class: \"%@\"!!!", [self class]);
}

-(void) listMyPurchasedWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText {
    NSLog(@"WARNING!! Please implement the \"listMyPurchasedWithPage...\" method in class: \"%@\"!!!", [self class]);
}

-(void) listMyPurchasedWithMetaWithPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withSearchText:(NSString *)searchText; {
    NSLog(@"WARNING!! Please implement the \"listMyPurchasedWithMetaWithPage...\" method in class: \"%@\"!!!", [self class]);
}

-(void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter {
    [self listWithPage:page withPageSize:size withFilter:filter withExpand:nil];
}

-(void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpand:(NSDictionary *)expand {
    [self listWithPage:page withPageSize:size withFilter:filter withExpand:expand withSort:nil];
}

-(void)listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withSort:(NSDictionary *)sort {
    [self listWithPage:page withPageSize:size withFilter:filter withExpand:nil withSort:sort];
}

- (void) listWithPage:(int)page withPageSize:(int)size withFilter:(NSDictionary *)filter withExpand:(NSDictionary *)expand withSort:(NSDictionary *)sort {
    NSLog(@"WARNING!! Method listWithPage:withPageSize:withFilter:withExpand:withSort not implemented in class: \"%@\"!!!", [self class]);
}

-(void) listWithCategory:(NSString*)category {
    [self listWithCategory:category withPage:1];
}

-(void) listWithCategory:(NSString*)category withPage:(int)page {
    [self listWithCategory:category withPage:page withPageSize:[[PVPConfigHelper getLocalConfiguration:@"pageSize"] intValue]];
}

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size {
    [self listWithCategory:category withPage:page withPageSize:size withSort:0 params:@{}];
}

-(void) listWithCategory:(NSString*)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)params {
    NSLog(@"WARNING!! Please implement at least one of the \"listWithCategory\" methods in class: \"%@\"!!!", [self class]);
}

- ( void ) get: ( NSString * ) idString {
    NSLog(@"WARNING!! Method get: not implemented in class: \"%@\"!!!", [self class]);
}

- ( NSDictionary * ) addIIDtoParams: ( NSDictionary * ) origParams {
    NSMutableDictionary *newParams = origParams ? [NSMutableDictionary dictionaryWithDictionary:origParams] : [NSMutableDictionary dictionaryWithCapacity:3];
    newParams[@"iid"] = [[PVPConfigHelper sharedInstance] currentInstanceId];
    NSDictionary *params = [newParams copy];

    return params;
}

-(NSDictionary *)addNoContentFilterToParamsIfNeeded:(NSDictionary *)origParams {
    if ([[PVPConfigHelper sharedInstance] showNoContent]) {
        NSMutableDictionary *newParams = origParams ? [NSMutableDictionary dictionaryWithDictionary:origParams] : [NSMutableDictionary dictionaryWithCapacity:3];
        if (newParams[@"statusIn"] == nil) {
            newParams[@"statusIn"] = @"2,7";
        }

        return [newParams copy];
    }
    
    return origParams;
}

- (NSString *)searchAPIFilterFrom:(NSArray *)filters {
    return [filters componentsJoinedByString:@","];
}

- (NSArray *)searchVideoAudioFilterArray {
    return @[@([KalturaMediaType VIDEO]), @([KalturaMediaType AUDIO])];
}


@end
