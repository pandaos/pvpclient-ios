//
//  PVPEntryModel.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/10/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "PVPBaseModel.h"

@class PVPEntry;

/**
 *  The PVPEntryModel delegate provides an interface for responding to events from the PVPEntryModel.
 */
@protocol PVPEntryModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 *
 *  @see [PVPEntryModel update:to:]
 *  @see [PVPEntryModel remove:]
 */
-(void)entryRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPEntry object has to be returned.
 *
 *  @param entry The PVPEntry object returned from the API request.
 *
 *  @see [PVPEntryModel get:]
 *  @see [PVPEntryModel add:]
 */
-(void)entryRequestSuccessWithEntry:(PVPEntry *)entry;

/**
 *  Triggered upon a successful API request where an array of PVPEntry objects has to be returned.
 *
 *  @param entriesArray The array of PVPEntry objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
-(void)entryRequestSuccessWithEntriesArray:(NSArray *)entriesArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 *  @see [PVPEntryModel get:]
 *  @see [PVPEntryModel add:]
 *  @see [PVPEntryModel update:to:]
 *  @see [PVPEntryModel remove:]
 *  @see [PVPEntryModel uploadImage:]
 */
-(void)entryRequestFail;

/**
 *  Triggered upon a successful image upload process.
 *
 *  @param entry The ID of the PVPEntry where the image is stored.
 *
 *  @see [PVPEntryModel uploadImage:]
 */
-(void)imageUploadSuccessWithEntry:(PVPEntry *)entry;

/**
 *  Triggered upon a successful entry purchase API request.
 *
 *  @see [PVPEntryModel finalizePurchaseForEntry:withProduct:withRequestInfo:]
 *  @see [PVPEntryModel verifyPurchaseForEntry:]
 */
-(void)entryPurchaseSuccess;

/**
 *  Triggered upon a failed entry purchase API request.
 *
 *  @see [PVPEntryModel finalizePurchaseForEntry:withProduct:withRequestInfo:]
 *  @see [PVPEntryModel verifyPurchaseForEntry:]
 */
-(void)entryPurchaseFail;

/**
 * Triggered upon a successful entry cuepoint request.
 * @param cuepointsArray The array of requested cuepoints.
 */
-(void)entryCuepointsSuccessWithCuepoints:(NSArray <PVPCuePoint *>*)cuepointsArray;

/**
 * Triggered upon a failed entry cuepoint request.
 */
-(void)entryCuepointsFail;

-(void)entryPackageRequestSuccessWithPackages:(NSArray<PVPPackage *> *)packages;
-(void)entryPackageRequestFail;

@end

/**
 *  The PVPEntryModel provides an interface for using the PVP entry service.
 */
@interface PVPEntryModel : PVPBaseModel <PVPBaseModelProtocol, ASIHTTPRequestDelegate>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPEntryModelDelegate, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPEntryModelDelegate> delegate;

/**
 *  @name Public Methods
 */

-(void) listWithEndpoint:(NSString *)endpoint withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort withFilter:(NSDictionary *)filter;


-(void) get:(NSString *)idString completionBlock:(void(^)(PVPEntry* entry, NSError *error))completion;
/**
 *  Adds a new PVPEntry to the database.
 *
 *  @param entry The entry we wish to add.
 */
-(void) add:(PVPEntry *)entry;

/**
 *  Updates an existing PVPEntry in the database.
 *
 *  @param entryId The ID of the PVPEntry we wish to update.
 *  @param updated An updated version of the PVPEntry we wish to update.
 */
-(void) update:(NSString *)entryId to:(PVPEntry *)updated;

/**
 *  Deletes a PVPEntry from the database.
 *
 *  @param entryId The ID of the PVPEntry we wish to delete.
 */
-(void) remove:(NSString *)entryId;

/**
 *  Uploads an image object to Kaltura and adds it as a PVPEntry.
 *
 *  @param image The image object we wish to upload.
 */
-(void) uploadImage:(UIImage *)image;

/**
 *  Finalizes a purchase process for an entry, adds the purchased object to the list us the user's purchases.
 *
 *  @param entry   the entry we have purchased
 *  @param product the product that was purchased
 *  @param info    the purchase info (in this case, the receipt data)
 */
-(void) finalizePurchaseForEntry:(PVPEntry *)entry withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info;
-(void) finalizePurchaseForPackageWithEntry:(PVPEntry *)entry withProduct:(SKProduct *)product withPackage:(PVPPackage *)package withRequestInfo:(NSDictionary *)info;
- (void)finalizePurchaseForPackage:(NSString *)packageID name:(NSString *)name withProduct:(SKProduct *)product withRequestInfo:(NSDictionary *)info;
/**
 *  Verifies if the user has a valid receipt for this purchased entry.
 *
 *  @param entry the entry we wish to verify
 */
-(void) verifyPurchaseForEntry:(PVPEntry *)entry;
-(void) verifyPurchaseForEntry:(PVPEntry *)entry withCompletion:(void(^)(BOOL isPurchased, NSError *error))completionBlock;
- (void)verifyOfflineDownloadPurchaseForEntry:(PVPEntry *)entry withOfflinePackage:(NSString *)packageID completionBlock:(void(^)(BOOL result, NSError *error))success;
/**
 *  Indicates wether an entry if free or paid.
 *
 *  @param entry The entry in question.
 *
 *  @return YES for free, NO for paid.
 *
 *  @warning This method searches among the locally stored purchases in the app. We must retrieve the user's purchases first.
 *
 *  @see [PVPUserModel getMyPurchases]
 */
-(BOOL) isEntryFree:(PVPEntry *)entry;

/**
 *  Indicated if an entry has been purchased by the user or not.
 *
 *  @param entryId The ID of the entry in question.
 *
 *  @return YES if it was purchased by the user, NO otherwise.
 */
-(BOOL) isEntryPurchased:(NSString *)entryId;

/**
 *  Adds an entry to the locally stored list of purchased objects.
 *
 *  @param entryId The ID of the entry we wish to add.
 */
-(void) saveToPurchasedEntries:(NSString *)entryId;

/**
 *  Lists Entries with specific service type (e.g - Youtube)
 *
 *  @param serviceType The enum of the serviceType we wish to use.
 *  @param pageToken   The page token.
 *  @param size        The page size.
 *  @param sort        The way we want to sort the results.
 *  @param filter      The query filter we wish to use.
 */
-(void) listWithServiceType:(int)serviceType withPageToken:(NSString *)pageToken withPageSize:(int)size withSort:(NSDictionary *)sort withFilter:(NSDictionary *)filter;

/**
 * Gets the cuepoints for a given entry.
 * @param entryId The ID of the entry to get the cuepoints for.
 */
-(void) getCuepointsForEntry:(NSString *)entryId;

-(void) getPackageForEntry:(PVPEntry *)entry;
-(void) getPackageWithId:(NSString *)packageId withCompletion:(void(^)(NSArray<PVPPackage *> *package))completionBlock;

-(void) listWithCategory:(NSString *)category withPage:(int)page withPageSize:(int)size withSort:(NSString*)sort params:(NSDictionary *)filterParams completionBlock:(void(^)(NSArray *result, NSError *e))completion;

@end
