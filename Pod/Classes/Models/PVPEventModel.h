//
//  PVPEventModel.h
//  Pods
//
//  Created by Jacob Barr on 10/04/2016.
//
//

#import "PVPBaseModel.h"

@class PVPEvent;

/**
 *  The PVPEventModelDelegate delegate provides an interface for responding to events from the PVPEventModel.
 */
@protocol PVPEventModelDelegate <NSObject>
@optional

/**
 *  Triggered upon a successful API request where no object has to be returned.
 */
- (void)eventRequestSuccess;

/**
 *  Triggered upon a successful API request where a single PVPEvent object has to be returned.
 *
 *  @param event The event object that was returned in the API request.
 *
 *  @deprecated Unused, will be removed in the next version.
 */
- (void)eventRequestSuccessWithEvent:(PVPEvent *)event DEPRECATED_MSG_ATTRIBUTE("Unused");

/**
 *  Triggered upon a successful API request where an array of PVPEvent objects has to be returned.
 *
 *  @param eventsArray The array of PVPEvent objects returned from the API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)eventRequestSuccessWithEventsArray:(NSArray *)eventsArray;

/**
 *  Triggered upon a failed API request.
 *
 *  @see [PVPBaseModel list] - And all list functions.
 */
- (void)eventRequestFail;

@end

/**
 *  The PVPEventModel provides an interface for firing news feed events.
 */
@interface PVPEventModel : PVPBaseModel <PVPBaseModelProtocol>

/**
 *  @name Public Properties
 */

/**
 *  The delegate that conforms to the PVPEventModelDelegate protocol, and will respond to its methods when triggered by this instance.
 */
@property (nonatomic, weak) id<PVPEventModelDelegate> delegate;

/**
 *  @name Public Methods
 */

/**
 *  Sends an event creation request to the server
 *
 *  @param event PVPEvent object of the event we'd like to create.
 */
- (void) fire:(PVPEvent *)event;

@end
