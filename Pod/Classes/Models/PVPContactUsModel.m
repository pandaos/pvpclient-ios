//
//  PVPContactUsModel.m
//  Pods
//
//  Created by Oren Kosto,  on 8/4/16.
//
//

#import "PVPContactUsModel.h"

@implementation PVPContactUsModel

-(void) send:(NSDictionary *)contactUsForm {
    [[ApiManager sharedInstance] postRequest:@"contactus"
                     object:nil
                     params:contactUsForm
                    success:^(NSArray *result) {
                        [AppUtils performSelector:@selector(contactUsSuccess) on:self.delegate];
                    } failure:^(NSError *e, NSString *bambooError) {
                        [AppUtils performSelector:@selector(contactUsFail) on:self.delegate];
                    }];
}

@end
