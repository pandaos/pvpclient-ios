//
//  PVPAudioManager.m
//  AFNetworking
//
//  Created by Kovtun Vladimir on 2/19/20.
//

#import "PVPAudioManager.h"

#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PVPAudioManager ()

@property (copy, nonatomic) NSString *currentPlayingAudioURL;
@property(nullable) AVPlayer * player;

@end

@implementation PVPAudioManager

static BOOL isSetup = NO;
static PVPAudioManager *sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedInstance
{
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (void)setup
{
    NSError *sessionError = nil;

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];

    if (sessionError == nil) {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    }

    MPRemoteCommandCenter *remoteCenter = [MPRemoteCommandCenter sharedCommandCenter];
    [remoteCenter.playCommand setEnabled:YES];
    [remoteCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if (self.currentPlayingAudioURL != nil && self.player != nil) {
            [[self player] play];
            self.playStatusDidUpdate(YES);
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [remoteCenter.pauseCommand setEnabled:YES];
    [remoteCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        if (self.currentPlayingAudioURL != nil && self.player != nil) {
            [[self player] pause];
            self.playStatusDidUpdate(NO);
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(handleAudioSessionInterruption:)
        name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
}

- (void)handleAudioSessionInterruption:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    if (userInfo[AVAudioSessionInterruptionTypeKey] != nil) {
        AVAudioSessionInterruptionType type = (AVAudioSessionInterruptionType)[(NSNumber *) userInfo[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
        switch (type) {
            case AVAudioSessionInterruptionTypeBegan:
            {
                if (self.player != nil) {
                    [self.player pause];
                    _playStatusDidUpdate(NO);
                }
                
                break;
            }
                
            case AVAudioSessionInterruptionTypeEnded:
            {
                NSNumber *result = userInfo[AVAudioSessionInterruptionOptionKey];
                AVAudioSessionInterruptionOptions options = (AVAudioSessionInterruptionOptions)[result unsignedIntegerValue];
                if (options == AVAudioSessionInterruptionOptionShouldResume && [[self player] timeControlStatus] == AVPlayerTimeControlStatusPaused) {
                    [[self player] play];
                    _playStatusDidUpdate(YES);
                }
                break;
            }
                
            default:
                break;
        }
    }
}

- (void)setupIfNeeded {
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
}

- (void)togglePlayAudioStreamWithURLString:(NSString *)urlString {
    [self setupIfNeeded];
    
    if ([_currentPlayingAudioURL isEqualToString:urlString]) {
        if ([[self player] timeControlStatus] == AVPlayerTimeControlStatusPlaying) {
            [[self player] pause];
            _playStatusDidUpdate(NO);
        } else if ([[self player] timeControlStatus] == AVPlayerTimeControlStatusPaused) {
            [[self player] play];
            _playStatusDidUpdate(YES);
        }
    } else {
        if (_currentPlayingAudioURL != nil && _player != nil) {
            [_player pause];
            _currentPlayingAudioURL = nil;
        }
        
        NSURL *streamURL = [NSURL URLWithString:urlString];
        if (streamURL != nil) {
            self.player = [[AVPlayer alloc] initWithURL:streamURL];
            [[self player] play];
            _currentPlayingAudioURL = urlString;
            _playStatusDidUpdate(YES);
        }
    }
}

- (void)playAudio {
    [[self player] play];
}

- (void)pauseAudio {
    [[self player] pause];
}

- (void)muteAudio {
    if (_currentPlayingAudioURL != nil && [[self player] timeControlStatus] == AVPlayerTimeControlStatusPlaying) {
        [[self player] setMuted:YES];
    }
}

- (void)unmuteAudio {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.currentPlayingAudioURL != nil && [[self player] isMuted]) {
            [[self player] setMuted:NO];
            [[self player] play];
        }
    });
}

#pragma mark - Private

- (void)updateRemoteControlUI {
    //MPNowPlayingInfoCenter *playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
}

@end
