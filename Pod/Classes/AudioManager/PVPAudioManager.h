//
//  PVPAudioManager.h
//  AFNetworking
//
//  Created by Kovtun Vladimir on 2/19/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PVPAudioManager : NSObject

@property(nonatomic, copy) void (^playStatusDidUpdate)(BOOL isPlaying);

+(nonnull instancetype)sharedInstance;

- (void)togglePlayAudioStreamWithURLString:(NSString *)urlString;
- (void)playAudio;
- (void)pauseAudio;
- (void)muteAudio;
- (void)unmuteAudio;

@end

NS_ASSUME_NONNULL_END
