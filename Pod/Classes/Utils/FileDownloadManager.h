//
//  FileDownloadManager.h
//  Pods
//
//  Created by Oren Kosto,  on 7/28/16.
//
//

#import <Foundation/Foundation.h>
#import "PVPConfigHelper.h"
#import "PVPEntry.h"
#import "PVPCuePoint.h"
#import "PVPAlertHelper.h"

#define PVP_DOWNLOAD_TASK_ID_NONE -1

/**
 * The name of the notification that is sent when a download task starts.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskStartedNotification;

/**
 * The name of the notification that is sent when a download task's progress changes.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskProgressNotification;

/**
 * The name of the notification that is sent when a download task is paused.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskPausedNotification;

/**
 * The name of the notification that is sent when a download task is canceled.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskCancelledNotification;

/**
 * The name of the notification that is sent when a download task finishes.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskFinishedNotification;

/**
 * The name of the notification that is sent when a download task fails.
 */
UIKIT_EXTERN NSString *const FileDownloadManagerTaskFailedNotification;

/**
 * The FileDownloadInfo class represents a file download action in the system.
 * It contains information about the download like the file's name and URL, the state of the download, and the download task itself.
 */
@interface FileDownloadInfo : PVPBaseObject

/**
 * @name Public Properties
 */

/**
 * The name to be used for the downloaded file.
 */
@property (nonatomic, strong) NSString *filename;

/**
 * The entry id of the downloaded file.
 */
@property (nonatomic, strong) NSString *entryId;

/**
 * The source URL of the file.
 */
@property (nonatomic, strong) NSString *source;

/**
 * The download task we use for the file download.
 */
@property (nonatomic, strong) NSURLSessionDownloadTask *task;

/**
 * The unique task identifier, generated automatically by the task.
 */
@property (nonatomic, assign) NSInteger taskIdentifier;

/**
 * The data to be used when resuming the download.
 */
@property (nonatomic, strong) NSData *taskResumeData;

/**
 * The progress of the download, represented as a value between 0.0 and 1.0.
 */
@property (nonatomic, assign) CGFloat progress;

/**
 * Indicates whether the download is currently in progress.
 */
@property (nonatomic, assign) BOOL isInProgress;

/**
 * Indicates whether the download is complete.
 */
@property (nonatomic, assign) BOOL isComplete;

/**
 * Initializes an instance of this object with a given filename and source url.
 * @param filename The name of the file that will be saved.
 * @param source The URL of the file source.
 * @return An instance of this object with a given filename and source url.
 */
- (FileDownloadInfo *)initWithFilename:(NSString *)filename withsource:(NSString *)source;


/**
 Initializes an instance of this object with a given entry id, filename and source url.

 @param entryId  The entry ID of the file.
 @param filename The name of the file that will be saved.
 @param source   The URL of the file source.

 @return An instance of this object with a given entry id, filename and source url.
 */
- (FileDownloadInfo *)initWithEntryId:(NSString *)entryId withFilename:(NSString *)filename withsource:(NSString *)source;

@end

/**
 * The FileDownloadManager provides an interface for managing file downloads.
 * We can perform downloads in the background, keep track on them, and pause/resume them as we please.
 */
@interface FileDownloadManager : NSObject <NSURLSessionDownloadDelegate>

/**
 * @name Public Properties
 */

/**
 * The dictionary of the FileDownloadInfo objects. The keys represent the download task identifiers.
 */
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, FileDownloadInfo *> *fileDownloadInfoDict;

@property (nonatomic, strong) NSMutableArray<PVPEntry *> *inProgressEntries;
@property (nonatomic, strong) NSMutableArray<PVPEntry *> *localEntries;
@property (nonatomic, strong) NSMutableArray<PVPEntry *> *failedEntries;
@property (nonatomic, strong) NSMutableArray<PVPCuePoint *> *localCuepoints;

/**
 * @name Public Methods
 */

/**
 * The singleton instance of this class.
 * @return The singleton instance of the FileDownloadManager class.
 */
+ (instancetype)sharedInstance;

/**
 * Starts a new download, or resumes an existing one, depending on whether the resume data for the download is provided.
 * @param info The FileDownloadInfo representation of the download.
 */
- (void)startOrResumeDownload:(FileDownloadInfo *)info;

/**
 * Pauses a download, with the option of resuming it later on.
 * @param info The FileDownloadInfo representation of the download.
 */
- (void)pauseDownload:(FileDownloadInfo *)info;

/**
 * Cancels a download, without the option of resuming it later on.
 * @param info The FileDownloadInfo representation of the download.
 */
- (void)cancelDownload:(FileDownloadInfo *)info;

- (void)downloadEntry:(PVPEntry *)entry;
- (void)downloadEntry:(PVPEntry *)entry withCuepoints:(NSArray <PVPCuePoint *> *)cuepoints;
- (void)deleteEntry:(PVPEntry *)entry;

- (BOOL)entryExistsOrInProgress:(NSString *)entryId;
- (BOOL)entryExists:(NSString *)entryId;
- (BOOL)entryInProgress:(NSString *)entryId;

- (UIImage *) localImageForEntry:(PVPEntry *)entry;

- (NSString *)localPlaybackUrlForEntry:(PVPEntry *)entry;

- (NSArray<PVPCuePoint *> *)localEntryCuepointsByEntryId:(NSString *)entryId;

@end
