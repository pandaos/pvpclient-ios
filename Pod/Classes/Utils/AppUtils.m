//
//  AppUtils.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/13/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "AppUtils.h"
#import "NSString+MD5.h"
#import "PVPConfigHelper.h"
#include <sys/sysctl.h>
#import <CommonCrypto/CommonCrypto.h>
//#include <openssl/md5.h>
#import <CommonCrypto/CommonDigest.h>
@implementation AppUtils

static NSMutableDictionary *plistDictionary;
static BOOL isSetup = NO;

+(AppUtils *) sharedUtils
{
    static AppUtils *sharedUtils = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUtils = [[self alloc] init];
        // Load configuration from settings.plist
    });
    if (!isSetup) {
        [sharedUtils setup];
        isSetup = YES;
    }
    return sharedUtils;
}

-(void)setup {
//    self.userPurchasedEntries = [NSMutableArray arrayWithCapacity:10];
//    self.userPurchasedUsers = [NSMutableArray arrayWithCapacity:10];
}

-(NSString *) localizedPriceStringForProductIndex:(int)productIndex
{
    if (productIndex == -1) {
        return @"Free";
    }
    SKProduct *product = (SKProduct *) self.eventProducts[productIndex];
    return [self localizedPriceStringForProduct:product];
}

-(NSString *) localizedPriceStringForProduct:(SKProduct *)product
{
    if (!priceFormatter) {
        priceFormatter = [[NSNumberFormatter alloc] init];
        [priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
    [priceFormatter setLocale:product.priceLocale];
    return [priceFormatter stringFromNumber:product.price];
}

-(NSString *) currencyCodeForProduct:(SKProduct *)product
{
    if (!priceFormatter) {
        priceFormatter = [[NSNumberFormatter alloc] init];
        [priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
    [priceFormatter setLocale:product.priceLocale];
    return [priceFormatter currencyCode];
}

-(void)imageUploadRequestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *responseDict = [[XMLDictionaryParser alloc] dictionaryWithData:request.responseData];
    [self.imageUploadDelegate imageUploadSuccess:responseDict];
}

-(void)imageUploadRequestFailed:(ASIHTTPRequest *)request
{
    [self.imageUploadDelegate imageUploadFailure];
}

-(void) setProgress:(float)newProgress
{
    //do progress stuff.....
    NSLog(@"upload progress: %f", newProgress);
}

#pragma mark get player from url and get configs method
+(NSString *)getHTML5PlayerUrl:(NSString *)entryId
{
    NSString *playerUrl = [[[self sharedUtils] playerUrl] stringByReplacingOccurrencesOfString:@"mwEmbedLoader.php" withString:@"mwEmbedFrame.php"];
    
    return [NSString stringWithFormat:@"%@?wid=_%@&uiconf_id=%@&entry_id=%@",
            playerUrl,
            [[self sharedUtils] userPartnerId],
            [[self sharedUtils] playerUiConfId],
            entryId];
}

+(NSString *)getPlayerUrl
{
    
    NSString *playerUrl = [[[self sharedUtils] playerUrl] stringByReplacingOccurrencesOfString:@"mwEmbedLoader.php" withString:@"mwEmbedFrame.php"];
    
    if([playerUrl hasPrefix:@"//"]) {
        playerUrl = [NSString stringWithFormat:@"http:%@", playerUrl];
    }
    
    return playerUrl;
}

+(NSString *)addParamsToPictureUrl:(NSString *)profilePictureUrl
{
    if ([profilePictureUrl containsString:@"graph.facebook"]) {
        profilePictureUrl = [profilePictureUrl stringByAppendingString:@"&width=400&height=400"];
    } else if ([profilePictureUrl containsString:[NSString stringWithFormat:@"/thumbnail/"]] || [profilePictureUrl containsString:[NSString stringWithFormat:@"/%@/", [[self sharedUtils] userPartnerId]]]) {
        profilePictureUrl = [profilePictureUrl stringByAppendingString:@"/width/400/height/400"];
    }
    return profilePictureUrl;
}

+(NSString *) getConfiguration:(NSString *)name
{
    if (!plistDictionary) {
        // Load configuration from settings.plist
        NSString *pathToPlist = [[NSBundle mainBundle] pathForResource:@"PVPSettings" ofType:@"plist"];
        plistDictionary = [[NSMutableDictionary alloc ]initWithContentsOfFile:pathToPlist];
    }
    return [plistDictionary objectForKey:name];
}

-(NSString *) getThumbnailUrlWithEntryId:(NSString *)entryId
{
    return [NSString stringWithFormat:@"https://video.panda-os.com/p/%@/thumbnail/entry_id/%@/", self.userPartnerId, entryId];
}

+(NSString *) getDeviceUserAgent
{
#if TARGET_OS_TV
    return @"AppleCoreMedia/1.0.0.14W260 (Apple TV; U; CPU OS 10_2 like Mac OS X; en_us)";//@"TVOS";
#elif TARGET_OS_IOS
    WKWebView *webview = [[WKWebView alloc] initWithFrame:CGRectZero];
    NSString *userAgent = [webview valueForKey:@"userAgent"];
    
//    NSLog(@"userAgent: %@", userAgent);
    //TODO: switch to WKWebView and get user agent
    return userAgent;
#else
    return @"N/A";
#endif
}

+(NSString *)getDeviceModel {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
    return deviceModel;
}

+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+(void) saveDefaultDictionary:(NSDictionary *)value forKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+(void) saveDefaultNumber:(NSNumber *)number forKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:number forKey:key];
    [defaults synchronize];
}

+(void) deleteDefaultValueWithKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

+(NSString *) getDefaultValueFofKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
}

+(NSDictionary *) getDefaultDictionaryForKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults dictionaryForKey:key];
}

+(NSNumber *) getDefaultNumberFofKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}

+(void)clearAllNotificationDefaults {
    NSArray *defaultsKeys = [NSArray arrayWithObjects:
                             @"sessionId",
                             @"apiKey",
                             @"token",
                             @"callerName",
                             @"callerProfilePictureUrl",
                             @"entryToPlayId",
                             @"entryToPlayName",
                             @"entryToPlayCreator",
                             @"entryToPlayCreatorId",
                             nil];
    for (NSString *key in defaultsKeys) {
        [AppUtils deleteDefaultValueWithKey:key];
    }
}


+ (NSString*) MD5Hasher: (NSString*) query {
    NSData* hashed = [query dataUsingEncoding:NSUTF8StringEncoding];
//    unsigned char *digest = MD5([hashed bytes], [hashed length], NULL);
    unsigned char *digest;
    NSString *final = [NSString stringWithUTF8String: (char *)digest];
    return final;
}

+ (NSString*)md5HexDigest:(NSString*)input withRaw:(BOOL)raw {
    const char* str = [input UTF8String];
    NSData* data = [input dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
//    unsigned char result2[CC_MD5_DIGEST_LENGTH];
//    CC_MD5([data bytes], [data length], result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    
    if(raw == NO || YES) {
        for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
            [ret appendFormat:@"%02X",result[i]];
        }
    } else {
//        ret = [NSString stringWithUTF8String:(char *)result];
//        ret = [ret initWithUTF8String:result];
//        [ret appendString:[NSString stringWithCString:result encoding:NSUTF8StringEncoding]];
    }
    
    return ret;
}



+(NSDictionary *) dictionaryFromJSONString:(NSString *)string
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    return jsonDict;
}

+(NSString *) JSONStringFromDictionary:(NSMutableDictionary *)dictionary
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
}

+(NSString *) hexadecimalStringFromData:(NSData *)data
{
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

+(NSString *)normalizedNumber:(int)number
{
    if (number >= 1000000) {
        float numberFloat = number / 1000000;
        return [NSString stringWithFormat:@"%.01fM", numberFloat];
    } else if (number >= 1000) {
        float numberFloat = number / 1000;
        return [NSString stringWithFormat:@"%.01fK", numberFloat];
    }
    return [NSString stringWithFormat:@"%d", number];
}

+(NSString *)shareTextFrom:(NSString *)originalText withUserName:(NSString *)userName withEntryName:(NSString *)entryName withEntryId:(NSString *)entryId
{
    NSString *finalText = [originalText stringByReplacingOccurrencesOfString:@"<entry-name>" withString:entryName];
    finalText = [finalText stringByReplacingOccurrencesOfString:@"<user-name>" withString:userName];
    finalText = [finalText stringByReplacingOccurrencesOfString:@"<player-url>" withString:[AppUtils playPageUrlWithEntryId:entryId]];
    finalText = [finalText stringByReplacingOccurrencesOfString:@"<store-url>" withString:[AppUtils getConfiguration:@"AppStoreUrl"]];
    
    return finalText;
}

+(NSString *)playPageUrlWithEntryId:(NSString *)entryId
{
    return [NSString stringWithFormat:@"%@/play//IC/%@", [AppUtils getConfiguration:@"pvpServerDomain"], entryId];
}

+(void) performSelector:(SEL)selector on:(id)delegate
{
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(delegate, selector);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object
{
    if ([delegate respondsToSelector:selector]) {
        IMP imp = [delegate methodForSelector:selector];
        void (*func)(id, SEL, id) = (void *)imp;
        func(delegate, selector, object);
    } else {
        NSLog(@"Method: %@ missing for delegate: %@", NSStringFromSelector(selector), NSStringFromClass([delegate class]));
    }
}

#if TARGET_OS_IOS
+(void) openUrlWithString:(NSString *)urlString
{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:urlString];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0 && [application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{}
           completionHandler:^(BOOL success) {
               NSLog(@"Open: %d",success);
           }];
    } else {
        BOOL canOpen = [application canOpenURL:URL];
        if (canOpen) {
            BOOL success = [application openURL:URL];
            NSLog(@"Open: %d",success);
        }
    }
}

-(void) uploadImage:(UIImage *)image withName:(NSString *)name withUploadToken:(NSString *)uploadTokenId
{
    NSURL *fileUploadUrl = [NSURL URLWithString:[AppUtils getConfiguration:@"kalturaApiUploadUrl"]];
    NSData *imageData = [[NSData alloc] initWithData:UIImagePNGRepresentation(image)];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:fileUploadUrl]; //prepare upload request
    [request setDidFinishSelector:@selector(imageUploadRequestFinished:)];
    [request setDidFailSelector:@selector(imageUploadRequestFailed:)];
    [request setDelegate:self];
    [request setUploadProgressDelegate:self];
    [request setUserInfo:[NSDictionary dictionaryWithObject:name forKey:@"fileName"]];
    [request setRequestMethod:@"POST"];
    [request setData:imageData withFileName:name andContentType:@"image/png" forKey:@"fileData"];
    [request addPostValue:uploadTokenId forKey:@"uploadTokenId"];
    [request addPostValue:self.userKS forKey:@"ks"];
    [request setTimeOutSeconds:20];
    [request startAsynchronous];
}

+(UIImage *)imageWithColor:(UIColor *)color withFrame:(CGRect) rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(void) showAlertWithDefaultErrorMessage
{
    [AppUtils showAlertWithTitle:ALERT_ERROR_TITLE andMessage:ALERT_ERROR_MESSAGE];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:ALERT_OK
                                          otherButtonTitles:nil];
    [alert show];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withDelegate:(id)delegate withTag:(NSInteger)tag
{
    [self showAlertWithTitle:title andMessage:message withDelegate:delegate withTag:tag withCancelButtonTitle:ALERT_CANCEL withOtherButtonTitle:ALERT_OK];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withDelegate:(id)delegate withTag:(NSInteger)tag withCancelButtonTitle:(NSString *)cancelButtonTitle withOtherButtonTitle:(NSString *)otherButtonTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:otherButtonTitle, nil];
    alert.tag = tag;
    [alert show];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withTextFieldType:(UIKeyboardType)keyboardType withPlaceholder:(NSString *)placeholder withDelegate:(id /**<UIAlertViewDelegate>*/)delegate withTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:ALERT_CANCEL
                                          otherButtonTitles:ALERT_OK, nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = tag;
    [alert textFieldAtIndex:0].keyboardType = keyboardType;
    [alert textFieldAtIndex:0].placeholder = placeholder;
    [alert show];
}

+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withTextFieldType:(UIKeyboardType)keyboardType withInitialText:(NSString *)text withDelegate:(id /**<UIAlertViewDelegate>*/)delegate withTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:ALERT_CANCEL
                                          otherButtonTitles:ALERT_OK, nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = tag;
    [alert textFieldAtIndex:0].keyboardType = keyboardType;
    [alert textFieldAtIndex:0].text = text;
    [alert show];
}
#endif

-(NSString *)tokenizePlaybackUrl:(NSString *)url {
    NSString *newUrl = url;
    
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://checkip.amazonaws.com/"] encoding:NSUTF8StringEncoding error:nil];
    publicIP = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]; // IP comes with a newline for some reason
    
    NSString * validminutes = @"1";
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    
    [dateFormatter setDateFormat:@"M/dd/yyyy h:mm:ss a"];
    NSString *today = [dateFormatter stringFromDate:[NSDate date]];
    //Today example format: 7/30/2018 7:29:04 AM
    
    NSString *token = [PVPConfigHelper getLocalConfiguration:@"tokenizationSharedSecret"];
    NSString *to_hash = [NSString stringWithFormat:@"%@%@%@%@", publicIP, token, today, validminutes];
    NSLog(@"string to Hash: %@", to_hash);
    NSString *to_hash_md5 = [AppUtils md5HexDigest:to_hash withRaw:YES];
    NSLog(@"string to Hash MD5: %@", to_hash_md5);
    
    NSData *to_hash_md52 = [to_hash MD5CharData];
    NSLog(@"NSDATA to Hash: %@", to_hash_md52);
    
    NSData *data = [to_hash_md5 dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [to_hash_md52 base64EncodedStringWithOptions:0];
    NSLog(@"string base64encoded: %@", base64Encoded);
    
    NSString *urlsignature = [NSString stringWithFormat:@"server_time=%@&hash_value=%@&validminutes=%@", today, base64Encoded, validminutes];
    
    NSData *signatureData = [urlsignature dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *signatureBase64Encoded = [signatureData base64EncodedStringWithOptions:0];
    
    NSLog(@"string signatureBase64Encoded: %@", signatureData);
    
    newUrl = [NSString stringWithFormat:@"%@?wmsAuthSign=%@", url, signatureBase64Encoded];
    
    NSLog(@"New Tokenized url is: %@", newUrl);
    
    return newUrl;
}

@end
