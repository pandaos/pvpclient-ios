//
//  AutoAlignLabel.m
//  Pods
//
//  Created by Oren Kosto,  on 6/12/17.
//
//

#import "AutoAlignLabel.h"

@implementation AutoAlignLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- ( void ) setText:( NSString * )text {
    if ( [ [PVPConfigHelper sharedInstance] forceRTL] ) {
        [self setTextAlignment:NSTextAlignmentRight];
    }
    else if ( [PVPLocalizationHelper isRTL] && [PVPLocalizationHelper isRTLEnabled]) {
        [self setTextAlignment:NSTextAlignmentRight];
    }
    else {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
    [super setText:text];
}

@end
