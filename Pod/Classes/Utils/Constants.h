//
//  Constants.h
//  PVPClient
//
//  Created by Oren Kosto,  on 10/30/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#ifndef PVPClient_Constants_h
#define PVPClient_Constants_h

/**
 * The Bamboo gallery type enum.
 */
typedef NS_ENUM (NSUInteger, BambooGalleryType) {

    /**
     * A single category gallery.
     */
    BambooGalleryTypeCategory = 0,

    /**
     * A VOD gallery.
     */
    BambooGalleryTypeVOD,

    /**
     * A search results gallery.
     */
    BambooGalleryTypeSearch,
    BambooGalleryTypeSearchNode,
    BambooGalleryTypeSearchEntry,

    /**
     * A live entry gallery.
     */
    BambooGalleryTypeLive,

    /**
     * A channels gallery.
     */
    BambooGalleryTypeChannels,
    
    BambooGalleryTypeTwoCol,
    BambooGalleryTypeOneCol,
    /**
     * A category tree gallery.
     */
    BambooGalleryTypeCategories,

    /**
     * An offline (local entries) gallery.
     */
    BambooGalleryTypeOffline,

    /**
     * A watch history gallery.
     */
    BambooGalleryTypeWatchHistory,

    /**
     * A watch history gallery.
     */
    BambooGalleryTypeWatchHistoryTree,

    /**
     * A purchase history gallery.
     */
    BambooGalleryTypePurchaseHistory,

    /**
     * A news page node cateory gallery.
     */
    BambooGalleryTypeNode,
    
    /**
     * A news page node gallery
     */
    BambooGalleryTypeNodeCell,
    
    /**
    * Category list view gallery
    */
    BambooGalleryTypeList,
    
    /**
    * Category gallery view with horizontal  cards horizontal scroll
    */
    BambooGalleryTypeHorizontalCards,
    
    /**
    * Category gallery view with vertical cards horizontal scroll
    */
    BambooGalleryTypeVerticalCards
};

/**
 * The watch history item state enum.
 */
typedef NS_ENUM(NSUInteger, PVPWatchHistoryItemState) {

    /**
     * Currently watching (between 20% and 95%).
     */
    PVPWatchHistoryItemStateWatching = 1,

    /**
     * Previously watched (over 95%).
     */
    PVPWatchHistoryItemStateWatched = 2
};

#define DEFAULTS_KEY_CURRENT_CONFIG @"currentConfig"
#define DEFAULTS_KEY_HAS_VISITED_APP @"hasVisitedApp"

#define OPENED_FROM_DEEPLINK @"deepLinkOpened"
#define DEEPLINK_ROUTE @"deeplinkRoute"

#define PUSH_TYPE_EVENT_STARTING 1
#define PUSH_TYPE_INCOMING_CALL 2

#define ENTRY_TYPE_LIVE 1
#define ENTRY_TYPE_VOD 2

#define ENTRYBACKEND_SERVICE_KALTURA 0
#define ENTRYBACKEND_SERVICE_YOUTUBE 1
//#define ENTRYBACKEND_SERVICE_BAMBOO  2

#define SORT_RECENT 1
#define SORT_VIEWS 2

#define SIGNAL_WEAK 1
#define SIGNAL_MEDIUM 2
#define SIGNAL_STRONG 3

#define CELLS_TO_END_BEFORE_LOADING_MORE 3

#define GENERAL_NONE @"N/A"

#define GENDER_MALE @"Male"
#define GENDER_FEMALE @"Female"
#define GENDER_ANY @"Any"

#define SMOKER_TRUE @"Smoker"
#define SMOKER_FALSE @"Non-Smoker"

#define PVP_ROLE_VIEWER @"viewerRole"
#define PVP_ROLE_CONTRIBUTOR @"contributorRole"
#define PVP_ROLE_MANAGER @"managerRole"
#define PVP_ROLE_ADMIN @"adminRole"
#define PVP_ROLE_ROOT @"rootRole"

#define TITLE_ANY @"Any"
#define TITLE_CANCEL @"Cancel"

#define TITLE_CONFIRM @"Confirm"

#define ALERT_OK @"OK"
#define ALERT_CANCEL @"Cancel"
#define ALERT_YES @"Yes"
#define ALERT_NO @"No Thanks"
#define ALERT_LATER @"Maybe Later"
#define ALERT_CONTINUE_ANYWAY @"Continue Anyway"
#define ALERT_TRY_AGAIN @"Try Again"
#define ALERT_ERROR_TITLE @"Error!"
#define ALERT_ERROR_MESSAGE @"Oops! Something went wrong. Please try again."

#define ALERT_POST_SUCCESSFUL @"Post Successful"
#define ALERT_POST_CANCELED @"Post Canceled"
#define ALERT_POST_FAILED @"There was a problem sharing your post. Please try again."

#define EVENT_STARTING_NOTIFICATION @"eventStarting"

#define USER_TOKEN_KEY @"loggedInUserToken"
#define USER_AGENT_KEY @"UserAgent"
#define USER_AGENT_PREFIX_KEY @"UserAgentPrefixKey"
#define IS_SIMULATOR_KEY @"isSimulator"

#define FACEBOOK_TITLE @"Facebook"
#define FACEBOOK_APP_ID @"facebookAppId"
#define FACEBOOK_POST_TO_FAN_PAGE @"facebookPostToFanPage"
#define FACEBOOK_FAN_PAGE_NAME @"facebookFanPageName"
#define FACEBOOK_FAN_PAGE_ID @"facebookFanPageID"
#define FACEBOOK_FAN_PAGE_TOKEN @"facebookFanPageToken"
#define TWITTER_TITLE @"Twitter"
#define TWITTER_CONSUMER_KEY @"twitterConsumerKey"
#define TWITTER_CONSUMER_SECRET @"twitterConsumerSecret"
#define LOGGED_IN_KEY @"isLoggedIn"
#define CREDENTIALS_DICT_KEY @"credentialsDict"

#define PURCHASES_DICT_KEY @"purchasesDictKey"
#define PURCHASE_SERVICE_APPLE 1

#define PURCHASE_TYPE_ENTRY 1
#define PURCHASE_TYPE_IMAGE 2
#define PURCHASE_TYPE_USER 3
#define PURCHASE_TYPE_CATEGORY 4
#define PURCHASE_TYPE_AUDIO 5
#define PURCHASE_TYPE_INSTANCE 6
#define PURCHASE_TYPE_LIVE_STREAM 7
#define PURCHASE_TYPE_PACKAGE 8

#define FORCE_REFRESH_KEY @"forceRefresh"
#define SEARCH_FILTER_KEY @"searchFilter"
#define DO_SEARCH_NOTIFICATION @"doSearchNotification"
#define PRESENT_CHAT_SEGUE @"presentChatWindow"
#define PRESENT_CHAT_NOTIFICATION @"presentChatWindowNotification"
#define RELOAD_CHAT_NOTIFICATION @"reloadChatNotification"
#define DO_REFRESH_NOTIFICATION @"doRefreshNotification"

#define PRESENT_PROFILE_SEGUE @"presentUserProfile"
#define PRESENT_PROFILE_NOTIFICATION @"presentUserProfileNotification"

#define PRESENT_OTHER_USERS_PROFILE_SEGUE @"presentOtherUsersProfile"
#define PRESENT_OTHER_USERS_PROFILE_NOTIFICATION @"presentOtherUsersProfileNotification"

#define DEFAULT_USER_IMAGE @"defaultUserImage"
#define DEFAULT_USER_IMAGE_MALE @"default_profile_male"
#define DEFAULT_USER_IMAGE_FEMALE @"default_profile_female"

#define EVENT_ICON_ONLINE @"event_online"
#define EVENT_ICON_LIKE @"event_love"
#define EVENT_ICON_FAVORITE @"event_favorite"
#define EVENT_ICON_VIEWED @"event_viewed"

#define MENU_CURRENT_PAGE_KEY @"menuCurrentPageKey"
#define MENU_PAGE_BROWSE_EVENTS 0
#define MENU_PAGE_MY_EVENTS 1
#define MENU_PAGE_MY_FOLLOW 2
#define MENU_PAGE_MY_PROFILE 3
#define MENU_PAGE_LOGOUT 4

#define ENUM_GENDER_MALE 1
#define ENUM_GENDER_FEMALE 2

#define BLOCK_REASON_SPAM 1
#define BLOCK_REASON_OFFENSIVE 2
#define BLOCK_REASON_TYPE_NO_SPECIAL_REASON 3
#define BLOCK_REASON_TYPE_NOT_INTERESTED 4
#define BLOCK_REASON_TYPE_OTHER 5

#define INSET_HORIZONTAL 7.5f
#define INSET_VERTICAL  7.5f
#define CORNER_RADIUS 7.5f
#define BORDER_LINE_WIDTH 2.5f
#define TVOS_ENTRY_HEIGHT 200.0f

#define PROFILE_KEY @"profile"
#define INBOX_KEY @"inbox"
#define FAVORITES_KEY @"favorites"
#define SEARCH_KEY @"search"
#define NEWS_FEED_KEY @"newsFeed"

#define NOTIFICATION_KEY_USER_ID @"userId"
#define NOTIFICATION_KEY_CHANGED_FAVORITE @"changedFavorite"
#define NOTIFICATION_KEY_SELECTED @"selected"

#define NOTIFICATION_KEY_SETTINGS_CHANGED @"settingsChanged"
#define DEFAULTS_KEY_USER_LANGUAGE @"userLanguage"

#define CATEGORY_OFFLINE_TITLE @"My Downloads"
#define CATEGORY_OFFLINE_TYPE @"offline"
#define CATEGORY_SEARCH_TITLE @"Search"

#define NOTIFICATION_BEACON_SUCCESS @"PVPBeaconSuccessNotification"
#define NOTIFICATION_BEACON_FAILURE @"PVPBeaconFailedNotification"

#define NOTIFICATION_MUTE_ALL_PLAYERS @"muteAllPlayersNotification"
#define NOTIFICATION_RESUME_ALL_PLAYERS @"resumeAllPlayersNotification"
#define NOTIFICATION_STOP_ALL_PLAYERS @"stopAllPlayersNotification"
#define NOTIFICATION_ROUTER_LOAD @"loadNotification"
#define NOTIFICATION_DEEP_LINK_LOADED @"loadDeepLink"
#define NOTIFICATION_HANDLE @"handleNotification"

#define IDFA_SYSTEM_REQUEST @"idfaSystemRequest"

#define BAMBOO_PLAYER_FINISHED @"BambooPlayerFinished"
#define BAMBOO_PLAYER_TAPPED @"BambooPlayerTapped"

static NSString *const kFIREventLogout NS_SWIFT_NAME(AnalyticsEventLogout) = @"logout";
static NSString *const kFIREventBlockedCountry NS_SWIFT_NAME(AnalyticsEventBlockedCountry) = @"country_bocked";
static NSString *const kFIREventStartSignUp NS_SWIFT_NAME(AnalyticsEventStartSignUp) = @"entered_registration";
static NSString *const kFIREventCancelSignUp NS_SWIFT_NAME(AnalyticsEventCancelSignUp) = @"canceled_registration";
static NSString *const kFIREventInvalidSignUp NS_SWIFT_NAME(AnalyticsEventCancelSignUp) = @"invalid_registration";
static NSString *const kFIREventPurchaseSignUp NS_SWIFT_NAME(AnalyticsEventPurchaseSignUp) = @"register_user_with_purchase";
static NSString *const kFIREventPurchaseSuccess NS_SWIFT_NAME(AnalyticsEventPurchaseSignUp) = @"purchase_success";
static NSString *const kFIREventPurchaseFailed NS_SWIFT_NAME(AnalyticsEventPurchaseSignUp) = @"purchase_failed";

#endif

