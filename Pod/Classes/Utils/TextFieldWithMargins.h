//
//  TextFieldWithMargins.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/28/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  The TextFieldWithMargins class extends the UITextField and provides a flat, rectangular text field, with margins on all sides.
 */
@interface TextFieldWithMargins : UITextField

@end
