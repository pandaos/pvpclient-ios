//
//  AutoAlignLabel.h
//  Pods
//
//  Created by Oren Kosto,  on 6/12/17.
//
//

#import <UIKit/UIKit.h>
#import "NSString+PVPClient.h"
#import "PVPConfigHelper.h"
#import "PVPLocalizationHelper.h"

@interface AutoAlignLabel : UILabel

@end
