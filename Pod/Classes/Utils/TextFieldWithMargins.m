//
//  TextFieldWithMargins.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/28/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "TextFieldWithMargins.h"

@implementation TextFieldWithMargins

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 5, 5);
}

-(CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 5, 5);
}

@end
