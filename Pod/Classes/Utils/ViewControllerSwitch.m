//
//  ViewControllerSwitch.m
//  PVPClient
//
//  Created by Oren Kosto,  on 7/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "ViewControllerSwitch.h"
#import <objc/message.h>

@implementation ViewControllerSwitch

+(void)loadController:(UIViewController *)VControllerToLoad
{
    [self loadController:VControllerToLoad
              andRelease:(UIViewController *) [[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder]];
}

+(void)loadController:(UIViewController *)VControllerToLoad andRelease:(UIViewController *)VControllerToRelease
{
    [self loadController:VControllerToLoad
              andRelease:VControllerToRelease
             withOptions:UIViewAnimationOptionTransitionFlipFromLeft];
}

+(void)loadController:(UIViewController *)VControllerToLoad withOptions:(UIViewAnimationOptions)options
{
    [self loadController:VControllerToLoad
              andRelease:(UIViewController *) [[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder]
             withOptions:options];
}

+(void)loadController:(UIViewController *)VControllerToLoad andRelease:(UIViewController *)VControllerToRelease withOptions:(UIViewAnimationOptions)options
{
//    UIViewController *nextResponder = (UIViewController *) [[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//    bool isReleasingLoginController = [NSStringFromClass([VControllerToRelease class]) isEqualToString:@"BambooLoginViewController"];
//    if (!(nextResponder.class == [UIWindow class] && isReleasingLoginController) && VControllerToRelease.class != nextResponder.class) { //if the controller being released is the root vc
//        return;
//    }

    //adjust the frame of the new controller
    CGRect statusBarFrame = CGRectZero;
    CGRect windowFrame = [[UIScreen mainScreen] bounds];
    CGRect firstViewFrame = CGRectMake(statusBarFrame.origin.x, statusBarFrame.size.height, windowFrame.size.width, windowFrame.size.height - statusBarFrame.size.height);
    VControllerToLoad.view.frame = firstViewFrame;
    // init transition animation
    [UIView transitionWithView:[[UIApplication sharedApplication].delegate window]
                      duration:0.5
                       options:options
                    animations:^{
                        //set the new controller as the root controller
                        [[[UIApplication sharedApplication].delegate window] setRootViewController:VControllerToLoad];
                    }
                    completion:^(BOOL finished) {
                        //kill the previous view controller
                        if(class_getProperty([VControllerToRelease class], "view")) {
                            [VControllerToRelease.view removeFromSuperview];
                        }
                    }];
}

@end
