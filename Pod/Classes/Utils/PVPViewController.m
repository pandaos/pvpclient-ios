//
//  PVPViewController.m
//  PVPClient
//
//  Created by Oren Kosto,  on 8/27/15.
//  Copyright (c) 2015 Panda-OS. All rights reserved.
//

#import "PVPViewController.h"

@interface PVPViewController ()

@end

@implementation PVPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.trackedViewName = [NSStringFromClass([self class]) stringByReplacingOccurrencesOfString:@"ViewController" withString:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
