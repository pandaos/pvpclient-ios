//
//  AppUtils.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/13/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "XMLDictionary.h"
#import <StoreKit/StoreKit.h>
#import "Constants.h"

#if TARGET_OS_TV

#elif TARGET_OS_IOS
#import <WebKit/WebKit.h>

@interface WKWebView(SynchronousEvaluateJavaScript)
- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script;
@end

@implementation WKWebView(SynchronousEvaluateJavaScript)

- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script
{
    __block NSString *resultString = nil;
    __block BOOL finished = NO;

    [self evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
                resultString = [NSString stringWithFormat:@"%@", result];
            }
        } else {
            NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
        }
        finished = YES;
    }];

    while (!finished)
    {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    return resultString;
}
@end
#endif

__deprecated_msg("Full upload process moved to PVPEntryModel")
@protocol ImageUploadDelegate <NSObject>
-(void) imageUploadSuccess:(NSDictionary *)responseDict;
-(void) imageUploadFailure;
@end

@interface AppUtils : NSObject <ASIHTTPRequestDelegate> {
    void (^_success)(NSArray *result);
    void (^_failure)(NSError *e);
    
    NSNumberFormatter *priceFormatter;
}

@property (nonatomic, weak) id<ImageUploadDelegate> imageUploadDelegate DEPRECATED_MSG_ATTRIBUTE("Full upload process moved to PVPEntryModel");

@property (nonatomic, strong, getter=getChunkSize) NSNumber *chunkSize DEPRECATED_ATTRIBUTE;
@property (nonatomic, strong, getter=getWowzaRtmpServer) NSNumber *wowzaRtmpServer DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
@property (nonatomic, strong, getter=getAppName) NSNumber *appName DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");

@property (nonatomic, strong) NSString *userKS DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper's currentConfig property");
@property (nonatomic, strong) NSString *userPartnerId DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
@property (nonatomic, strong) NSString *kalturaServiceUrl DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
@property (nonatomic, strong) NSString *playerUrl DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
@property (nonatomic, strong) NSString *playerUiConfId DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
@property (nonatomic, assign) BOOL sendPushOnEventCreated;
@property (nonatomic, assign) BOOL sendPushOnEventStarted;
@property (nonatomic, strong) NSString *eventCreatedText;
@property (nonatomic, strong) NSString *eventStartedText;
@property (nonatomic, strong) NSString *socialWatchVODShareText;
@property (nonatomic, strong) NSString *socialWatchLiveShareText;
@property (nonatomic, strong) NSString *socialStreamShareText;
@property (nonatomic, strong) NSArray *eventProducts DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use IAPHelper");
@property (nonatomic, assign) NSTimeInterval broadcastTime;
@property (nonatomic, strong) NSString *piwikBaseUrl DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use PVPConfigHelper");
@property (nonatomic, strong) NSString *piwikSiteId DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use PVPConfigHelper");
@property (nonatomic, strong) NSMutableArray *userPurchasedEntries DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use IAPHelper");
@property (nonatomic, strong) NSMutableArray *userPurchasedUsers DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use IAPHelper");

+(id) sharedUtils;
-(NSString *) localizedPriceStringForProductIndex:(int)productIndex DEPRECATED_MSG_ATTRIBUTE("Not used anymore, use IAPHelper");

-(NSString *) localizedPriceStringForProduct:(SKProduct *)product DEPRECATED_MSG_ATTRIBUTE("Please use IAPHelper");

+(NSString *) getHTML5PlayerUrl:(NSString *)entryId DEPRECATED_MSG_ATTRIBUTE("Method not needed anymore, will soon be removed");
+(NSString *) getPlayerUrl DEPRECATED_MSG_ATTRIBUTE("Please use PVPConfigHelper");
+(NSString *)addParamsToPictureUrl:(NSString *)profilePictureUrl DEPRECATED_MSG_ATTRIBUTE("Please use [PVPUserInfo profilePictureUrlWithSize:]");
+(NSString *) getConfiguration:(NSString *)name DEPRECATED_MSG_ATTRIBUTE("Please use [PVPConfigHelper getLocalConfiguration:]");
-(NSString *) getThumbnailUrlWithEntryId:(NSString *)entryId DEPRECATED_MSG_ATTRIBUTE("Please use [PVPEntry kalturaThumbnailUrlForEntryId:]");
+(NSString *) getDeviceUserAgent;
+(NSString *)getDeviceModel;
+(NSString *) getDefaultValueFofKey:(NSString *)key;
+(NSDictionary *) getDefaultDictionaryForKey:(NSString *)key;
+(NSNumber *) getDefaultNumberFofKey:(NSString *)key;
+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key;
+(void) saveDefaultDictionary:(NSDictionary *)value forKey:(NSString *)key;
+(void) saveDefaultNumber:(NSNumber *)number forKey:(NSString *)key;
+(void) deleteDefaultValueWithKey:(NSString *)key;
+(void)clearAllNotificationDefaults;
+(NSString *) md5HexDigest:(NSString *)input withRaw:(BOOL) raw;
+ (NSString*) MD5Hasher: (NSString*) query;

+(NSDictionary *) dictionaryFromJSONString:(NSString *)string DEPRECATED_MSG_ATTRIBUTE("Please use the NSDictionary+PVPClient category");
+(NSString *) JSONStringFromDictionary:(NSDictionary *)dictionary DEPRECATED_MSG_ATTRIBUTE("Please use the NSString+PVPClient category");
+(NSString *) hexadecimalStringFromData:(NSData *)data DEPRECATED_MSG_ATTRIBUTE("Please use the NSString+PVPClient category");
+(NSString *)normalizedNumber:(int)number DEPRECATED_MSG_ATTRIBUTE("Please use the NSString+PVPClient category");

+(NSString *)shareTextFrom:(NSString *)originalText withUserName:(NSString *)userName withEntryName:(NSString *)entryName withEntryId:(NSString *)entryId;
+(NSString *)playPageUrlWithEntryId:(NSString *)entryId DEPRECATED_MSG_ATTRIBUTE("Method not needed anymore, will soon be removed");
+(void) performSelector:(SEL)selector on:(id)delegate;
+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object;

#if TARGET_OS_IOS
+(void) openUrlWithString:(NSString *)urlString;
-(void) uploadImage:(UIImage *)image withName:(NSString *)name withUploadToken:(NSString *)uploadTokenId DEPRECATED_MSG_ATTRIBUTE("Please use [PVPEntryModel uploadImageToKaltura:withName:withUploadToken:]");
+(void) showAlertWithDefaultErrorMessage DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withDelegate:(id)delegate withTag:(NSInteger)tag DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withDelegate:(id)delegate withTag:(NSInteger)tag withCancelButtonTitle:(NSString *)cancelButtonTitle withOtherButtonTitle:(NSString *)otherButtonTitle DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withTextFieldType:(UIKeyboardType)keyboardType withPlaceholder:(NSString *)placeholder withDelegate:(id /**<UIAlertViewDelegate>*/)delegate withTag:(NSInteger)tag DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message withTextFieldType:(UIKeyboardType)keyboardType withInitialText:(NSString *)text withDelegate:(id /**<UIAlertViewDelegate>*/)delegate withTag:(NSInteger)tag DEPRECATED_MSG_ATTRIBUTE("Please use PVPAlertHelper");
+(UIImage *)imageWithColor:(UIColor *)color withFrame:(CGRect) rect DEPRECATED_MSG_ATTRIBUTE("Please use UIImage+PVPClient");
#endif

- (NSString *)tokenizePlaybackUrl:(NSString *)url;

@end
