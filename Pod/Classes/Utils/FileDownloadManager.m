//
//  FileDownloadManager.m
//  Pods
//
//  Created by Oren Kosto on 7/28/16.
//
//

#import "FileDownloadManager.h"

#define MAX_CONNECTIONS 10
#define ROOT_FILE_DIRECTORY @"Bamboo"
#define ENTRIES_FILE_DIRECTORY @"Entries"
#define THUMBNAILS_FILE_DIRECTORY @"Thumbnails"
#define IN_PROGRESS_FILE_DOWNLOAD_INFO_FILE_NAME @"inProgressFileDownloadInfo.txt"
#define IN_PROGRESS_ENTRIES_FILE_NAME @"inProgressEntries.txt"
#define DOWNLOADED_ENTRIES_FILE_NAME @"downloadedEntries.txt"
#define FAILED_ENTRIES_FILE_NAME @"failedEntries.txt"
#define DOWNLOADED_ENTRY_CUEPOINTS_FILE_NAME @"downloadedEntryCuepoints.txt"

@implementation FileDownloadInfo

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"filename": @"filename",
             @"entryId": @"entryId",
             @"source": @"source",
             @"task": @"task",
             @"progress": @"progress",
             @"isInProgress": @"isInProgress",
             @"isComplete": @"isComplete",
             @"taskIdentifier": @"taskIdentifier",
             @"taskResumeData": @"taskResumeData",
             };
}

- (FileDownloadInfo *)initWithFilename:(NSString *)filename withsource:(NSString *)source {
    NSString *entryId = [filename stringByReplacingOccurrencesOfString:@".mp4" withString:@""];
    return [self initWithEntryId:entryId withFilename:filename withsource:source];
}

- (FileDownloadInfo *)initWithEntryId:(NSString *)entryId withFilename:(NSString *)filename withsource:(NSString *)source {
    self = [super init];
    if (self) {
        self.entryId = entryId;
        self.filename = filename;
        self.source = source;
        self.progress = 0.0;
        self.isInProgress = NO;
        self.isComplete = NO;
        self.taskIdentifier = PVP_DOWNLOAD_TASK_ID_NONE;
    }
    
    return self;
}

@end

@interface FileDownloadManager()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionConfiguration *sessionConfiguration;
@property (nonatomic, strong) NSFileManager *fileManager;
@property (nonatomic, strong) NSString *docDirectoryPath;

@end

@implementation FileDownloadManager

NSString *const FileDownloadManagerTaskStartedNotification = @"FileDownloadManagerTaskStartedNotification";
NSString *const FileDownloadManagerTaskProgressNotification = @"FileDownloadManagerTaskProgressNotification";
NSString *const FileDownloadManagerTaskPausedNotification = @"FileDownloadManagerTaskPausedNotification";
NSString *const FileDownloadManagerTaskCancelledNotification = @"FileDownloadManagerTaskCancelledNotification";
NSString *const FileDownloadManagerTaskFinishedNotification = @"FileDownloadManagerTaskFinishedNotification";
NSString *const FileDownloadManagerTaskFailedNotification = @"FileDownloadManagerTaskFailedNotification";

static FileDownloadManager *sharedManager = nil;
static dispatch_once_t onceToken;
static BOOL isSetup = NO;

+ (instancetype) sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedManager setup];
        isSetup = YES;
    }
    return sharedManager;
}

-(void)setup {
    self.fileManager = [NSFileManager defaultManager];
    self.docDirectoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    self.docDirectoryPath = [self.docDirectoryPath stringByAppendingPathComponent:ROOT_FILE_DIRECTORY];
    if (![self.fileManager fileExistsAtPath:self.docDirectoryPath]) {
        [self.fileManager createDirectoryAtPath:self.docDirectoryPath withIntermediateDirectories:NO attributes:nil error:nil];
        [self.fileManager createDirectoryAtPath:[self.docDirectoryPath stringByAppendingPathComponent:ENTRIES_FILE_DIRECTORY] withIntermediateDirectories:NO attributes:nil error:nil];
        [self.fileManager createDirectoryAtPath:[self.docDirectoryPath stringByAppendingPathComponent:THUMBNAILS_FILE_DIRECTORY] withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    self.sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[[NSBundle mainBundle] bundleIdentifier]];
    self.sessionConfiguration.HTTPMaximumConnectionsPerHost = MAX_CONNECTIONS;
    self.session = [NSURLSession sessionWithConfiguration:self.sessionConfiguration delegate:self delegateQueue:nil];
    
    self.fileDownloadInfoDict = [self loadInProgressFileInfo];
    self.inProgressEntries = [self loadObjectsFromFile:IN_PROGRESS_ENTRIES_FILE_NAME withClass:[PVPEntry class]];
    self.localEntries = [self loadObjectsFromFile:DOWNLOADED_ENTRIES_FILE_NAME withClass:[PVPEntry class]];
    self.failedEntries = [self loadObjectsFromFile:FAILED_ENTRIES_FILE_NAME withClass:[PVPEntry class]];
    self.localCuepoints = [self loadObjectsFromFile:DOWNLOADED_ENTRY_CUEPOINTS_FILE_NAME withClass:[PVPCuePoint class]];
    
    [self checkForFailedDownloads];
}

- (FileDownloadInfo *)fileDownloadInfoByTaskIdentifier:(NSUInteger)taskIdentifier {
    NSNumber *key = @(taskIdentifier);
    return self.fileDownloadInfoDict[key];
}

#pragma mark loading and saving permanent data that is stored in files
- (NSMutableDictionary<NSNumber *, FileDownloadInfo *> *)loadInProgressFileInfo {
    NSMutableDictionary<NSNumber *, FileDownloadInfo *> *result = self.fileDownloadInfoDict = [NSMutableDictionary dictionaryWithCapacity:MAX_CONNECTIONS];
    NSMutableArray<FileDownloadInfo *> *objectsArray = [self loadObjectsFromFile:IN_PROGRESS_FILE_DOWNLOAD_INFO_FILE_NAME withClass:[FileDownloadInfo class]];
    
    for (FileDownloadInfo *info in objectsArray) {
        if (info.taskIdentifier && info.taskIdentifier != PVP_DOWNLOAD_TASK_ID_NONE) {
            self.fileDownloadInfoDict[@(info.taskIdentifier)] = info;
        }
    }
    return result;
}

- (void) saveInProgressFileInfo {
    NSArray *fileDownloadInfoArray = [self.fileDownloadInfoDict allValues];
    for (FileDownloadInfo *info in fileDownloadInfoArray) {
        if (info.task) {
            info.task = nil;
        }
    }
    [self saveObjectsFromArray:[fileDownloadInfoArray mutableCopy] toFile:IN_PROGRESS_FILE_DOWNLOAD_INFO_FILE_NAME];
}

- (NSMutableArray *) loadObjectsFromFile:(NSString *)file withClass:(Class)class {
    NSError *error = nil;
    NSString *destinationPath = [self.docDirectoryPath stringByAppendingPathComponent:file];
//    NSURL *destinationUrl = [NSURL fileURLWithPath:destinationPath]; //parse the destination path for the file
    
    if ([self.fileManager fileExistsAtPath:destinationPath]) { //if the file already exists, we will overwrite it
        NSArray *jsonArray = [NSKeyedUnarchiver unarchiveObjectWithFile:destinationPath];
        NSArray *objectsArray = [PVPJSONAdapter modelsOfClass:class fromJSONArray:jsonArray error:&error];
        if (objectsArray) {
            return [objectsArray mutableCopy];
        }
    }
    
    return [NSMutableArray arrayWithCapacity:MAX_CONNECTIONS];
}

- (void) saveObjectsFromArray:(NSMutableArray *)objectsArray toFile:(NSString *)file {
    NSError *error = nil;
    NSString *destinationPath = [self.docDirectoryPath stringByAppendingPathComponent:file];
//    NSURL *destinationUrl = [NSURL fileURLWithPath:destinationPath]; //parse the destination path for the file
    
    if ([self.fileManager fileExistsAtPath:destinationPath]) { //if the file already exists, we will overwrite it
        [self.fileManager removeItemAtPath:destinationPath error:&error];
    }
    NSArray *jsonArray = [PVPJSONAdapter JSONArrayFromModels:objectsArray error:&error];
    if ([jsonArray count] > 0) {
        BOOL success = [NSKeyedArchiver archiveRootObject:jsonArray toFile:destinationPath];
        NSLog(@"file update completed with result %d", success);
    }
}

- (NSString *)localPlaybackUrlForEntry:(PVPEntry *)entry {
//    NSError *error = nil;
    NSString *filename = [NSString stringWithFormat:@"%@.mp4", entry.id];
    
    NSString *entryDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:ENTRIES_FILE_DIRECTORY] stringByAppendingPathComponent:filename];
//    NSURL *entryDestinationUrl = [NSURL fileURLWithPath:entryDestinationPath]; //parse the destination path for the file
    if ([self.fileManager fileExistsAtPath:entryDestinationPath]) { //if the file already exists, we will overwrite it
        return entryDestinationPath;
    }
    return nil;
}

- (UIImage *) localImageForEntry:(PVPEntry *)entry {
//    NSError *error = nil;
    NSString *thumbnailFilename = [NSString stringWithFormat:@"%@.png", entry.id];
    
    NSString *thumbnailDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:THUMBNAILS_FILE_DIRECTORY] stringByAppendingPathComponent:thumbnailFilename];
//    NSURL *thumbnailDestinationUrl = [NSURL fileURLWithPath:thumbnailDestinationPath]; //parse the destination path for the file
    if ([self.fileManager fileExistsAtPath:thumbnailDestinationPath]) { //if the file already exists, we will overwrite it
        NSData *data = [self.fileManager contentsAtPath:thumbnailDestinationPath];
        return [UIImage imageWithData:data];
    }
    return nil;
}

#pragma mark download management
- (void)startOrResumeDownload:(FileDownloadInfo *)info {
    if (info && !info.isInProgress) {
        info.task = nil;

        if (info.taskIdentifier != PVP_DOWNLOAD_TASK_ID_NONE && info.taskResumeData) {
            [self.fileDownloadInfoDict removeObjectForKey:@(info.taskIdentifier)]; //remove existing task with the same identifier if exists
            info.task = [self.session downloadTaskWithResumeData:info.taskResumeData]; //resume existing download
        }

        if (!info.task) {
            info.task = [self.session downloadTaskWithURL:[NSURL URLWithString:info.source]]; //start new download
        }

        info.taskIdentifier = info.task.taskIdentifier;
        [info.task resume];
        info.isInProgress = YES;
        info.isComplete = NO;
        self.fileDownloadInfoDict[@(info.taskIdentifier)] = info;
        [self saveInProgressFileInfo];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
            [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskStartedNotification object:info];
        }];
        NSLog(@"started downloading file: %@.", info.filename);
    }
}

- (void)pauseDownload:(FileDownloadInfo *)info {
    //cancel the download task and produce data for rsuming the download later on
    [info.task cancelByProducingResumeData:^(NSData *resumeData) {
        info.taskResumeData = [[NSData alloc] initWithData:resumeData];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
            [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskPausedNotification object:info];
        }];
    }];
}

- (void)cancelDownload:(FileDownloadInfo *)info {
    [info.task cancel];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
        [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskCancelledNotification object:info];
    }];
}

-(void)downloadEntry:(PVPEntry *)entry {
    NSString *filename = [NSString stringWithFormat:@"%@.mp4", entry.id];
    NSNumber *flavorParamId = [PVPConfigHelper sharedInstance].currentConfig.mobile[@"downloadFlavorParamId"];
    if(flavorParamId == nil) {
        flavorParamId = @4;
    }
    
    FileDownloadInfo *downloadInfo = [[FileDownloadInfo alloc] initWithFilename:filename withsource:[entry pvpDownloadUrlWithFlavorParamId:flavorParamId.intValue]]; //we download flavor param 4 by default or take from config
    
    if (![self entryExistsOrInProgress:entry.id]) {
        //start the download
        [self downloadImageForEntry:entry];
        [self.inProgressEntries addObject:entry];
        [self saveObjectsFromArray:self.inProgressEntries toFile:IN_PROGRESS_ENTRIES_FILE_NAME];
        PVPEntry *failedEntry = [self failedEntryByEntryId:entry.id];
        if (failedEntry) {
            NSLog(@"removing existing failed entry...");
            [self.failedEntries removeObject:failedEntry];
            [self saveObjectsFromArray:self.failedEntries toFile:FAILED_ENTRIES_FILE_NAME];
        }
        
        [self startOrResumeDownload:downloadInfo];
        //TODO add cue points
        if ([PVPConfigHelper alertsEnabled]) {
            [PVPAlertHelper showAlertWithTitle:@"Download Started" andMessage:@"Your download is starting. Please do not close the app."];
        }
    } else if ([PVPConfigHelper alertsEnabled]) {
        [PVPAlertHelper showAlertWithTitle:@"Already Exists" andMessage:@"This file was either already downloaded or is currently downloading."];
    }
}

- (void)downloadEntry:(PVPEntry *)entry withCuepoints:(NSArray <PVPCuePoint *> *)cuepoints {
    [self saveCuepoints:cuepoints forEntry:entry];
    [self downloadEntry:entry];
}

- (void)deleteEntry:(PVPEntry *)entry {
    
    [self.localEntries filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return ![[(PVPEntry *)evaluatedObject id] isEqualToString:entry.id];
    }]];
    [self saveObjectsFromArray:self.localEntries toFile:DOWNLOADED_ENTRIES_FILE_NAME];
    
    [self deleteVideoForEntry:entry];
    [self deleteImageForEntry:entry];
}

- (void)saveCuepoints:(NSArray <PVPCuePoint *> *)cuepoints forEntry:(PVPEntry *)entry {
    [self.localCuepoints addObjectsFromArray:cuepoints];
    [self saveObjectsFromArray:self.localCuepoints toFile:DOWNLOADED_ENTRY_CUEPOINTS_FILE_NAME];
}

- (void)downloadImageForEntry:(PVPEntry *)entry {
    NSError *error = nil;
    NSString *thumbnailFilename = [NSString stringWithFormat:@"%@.png", entry.id];
    
    NSString *thumbnailDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:THUMBNAILS_FILE_DIRECTORY] stringByAppendingPathComponent:thumbnailFilename];
//    NSURL *thumbnailDestinationUrl = [NSURL fileURLWithPath:thumbnailDestinationPath]; //parse the destination path for the file
    if ([self.fileManager fileExistsAtPath:thumbnailDestinationPath]) { //if the file already exists, we will overwrite it
        [self.fileManager removeItemAtPath:thumbnailDestinationPath error:&error];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul), ^{
        NSURL *imageUrl = [NSURL URLWithString:[PVPEntry kalturaThumbnailUrlForEntryId:entry.id withSize:CGSizeMake(600, 600)]];
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        [imageData writeToFile:thumbnailDestinationPath atomically:YES];
    });
}

- (void)deleteImageForEntry:(PVPEntry *)entry {
    NSError *error = nil;
    NSString *thumbnailFilename = [NSString stringWithFormat:@"%@.png", entry.id];
    
    NSString *thumbnailDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:THUMBNAILS_FILE_DIRECTORY] stringByAppendingPathComponent:thumbnailFilename];
    if ([self.fileManager fileExistsAtPath:thumbnailDestinationPath]) {
        [self.fileManager removeItemAtPath:thumbnailDestinationPath error:&error];
    }
}

- (void)deleteVideoForEntry:(PVPEntry *)entry {
    NSError *error = nil;
    NSString *filename = [NSString stringWithFormat:@"%@.mp4", entry.id];
    NSString *entryDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:ENTRIES_FILE_DIRECTORY] stringByAppendingPathComponent:filename];
    if ([self.fileManager fileExistsAtPath:entryDestinationPath]) {
        [self.fileManager removeItemAtPath:entryDestinationPath error:&error];
    }
}

- (BOOL)entryExistsOrInProgress:(NSString *)entryId {
    return [self inProgressEntryByEntryId:entryId] || [self localEntryByEntryId:entryId];
}

- (BOOL)entryExists:(NSString *)entryId {
    return [self localEntryByEntryId:entryId];
}

- (BOOL)entryInProgress:(NSString *)entryId {
    return [self inProgressEntryByEntryId:entryId];
}

- (PVPEntry *)inProgressEntryByEntryId:(NSString *)entryId {
    for (PVPEntry *entry in self.inProgressEntries) {
        if ([entry.id isEqualToString:entryId]) {
            return entry;
        }
    }
    return nil;
}

- (PVPEntry *)localEntryByEntryId:(NSString *)entryId {
    for (PVPEntry *entry in self.localEntries) {
        if ([entry.id isEqualToString:entryId]) {
            return entry;
        }
    }
    return nil;
}

- (PVPEntry *)failedEntryByEntryId:(NSString *)entryId {
    for (PVPEntry *entry in self.failedEntries) {
        if ([entry.id isEqualToString:entryId]) {
            return entry;
        }
    }
    return nil;
}

- (FileDownloadInfo *)fileDownloadInfoByEntryId:(NSString *)entryId {
    for (FileDownloadInfo *info in [self.fileDownloadInfoDict allValues]) {
        if ([info.entryId isEqualToString:entryId]) {
            return info;
        }
    }
    return nil;
}

- (NSArray<PVPCuePoint *> *)localEntryCuepointsByEntryId:(NSString *)entryId {
    NSArray *filteredCuepoints = [self.localCuepoints filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(PVPCuePoint * _Nullable cuepoint, NSDictionary<NSString *,id> * _Nullable bindings) {
        return cuepoint && [cuepoint isKindOfClass:[PVPCuePoint class]] && [cuepoint.entryId isEqualToString:entryId];
    }]];
    
    return filteredCuepoints ? filteredCuepoints : @[];
}

- (void)checkForFailedDownloads {
    NSMutableArray *entriesToRemove = [NSMutableArray arrayWithCapacity:self.inProgressEntries.count + 1];
    for (PVPEntry *entry in self.inProgressEntries) {
        FileDownloadInfo *info = [self fileDownloadInfoByEntryId:entry.id];
        if (!info) {
//            [self.inProgressEntries removeObject:entry];
            [entriesToRemove addObject:entry];
            [self.failedEntries addObject:entry];
        }
    }
    [self.inProgressEntries removeObjectsInArray:entriesToRemove];
    
    [self saveObjectsFromArray:self.inProgressEntries toFile:IN_PROGRESS_ENTRIES_FILE_NAME];
    [self saveObjectsFromArray:self.failedEntries toFile:FAILED_ENTRIES_FILE_NAME];
}

#pragma mark NSURLSessionDownloadDelegate methods

//download progress
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    FileDownloadInfo *info = [self fileDownloadInfoByTaskIdentifier:downloadTask.taskIdentifier];
    if (info) {
        if (!info.task) {
            info.task = downloadTask;
        }
        if (totalBytesExpectedToWrite != NSURLSessionTransferSizeUnknown) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
                info.progress = (CGFloat)totalBytesWritten / (CGFloat)totalBytesExpectedToWrite;
                [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskProgressNotification object:info];
                NSLog(@"progress: %f", info.progress);
            }];
        }
    } else {
        NSLog(@"cannot find task with identifier: %lul", (unsigned long)downloadTask.taskIdentifier);
    }
}

//download completion
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSError *error = nil;
    FileDownloadInfo *info = [self fileDownloadInfoByTaskIdentifier:downloadTask.taskIdentifier];
    PVPEntry *entry = [self inProgressEntryByEntryId:info.entryId];
    NSString *entryDestinationPath = [[self.docDirectoryPath stringByAppendingPathComponent:ENTRIES_FILE_DIRECTORY] stringByAppendingPathComponent:info.filename];
    NSURL *entryDestinationUrl = [NSURL fileURLWithPath:entryDestinationPath]; //parse the destination path for the file

    if ([self.fileManager fileExistsAtPath:entryDestinationPath]) { //if the file already exists, we will overwrite it
        [self.fileManager removeItemAtPath:entryDestinationPath error:&error];
    }
    BOOL success = [self.fileManager copyItemAtURL:location toURL:entryDestinationUrl error:&error]; //copy downloaded file into the final path

    info.taskIdentifier = PVP_DOWNLOAD_TASK_ID_NONE;
    info.isComplete = YES;
    info.isInProgress = NO;
    [self.fileDownloadInfoDict removeObjectForKey:@(downloadTask.taskIdentifier)]; //discard finished download data
    [self saveInProgressFileInfo];
    
    [self.inProgressEntries removeObject:entry];
    [self saveObjectsFromArray:self.inProgressEntries toFile:IN_PROGRESS_ENTRIES_FILE_NAME];
    
    if (!success || error) { //file save failed
        [self URLSession:session task:downloadTask didCompleteWithError:error];
    } else { //file save success
        if (entry) {
            [self.localEntries addObject:entry];
            [self saveObjectsFromArray:self.localEntries toFile:DOWNLOADED_ENTRIES_FILE_NAME];
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
            [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskFinishedNotification object:info];
        }];
        NSLog(@"finished saving file: %@.", info.filename);
    }
}

//download stop
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    FileDownloadInfo *info = [self fileDownloadInfoByTaskIdentifier:task.taskIdentifier];
    if (error) {
        NSLog(@"did not save file: %@. Reason: %@", info.filename, error.localizedDescription);
    }
    info.isInProgress = NO;
    
    PVPEntry *inProgressEntry = [self inProgressEntryByEntryId:info.entryId];
    PVPEntry *localEntry = [self localEntryByEntryId:info.entryId];
    if (inProgressEntry) {
        NSLog(@"Removing failed download from in progress entries...");
        [self.inProgressEntries removeObject:inProgressEntry];
        [self saveObjectsFromArray:self.inProgressEntries toFile:IN_PROGRESS_ENTRIES_FILE_NAME];
    }
    
    [self.fileDownloadInfoDict removeObjectForKey:@(task.taskIdentifier)]; //discard finished download data
    [self saveInProgressFileInfo];
    
    if (localEntry) {
        NSLog(@"Removing failed download from local entries...");
        [self.localEntries removeObject:localEntry];
        [self saveObjectsFromArray:self.localEntries toFile:DOWNLOADED_ENTRIES_FILE_NAME];
    }
    
    if (error) {
//        if ([error.userInfo objectForKey:NSURLSessionDownloadTaskResumeData]) {
//            NSLog(@"attempting to resume...");
//            info.taskResumeData = [error.userInfo objectForKey:NSURLSessionDownloadTaskResumeData];
//            [self startOrResumeDownload:info];
//        } else
        if (inProgressEntry) {
            NSLog(@"download failed");
            [self.failedEntries addObject:inProgressEntry];
            [self saveObjectsFromArray:self.failedEntries toFile:FAILED_ENTRIES_FILE_NAME];
        } else if (localEntry) {
            [self.failedEntries addObject:localEntry];
            [self saveObjectsFromArray:self.failedEntries toFile:FAILED_ENTRIES_FILE_NAME];
        }
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{ //posting NSNotifications must be done on the main thread
        [[NSNotificationCenter defaultCenter] postNotificationName:FileDownloadManagerTaskFailedNotification object:info];
    }];
}

@end
