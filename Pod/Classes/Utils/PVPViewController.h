//
//  PVPViewController.h
//  PVPClient
//
//  Created by Oren Kosto,  on 8/27/15.
//  Copyright (c) 2015 Panda-OS. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  The PVPViewController class is the base ViewController used by the PVP client.
 */
@interface PVPViewController : UIViewController

@property (nonatomic, strong) NSString *trackedViewName;

@end
