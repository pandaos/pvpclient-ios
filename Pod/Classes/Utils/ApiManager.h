//
//  ApiManager.h
//  PVPClient
//
//  Created by Oren Kosto,  on 6/23/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Overcoat/Overcoat.h>
#import "ObjectClasses.h"
#import "PVPDataResponse.h"
#import "PVPConfigResponse.h"
#import "PVPResultResponse.h"
#import "AppUtils.h"

/**
 *  The ApiManager provides an interface for performing HTTP requests (GET, POST, PUT DELETE) and handling the responses.
 *  This class is accessed by the different model classes in order to use different services of the platform.
 *
 *  @warning In order to ensure correct library hierarchy, and that the API calls are sent and handled correctly, only the library should access and manage this class throughout the application's life cycle. This class should never be accessed directly by the developer using this library.
 */
@interface ApiManager : OVCHTTPSessionManager

/**
 *  @name Public Properties
 */

/**
 *  The RestKit object manager, in charge of sending the HTTP requests.
 */
//@property (strong, nonatomic) RKObjectManager *objectManager;

/**
 *  The current logged in user.
 */
@property (strong, nonatomic, setter=setLoggedInUser:) PVPUser * _Nullable loggedInUser;

@property (strong, nonatomic, setter=setLoggedInUserInstances:) NSArray<PVPInstance *> * _Nullable instances;

/**
 *  @name Public Methods
 */

/**
 *  Init and return singleton instance.
 *
 *  @return ApiManager singleton instance
 */
+(nonnull id) sharedInstance;
+ ( void ) InitSharedInstance;
/**
 *  Init and return singleton instance.
 *
 *  @return ApiManager singleton instance
 */
+(nonnull id) cdnInstance;

/**
 *  Setup ApiManager with default server prefix and settings from PVPSettings.plist
 */
//-(void) setup;

/**
 *  Setup ApiManager with custom server prefix and settings
 *
 *  @param serverPrefix the base URL for the server
 */
//-(void) setup:(NSString *)serverPrefix;

/**
 *  Initiates a GET request for a given path
 *
 *  @param url     the request URL path
 *  @param params  the request URL params
 *  @param success the block to execute if the request succeeds
 *  @param failure the block to execute if the request fails
 */
-(void) getRequest:(nonnull NSString *)url
            params:(nullable NSDictionary *)params
           success:(nullable void(^)( NSArray * _Nonnull result))success
           failure:(nullable void(^)(NSError * _Nonnull e))failure;

/**
 *  Initiates a GET request in which the result needs to include the metadata object (useful for INDEX actions).
 *
 *  @param url     the request URL path
 *  @param params  the request URL params
 *  @param success the block to execute if the request succeeds
 *  @param failure the block to execute if the request fails
 */
-(void) getRequestWithMeta:(nonnull NSString *)url
                    params:(nullable NSDictionary *)params
                   success:(nullable void(^)(NSArray * _Nonnull result))success
                   failure:(nullable void(^)(NSError *_Nonnull e))failure;

/**
 *  Initiates a POST request for a given URL path
 *
 *  @param url     the request URL path
 *  @param object  the request payload (i.e. the object to send)
 *  @param params  the request URL params
 *  @param success the block to execute if the request succeeds
 *  @param failure the block to execute if the request fails
 */
-(void) postRequest:(nonnull NSString *)url
             object:(nullable NSObject *)object
             params:(nullable NSDictionary *)params
            success:(nullable void(^)(NSArray * _Nonnull result))success
            failure:(nullable void(^)(NSError * _Nonnull e, NSString *bambooError))failure;

/**
 *  Initiates a PUT request for a given URL path
 *
 *  @param url     the request URL path
 *  @param object  the request payload (i.e. the object to update)
 *  @param params  the request URL params
 *  @param success the block to execute if the request succeeds
 *  @param failure the block to execute if the request fails
 */
-(void) putRequest:(nonnull NSString *)url
            object:(nullable NSObject *)object
            params:(nullable NSDictionary *)params
           success:(nullable void(^)(NSArray * _Nonnull result))success
           failure:(nullable void(^)(NSError * _Nonnull e))failure;

/**
 *  Initiates a DELETE request for a given URL path
 *
 *  @param url     the request URL path
 *  @param object  the request payload (i.e. the object to delete)
 *  @param params  the request URL params
 *  @param success the block to execute if the request succeeds
 *  @param failure the block to execute if the request fails
 */
-(void) deleteRequest:(nonnull NSString *)url
               object:(nullable NSObject *)object
               params:(nullable NSDictionary *)params
              success:(nullable void(^)(NSArray * _Nonnull result))success
              failure:(nullable void(^)(NSError * _Nonnull e))failure;

/**
 *  Updates the currently used user token to the token saved in the device defaults
 *
 *  @see -updateUserToken:
 */
-(void)updateUserTokenFromDefaults;

/**
 *  Updates the current user token used in the cookie in API calls.
 *  This method will set the current "token" field of the cookie sent in every API call to the given value.
 *
 *  @param token the updated user token in the format: "<tokenFieldName>=<tokenValue>"
 */
-(void)updateUserToken:(nullable NSString *)token;
@end
