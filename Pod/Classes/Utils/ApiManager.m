//
//  ApiManager.m
//  PVPClient
//
//  Created by Oren Kosto,  on 6/23/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import "ApiManager.h"
#import "PVPURLMatcher.h"

#if TARGET_IPHONE_SIMULATOR
NSString * const isSimulator = @"YES";
#else
NSString * const isSimulator = @"NO";
#endif

#define LOGGED_IN_USER_KEY @"loggedInUser"
#define LOGGED_IN_USER_INSTANCES_KEY @"loggedInUserInstances"

@implementation ApiManager

static ApiManager *sharedApiManager = nil;
static ApiManager *sharedCDNApiManager = nil;

static dispatch_once_t onceToken;
static dispatch_once_t onceTokenCDN;

-(instancetype)init {
    NSString *serverPrefix = [NSString stringWithFormat:@"%@%@", [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"], [PVPConfigHelper pvpServerPrefix]];
    NSURL *baseURL = [NSURL URLWithString:serverPrefix];

    self = [super initWithBaseURL:baseURL];
    if (self) {
        [self setup];
    }

    return self;
}

- ( instancetype ) initWithCDN {
    NSString *serverPrefix = [NSString stringWithFormat:@"%@%@", [PVPConfigHelper getLocalConfiguration:@"pvpDomainCDN"], [PVPConfigHelper pvpServerPrefix]];
    NSURL *baseURL = [NSURL URLWithString:serverPrefix];

    self = [super initWithBaseURL:baseURL];
    if ( self ) {
        [self setup];
    }

    return self;
}

+ ( void ) InitSharedInstance{
    @synchronized( self ) {
        sharedApiManager = nil;
        sharedCDNApiManager = nil;
        onceToken = 0;
    }
}

+ ( ApiManager * ) sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedApiManager = [[self alloc] init];
    });
    
    return sharedApiManager;
}

+ ( ApiManager * ) cdnInstance {
    dispatch_once(&onceTokenCDN, ^{
        sharedCDNApiManager = [[self alloc] initWithCDN];
    });
    return sharedCDNApiManager;
}

- ( void ) setup {
    NSDictionary *loggedInUserDict = [NSDictionary dictionaryFromJSONString:[AppUtils getDefaultValueFofKey:LOGGED_IN_USER_KEY]];
    NSError *e;

    if (loggedInUserDict && [loggedInUserDict isKindOfClass:[NSDictionary class]]) {
        PVPUser *loggedInUserObj = [PVPJSONAdapter modelOfClass:[PVPUser class] fromJSONDictionary:loggedInUserDict error:&e];
        if (loggedInUserObj && !e) {
            [self setLoggedInUser:loggedInUserObj];
        }
    }
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];

    [self updateUserTokenFromDefaults];
}

- (void)setLoggedInUserInstances:(NSArray<PVPInstance *> *)instances {
    _instances = instances;
}

- (void)setLoggedInUser:(PVPUser *)loggedInUser {
    NSError *e;
    NSDictionary *loggedInUserDict = loggedInUser ? [PVPJSONAdapter JSONDictionaryFromModel:loggedInUser error:&e] : nil;

    if (loggedInUserDict && !e) {
        [AppUtils saveDefaultValue:[NSString JSONStringFromDictionary:loggedInUserDict] forKey:LOGGED_IN_USER_KEY];
    } else {
        [AppUtils deleteDefaultValueWithKey:LOGGED_IN_USER_KEY];
    }

    _loggedInUser = loggedInUser;
}

-(void) getRequest:(NSString *)url params:(NSDictionary *)params success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e))failure {
    NSString *modifiedURL = [NSString stringWithFormat:@"%@%@", url, [PVPConfigHelper pvpServerSuffix]];
    NSLog(@"%@ --- URL",modifiedURL);
    NSLog(@"%@ --- PARAMS",params);
    [self GET:modifiedURL
   parameters:params
   completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
       if (!error) {
           [self updateUserTokenFromResponseHeaders:response.HTTPResponse];
           [self updateServerParamsFromResponseHeaders:response.HTTPResponse];
           NSArray *res;
           if (response && response.result) {
               res = [response.result isKindOfClass:[NSArray class]] ? response.result : @[response.result];
           } else {
               res = @[];
           }
           success(res);
       } else {
           failure(error);
       }
   }];
}

-(void) getRequestWithMeta:(NSString *)url params:(NSDictionary *)params success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e))failure {
    [self GET:[NSString stringWithFormat:@"%@%@", url, [PVPConfigHelper pvpServerSuffix]]
   parameters:params
   completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
       if (!error) {
           [self updateUserTokenFromResponseHeaders:response.HTTPResponse];
           [self updateServerParamsFromResponseHeaders:response.HTTPResponse];
           
           NSArray *res;
           if (response && response.result) {
               res = [self responseWithMetadata:response.rawResult withResultArray:[response.result isKindOfClass:[NSArray class]] ? response.result : @[response.result]];
           } else {
               res = @[];
           }
           success(res);
       } else {
           failure(error);
       }
   }];
}

-(void) postRequest:(NSString *)url object:(NSObject *)object params:(NSDictionary *)params success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e, NSString *bambooError))failure {
    [self POST:[NSString stringWithFormat:@"%@%@", url, [PVPConfigHelper pvpServerSuffix]]
    parameters:[self JSONFromObject:object withParams:params]
    completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
//                          NSLog(@"RETURNED RESPONSE: %@", operation.HTTPRequestOperation.responseString);
        if (!error) {
            [self updateUserTokenFromResponseHeaders:response.HTTPResponse];
            [self updateServerParamsFromResponseHeaders:response.HTTPResponse];
            
            NSArray *res;
            if (response && response.result) {
                res = [self responseWithMetadata:response.rawResult withResultArray:[response.result isKindOfClass:[NSArray class]] ? response.result : @[response.result]];
            } else {
                res = @[];
            }
            success(res);
        } else {
            NSString *bambooError = response.HTTPResponse.allHeaderFields[@"x-message"];
            failure(error, bambooError);
        }
    }];
}

-(void) putRequest:(NSString *)url object:(NSObject *)object params:(NSDictionary *)params success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e))failure {
    [self PUT:[NSString stringWithFormat:@"%@%@", url, [PVPConfigHelper pvpServerSuffix]]
    parameters:[self JSONFromObject:object withParams:params]
    completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
        //                          NSLog(@"RETURNED RESPONSE: %@", operation.HTTPRequestOperation.responseString);
        if (!error) {
            [self updateUserTokenFromResponseHeaders:response.HTTPResponse];
            [self updateServerParamsFromResponseHeaders:response.HTTPResponse];
            
            NSArray *res;
            if (response && response.result) {
                res = [self responseWithMetadata:response.rawResult withResultArray:[response.result isKindOfClass:[NSArray class]] ? response.result : @[response.result]];
            } else {
                res = @[];
            }
            success(res);
        } else {
            failure(error);
        }
    }];
}

-(void) deleteRequest:(NSString *)url object:(NSObject *)object params:(NSDictionary *)params success:(void(^)(NSArray *result))success failure:(void(^)(NSError *e))failure {
    [self DELETE:[NSString stringWithFormat:@"%@%@", url, [PVPConfigHelper pvpServerSuffix]]
    parameters:[self JSONFromObject:object withParams:params]
    completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
        //                          NSLog(@"RETURNED RESPONSE: %@", operation.HTTPRequestOperation.responseString);
        if (!error) {
            [self updateUserTokenFromResponseHeaders:response.HTTPResponse];
            [self updateServerParamsFromResponseHeaders:response.HTTPResponse];
            
            NSArray *res;
            if (response && response.result) {
                res = [self responseWithMetadata:response.rawResult withResultArray:[response.result isKindOfClass:[NSArray class]] ? response.result : @[response.result]];
            } else {
                res = @[];
            }
            success(res);
        } else {
            failure(error);
        }
    }];
}

/**
 *  @name Private Methods
 */

+ ( NSDictionary<NSString *,id> * ) modelClassesByResourcePath {
    return @{@"config": [PVPConfig class],
            @"token": [PVPLogin class],
            @"facebookuser": [PVPLogin class],
            @"twitteruser": [PVPLogin class],
            @"user": [PVPUser class],
            @"user/*/instances": [PVPInstance class],
            @"user/*/purchasetree": [PVPCategory class],
            @"user/*/purchase": [PVPSubscription class],
            @"user/*/purchase/*": [PVPResult class],
            @"user/*/block": [PVPResult class],
            @"user/*/historytree": [PVPCategory class],
            @"user/*/history": [PVPWatchHistoryItem class],
            @"user/**": [PVPUser class],
            @"entry": [PVPEntry class],
            @"entry/*/flavors": [PVPFlavorAsset class],
            @"entry/**": [PVPEntry class],
            @"live": [PVPEntry class],
            @"live/**": [PVPEntry class],
            @"categorytree/**": [PVPCategory class],
            @"categorytree": [PVPCategory class],
            @"category/*/entries": [PVPEntry class],
            @"category/*/entries/*": [PVPEntry class],
            @"category/*/entries/**": [PVPEntry class],
            @"category": [PVPCategory class],
            @"category/**": [PVPCategory class],
            @"livepublish": [PVPLiveEntryPublish class],
            @"entry/*/viewers": [PVPResult class],
            @"livepublish/*": [PVPResult class],
            @"upload-token": [PVPUploadToken class],
            @"conversation": [PVPConversation class],
            @"event": [PVPEvent class],
            @"pushnotifications": [PVPPushNotificationRequestObject class],
            @"message": [PVPMessage class],
            @"opentok": [PVPOpenTok class],
            @"cuepoint": [PVPCuePoint class],
            @"channels/livechannel": [PVPChannelSchedule class],
            @"channels/livechannel/*": [PVPChannelSchedule class],
            @"channels/**": [PVPChannel class],
            @"package": [PVPPackage class],
            @"package/**": [PVPPackage class],
            @"node-category": [PVPNodeCategory class],
            @"node-category/**": [PVPNodeCategory class],
            @"node": [PVPNode class],
            @"node/**": [PVPNode class]
    };
}

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration {
    if (self = [super initWithBaseURL:url sessionConfiguration:configuration]) {
        self.responseSerializer =
        [OVCModelResponseSerializer
         serializerWithURLMatcher:[PVPURLMatcher matcherWithBasePath:self.baseURL.path
                                                  modelClassesByPath:[[self class] modelClassesByResourcePath]]
         responseClassURLMatcher:[PVPURLMatcher matcherWithBasePath:self.baseURL.path
                                                 modelClassesByPath:[[self class] responseClassesByResourcePath]]
         errorModelClassURLMatcher:[PVPURLMatcher matcherWithBasePath:self.baseURL.path
                                                   modelClassesByPath:[[self class] errorModelClassesByResourcePath]]];
    }
    return self;
}

+ ( NSDictionary<NSString *,id> * ) responseClassesByResourcePath {
    return @{@"config": [PVPConfigResponse class],
             @"**": [PVPDataResponse class]};
}

/**
 *  Generates and returns a JSON combined from a given object and params
 *
 *  @param object The object to parse into JSON.
 *  @param params The params to add to the object.
 *
 *  @return A NSDictionary object combined from the given object and params.
 */
- ( NSDictionary * ) JSONFromObject: ( NSObject * ) object withParams: ( NSDictionary * ) params {
    NSError *e;
    NSMutableDictionary *dict;
    @try {
        if (object) {
            dict = [[PVPJSONAdapter JSONDictionaryFromModel:(id<MTLJSONSerializing>) object error:&e] mutableCopy]; //try to parse the object into a JSON dictionary
        }
    } @catch (NSException *exception) {
        
    } @finally {
        if (e || !dict) { //parsing failed, use an empty dictionary instead
            dict = [NSMutableDictionary dictionaryWithCapacity:5];
        }
        if (params) { //additional params were sent
            [dict addEntriesFromDictionary:params];
        }
        return dict;
    }
}

/**
 *  Finds and adds the "meta" object to the response if it exists
 *
 *  @param responseDict the response in daw data format
 *  @param responseArray  the responsein array format
 *
 *  @return the response, with the "meta" object
 */
- ( NSArray * ) responseWithMetadata: ( NSDictionary * ) responseDict withResultArray: ( NSArray * ) responseArray {
    if (responseDict && [responseDict isKindOfClass:[NSDictionary class]] && [responseDict allKeys].count && responseDict[@"meta"] && responseDict[@"meta"][@"total"]) { //this is an entry
        NSMutableArray *responseMutableArray = [[NSMutableArray alloc] initWithArray:responseArray];
        int total = [responseDict[@"meta"][@"total"] intValue];
        NSString *prevPageToken = responseDict[@"meta"][@"prevPageToken"];
        NSString *nextPageToken = responseDict[@"meta"][@"nextPageToken"];
        [responseMutableArray addObject:[[PVPMeta alloc] initWithTotal:total withPrevPageToken:prevPageToken withNextPageToken:nextPageToken]];
        responseArray = [responseMutableArray copy];
    }
    return responseArray;
}

/**
 *  Builds a response object in case the response is not REST compliant
 *
 *  @param responseData the response in raw data format
 *
 *  @return the parsed response in array format
 */
- ( NSArray * ) resultFromJsonResponseData: ( NSData * ) responseData {
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&e];
    if ([dict isKindOfClass:[NSArray class]]) {
        return @[];
    }
    if ([dict objectForKey:@"data"]) { //this response has data in it
        if ([[dict objectForKey:@"data"] isKindOfClass:[NSArray class]]) { //the data is an entry
            return [dict objectForKey:@"data"];
        }
        return @[[dict objectForKey:@"data"]];
    }
    return @[]; //return an empty response
}

/**
 *  Updates the currently used user token to the token received in the response headers
 *
 *  @param response the response from the server
 *
 *  @see -updateUserToken:
 */
- ( void ) updateUserTokenFromResponseHeaders: ( NSHTTPURLResponse * ) response {
    NSDictionary *responseHeaders = response.allHeaderFields;
    NSString *cookieName = [PVPConfigHelper tokenField];
    if (responseHeaders && responseHeaders[@"Set-Cookie"] && [responseHeaders[@"Set-Cookie"] isKindOfClass:[NSString class]]) {
        NSString *setCookieString = responseHeaders[@"Set-Cookie"];
        NSArray *setCookieComponents = [setCookieString componentsSeparatedByString:@"; "];
        for (NSString *component in setCookieComponents) {
            if ([component hasPrefix:cookieName]) {
                [self updateUserToken:[component stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                break;
            }
        }
    }
}

- ( void ) updateUserTokenFromDefaults {
    NSString *cookieName = [NSString stringWithFormat:@"%@=", [PVPConfigHelper tokenField]];
    NSString *token = [AppUtils getDefaultValueFofKey:USER_TOKEN_KEY];
    
    [self updateUserToken:token ? [NSString stringWithFormat:@"%@%@", [token hasPrefix:cookieName] ? @"" : cookieName, token] : nil]; // get currently stored token
}

- ( void ) updateUserToken: ( NSString * ) token {
    NSString *cookieName = [NSString stringWithFormat:@"%@=", [PVPConfigHelper tokenField]];
    BOOL debugEnabled = [PVPConfigHelper debugEnabled];

    if (token) {
        [AppUtils saveDefaultValue:token forKey:USER_TOKEN_KEY];
        
        if (token.length > 0 && ![token hasPrefix:cookieName]) {
            token = [NSString stringWithFormat:@"%@%@", cookieName, token];
        }
    } else {
        token = @"";
    }

    NSString *tokenCookieString = [NSString stringWithFormat:@"%@%@", (debugEnabled ? @"XDEBUG_SESSION=PHPSTORM;" : @""), token];
    [self.requestSerializer setValue:tokenCookieString forHTTPHeaderField:@"Cookie"];
}

- ( void ) updateServerParamsFromResponseHeaders: ( NSHTTPURLResponse * ) response {

    NSString *urlToCheck = [NSString stringWithString:response.URL.host];
    BOOL isCDNrequest = [[PVPConfigHelper getLocalConfiguration:@"pvpDomainCDN"] containsString:urlToCheck];

    if ( !isCDNrequest ) {

        NSDictionary *responseHeaders = response.allHeaderFields;

        if (responseHeaders && responseHeaders[@"Date"] && [responseHeaders[@"Date"] isKindOfClass:[NSString class]]) {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formatter setLocale:locale];
            [formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
            
            NSString *dateString = responseHeaders[@"Date"];
            NSDate *date = [formatter dateFromString:dateString];
            NSDate *nowDate = [NSDate date];
            CGFloat offset = [date timeIntervalSince1970] - [nowDate timeIntervalSince1970];
            [PVPConfigHelper setLocalConfiguration:[NSString stringWithFormat:@"%f", offset] forKey:@"serverTimeOffset"];
            [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithName:[[PVPConfigHelper sharedInstance] timezoneString]]];
        }

        NSString *country = @"";

        if (responseHeaders && responseHeaders[@"X-Bamboo-Country"] && [responseHeaders[@"X-Bamboo-Country"] isKindOfClass:[NSString class]]) {
            country = responseHeaders[@"X-Bamboo-Country"];
        }

        [PVPConfigHelper setLocalConfiguration:country forKey:@"X-Bamboo-Country"];
    }
}

@end
