//
//  ViewControllerSwitch.h
//  PVPClient
//
//  Created by Oren Kosto,  on 7/16/14.
//  Copyright (c) 2014 Panda-OS. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The ViewControllerSwitch is a tool for making transitions between the current root VC to a different one.
 *  This utility is useful when switching between screens that support different orientations, or between screens with different navigation methods.
 *  Keep in mind that this completely removes and releases the current root VC, so be sure to save the data you'll need next time you load it.
 */
@interface ViewControllerSwitch : NSObject

/**
 *  Loads a new VC and releases the current root VC, with the default transition animation.
 *
 *  @param VControllerToLoad the VC to be loaded.
 *
 *  @see -loadController:andRelease:withOptions:(UIViewAnimationOptions)options
 */
+(void)loadController:(UIViewController *)VControllerToLoad;

/**
 *  Loads a new VC and releases the provided VC, with the default transition animation.
 *
 *  @param VControllerToLoad    the VC to be loaded.
 *  @param VControllerToRelease the VC to be released.
 *
 *  @see -loadController:andRelease:withOptions:(UIViewAnimationOptions)options
 */
+(void)loadController:(UIViewController*)VControllerToLoad andRelease:(UIViewController*)VControllerToRelease;

/**
 *  Loads a new VC and releases the current root VC, with a custom transition animation.
 *
 *  @param VControllerToLoad the VC to be loaded.
 *  @param options           the transition animation options to be used for the transition.
 *
 *  @see -loadController:andRelease:withOptions:(UIViewAnimationOptions)options
 */
+(void)loadController:(UIViewController *)VControllerToLoad withOptions:(UIViewAnimationOptions)options;

/**
 *  Loads a new VC and releases the provided VC, with a custom transition animation.
 *
 *  @param VControllerToLoad    the VC to be loaded.
 *  @param VControllerToRelease the VC to be released.
 *  @param options              the transition animation options to be used for the transition.
 */
+(void)loadController:(UIViewController *)VControllerToLoad andRelease:(UIViewController *)VControllerToRelease withOptions:(UIViewAnimationOptions)options;

@end
