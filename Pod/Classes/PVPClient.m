//
//  PVPClient.m
//  PVPClient
//
//  Created by Oren Kosto,  on 11/19/15.
//
//

#import "PVPClient.h"

@implementation PVPClient

static BOOL isSetup = NO;
static PVPClient *sharedInstance = nil;
static dispatch_once_t onceToken;
static int retries = 0;

+ ( void ) InitSharedInstance {
    @synchronized( self ) {
        sharedInstance = nil;
        onceToken = 0;
    }
}

+ ( instancetype ) sharedInstance {
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if ( !isSetup ) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

- ( void ) setup {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    self.locationManager.distanceFilter = 3000;
    self.locationManager.delegate = self;
}

- ( void ) start {
    configModel = nil;
    loginModel = nil;

    [ApiManager sharedInstance];
    configModel = [[PVPConfigModel alloc] init];
    configModel.delegate = self;
    loginModel = [[PVPLoginModel alloc] init];
    loginModel.delegate = self;
    if ([PVPConfigHelper monitorReachability]) {
        [[ReachabilityHelper sharedInstance] start];
    }
    
    [FileDownloadManager sharedInstance];
    
    retries = 0;
    
    // Config model is also called in BambooLoadingViewController in iOS Version hence not needed to start here.
#if TARGET_OS_TV
    [configModel getInitialServerConfig];
#endif
    NSLog(@"PVPClient starting. Getting initial server config from %@...", [PVPConfigHelper getLocalConfiguration:@"pvpServerDomain"]);
    
#if TARGET_OS_IOS
    if (![[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications]) {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    }
#endif
}

- (void)startWithServerDomain:(NSString *)serverDomain {
    [PVPConfigHelper setLocalConfiguration:serverDomain forKey:@"pvpServerDomain"];
    [self start];
}

- (NSString *)userToken {
    NSString *token = [AppUtils getDefaultValueFofKey:USER_TOKEN_KEY];
    return token;
}

- (BOOL) isLoggedIn {
    NSString *token = [AppUtils getDefaultValueFofKey:USER_TOKEN_KEY];
    PVPUser *currentUser = [[ApiManager sharedInstance] loggedInUser];
    return currentUser != nil && token != nil && token.length > 0 && [AppUtils getDefaultValueFofKey:LOGGED_IN_KEY].boolValue;
}

#pragma mark config model delegate methods
- (void)initialConfigRequestSuccess {
    NSLog(@"PVPClient Ready");
    
#if TARGET_OS_IOS
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
#endif
}

- ( void ) configRequestFail {
    if ( retries == 3 ) {
        if ( [PVPConfigHelper alertsEnabled] ) {
            [PVPAlertHelper showAlertWithTitle:@"Connection Error"
                                    andMessage:@"There was a problem reaching our servers. The app might not function as expected. Please try again."
                         withCancelButtonTitle:ALERT_CONTINUE_ANYWAY
                          withOtherButtonTitle:ALERT_TRY_AGAIN
                                   withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
                                       if (confirmed) {
                                           [self start];
                                       }
                                   }];
        }
    }
    else {
        retries++;
        [configModel getInitialServerConfig];
        NSLog(@"Initial config request failed. Retrying... %d", retries);
    }
}

#pragma mark location manager delegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [manager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:locations[0] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        NSLog(@"GPS country is: %@", placemarks[0].ISOcountryCode);
        [AppUtils saveDefaultValue:[@(locations[0].coordinate.latitude) stringValue] forKey:@"latitude"];
        [AppUtils saveDefaultValue:[@(locations[0].coordinate.longitude) stringValue] forKey:@"longitude"];
        [AppUtils saveDefaultValue:placemarks[0].ISOcountryCode forKey:@"currentCountry"];
    }];
}

#pragma mark login model delegate methods
- (void)loginRequestSuccess{}
- (void)loginRequestFail{}

@end
