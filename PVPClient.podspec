Pod::Spec.new do |s|
  s.name             = 'PVPClient'
  s.version          = '1.6.9'
  s.summary          = 'The PVP Client library for iOS.'

  s.description      = 'This is the iOS client for the Panda-OS Bamboo Video Platform. With this library, any app can communicate with the PVP Server.'

  s.homepage         = 'https://bitbucket.org/pandaos/pvpclient-ios'
  s.authors = { 'Roni Cohen' => 'roni@panda-os.com' }
  s.source           = { :git => 'https://bitbucket.org/pandaos/pvpclient-ios.git', :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.ios.deployment_target = '11.0'
  s.tvos.deployment_target = '13.0'

  non_arc_files = 'Pod/Classes/Vendor/KalturaClient/**/*', 'Pod/Classes/Vendor/ASIHTTPRequest/**/*'

  s.source_files = 'Pod/Classes/**/*'
  s.exclude_files = non_arc_files
  s.requires_arc = true

  s.subspec 'no-arc' do |sp|
    sp.requires_arc = false
    sp.source_files = non_arc_files
  end

  s.frameworks = 'UIKit', 'SystemConfiguration', 'MobileCoreServices'
  s.dependency 'RNCryptor-objc'
  s.dependency 'AFNetworking', '~> 4.0.1'
  s.ios.dependency 'Firebase'
  s.ios.dependency 'Firebase/Core'
  s.ios.dependency 'Firebase/Analytics'
  s.tvos.dependency 'FirebaseCore'

  s.library = 'z', 'xml2', 'stdc++'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
  s.xcconfig = { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}

end
