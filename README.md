# PVPClient

[![CI Status](https://img.shields.io/badge/build-stable-brightgreen.svg?style=flat)](https://bitbucket.org/pandaos/pvpclient-ios)
[![Version](https://img.shields.io/badge/pod-v1.6.9-blue.svg?style=flat)](https://bitbucket.org/pandaos/pvpclient-ios)
[![License](https://img.shields.io/badge/license-all%20rights%20reserved-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/pvpclient-ios)
[![Language](https://img.shields.io/badge/language-objective%20c-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/pvpclient-ios)
[![Platform](https://img.shields.io/badge/platform-ios%20%7C%20tvos-lightgrey.svg?style=flat)](https://bitbucket.org/pandaos/pvpclient-ios)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
The PVPClient library has been fully tested on devices running iOS 8.0, and tvOS 9.0 and up.

## Installation

PVPClient is available through the [Panda-OS Podspec Repository](https://bitbucket.org/pandaos/panda-podspec-repo). To install
it, simply add both the Cocoapods and the Panda-OS podspec repositories to the sources in your Podfile:

````
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/pandaos/panda-podspec-repo'
````

And then add the following line to your Podfile to include the library:

````
pod "PVPClient"
````

And run `pod install` from the terminal, from the main project directory to install the library.

Use one of the activation commands to start the PVPClient:

````
[[PVPClient sharedInstance] start]; //to use the default Bamboo domain.
[[PVPClient sharedInstance] startWithServerDomain:<bamboo-server-domain>]; //to use the a custom Bamboo domain.
````

## Author

Oren Kosto, , vladimir@panda-os.com

## License

PVPClient is available under copyright (c) 2017 Panda-OS. All rights reserved.
See the LICENSE file for more info.

## What's New
* 1.6.9:
    * Silenced some warnings.
* 1.6.8:
    * Fixes some bugs and warnings.
    * Added support for GPS-based location in addition to server based location.
* 1.6.7:
    * RTL Support
* 1.6.6:
    * Custom player logo support.
    * Location-based blocking.
    * Beacon support.
    * Data encryption.
    * Restore purchases.
    * Fairplay DRM.
    * Translation and RTL support, including language switching at runtime.
* 1.6.5:
    * Warnings and documentation.
* 1.6.4:
    * Added channel GET method to the PVPChannelModel.
* 1.6.3:
    * Added sample code.
* 1.6.2:
    * Bug fixes.
* 1.6.1:
    * All PVP objects are now self-mapping.
    * Added Live Channel and EPG schedule objects + functionality.
    * Added Watch History objects + functionality.
    * Bug fixes.
* 1.6.0:
    * Fabric Support.
    * Integrated Fabric and Crashlytics tracking.
    * Improved user session persistence.
    * PVPEntry thumbnails can now be retrieved using the new Bamboo or Kaltura CDN.
    * Added PVPCategory and category tree support.
    * Bug fixes, performance improvements, misc. convenience methods.
* 1.5.2:
    * Bug fixes.
    * Added support for background color from the config.
* 1.5.1:
    * Bug fixes.
    * Added support for Bamboo EPG channels.
    * Added some convenience methods for generating entry thumbnail URLs using the PVP and Kaltura APIs.
    * Added support for mobile-related player configuration.
    * Added convenience a method to safely retrieve a NSDictionary value with a key-path. 
* 1.5.0:
    * Bug fixes.
    * PVPSettings.plist is now optional.
    * Added support for initializing the PVPClient with a custom Bamboo server domain at runtime.
    * Added support for changing the local app configuration at runtime.
    * Added support for generating colors from hex strings, and handling colors from the server config.
* 1.4.0:
    * Fixed user-session related bugs.
    * Made some of the fields in the PVPSetings.plist to be optional, with default values as fallbacks. See PVPSettings.plist in the example project to see what has been removed.
    * Added support for mobile-related configuration from the server, and an indication of whether a login is required to use the hosting app.
* 1.3.11:
    * Cuepoints (chapters) support.
    * Added support for generating custom entry thumbnails from the PVP API.
    * Better filter/pager generation and other minor tweaks.
* 1.3.10:
    * Added functions to generate download URLs and adaptive HLS playback URLs from PVPEntry objects.
* 1.3.9: 
    * Added "Contact Us" model, allowing you to send "contact us" forms. 
* 1.3.8:
    * Support for alternative entry backends (i.e. YouTube).
    * Fixed bugs with setting session tokens.
