//
//  EntriesViewController.m
//  PVPClient
//
//  Created by Oren Kosto on 3/26/17.
//  Copyright © 2017 orenk86. All rights reserved.
//

#import "EntriesViewController.h"

@interface EntriesViewController ()

@property (strong, nonatomic) PVPEntryModel *entryModel;
@property (strong, nonatomic) NSMutableArray<PVPEntry *> *entriesArray;

@end

@implementation EntriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.entryModel = [[PVPEntryModel alloc] init];
    self.entryModel.delegate = self;
    
    self.entriesArray = [NSMutableArray arrayWithCapacity:10];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.progressIndicator.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)getList:(id)sender {
    [self.view endEditing:YES];
    [self.entryModel list];
    [self.progressIndicator fadeIn];
}

- (IBAction)getOne:(id)sender {
    [self.view endEditing:YES];
    [self.entryModel get:self.entryIdTextView.text];
    [self.progressIndicator fadeIn];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", self.entriesArray[indexPath.row].name, self.entriesArray[indexPath.row].id];
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.entriesArray.count;
}

-(void)entryRequestSuccessWithEntriesArray:(NSArray *)entriesArray {
    [self.entriesArray removeAllObjects];
    [self.entriesArray addObjectsFromArray:entriesArray];
    
    [self.tableView reloadData];
    [self.progressIndicator fadeOut];
}

-(void)entryRequestSuccessWithEntry:(PVPEntry *)entry {
    [self entryRequestSuccessWithEntriesArray:@[entry]];
}

-(void)entryRequestFail {
    [self entryRequestSuccessWithEntriesArray:@[]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
