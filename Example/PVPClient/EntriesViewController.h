//
//  EntriesViewController.h
//  PVPClient
//
//  Created by Oren Kosto on 3/26/17.
//  Copyright © 2017 orenk86. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PVPClient.h>
#import <UIView+PVPClient.h>

@interface EntriesViewController : UIViewController <PVPEntryModelDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *entryIdTextView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@end
