//
//  LoginViewController.h
//  PVPClient
//
//  Created by Oren Kosto on 3/26/17.
//  Copyright © 2017 orenk86. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PVPClient.h>
#import <UIView+PVPClient.h>

@interface LoginViewController : UIViewController <PVPLoginModelDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextView *sessionTextView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@end
