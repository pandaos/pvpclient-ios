//
//  main.m
//  PVPClient
//
//  Created by orenk86 on 11/16/2015.
//  Copyright (c) 2015 orenk86. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
