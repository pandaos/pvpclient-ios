//
//  PVPAppDelegate.h
//  PVPClient
//
//  Created by orenk86 on 11/16/2015.
//  Copyright (c) 2015 orenk86. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
