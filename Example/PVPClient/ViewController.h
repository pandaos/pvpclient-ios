//
//  PVPViewController.h
//  PVPClient
//
//  Created by orenk86 on 11/16/2015.
//  Copyright (c) 2015 orenk86. All rights reserved.
//

@import UIKit;

#import <PVPClient.h>
#import <UIView+PVPClient.h>

@interface ViewController : UIViewController <PVPConfigModelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (weak, nonatomic) IBOutlet UIView *menuContainer;

@end
