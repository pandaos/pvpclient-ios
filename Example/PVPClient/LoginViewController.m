//
//  LoginViewController.m
//  PVPClient
//
//  Created by Oren Kosto on 3/26/17.
//  Copyright © 2017 orenk86. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property (strong, nonatomic) PVPLoginModel *loginModel;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.loginModel = [[PVPLoginModel alloc] init];
    self.loginModel.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshSessionTextView];
    self.progressIndicator.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)login:(id)sender {
    [self.view endEditing:YES];
    [self.loginModel loginUser:[[PVPLogin alloc] initWithUserId:self.emailTextField.text withPassword:self.passwordTextField.text]];
    [self.progressIndicator fadeIn];
}

- (IBAction)logout:(id)sender {
    [self.view endEditing:YES];
    [self.loginModel removeLoggedInUser];
}

-(void)refreshSessionTextView {
    if ([PVPClient isLoggedIn]) {
        self.sessionTextView.text = [NSString stringWithFormat:@"Hello %@!\nCurrent sesion: %@", [PVPClient getCurrentUser].fullName, [AppUtils getDefaultValueFofKey:USER_TOKEN_KEY]];
    } else {
        self.sessionTextView.text = @"Please log in";
    }
}

-(void)loginRequestSuccess {
    [self refreshSessionTextView];
    [self.progressIndicator fadeOut];
}

-(void)loginRequestFail {
    [self refreshSessionTextView];
    [self.progressIndicator fadeOut];
}

-(void)logoutRequestSuccess {
    [self refreshSessionTextView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
