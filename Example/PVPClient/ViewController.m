//
//  PVPViewController.m
//  PVPClient
//
//  Created by orenk86 on 11/16/2015.
//  Copyright (c) 2015 orenk86. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) PVPConfigModel *configModel;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.menuContainer.hidden = YES;
    self.configModel = [[PVPConfigModel alloc] init];
    self.configModel.delegate = self;
    
    [self.progressIndicator fadeIn];
    [self.menuContainer fadeOut];
    [self.configModel getServerConfig]; //recommended once per app lifecycle
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configRequestSuccess {
    [self.progressIndicator fadeOut];
    [self.menuContainer fadeIn];
}

-(void)configRequestFail {
    [PVPAlertHelper showAlertWithTitle:@"Server Error"
                            andMessage:@"We cannot reach servers at this time. Please try again."
                 withCancelButtonTitle:@"Later"
                  withOtherButtonTitle:@"Try again" withHandler:^(UIAlertController * _Nonnull alert, BOOL confirmed) {
        if (confirmed) {
            [self.progressIndicator fadeIn];
            [self.menuContainer fadeOut];
            [self.configModel getServerConfig];
        }
                  }];
}

@end
